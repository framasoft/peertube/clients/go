/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"encoding/json"
)

// ServerConfigCustomInstance struct for ServerConfigCustomInstance
type ServerConfigCustomInstance struct {
	Name *string `json:"name,omitempty"`
	ShortDescription *string `json:"shortDescription,omitempty"`
	Description *string `json:"description,omitempty"`
	Terms *string `json:"terms,omitempty"`
	DefaultClientRoute *string `json:"defaultClientRoute,omitempty"`
	IsNSFW *bool `json:"isNSFW,omitempty"`
	DefaultNSFWPolicy *string `json:"defaultNSFWPolicy,omitempty"`
	Customizations *ServerConfigInstanceCustomizations `json:"customizations,omitempty"`
}

// NewServerConfigCustomInstance instantiates a new ServerConfigCustomInstance object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewServerConfigCustomInstance() *ServerConfigCustomInstance {
	this := ServerConfigCustomInstance{}
	return &this
}

// NewServerConfigCustomInstanceWithDefaults instantiates a new ServerConfigCustomInstance object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewServerConfigCustomInstanceWithDefaults() *ServerConfigCustomInstance {
	this := ServerConfigCustomInstance{}
	return &this
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *ServerConfigCustomInstance) GetName() string {
	if o == nil || o.Name == nil {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerConfigCustomInstance) GetNameOk() (*string, bool) {
	if o == nil || o.Name == nil {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *ServerConfigCustomInstance) HasName() bool {
	if o != nil && o.Name != nil {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *ServerConfigCustomInstance) SetName(v string) {
	o.Name = &v
}

// GetShortDescription returns the ShortDescription field value if set, zero value otherwise.
func (o *ServerConfigCustomInstance) GetShortDescription() string {
	if o == nil || o.ShortDescription == nil {
		var ret string
		return ret
	}
	return *o.ShortDescription
}

// GetShortDescriptionOk returns a tuple with the ShortDescription field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerConfigCustomInstance) GetShortDescriptionOk() (*string, bool) {
	if o == nil || o.ShortDescription == nil {
		return nil, false
	}
	return o.ShortDescription, true
}

// HasShortDescription returns a boolean if a field has been set.
func (o *ServerConfigCustomInstance) HasShortDescription() bool {
	if o != nil && o.ShortDescription != nil {
		return true
	}

	return false
}

// SetShortDescription gets a reference to the given string and assigns it to the ShortDescription field.
func (o *ServerConfigCustomInstance) SetShortDescription(v string) {
	o.ShortDescription = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *ServerConfigCustomInstance) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerConfigCustomInstance) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *ServerConfigCustomInstance) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *ServerConfigCustomInstance) SetDescription(v string) {
	o.Description = &v
}

// GetTerms returns the Terms field value if set, zero value otherwise.
func (o *ServerConfigCustomInstance) GetTerms() string {
	if o == nil || o.Terms == nil {
		var ret string
		return ret
	}
	return *o.Terms
}

// GetTermsOk returns a tuple with the Terms field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerConfigCustomInstance) GetTermsOk() (*string, bool) {
	if o == nil || o.Terms == nil {
		return nil, false
	}
	return o.Terms, true
}

// HasTerms returns a boolean if a field has been set.
func (o *ServerConfigCustomInstance) HasTerms() bool {
	if o != nil && o.Terms != nil {
		return true
	}

	return false
}

// SetTerms gets a reference to the given string and assigns it to the Terms field.
func (o *ServerConfigCustomInstance) SetTerms(v string) {
	o.Terms = &v
}

// GetDefaultClientRoute returns the DefaultClientRoute field value if set, zero value otherwise.
func (o *ServerConfigCustomInstance) GetDefaultClientRoute() string {
	if o == nil || o.DefaultClientRoute == nil {
		var ret string
		return ret
	}
	return *o.DefaultClientRoute
}

// GetDefaultClientRouteOk returns a tuple with the DefaultClientRoute field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerConfigCustomInstance) GetDefaultClientRouteOk() (*string, bool) {
	if o == nil || o.DefaultClientRoute == nil {
		return nil, false
	}
	return o.DefaultClientRoute, true
}

// HasDefaultClientRoute returns a boolean if a field has been set.
func (o *ServerConfigCustomInstance) HasDefaultClientRoute() bool {
	if o != nil && o.DefaultClientRoute != nil {
		return true
	}

	return false
}

// SetDefaultClientRoute gets a reference to the given string and assigns it to the DefaultClientRoute field.
func (o *ServerConfigCustomInstance) SetDefaultClientRoute(v string) {
	o.DefaultClientRoute = &v
}

// GetIsNSFW returns the IsNSFW field value if set, zero value otherwise.
func (o *ServerConfigCustomInstance) GetIsNSFW() bool {
	if o == nil || o.IsNSFW == nil {
		var ret bool
		return ret
	}
	return *o.IsNSFW
}

// GetIsNSFWOk returns a tuple with the IsNSFW field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerConfigCustomInstance) GetIsNSFWOk() (*bool, bool) {
	if o == nil || o.IsNSFW == nil {
		return nil, false
	}
	return o.IsNSFW, true
}

// HasIsNSFW returns a boolean if a field has been set.
func (o *ServerConfigCustomInstance) HasIsNSFW() bool {
	if o != nil && o.IsNSFW != nil {
		return true
	}

	return false
}

// SetIsNSFW gets a reference to the given bool and assigns it to the IsNSFW field.
func (o *ServerConfigCustomInstance) SetIsNSFW(v bool) {
	o.IsNSFW = &v
}

// GetDefaultNSFWPolicy returns the DefaultNSFWPolicy field value if set, zero value otherwise.
func (o *ServerConfigCustomInstance) GetDefaultNSFWPolicy() string {
	if o == nil || o.DefaultNSFWPolicy == nil {
		var ret string
		return ret
	}
	return *o.DefaultNSFWPolicy
}

// GetDefaultNSFWPolicyOk returns a tuple with the DefaultNSFWPolicy field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerConfigCustomInstance) GetDefaultNSFWPolicyOk() (*string, bool) {
	if o == nil || o.DefaultNSFWPolicy == nil {
		return nil, false
	}
	return o.DefaultNSFWPolicy, true
}

// HasDefaultNSFWPolicy returns a boolean if a field has been set.
func (o *ServerConfigCustomInstance) HasDefaultNSFWPolicy() bool {
	if o != nil && o.DefaultNSFWPolicy != nil {
		return true
	}

	return false
}

// SetDefaultNSFWPolicy gets a reference to the given string and assigns it to the DefaultNSFWPolicy field.
func (o *ServerConfigCustomInstance) SetDefaultNSFWPolicy(v string) {
	o.DefaultNSFWPolicy = &v
}

// GetCustomizations returns the Customizations field value if set, zero value otherwise.
func (o *ServerConfigCustomInstance) GetCustomizations() ServerConfigInstanceCustomizations {
	if o == nil || o.Customizations == nil {
		var ret ServerConfigInstanceCustomizations
		return ret
	}
	return *o.Customizations
}

// GetCustomizationsOk returns a tuple with the Customizations field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerConfigCustomInstance) GetCustomizationsOk() (*ServerConfigInstanceCustomizations, bool) {
	if o == nil || o.Customizations == nil {
		return nil, false
	}
	return o.Customizations, true
}

// HasCustomizations returns a boolean if a field has been set.
func (o *ServerConfigCustomInstance) HasCustomizations() bool {
	if o != nil && o.Customizations != nil {
		return true
	}

	return false
}

// SetCustomizations gets a reference to the given ServerConfigInstanceCustomizations and assigns it to the Customizations field.
func (o *ServerConfigCustomInstance) SetCustomizations(v ServerConfigInstanceCustomizations) {
	o.Customizations = &v
}

func (o ServerConfigCustomInstance) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Name != nil {
		toSerialize["name"] = o.Name
	}
	if o.ShortDescription != nil {
		toSerialize["shortDescription"] = o.ShortDescription
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.Terms != nil {
		toSerialize["terms"] = o.Terms
	}
	if o.DefaultClientRoute != nil {
		toSerialize["defaultClientRoute"] = o.DefaultClientRoute
	}
	if o.IsNSFW != nil {
		toSerialize["isNSFW"] = o.IsNSFW
	}
	if o.DefaultNSFWPolicy != nil {
		toSerialize["defaultNSFWPolicy"] = o.DefaultNSFWPolicy
	}
	if o.Customizations != nil {
		toSerialize["customizations"] = o.Customizations
	}
	return json.Marshal(toSerialize)
}

type NullableServerConfigCustomInstance struct {
	value *ServerConfigCustomInstance
	isSet bool
}

func (v NullableServerConfigCustomInstance) Get() *ServerConfigCustomInstance {
	return v.value
}

func (v *NullableServerConfigCustomInstance) Set(val *ServerConfigCustomInstance) {
	v.value = val
	v.isSet = true
}

func (v NullableServerConfigCustomInstance) IsSet() bool {
	return v.isSet
}

func (v *NullableServerConfigCustomInstance) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableServerConfigCustomInstance(val *ServerConfigCustomInstance) *NullableServerConfigCustomInstance {
	return &NullableServerConfigCustomInstance{value: val, isSet: true}
}

func (v NullableServerConfigCustomInstance) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableServerConfigCustomInstance) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


