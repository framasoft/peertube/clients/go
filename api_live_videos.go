/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"strings"
	"os"
)

// Linger please
var (
	_ _context.Context
)

// LiveVideosApiService LiveVideosApi service
type LiveVideosApiService service

type ApiAddLiveRequest struct {
	ctx _context.Context
	ApiService *LiveVideosApiService
	channelId *int32
	name *string
	saveReplay *bool
	permanentLive *bool
	thumbnailfile **os.File
	previewfile **os.File
	privacy *VideoPrivacySet
	category *int32
	licence *int32
	language *string
	description *string
	support *string
	nsfw *bool
	tags *[]string
	commentsEnabled *bool
	downloadEnabled *bool
}

// Channel id that will contain this live video
func (r ApiAddLiveRequest) ChannelId(channelId int32) ApiAddLiveRequest {
	r.channelId = &channelId
	return r
}
// Live video/replay name
func (r ApiAddLiveRequest) Name(name string) ApiAddLiveRequest {
	r.name = &name
	return r
}
func (r ApiAddLiveRequest) SaveReplay(saveReplay bool) ApiAddLiveRequest {
	r.saveReplay = &saveReplay
	return r
}
// User can stream multiple times in a permanent live
func (r ApiAddLiveRequest) PermanentLive(permanentLive bool) ApiAddLiveRequest {
	r.permanentLive = &permanentLive
	return r
}
// Live video/replay thumbnail file
func (r ApiAddLiveRequest) Thumbnailfile(thumbnailfile *os.File) ApiAddLiveRequest {
	r.thumbnailfile = &thumbnailfile
	return r
}
// Live video/replay preview file
func (r ApiAddLiveRequest) Previewfile(previewfile *os.File) ApiAddLiveRequest {
	r.previewfile = &previewfile
	return r
}
func (r ApiAddLiveRequest) Privacy(privacy VideoPrivacySet) ApiAddLiveRequest {
	r.privacy = &privacy
	return r
}
// category id of the video (see [/videos/categories](#operation/getCategories))
func (r ApiAddLiveRequest) Category(category int32) ApiAddLiveRequest {
	r.category = &category
	return r
}
// licence id of the video (see [/videos/licences](#operation/getLicences))
func (r ApiAddLiveRequest) Licence(licence int32) ApiAddLiveRequest {
	r.licence = &licence
	return r
}
// language id of the video (see [/videos/languages](#operation/getLanguages))
func (r ApiAddLiveRequest) Language(language string) ApiAddLiveRequest {
	r.language = &language
	return r
}
// Live video/replay description
func (r ApiAddLiveRequest) Description(description string) ApiAddLiveRequest {
	r.description = &description
	return r
}
// A text tell the audience how to support the creator
func (r ApiAddLiveRequest) Support(support string) ApiAddLiveRequest {
	r.support = &support
	return r
}
// Whether or not this live video/replay contains sensitive content
func (r ApiAddLiveRequest) Nsfw(nsfw bool) ApiAddLiveRequest {
	r.nsfw = &nsfw
	return r
}
// Live video/replay tags (maximum 5 tags each between 2 and 30 characters)
func (r ApiAddLiveRequest) Tags(tags []string) ApiAddLiveRequest {
	r.tags = &tags
	return r
}
// Enable or disable comments for this live video/replay
func (r ApiAddLiveRequest) CommentsEnabled(commentsEnabled bool) ApiAddLiveRequest {
	r.commentsEnabled = &commentsEnabled
	return r
}
// Enable or disable downloading for the replay of this live video
func (r ApiAddLiveRequest) DownloadEnabled(downloadEnabled bool) ApiAddLiveRequest {
	r.downloadEnabled = &downloadEnabled
	return r
}

func (r ApiAddLiveRequest) Execute() (VideoUploadResponse, *_nethttp.Response, error) {
	return r.ApiService.AddLiveExecute(r)
}

/*
AddLive Create a live

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAddLiveRequest
*/
func (a *LiveVideosApiService) AddLive(ctx _context.Context) ApiAddLiveRequest {
	return ApiAddLiveRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoUploadResponse
func (a *LiveVideosApiService) AddLiveExecute(r ApiAddLiveRequest) (VideoUploadResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoUploadResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "LiveVideosApiService.AddLive")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/videos/live"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.channelId == nil {
		return localVarReturnValue, nil, reportError("channelId is required and must be specified")
	}
	if r.name == nil {
		return localVarReturnValue, nil, reportError("name is required and must be specified")
	}
	if strlen(*r.name) < 3 {
		return localVarReturnValue, nil, reportError("name must have at least 3 elements")
	}
	if strlen(*r.name) > 120 {
		return localVarReturnValue, nil, reportError("name must have less than 120 elements")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"multipart/form-data"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarFormParams.Add("channelId", parameterToString(*r.channelId, ""))
	if r.saveReplay != nil {
		localVarFormParams.Add("saveReplay", parameterToString(*r.saveReplay, ""))
	}
	if r.permanentLive != nil {
		localVarFormParams.Add("permanentLive", parameterToString(*r.permanentLive, ""))
	}
	localVarFormFileName = "thumbnailfile"
	var localVarFile *os.File
	if r.thumbnailfile != nil {
		localVarFile = *r.thumbnailfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	localVarFormFileName = "previewfile"
	var localVarFile *os.File
	if r.previewfile != nil {
		localVarFile = *r.previewfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	if r.privacy != nil {
		localVarFormParams.Add("privacy", parameterToString(*r.privacy, ""))
	}
	if r.category != nil {
		localVarFormParams.Add("category", parameterToString(*r.category, ""))
	}
	if r.licence != nil {
		localVarFormParams.Add("licence", parameterToString(*r.licence, ""))
	}
	if r.language != nil {
		localVarFormParams.Add("language", parameterToString(*r.language, ""))
	}
	if r.description != nil {
		localVarFormParams.Add("description", parameterToString(*r.description, ""))
	}
	if r.support != nil {
		localVarFormParams.Add("support", parameterToString(*r.support, ""))
	}
	if r.nsfw != nil {
		localVarFormParams.Add("nsfw", parameterToString(*r.nsfw, ""))
	}
	localVarFormParams.Add("name", parameterToString(*r.name, ""))
	if r.tags != nil {
		localVarFormParams.Add("tags", parameterToString(*r.tags, "csv"))
	}
	if r.commentsEnabled != nil {
		localVarFormParams.Add("commentsEnabled", parameterToString(*r.commentsEnabled, ""))
	}
	if r.downloadEnabled != nil {
		localVarFormParams.Add("downloadEnabled", parameterToString(*r.downloadEnabled, ""))
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetLiveIdRequest struct {
	ctx _context.Context
	ApiService *LiveVideosApiService
	id OneOfintegerUUIDstring
}


func (r ApiGetLiveIdRequest) Execute() (LiveVideoResponse, *_nethttp.Response, error) {
	return r.ApiService.GetLiveIdExecute(r)
}

/*
GetLiveId Get information about a live

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param id The object id, uuid or short uuid
 @return ApiGetLiveIdRequest
*/
func (a *LiveVideosApiService) GetLiveId(ctx _context.Context, id OneOfintegerUUIDstring) ApiGetLiveIdRequest {
	return ApiGetLiveIdRequest{
		ApiService: a,
		ctx: ctx,
		id: id,
	}
}

// Execute executes the request
//  @return LiveVideoResponse
func (a *LiveVideosApiService) GetLiveIdExecute(r ApiGetLiveIdRequest) (LiveVideoResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  LiveVideoResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "LiveVideosApiService.GetLiveId")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/videos/live/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", _neturl.PathEscape(parameterToString(r.id, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiUpdateLiveIdRequest struct {
	ctx _context.Context
	ApiService *LiveVideosApiService
	id OneOfintegerUUIDstring
	liveVideoUpdate *LiveVideoUpdate
}

func (r ApiUpdateLiveIdRequest) LiveVideoUpdate(liveVideoUpdate LiveVideoUpdate) ApiUpdateLiveIdRequest {
	r.liveVideoUpdate = &liveVideoUpdate
	return r
}

func (r ApiUpdateLiveIdRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.UpdateLiveIdExecute(r)
}

/*
UpdateLiveId Update information about a live

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param id The object id, uuid or short uuid
 @return ApiUpdateLiveIdRequest
*/
func (a *LiveVideosApiService) UpdateLiveId(ctx _context.Context, id OneOfintegerUUIDstring) ApiUpdateLiveIdRequest {
	return ApiUpdateLiveIdRequest{
		ApiService: a,
		ctx: ctx,
		id: id,
	}
}

// Execute executes the request
func (a *LiveVideosApiService) UpdateLiveIdExecute(r ApiUpdateLiveIdRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPut
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "LiveVideosApiService.UpdateLiveId")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/videos/live/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", _neturl.PathEscape(parameterToString(r.id, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.liveVideoUpdate
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}
