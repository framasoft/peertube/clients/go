/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"strings"
	"reflect"
)

// Linger please
var (
	_ _context.Context
)

// AbusesApiService AbusesApi service
type AbusesApiService service

type ApiAbusesAbuseIdDeleteRequest struct {
	ctx _context.Context
	ApiService *AbusesApiService
	abuseId int32
}


func (r ApiAbusesAbuseIdDeleteRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.AbusesAbuseIdDeleteExecute(r)
}

/*
AbusesAbuseIdDelete Delete an abuse

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param abuseId Abuse id
 @return ApiAbusesAbuseIdDeleteRequest
*/
func (a *AbusesApiService) AbusesAbuseIdDelete(ctx _context.Context, abuseId int32) ApiAbusesAbuseIdDeleteRequest {
	return ApiAbusesAbuseIdDeleteRequest{
		ApiService: a,
		ctx: ctx,
		abuseId: abuseId,
	}
}

// Execute executes the request
func (a *AbusesApiService) AbusesAbuseIdDeleteExecute(r ApiAbusesAbuseIdDeleteRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodDelete
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "AbusesApiService.AbusesAbuseIdDelete")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/abuses/{abuseId}"
	localVarPath = strings.Replace(localVarPath, "{"+"abuseId"+"}", _neturl.PathEscape(parameterToString(r.abuseId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.abuseId < 1 {
		return nil, reportError("abuseId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiAbusesAbuseIdMessagesAbuseMessageIdDeleteRequest struct {
	ctx _context.Context
	ApiService *AbusesApiService
	abuseId int32
	abuseMessageId int32
}


func (r ApiAbusesAbuseIdMessagesAbuseMessageIdDeleteRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.AbusesAbuseIdMessagesAbuseMessageIdDeleteExecute(r)
}

/*
AbusesAbuseIdMessagesAbuseMessageIdDelete Delete an abuse message

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param abuseId Abuse id
 @param abuseMessageId Abuse message id
 @return ApiAbusesAbuseIdMessagesAbuseMessageIdDeleteRequest
*/
func (a *AbusesApiService) AbusesAbuseIdMessagesAbuseMessageIdDelete(ctx _context.Context, abuseId int32, abuseMessageId int32) ApiAbusesAbuseIdMessagesAbuseMessageIdDeleteRequest {
	return ApiAbusesAbuseIdMessagesAbuseMessageIdDeleteRequest{
		ApiService: a,
		ctx: ctx,
		abuseId: abuseId,
		abuseMessageId: abuseMessageId,
	}
}

// Execute executes the request
func (a *AbusesApiService) AbusesAbuseIdMessagesAbuseMessageIdDeleteExecute(r ApiAbusesAbuseIdMessagesAbuseMessageIdDeleteRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodDelete
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "AbusesApiService.AbusesAbuseIdMessagesAbuseMessageIdDelete")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/abuses/{abuseId}/messages/{abuseMessageId}"
	localVarPath = strings.Replace(localVarPath, "{"+"abuseId"+"}", _neturl.PathEscape(parameterToString(r.abuseId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"abuseMessageId"+"}", _neturl.PathEscape(parameterToString(r.abuseMessageId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.abuseId < 1 {
		return nil, reportError("abuseId must be greater than 1")
	}
	if r.abuseMessageId < 1 {
		return nil, reportError("abuseMessageId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiAbusesAbuseIdMessagesGetRequest struct {
	ctx _context.Context
	ApiService *AbusesApiService
	abuseId int32
}


func (r ApiAbusesAbuseIdMessagesGetRequest) Execute() (InlineResponse2007, *_nethttp.Response, error) {
	return r.ApiService.AbusesAbuseIdMessagesGetExecute(r)
}

/*
AbusesAbuseIdMessagesGet List messages of an abuse

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param abuseId Abuse id
 @return ApiAbusesAbuseIdMessagesGetRequest
*/
func (a *AbusesApiService) AbusesAbuseIdMessagesGet(ctx _context.Context, abuseId int32) ApiAbusesAbuseIdMessagesGetRequest {
	return ApiAbusesAbuseIdMessagesGetRequest{
		ApiService: a,
		ctx: ctx,
		abuseId: abuseId,
	}
}

// Execute executes the request
//  @return InlineResponse2007
func (a *AbusesApiService) AbusesAbuseIdMessagesGetExecute(r ApiAbusesAbuseIdMessagesGetRequest) (InlineResponse2007, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse2007
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "AbusesApiService.AbusesAbuseIdMessagesGet")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/abuses/{abuseId}/messages"
	localVarPath = strings.Replace(localVarPath, "{"+"abuseId"+"}", _neturl.PathEscape(parameterToString(r.abuseId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.abuseId < 1 {
		return localVarReturnValue, nil, reportError("abuseId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAbusesAbuseIdMessagesPostRequest struct {
	ctx _context.Context
	ApiService *AbusesApiService
	abuseId int32
	inlineObject13 *InlineObject13
}

func (r ApiAbusesAbuseIdMessagesPostRequest) InlineObject13(inlineObject13 InlineObject13) ApiAbusesAbuseIdMessagesPostRequest {
	r.inlineObject13 = &inlineObject13
	return r
}

func (r ApiAbusesAbuseIdMessagesPostRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.AbusesAbuseIdMessagesPostExecute(r)
}

/*
AbusesAbuseIdMessagesPost Add message to an abuse

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param abuseId Abuse id
 @return ApiAbusesAbuseIdMessagesPostRequest
*/
func (a *AbusesApiService) AbusesAbuseIdMessagesPost(ctx _context.Context, abuseId int32) ApiAbusesAbuseIdMessagesPostRequest {
	return ApiAbusesAbuseIdMessagesPostRequest{
		ApiService: a,
		ctx: ctx,
		abuseId: abuseId,
	}
}

// Execute executes the request
func (a *AbusesApiService) AbusesAbuseIdMessagesPostExecute(r ApiAbusesAbuseIdMessagesPostRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "AbusesApiService.AbusesAbuseIdMessagesPost")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/abuses/{abuseId}/messages"
	localVarPath = strings.Replace(localVarPath, "{"+"abuseId"+"}", _neturl.PathEscape(parameterToString(r.abuseId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.abuseId < 1 {
		return nil, reportError("abuseId must be greater than 1")
	}
	if r.inlineObject13 == nil {
		return nil, reportError("inlineObject13 is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.inlineObject13
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiAbusesAbuseIdPutRequest struct {
	ctx _context.Context
	ApiService *AbusesApiService
	abuseId int32
	inlineObject12 *InlineObject12
}

func (r ApiAbusesAbuseIdPutRequest) InlineObject12(inlineObject12 InlineObject12) ApiAbusesAbuseIdPutRequest {
	r.inlineObject12 = &inlineObject12
	return r
}

func (r ApiAbusesAbuseIdPutRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.AbusesAbuseIdPutExecute(r)
}

/*
AbusesAbuseIdPut Update an abuse

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param abuseId Abuse id
 @return ApiAbusesAbuseIdPutRequest
*/
func (a *AbusesApiService) AbusesAbuseIdPut(ctx _context.Context, abuseId int32) ApiAbusesAbuseIdPutRequest {
	return ApiAbusesAbuseIdPutRequest{
		ApiService: a,
		ctx: ctx,
		abuseId: abuseId,
	}
}

// Execute executes the request
func (a *AbusesApiService) AbusesAbuseIdPutExecute(r ApiAbusesAbuseIdPutRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPut
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "AbusesApiService.AbusesAbuseIdPut")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/abuses/{abuseId}"
	localVarPath = strings.Replace(localVarPath, "{"+"abuseId"+"}", _neturl.PathEscape(parameterToString(r.abuseId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.abuseId < 1 {
		return nil, reportError("abuseId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.inlineObject12
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiAbusesPostRequest struct {
	ctx _context.Context
	ApiService *AbusesApiService
	inlineObject11 *InlineObject11
}

func (r ApiAbusesPostRequest) InlineObject11(inlineObject11 InlineObject11) ApiAbusesPostRequest {
	r.inlineObject11 = &inlineObject11
	return r
}

func (r ApiAbusesPostRequest) Execute() (InlineResponse2006, *_nethttp.Response, error) {
	return r.ApiService.AbusesPostExecute(r)
}

/*
AbusesPost Report an abuse

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAbusesPostRequest
*/
func (a *AbusesApiService) AbusesPost(ctx _context.Context) ApiAbusesPostRequest {
	return ApiAbusesPostRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return InlineResponse2006
func (a *AbusesApiService) AbusesPostExecute(r ApiAbusesPostRequest) (InlineResponse2006, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse2006
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "AbusesApiService.AbusesPost")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/abuses"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.inlineObject11 == nil {
		return localVarReturnValue, nil, reportError("inlineObject11 is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.inlineObject11
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetAbusesRequest struct {
	ctx _context.Context
	ApiService *AbusesApiService
	id *int32
	predefinedReason *[]string
	search *string
	state *AbuseStateSet
	searchReporter *string
	searchReportee *string
	searchVideo *string
	searchVideoChannel *string
	videoIs *string
	filter *string
	start *int32
	count *int32
	sort *string
}

// only list the report with this id
func (r ApiGetAbusesRequest) Id(id int32) ApiGetAbusesRequest {
	r.id = &id
	return r
}
// predefined reason the listed reports should contain
func (r ApiGetAbusesRequest) PredefinedReason(predefinedReason []string) ApiGetAbusesRequest {
	r.predefinedReason = &predefinedReason
	return r
}
// plain search that will match with video titles, reporter names and more
func (r ApiGetAbusesRequest) Search(search string) ApiGetAbusesRequest {
	r.search = &search
	return r
}
func (r ApiGetAbusesRequest) State(state AbuseStateSet) ApiGetAbusesRequest {
	r.state = &state
	return r
}
// only list reports of a specific reporter
func (r ApiGetAbusesRequest) SearchReporter(searchReporter string) ApiGetAbusesRequest {
	r.searchReporter = &searchReporter
	return r
}
// only list reports of a specific reportee
func (r ApiGetAbusesRequest) SearchReportee(searchReportee string) ApiGetAbusesRequest {
	r.searchReportee = &searchReportee
	return r
}
// only list reports of a specific video
func (r ApiGetAbusesRequest) SearchVideo(searchVideo string) ApiGetAbusesRequest {
	r.searchVideo = &searchVideo
	return r
}
// only list reports of a specific video channel
func (r ApiGetAbusesRequest) SearchVideoChannel(searchVideoChannel string) ApiGetAbusesRequest {
	r.searchVideoChannel = &searchVideoChannel
	return r
}
// only list deleted or blocklisted videos
func (r ApiGetAbusesRequest) VideoIs(videoIs string) ApiGetAbusesRequest {
	r.videoIs = &videoIs
	return r
}
// only list account, comment or video reports
func (r ApiGetAbusesRequest) Filter(filter string) ApiGetAbusesRequest {
	r.filter = &filter
	return r
}
// Offset used to paginate results
func (r ApiGetAbusesRequest) Start(start int32) ApiGetAbusesRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiGetAbusesRequest) Count(count int32) ApiGetAbusesRequest {
	r.count = &count
	return r
}
// Sort abuses by criteria
func (r ApiGetAbusesRequest) Sort(sort string) ApiGetAbusesRequest {
	r.sort = &sort
	return r
}

func (r ApiGetAbusesRequest) Execute() (InlineResponse2005, *_nethttp.Response, error) {
	return r.ApiService.GetAbusesExecute(r)
}

/*
GetAbuses List abuses

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGetAbusesRequest
*/
func (a *AbusesApiService) GetAbuses(ctx _context.Context) ApiGetAbusesRequest {
	return ApiGetAbusesRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return InlineResponse2005
func (a *AbusesApiService) GetAbusesExecute(r ApiGetAbusesRequest) (InlineResponse2005, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse2005
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "AbusesApiService.GetAbuses")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/abuses"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.id != nil {
		localVarQueryParams.Add("id", parameterToString(*r.id, ""))
	}
	if r.predefinedReason != nil {
		t := *r.predefinedReason
		if reflect.TypeOf(t).Kind() == reflect.Slice {
			s := reflect.ValueOf(t)
			for i := 0; i < s.Len(); i++ {
				localVarQueryParams.Add("predefinedReason", parameterToString(s.Index(i), "multi"))
			}
		} else {
			localVarQueryParams.Add("predefinedReason", parameterToString(t, "multi"))
		}
	}
	if r.search != nil {
		localVarQueryParams.Add("search", parameterToString(*r.search, ""))
	}
	if r.state != nil {
		localVarQueryParams.Add("state", parameterToString(*r.state, ""))
	}
	if r.searchReporter != nil {
		localVarQueryParams.Add("searchReporter", parameterToString(*r.searchReporter, ""))
	}
	if r.searchReportee != nil {
		localVarQueryParams.Add("searchReportee", parameterToString(*r.searchReportee, ""))
	}
	if r.searchVideo != nil {
		localVarQueryParams.Add("searchVideo", parameterToString(*r.searchVideo, ""))
	}
	if r.searchVideoChannel != nil {
		localVarQueryParams.Add("searchVideoChannel", parameterToString(*r.searchVideoChannel, ""))
	}
	if r.videoIs != nil {
		localVarQueryParams.Add("videoIs", parameterToString(*r.videoIs, ""))
	}
	if r.filter != nil {
		localVarQueryParams.Add("filter", parameterToString(*r.filter, ""))
	}
	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetMyAbusesRequest struct {
	ctx _context.Context
	ApiService *AbusesApiService
	id *int32
	state *AbuseStateSet
	sort *string
	start *int32
	count *int32
}

// only list the report with this id
func (r ApiGetMyAbusesRequest) Id(id int32) ApiGetMyAbusesRequest {
	r.id = &id
	return r
}
func (r ApiGetMyAbusesRequest) State(state AbuseStateSet) ApiGetMyAbusesRequest {
	r.state = &state
	return r
}
// Sort abuses by criteria
func (r ApiGetMyAbusesRequest) Sort(sort string) ApiGetMyAbusesRequest {
	r.sort = &sort
	return r
}
// Offset used to paginate results
func (r ApiGetMyAbusesRequest) Start(start int32) ApiGetMyAbusesRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiGetMyAbusesRequest) Count(count int32) ApiGetMyAbusesRequest {
	r.count = &count
	return r
}

func (r ApiGetMyAbusesRequest) Execute() (InlineResponse2005, *_nethttp.Response, error) {
	return r.ApiService.GetMyAbusesExecute(r)
}

/*
GetMyAbuses List my abuses

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGetMyAbusesRequest
*/
func (a *AbusesApiService) GetMyAbuses(ctx _context.Context) ApiGetMyAbusesRequest {
	return ApiGetMyAbusesRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return InlineResponse2005
func (a *AbusesApiService) GetMyAbusesExecute(r ApiGetMyAbusesRequest) (InlineResponse2005, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse2005
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "AbusesApiService.GetMyAbuses")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/users/me/abuses"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.id != nil {
		localVarQueryParams.Add("id", parameterToString(*r.id, ""))
	}
	if r.state != nil {
		localVarQueryParams.Add("state", parameterToString(*r.state, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
