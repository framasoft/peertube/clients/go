/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"strings"
)

// Linger please
var (
	_ _context.Context
)

// FeedsApiService FeedsApi service
type FeedsApiService service

type ApiGetSyndicatedCommentsRequest struct {
	ctx _context.Context
	ApiService *FeedsApiService
	format string
	videoId *string
	accountId *string
	accountName *string
	videoChannelId *string
	videoChannelName *string
}

// limit listing to a specific video
func (r ApiGetSyndicatedCommentsRequest) VideoId(videoId string) ApiGetSyndicatedCommentsRequest {
	r.videoId = &videoId
	return r
}
// limit listing to a specific account
func (r ApiGetSyndicatedCommentsRequest) AccountId(accountId string) ApiGetSyndicatedCommentsRequest {
	r.accountId = &accountId
	return r
}
// limit listing to a specific account
func (r ApiGetSyndicatedCommentsRequest) AccountName(accountName string) ApiGetSyndicatedCommentsRequest {
	r.accountName = &accountName
	return r
}
// limit listing to a specific video channel
func (r ApiGetSyndicatedCommentsRequest) VideoChannelId(videoChannelId string) ApiGetSyndicatedCommentsRequest {
	r.videoChannelId = &videoChannelId
	return r
}
// limit listing to a specific video channel
func (r ApiGetSyndicatedCommentsRequest) VideoChannelName(videoChannelName string) ApiGetSyndicatedCommentsRequest {
	r.videoChannelName = &videoChannelName
	return r
}

func (r ApiGetSyndicatedCommentsRequest) Execute() ([]map[string]interface{}, *_nethttp.Response, error) {
	return r.ApiService.GetSyndicatedCommentsExecute(r)
}

/*
GetSyndicatedComments List comments on videos

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param format format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
 @return ApiGetSyndicatedCommentsRequest
*/
func (a *FeedsApiService) GetSyndicatedComments(ctx _context.Context, format string) ApiGetSyndicatedCommentsRequest {
	return ApiGetSyndicatedCommentsRequest{
		ApiService: a,
		ctx: ctx,
		format: format,
	}
}

// Execute executes the request
//  @return []map[string]interface{}
func (a *FeedsApiService) GetSyndicatedCommentsExecute(r ApiGetSyndicatedCommentsRequest) ([]map[string]interface{}, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  []map[string]interface{}
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "FeedsApiService.GetSyndicatedComments")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/feeds/video-comments.{format}"
	localVarPath = strings.Replace(localVarPath, "{"+"format"+"}", _neturl.PathEscape(parameterToString(r.format, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.videoId != nil {
		localVarQueryParams.Add("videoId", parameterToString(*r.videoId, ""))
	}
	if r.accountId != nil {
		localVarQueryParams.Add("accountId", parameterToString(*r.accountId, ""))
	}
	if r.accountName != nil {
		localVarQueryParams.Add("accountName", parameterToString(*r.accountName, ""))
	}
	if r.videoChannelId != nil {
		localVarQueryParams.Add("videoChannelId", parameterToString(*r.videoChannelId, ""))
	}
	if r.videoChannelName != nil {
		localVarQueryParams.Add("videoChannelName", parameterToString(*r.videoChannelName, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/xml", "application/rss+xml", "text/xml", "application/atom+xml", "application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetSyndicatedSubscriptionVideosRequest struct {
	ctx _context.Context
	ApiService *FeedsApiService
	format string
	accountId *string
	token *string
	sort *string
	nsfw *string
	filter *string
}

// limit listing to a specific account
func (r ApiGetSyndicatedSubscriptionVideosRequest) AccountId(accountId string) ApiGetSyndicatedSubscriptionVideosRequest {
	r.accountId = &accountId
	return r
}
// private token allowing access
func (r ApiGetSyndicatedSubscriptionVideosRequest) Token(token string) ApiGetSyndicatedSubscriptionVideosRequest {
	r.token = &token
	return r
}
// Sort column
func (r ApiGetSyndicatedSubscriptionVideosRequest) Sort(sort string) ApiGetSyndicatedSubscriptionVideosRequest {
	r.sort = &sort
	return r
}
// whether to include nsfw videos, if any
func (r ApiGetSyndicatedSubscriptionVideosRequest) Nsfw(nsfw string) ApiGetSyndicatedSubscriptionVideosRequest {
	r.nsfw = &nsfw
	return r
}
// Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges) 
func (r ApiGetSyndicatedSubscriptionVideosRequest) Filter(filter string) ApiGetSyndicatedSubscriptionVideosRequest {
	r.filter = &filter
	return r
}

func (r ApiGetSyndicatedSubscriptionVideosRequest) Execute() ([]map[string]interface{}, *_nethttp.Response, error) {
	return r.ApiService.GetSyndicatedSubscriptionVideosExecute(r)
}

/*
GetSyndicatedSubscriptionVideos List videos of subscriptions tied to a token

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param format format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
 @return ApiGetSyndicatedSubscriptionVideosRequest
*/
func (a *FeedsApiService) GetSyndicatedSubscriptionVideos(ctx _context.Context, format string) ApiGetSyndicatedSubscriptionVideosRequest {
	return ApiGetSyndicatedSubscriptionVideosRequest{
		ApiService: a,
		ctx: ctx,
		format: format,
	}
}

// Execute executes the request
//  @return []map[string]interface{}
func (a *FeedsApiService) GetSyndicatedSubscriptionVideosExecute(r ApiGetSyndicatedSubscriptionVideosRequest) ([]map[string]interface{}, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  []map[string]interface{}
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "FeedsApiService.GetSyndicatedSubscriptionVideos")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/feeds/subscriptions.{format}"
	localVarPath = strings.Replace(localVarPath, "{"+"format"+"}", _neturl.PathEscape(parameterToString(r.format, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.accountId == nil {
		return localVarReturnValue, nil, reportError("accountId is required and must be specified")
	}
	if r.token == nil {
		return localVarReturnValue, nil, reportError("token is required and must be specified")
	}

	localVarQueryParams.Add("accountId", parameterToString(*r.accountId, ""))
	localVarQueryParams.Add("token", parameterToString(*r.token, ""))
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	if r.nsfw != nil {
		localVarQueryParams.Add("nsfw", parameterToString(*r.nsfw, ""))
	}
	if r.filter != nil {
		localVarQueryParams.Add("filter", parameterToString(*r.filter, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/xml", "application/rss+xml", "text/xml", "application/atom+xml", "application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetSyndicatedVideosRequest struct {
	ctx _context.Context
	ApiService *FeedsApiService
	format string
	accountId *string
	accountName *string
	videoChannelId *string
	videoChannelName *string
	sort *string
	nsfw *string
	filter *string
}

// limit listing to a specific account
func (r ApiGetSyndicatedVideosRequest) AccountId(accountId string) ApiGetSyndicatedVideosRequest {
	r.accountId = &accountId
	return r
}
// limit listing to a specific account
func (r ApiGetSyndicatedVideosRequest) AccountName(accountName string) ApiGetSyndicatedVideosRequest {
	r.accountName = &accountName
	return r
}
// limit listing to a specific video channel
func (r ApiGetSyndicatedVideosRequest) VideoChannelId(videoChannelId string) ApiGetSyndicatedVideosRequest {
	r.videoChannelId = &videoChannelId
	return r
}
// limit listing to a specific video channel
func (r ApiGetSyndicatedVideosRequest) VideoChannelName(videoChannelName string) ApiGetSyndicatedVideosRequest {
	r.videoChannelName = &videoChannelName
	return r
}
// Sort column
func (r ApiGetSyndicatedVideosRequest) Sort(sort string) ApiGetSyndicatedVideosRequest {
	r.sort = &sort
	return r
}
// whether to include nsfw videos, if any
func (r ApiGetSyndicatedVideosRequest) Nsfw(nsfw string) ApiGetSyndicatedVideosRequest {
	r.nsfw = &nsfw
	return r
}
// Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges) 
func (r ApiGetSyndicatedVideosRequest) Filter(filter string) ApiGetSyndicatedVideosRequest {
	r.filter = &filter
	return r
}

func (r ApiGetSyndicatedVideosRequest) Execute() ([]map[string]interface{}, *_nethttp.Response, error) {
	return r.ApiService.GetSyndicatedVideosExecute(r)
}

/*
GetSyndicatedVideos List videos

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param format format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
 @return ApiGetSyndicatedVideosRequest
*/
func (a *FeedsApiService) GetSyndicatedVideos(ctx _context.Context, format string) ApiGetSyndicatedVideosRequest {
	return ApiGetSyndicatedVideosRequest{
		ApiService: a,
		ctx: ctx,
		format: format,
	}
}

// Execute executes the request
//  @return []map[string]interface{}
func (a *FeedsApiService) GetSyndicatedVideosExecute(r ApiGetSyndicatedVideosRequest) ([]map[string]interface{}, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  []map[string]interface{}
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "FeedsApiService.GetSyndicatedVideos")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/feeds/videos.{format}"
	localVarPath = strings.Replace(localVarPath, "{"+"format"+"}", _neturl.PathEscape(parameterToString(r.format, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.accountId != nil {
		localVarQueryParams.Add("accountId", parameterToString(*r.accountId, ""))
	}
	if r.accountName != nil {
		localVarQueryParams.Add("accountName", parameterToString(*r.accountName, ""))
	}
	if r.videoChannelId != nil {
		localVarQueryParams.Add("videoChannelId", parameterToString(*r.videoChannelId, ""))
	}
	if r.videoChannelName != nil {
		localVarQueryParams.Add("videoChannelName", parameterToString(*r.videoChannelName, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	if r.nsfw != nil {
		localVarQueryParams.Add("nsfw", parameterToString(*r.nsfw, ""))
	}
	if r.filter != nil {
		localVarQueryParams.Add("filter", parameterToString(*r.filter, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/xml", "application/rss+xml", "text/xml", "application/atom+xml", "application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
