/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"time"
)

// Linger please
var (
	_ _context.Context
)

// SearchApiService SearchApi service
type SearchApiService service

type ApiSearchChannelsRequest struct {
	ctx _context.Context
	ApiService *SearchApiService
	search *string
	start *int32
	count *int32
	searchTarget *string
	sort *string
}

// String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete channel information and interact with it. 
func (r ApiSearchChannelsRequest) Search(search string) ApiSearchChannelsRequest {
	r.search = &search
	return r
}
// Offset used to paginate results
func (r ApiSearchChannelsRequest) Start(start int32) ApiSearchChannelsRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiSearchChannelsRequest) Count(count int32) ApiSearchChannelsRequest {
	r.count = &count
	return r
}
// If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API 
func (r ApiSearchChannelsRequest) SearchTarget(searchTarget string) ApiSearchChannelsRequest {
	r.searchTarget = &searchTarget
	return r
}
// Sort column
func (r ApiSearchChannelsRequest) Sort(sort string) ApiSearchChannelsRequest {
	r.sort = &sort
	return r
}

func (r ApiSearchChannelsRequest) Execute() (VideoChannelList, *_nethttp.Response, error) {
	return r.ApiService.SearchChannelsExecute(r)
}

/*
SearchChannels Search channels

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiSearchChannelsRequest
*/
func (a *SearchApiService) SearchChannels(ctx _context.Context) ApiSearchChannelsRequest {
	return ApiSearchChannelsRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoChannelList
func (a *SearchApiService) SearchChannelsExecute(r ApiSearchChannelsRequest) (VideoChannelList, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoChannelList
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "SearchApiService.SearchChannels")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/search/video-channels"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.search == nil {
		return localVarReturnValue, nil, reportError("search is required and must be specified")
	}

	localVarQueryParams.Add("search", parameterToString(*r.search, ""))
	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.searchTarget != nil {
		localVarQueryParams.Add("searchTarget", parameterToString(*r.searchTarget, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiSearchPlaylistsRequest struct {
	ctx _context.Context
	ApiService *SearchApiService
	search *string
	start *int32
	count *int32
	searchTarget *string
	sort *string
}

// String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete playlist information and interact with it. 
func (r ApiSearchPlaylistsRequest) Search(search string) ApiSearchPlaylistsRequest {
	r.search = &search
	return r
}
// Offset used to paginate results
func (r ApiSearchPlaylistsRequest) Start(start int32) ApiSearchPlaylistsRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiSearchPlaylistsRequest) Count(count int32) ApiSearchPlaylistsRequest {
	r.count = &count
	return r
}
// If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API 
func (r ApiSearchPlaylistsRequest) SearchTarget(searchTarget string) ApiSearchPlaylistsRequest {
	r.searchTarget = &searchTarget
	return r
}
// Sort column
func (r ApiSearchPlaylistsRequest) Sort(sort string) ApiSearchPlaylistsRequest {
	r.sort = &sort
	return r
}

func (r ApiSearchPlaylistsRequest) Execute() (InlineResponse20011, *_nethttp.Response, error) {
	return r.ApiService.SearchPlaylistsExecute(r)
}

/*
SearchPlaylists Search playlists

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiSearchPlaylistsRequest
*/
func (a *SearchApiService) SearchPlaylists(ctx _context.Context) ApiSearchPlaylistsRequest {
	return ApiSearchPlaylistsRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return InlineResponse20011
func (a *SearchApiService) SearchPlaylistsExecute(r ApiSearchPlaylistsRequest) (InlineResponse20011, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse20011
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "SearchApiService.SearchPlaylists")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/search/video-playlists"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.search == nil {
		return localVarReturnValue, nil, reportError("search is required and must be specified")
	}

	localVarQueryParams.Add("search", parameterToString(*r.search, ""))
	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.searchTarget != nil {
		localVarQueryParams.Add("searchTarget", parameterToString(*r.searchTarget, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiSearchVideosRequest struct {
	ctx _context.Context
	ApiService *SearchApiService
	search *string
	categoryOneOf *OneOfintegerarray
	isLive *bool
	tagsOneOf *OneOfstringarray
	tagsAllOf *OneOfstringarray
	licenceOneOf *OneOfintegerarray
	languageOneOf *OneOfstringarray
	nsfw *string
	filter *string
	skipCount *string
	start *int32
	count *int32
	searchTarget *string
	sort *string
	startDate *time.Time
	endDate *time.Time
	originallyPublishedStartDate *time.Time
	originallyPublishedEndDate *time.Time
	durationMin *int32
	durationMax *int32
}

// String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete video information and interact with it. 
func (r ApiSearchVideosRequest) Search(search string) ApiSearchVideosRequest {
	r.search = &search
	return r
}
// category id of the video (see [/videos/categories](#operation/getCategories))
func (r ApiSearchVideosRequest) CategoryOneOf(categoryOneOf OneOfintegerarray) ApiSearchVideosRequest {
	r.categoryOneOf = &categoryOneOf
	return r
}
// whether or not the video is a live
func (r ApiSearchVideosRequest) IsLive(isLive bool) ApiSearchVideosRequest {
	r.isLive = &isLive
	return r
}
// tag(s) of the video
func (r ApiSearchVideosRequest) TagsOneOf(tagsOneOf OneOfstringarray) ApiSearchVideosRequest {
	r.tagsOneOf = &tagsOneOf
	return r
}
// tag(s) of the video, where all should be present in the video
func (r ApiSearchVideosRequest) TagsAllOf(tagsAllOf OneOfstringarray) ApiSearchVideosRequest {
	r.tagsAllOf = &tagsAllOf
	return r
}
// licence id of the video (see [/videos/licences](#operation/getLicences))
func (r ApiSearchVideosRequest) LicenceOneOf(licenceOneOf OneOfintegerarray) ApiSearchVideosRequest {
	r.licenceOneOf = &licenceOneOf
	return r
}
// language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language
func (r ApiSearchVideosRequest) LanguageOneOf(languageOneOf OneOfstringarray) ApiSearchVideosRequest {
	r.languageOneOf = &languageOneOf
	return r
}
// whether to include nsfw videos, if any
func (r ApiSearchVideosRequest) Nsfw(nsfw string) ApiSearchVideosRequest {
	r.nsfw = &nsfw
	return r
}
// Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges) 
func (r ApiSearchVideosRequest) Filter(filter string) ApiSearchVideosRequest {
	r.filter = &filter
	return r
}
// if you don&#39;t need the &#x60;total&#x60; in the response
func (r ApiSearchVideosRequest) SkipCount(skipCount string) ApiSearchVideosRequest {
	r.skipCount = &skipCount
	return r
}
// Offset used to paginate results
func (r ApiSearchVideosRequest) Start(start int32) ApiSearchVideosRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiSearchVideosRequest) Count(count int32) ApiSearchVideosRequest {
	r.count = &count
	return r
}
// If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API 
func (r ApiSearchVideosRequest) SearchTarget(searchTarget string) ApiSearchVideosRequest {
	r.searchTarget = &searchTarget
	return r
}
// Sort videos by criteria
func (r ApiSearchVideosRequest) Sort(sort string) ApiSearchVideosRequest {
	r.sort = &sort
	return r
}
// Get videos that are published after this date
func (r ApiSearchVideosRequest) StartDate(startDate time.Time) ApiSearchVideosRequest {
	r.startDate = &startDate
	return r
}
// Get videos that are published before this date
func (r ApiSearchVideosRequest) EndDate(endDate time.Time) ApiSearchVideosRequest {
	r.endDate = &endDate
	return r
}
// Get videos that are originally published after this date
func (r ApiSearchVideosRequest) OriginallyPublishedStartDate(originallyPublishedStartDate time.Time) ApiSearchVideosRequest {
	r.originallyPublishedStartDate = &originallyPublishedStartDate
	return r
}
// Get videos that are originally published before this date
func (r ApiSearchVideosRequest) OriginallyPublishedEndDate(originallyPublishedEndDate time.Time) ApiSearchVideosRequest {
	r.originallyPublishedEndDate = &originallyPublishedEndDate
	return r
}
// Get videos that have this minimum duration
func (r ApiSearchVideosRequest) DurationMin(durationMin int32) ApiSearchVideosRequest {
	r.durationMin = &durationMin
	return r
}
// Get videos that have this maximum duration
func (r ApiSearchVideosRequest) DurationMax(durationMax int32) ApiSearchVideosRequest {
	r.durationMax = &durationMax
	return r
}

func (r ApiSearchVideosRequest) Execute() (VideoListResponse, *_nethttp.Response, error) {
	return r.ApiService.SearchVideosExecute(r)
}

/*
SearchVideos Search videos

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiSearchVideosRequest
*/
func (a *SearchApiService) SearchVideos(ctx _context.Context) ApiSearchVideosRequest {
	return ApiSearchVideosRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoListResponse
func (a *SearchApiService) SearchVideosExecute(r ApiSearchVideosRequest) (VideoListResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoListResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "SearchApiService.SearchVideos")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/search/videos"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.search == nil {
		return localVarReturnValue, nil, reportError("search is required and must be specified")
	}

	localVarQueryParams.Add("search", parameterToString(*r.search, ""))
	if r.categoryOneOf != nil {
		localVarQueryParams.Add("categoryOneOf", parameterToString(*r.categoryOneOf, ""))
	}
	if r.isLive != nil {
		localVarQueryParams.Add("isLive", parameterToString(*r.isLive, ""))
	}
	if r.tagsOneOf != nil {
		localVarQueryParams.Add("tagsOneOf", parameterToString(*r.tagsOneOf, ""))
	}
	if r.tagsAllOf != nil {
		localVarQueryParams.Add("tagsAllOf", parameterToString(*r.tagsAllOf, ""))
	}
	if r.licenceOneOf != nil {
		localVarQueryParams.Add("licenceOneOf", parameterToString(*r.licenceOneOf, ""))
	}
	if r.languageOneOf != nil {
		localVarQueryParams.Add("languageOneOf", parameterToString(*r.languageOneOf, ""))
	}
	if r.nsfw != nil {
		localVarQueryParams.Add("nsfw", parameterToString(*r.nsfw, ""))
	}
	if r.filter != nil {
		localVarQueryParams.Add("filter", parameterToString(*r.filter, ""))
	}
	if r.skipCount != nil {
		localVarQueryParams.Add("skipCount", parameterToString(*r.skipCount, ""))
	}
	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.searchTarget != nil {
		localVarQueryParams.Add("searchTarget", parameterToString(*r.searchTarget, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	if r.startDate != nil {
		localVarQueryParams.Add("startDate", parameterToString(*r.startDate, ""))
	}
	if r.endDate != nil {
		localVarQueryParams.Add("endDate", parameterToString(*r.endDate, ""))
	}
	if r.originallyPublishedStartDate != nil {
		localVarQueryParams.Add("originallyPublishedStartDate", parameterToString(*r.originallyPublishedStartDate, ""))
	}
	if r.originallyPublishedEndDate != nil {
		localVarQueryParams.Add("originallyPublishedEndDate", parameterToString(*r.originallyPublishedEndDate, ""))
	}
	if r.durationMin != nil {
		localVarQueryParams.Add("durationMin", parameterToString(*r.durationMin, ""))
	}
	if r.durationMax != nil {
		localVarQueryParams.Add("durationMax", parameterToString(*r.durationMax, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
