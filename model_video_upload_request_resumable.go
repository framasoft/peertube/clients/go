/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"encoding/json"
	"time"
	"os"
)

// VideoUploadRequestResumable struct for VideoUploadRequestResumable
type VideoUploadRequestResumable struct {
	// Video name
	Name string `json:"name"`
	// Channel id that will contain this video
	ChannelId int32 `json:"channelId"`
	Privacy *VideoPrivacySet `json:"privacy,omitempty"`
	// category id of the video (see [/videos/categories](#operation/getCategories))
	Category *int32 `json:"category,omitempty"`
	// licence id of the video (see [/videos/licences](#operation/getLicences))
	Licence *int32 `json:"licence,omitempty"`
	// language id of the video (see [/videos/languages](#operation/getLanguages))
	Language *string `json:"language,omitempty"`
	// Video description
	Description *string `json:"description,omitempty"`
	// Whether or not we wait transcoding before publish the video
	WaitTranscoding *bool `json:"waitTranscoding,omitempty"`
	// A text tell the audience how to support the video creator
	Support *string `json:"support,omitempty"`
	// Whether or not this video contains sensitive content
	Nsfw *bool `json:"nsfw,omitempty"`
	// Video tags (maximum 5 tags each between 2 and 30 characters)
	Tags *[]string `json:"tags,omitempty"`
	// Enable or disable comments for this video
	CommentsEnabled *bool `json:"commentsEnabled,omitempty"`
	// Enable or disable downloading for this video
	DownloadEnabled *bool `json:"downloadEnabled,omitempty"`
	// Date when the content was originally published
	OriginallyPublishedAt *time.Time `json:"originallyPublishedAt,omitempty"`
	ScheduleUpdate *VideoScheduledUpdate `json:"scheduleUpdate,omitempty"`
	// Video thumbnail file
	Thumbnailfile **os.File `json:"thumbnailfile,omitempty"`
	// Video preview file
	Previewfile **os.File `json:"previewfile,omitempty"`
	// Video filename including extension
	Filename string `json:"filename"`
}

// NewVideoUploadRequestResumable instantiates a new VideoUploadRequestResumable object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewVideoUploadRequestResumable(name string, channelId int32, filename string) *VideoUploadRequestResumable {
	this := VideoUploadRequestResumable{}
	this.Name = name
	this.ChannelId = channelId
	this.Filename = filename
	return &this
}

// NewVideoUploadRequestResumableWithDefaults instantiates a new VideoUploadRequestResumable object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewVideoUploadRequestResumableWithDefaults() *VideoUploadRequestResumable {
	this := VideoUploadRequestResumable{}
	return &this
}

// GetName returns the Name field value
func (o *VideoUploadRequestResumable) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetNameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *VideoUploadRequestResumable) SetName(v string) {
	o.Name = v
}

// GetChannelId returns the ChannelId field value
func (o *VideoUploadRequestResumable) GetChannelId() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.ChannelId
}

// GetChannelIdOk returns a tuple with the ChannelId field value
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetChannelIdOk() (*int32, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.ChannelId, true
}

// SetChannelId sets field value
func (o *VideoUploadRequestResumable) SetChannelId(v int32) {
	o.ChannelId = v
}

// GetPrivacy returns the Privacy field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetPrivacy() VideoPrivacySet {
	if o == nil || o.Privacy == nil {
		var ret VideoPrivacySet
		return ret
	}
	return *o.Privacy
}

// GetPrivacyOk returns a tuple with the Privacy field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetPrivacyOk() (*VideoPrivacySet, bool) {
	if o == nil || o.Privacy == nil {
		return nil, false
	}
	return o.Privacy, true
}

// HasPrivacy returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasPrivacy() bool {
	if o != nil && o.Privacy != nil {
		return true
	}

	return false
}

// SetPrivacy gets a reference to the given VideoPrivacySet and assigns it to the Privacy field.
func (o *VideoUploadRequestResumable) SetPrivacy(v VideoPrivacySet) {
	o.Privacy = &v
}

// GetCategory returns the Category field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetCategory() int32 {
	if o == nil || o.Category == nil {
		var ret int32
		return ret
	}
	return *o.Category
}

// GetCategoryOk returns a tuple with the Category field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetCategoryOk() (*int32, bool) {
	if o == nil || o.Category == nil {
		return nil, false
	}
	return o.Category, true
}

// HasCategory returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasCategory() bool {
	if o != nil && o.Category != nil {
		return true
	}

	return false
}

// SetCategory gets a reference to the given int32 and assigns it to the Category field.
func (o *VideoUploadRequestResumable) SetCategory(v int32) {
	o.Category = &v
}

// GetLicence returns the Licence field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetLicence() int32 {
	if o == nil || o.Licence == nil {
		var ret int32
		return ret
	}
	return *o.Licence
}

// GetLicenceOk returns a tuple with the Licence field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetLicenceOk() (*int32, bool) {
	if o == nil || o.Licence == nil {
		return nil, false
	}
	return o.Licence, true
}

// HasLicence returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasLicence() bool {
	if o != nil && o.Licence != nil {
		return true
	}

	return false
}

// SetLicence gets a reference to the given int32 and assigns it to the Licence field.
func (o *VideoUploadRequestResumable) SetLicence(v int32) {
	o.Licence = &v
}

// GetLanguage returns the Language field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetLanguage() string {
	if o == nil || o.Language == nil {
		var ret string
		return ret
	}
	return *o.Language
}

// GetLanguageOk returns a tuple with the Language field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetLanguageOk() (*string, bool) {
	if o == nil || o.Language == nil {
		return nil, false
	}
	return o.Language, true
}

// HasLanguage returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasLanguage() bool {
	if o != nil && o.Language != nil {
		return true
	}

	return false
}

// SetLanguage gets a reference to the given string and assigns it to the Language field.
func (o *VideoUploadRequestResumable) SetLanguage(v string) {
	o.Language = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *VideoUploadRequestResumable) SetDescription(v string) {
	o.Description = &v
}

// GetWaitTranscoding returns the WaitTranscoding field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetWaitTranscoding() bool {
	if o == nil || o.WaitTranscoding == nil {
		var ret bool
		return ret
	}
	return *o.WaitTranscoding
}

// GetWaitTranscodingOk returns a tuple with the WaitTranscoding field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetWaitTranscodingOk() (*bool, bool) {
	if o == nil || o.WaitTranscoding == nil {
		return nil, false
	}
	return o.WaitTranscoding, true
}

// HasWaitTranscoding returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasWaitTranscoding() bool {
	if o != nil && o.WaitTranscoding != nil {
		return true
	}

	return false
}

// SetWaitTranscoding gets a reference to the given bool and assigns it to the WaitTranscoding field.
func (o *VideoUploadRequestResumable) SetWaitTranscoding(v bool) {
	o.WaitTranscoding = &v
}

// GetSupport returns the Support field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetSupport() string {
	if o == nil || o.Support == nil {
		var ret string
		return ret
	}
	return *o.Support
}

// GetSupportOk returns a tuple with the Support field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetSupportOk() (*string, bool) {
	if o == nil || o.Support == nil {
		return nil, false
	}
	return o.Support, true
}

// HasSupport returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasSupport() bool {
	if o != nil && o.Support != nil {
		return true
	}

	return false
}

// SetSupport gets a reference to the given string and assigns it to the Support field.
func (o *VideoUploadRequestResumable) SetSupport(v string) {
	o.Support = &v
}

// GetNsfw returns the Nsfw field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetNsfw() bool {
	if o == nil || o.Nsfw == nil {
		var ret bool
		return ret
	}
	return *o.Nsfw
}

// GetNsfwOk returns a tuple with the Nsfw field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetNsfwOk() (*bool, bool) {
	if o == nil || o.Nsfw == nil {
		return nil, false
	}
	return o.Nsfw, true
}

// HasNsfw returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasNsfw() bool {
	if o != nil && o.Nsfw != nil {
		return true
	}

	return false
}

// SetNsfw gets a reference to the given bool and assigns it to the Nsfw field.
func (o *VideoUploadRequestResumable) SetNsfw(v bool) {
	o.Nsfw = &v
}

// GetTags returns the Tags field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetTags() []string {
	if o == nil || o.Tags == nil {
		var ret []string
		return ret
	}
	return *o.Tags
}

// GetTagsOk returns a tuple with the Tags field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetTagsOk() (*[]string, bool) {
	if o == nil || o.Tags == nil {
		return nil, false
	}
	return o.Tags, true
}

// HasTags returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasTags() bool {
	if o != nil && o.Tags != nil {
		return true
	}

	return false
}

// SetTags gets a reference to the given []string and assigns it to the Tags field.
func (o *VideoUploadRequestResumable) SetTags(v []string) {
	o.Tags = &v
}

// GetCommentsEnabled returns the CommentsEnabled field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetCommentsEnabled() bool {
	if o == nil || o.CommentsEnabled == nil {
		var ret bool
		return ret
	}
	return *o.CommentsEnabled
}

// GetCommentsEnabledOk returns a tuple with the CommentsEnabled field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetCommentsEnabledOk() (*bool, bool) {
	if o == nil || o.CommentsEnabled == nil {
		return nil, false
	}
	return o.CommentsEnabled, true
}

// HasCommentsEnabled returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasCommentsEnabled() bool {
	if o != nil && o.CommentsEnabled != nil {
		return true
	}

	return false
}

// SetCommentsEnabled gets a reference to the given bool and assigns it to the CommentsEnabled field.
func (o *VideoUploadRequestResumable) SetCommentsEnabled(v bool) {
	o.CommentsEnabled = &v
}

// GetDownloadEnabled returns the DownloadEnabled field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetDownloadEnabled() bool {
	if o == nil || o.DownloadEnabled == nil {
		var ret bool
		return ret
	}
	return *o.DownloadEnabled
}

// GetDownloadEnabledOk returns a tuple with the DownloadEnabled field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetDownloadEnabledOk() (*bool, bool) {
	if o == nil || o.DownloadEnabled == nil {
		return nil, false
	}
	return o.DownloadEnabled, true
}

// HasDownloadEnabled returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasDownloadEnabled() bool {
	if o != nil && o.DownloadEnabled != nil {
		return true
	}

	return false
}

// SetDownloadEnabled gets a reference to the given bool and assigns it to the DownloadEnabled field.
func (o *VideoUploadRequestResumable) SetDownloadEnabled(v bool) {
	o.DownloadEnabled = &v
}

// GetOriginallyPublishedAt returns the OriginallyPublishedAt field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetOriginallyPublishedAt() time.Time {
	if o == nil || o.OriginallyPublishedAt == nil {
		var ret time.Time
		return ret
	}
	return *o.OriginallyPublishedAt
}

// GetOriginallyPublishedAtOk returns a tuple with the OriginallyPublishedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetOriginallyPublishedAtOk() (*time.Time, bool) {
	if o == nil || o.OriginallyPublishedAt == nil {
		return nil, false
	}
	return o.OriginallyPublishedAt, true
}

// HasOriginallyPublishedAt returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasOriginallyPublishedAt() bool {
	if o != nil && o.OriginallyPublishedAt != nil {
		return true
	}

	return false
}

// SetOriginallyPublishedAt gets a reference to the given time.Time and assigns it to the OriginallyPublishedAt field.
func (o *VideoUploadRequestResumable) SetOriginallyPublishedAt(v time.Time) {
	o.OriginallyPublishedAt = &v
}

// GetScheduleUpdate returns the ScheduleUpdate field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetScheduleUpdate() VideoScheduledUpdate {
	if o == nil || o.ScheduleUpdate == nil {
		var ret VideoScheduledUpdate
		return ret
	}
	return *o.ScheduleUpdate
}

// GetScheduleUpdateOk returns a tuple with the ScheduleUpdate field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetScheduleUpdateOk() (*VideoScheduledUpdate, bool) {
	if o == nil || o.ScheduleUpdate == nil {
		return nil, false
	}
	return o.ScheduleUpdate, true
}

// HasScheduleUpdate returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasScheduleUpdate() bool {
	if o != nil && o.ScheduleUpdate != nil {
		return true
	}

	return false
}

// SetScheduleUpdate gets a reference to the given VideoScheduledUpdate and assigns it to the ScheduleUpdate field.
func (o *VideoUploadRequestResumable) SetScheduleUpdate(v VideoScheduledUpdate) {
	o.ScheduleUpdate = &v
}

// GetThumbnailfile returns the Thumbnailfile field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetThumbnailfile() *os.File {
	if o == nil || o.Thumbnailfile == nil {
		var ret *os.File
		return ret
	}
	return *o.Thumbnailfile
}

// GetThumbnailfileOk returns a tuple with the Thumbnailfile field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetThumbnailfileOk() (**os.File, bool) {
	if o == nil || o.Thumbnailfile == nil {
		return nil, false
	}
	return o.Thumbnailfile, true
}

// HasThumbnailfile returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasThumbnailfile() bool {
	if o != nil && o.Thumbnailfile != nil {
		return true
	}

	return false
}

// SetThumbnailfile gets a reference to the given *os.File and assigns it to the Thumbnailfile field.
func (o *VideoUploadRequestResumable) SetThumbnailfile(v *os.File) {
	o.Thumbnailfile = &v
}

// GetPreviewfile returns the Previewfile field value if set, zero value otherwise.
func (o *VideoUploadRequestResumable) GetPreviewfile() *os.File {
	if o == nil || o.Previewfile == nil {
		var ret *os.File
		return ret
	}
	return *o.Previewfile
}

// GetPreviewfileOk returns a tuple with the Previewfile field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetPreviewfileOk() (**os.File, bool) {
	if o == nil || o.Previewfile == nil {
		return nil, false
	}
	return o.Previewfile, true
}

// HasPreviewfile returns a boolean if a field has been set.
func (o *VideoUploadRequestResumable) HasPreviewfile() bool {
	if o != nil && o.Previewfile != nil {
		return true
	}

	return false
}

// SetPreviewfile gets a reference to the given *os.File and assigns it to the Previewfile field.
func (o *VideoUploadRequestResumable) SetPreviewfile(v *os.File) {
	o.Previewfile = &v
}

// GetFilename returns the Filename field value
func (o *VideoUploadRequestResumable) GetFilename() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Filename
}

// GetFilenameOk returns a tuple with the Filename field value
// and a boolean to check if the value has been set.
func (o *VideoUploadRequestResumable) GetFilenameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Filename, true
}

// SetFilename sets field value
func (o *VideoUploadRequestResumable) SetFilename(v string) {
	o.Filename = v
}

func (o VideoUploadRequestResumable) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["name"] = o.Name
	}
	if true {
		toSerialize["channelId"] = o.ChannelId
	}
	if o.Privacy != nil {
		toSerialize["privacy"] = o.Privacy
	}
	if o.Category != nil {
		toSerialize["category"] = o.Category
	}
	if o.Licence != nil {
		toSerialize["licence"] = o.Licence
	}
	if o.Language != nil {
		toSerialize["language"] = o.Language
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.WaitTranscoding != nil {
		toSerialize["waitTranscoding"] = o.WaitTranscoding
	}
	if o.Support != nil {
		toSerialize["support"] = o.Support
	}
	if o.Nsfw != nil {
		toSerialize["nsfw"] = o.Nsfw
	}
	if o.Tags != nil {
		toSerialize["tags"] = o.Tags
	}
	if o.CommentsEnabled != nil {
		toSerialize["commentsEnabled"] = o.CommentsEnabled
	}
	if o.DownloadEnabled != nil {
		toSerialize["downloadEnabled"] = o.DownloadEnabled
	}
	if o.OriginallyPublishedAt != nil {
		toSerialize["originallyPublishedAt"] = o.OriginallyPublishedAt
	}
	if o.ScheduleUpdate != nil {
		toSerialize["scheduleUpdate"] = o.ScheduleUpdate
	}
	if o.Thumbnailfile != nil {
		toSerialize["thumbnailfile"] = o.Thumbnailfile
	}
	if o.Previewfile != nil {
		toSerialize["previewfile"] = o.Previewfile
	}
	if true {
		toSerialize["filename"] = o.Filename
	}
	return json.Marshal(toSerialize)
}

type NullableVideoUploadRequestResumable struct {
	value *VideoUploadRequestResumable
	isSet bool
}

func (v NullableVideoUploadRequestResumable) Get() *VideoUploadRequestResumable {
	return v.value
}

func (v *NullableVideoUploadRequestResumable) Set(val *VideoUploadRequestResumable) {
	v.value = val
	v.isSet = true
}

func (v NullableVideoUploadRequestResumable) IsSet() bool {
	return v.isSet
}

func (v *NullableVideoUploadRequestResumable) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableVideoUploadRequestResumable(val *VideoUploadRequestResumable) *NullableVideoUploadRequestResumable {
	return &NullableVideoUploadRequestResumable{value: val, isSet: true}
}

func (v NullableVideoUploadRequestResumable) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableVideoUploadRequestResumable) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


