/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"strings"
	"os"
)

// Linger please
var (
	_ _context.Context
)

// VideoChannelsApiService VideoChannelsApi service
type VideoChannelsApiService service

type ApiAccountsNameVideoChannelsGetRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	name string
	withStats *bool
	start *int32
	count *int32
	sort *string
}

// include view statistics for the last 30 days (only if authentified as the account user)
func (r ApiAccountsNameVideoChannelsGetRequest) WithStats(withStats bool) ApiAccountsNameVideoChannelsGetRequest {
	r.withStats = &withStats
	return r
}
// Offset used to paginate results
func (r ApiAccountsNameVideoChannelsGetRequest) Start(start int32) ApiAccountsNameVideoChannelsGetRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiAccountsNameVideoChannelsGetRequest) Count(count int32) ApiAccountsNameVideoChannelsGetRequest {
	r.count = &count
	return r
}
// Sort column
func (r ApiAccountsNameVideoChannelsGetRequest) Sort(sort string) ApiAccountsNameVideoChannelsGetRequest {
	r.sort = &sort
	return r
}

func (r ApiAccountsNameVideoChannelsGetRequest) Execute() (VideoChannelList, *_nethttp.Response, error) {
	return r.ApiService.AccountsNameVideoChannelsGetExecute(r)
}

/*
AccountsNameVideoChannelsGet List video channels of an account

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param name The username or handle of the account
 @return ApiAccountsNameVideoChannelsGetRequest
*/
func (a *VideoChannelsApiService) AccountsNameVideoChannelsGet(ctx _context.Context, name string) ApiAccountsNameVideoChannelsGetRequest {
	return ApiAccountsNameVideoChannelsGetRequest{
		ApiService: a,
		ctx: ctx,
		name: name,
	}
}

// Execute executes the request
//  @return VideoChannelList
func (a *VideoChannelsApiService) AccountsNameVideoChannelsGetExecute(r ApiAccountsNameVideoChannelsGetRequest) (VideoChannelList, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoChannelList
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.AccountsNameVideoChannelsGet")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/accounts/{name}/video-channels"
	localVarPath = strings.Replace(localVarPath, "{"+"name"+"}", _neturl.PathEscape(parameterToString(r.name, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.withStats != nil {
		localVarQueryParams.Add("withStats", parameterToString(*r.withStats, ""))
	}
	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAddVideoChannelRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	videoChannelCreate *VideoChannelCreate
}

func (r ApiAddVideoChannelRequest) VideoChannelCreate(videoChannelCreate VideoChannelCreate) ApiAddVideoChannelRequest {
	r.videoChannelCreate = &videoChannelCreate
	return r
}

func (r ApiAddVideoChannelRequest) Execute() (InlineResponse204, *_nethttp.Response, error) {
	return r.ApiService.AddVideoChannelExecute(r)
}

/*
AddVideoChannel Create a video channel

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAddVideoChannelRequest
*/
func (a *VideoChannelsApiService) AddVideoChannel(ctx _context.Context) ApiAddVideoChannelRequest {
	return ApiAddVideoChannelRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return InlineResponse204
func (a *VideoChannelsApiService) AddVideoChannelExecute(r ApiAddVideoChannelRequest) (InlineResponse204, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse204
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.AddVideoChannel")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.videoChannelCreate
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiDelVideoChannelRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	channelHandle string
}


func (r ApiDelVideoChannelRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.DelVideoChannelExecute(r)
}

/*
DelVideoChannel Delete a video channel

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param channelHandle The video channel handle
 @return ApiDelVideoChannelRequest
*/
func (a *VideoChannelsApiService) DelVideoChannel(ctx _context.Context, channelHandle string) ApiDelVideoChannelRequest {
	return ApiDelVideoChannelRequest{
		ApiService: a,
		ctx: ctx,
		channelHandle: channelHandle,
	}
}

// Execute executes the request
func (a *VideoChannelsApiService) DelVideoChannelExecute(r ApiDelVideoChannelRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodDelete
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.DelVideoChannel")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels/{channelHandle}"
	localVarPath = strings.Replace(localVarPath, "{"+"channelHandle"+"}", _neturl.PathEscape(parameterToString(r.channelHandle, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiGetVideoChannelRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	channelHandle string
}


func (r ApiGetVideoChannelRequest) Execute() (VideoChannel, *_nethttp.Response, error) {
	return r.ApiService.GetVideoChannelExecute(r)
}

/*
GetVideoChannel Get a video channel

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param channelHandle The video channel handle
 @return ApiGetVideoChannelRequest
*/
func (a *VideoChannelsApiService) GetVideoChannel(ctx _context.Context, channelHandle string) ApiGetVideoChannelRequest {
	return ApiGetVideoChannelRequest{
		ApiService: a,
		ctx: ctx,
		channelHandle: channelHandle,
	}
}

// Execute executes the request
//  @return VideoChannel
func (a *VideoChannelsApiService) GetVideoChannelExecute(r ApiGetVideoChannelRequest) (VideoChannel, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoChannel
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.GetVideoChannel")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels/{channelHandle}"
	localVarPath = strings.Replace(localVarPath, "{"+"channelHandle"+"}", _neturl.PathEscape(parameterToString(r.channelHandle, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetVideoChannelVideosRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	channelHandle string
	categoryOneOf *OneOfintegerarray
	isLive *bool
	tagsOneOf *OneOfstringarray
	tagsAllOf *OneOfstringarray
	licenceOneOf *OneOfintegerarray
	languageOneOf *OneOfstringarray
	nsfw *string
	filter *string
	skipCount *string
	start *int32
	count *int32
	sort *string
}

// category id of the video (see [/videos/categories](#operation/getCategories))
func (r ApiGetVideoChannelVideosRequest) CategoryOneOf(categoryOneOf OneOfintegerarray) ApiGetVideoChannelVideosRequest {
	r.categoryOneOf = &categoryOneOf
	return r
}
// whether or not the video is a live
func (r ApiGetVideoChannelVideosRequest) IsLive(isLive bool) ApiGetVideoChannelVideosRequest {
	r.isLive = &isLive
	return r
}
// tag(s) of the video
func (r ApiGetVideoChannelVideosRequest) TagsOneOf(tagsOneOf OneOfstringarray) ApiGetVideoChannelVideosRequest {
	r.tagsOneOf = &tagsOneOf
	return r
}
// tag(s) of the video, where all should be present in the video
func (r ApiGetVideoChannelVideosRequest) TagsAllOf(tagsAllOf OneOfstringarray) ApiGetVideoChannelVideosRequest {
	r.tagsAllOf = &tagsAllOf
	return r
}
// licence id of the video (see [/videos/licences](#operation/getLicences))
func (r ApiGetVideoChannelVideosRequest) LicenceOneOf(licenceOneOf OneOfintegerarray) ApiGetVideoChannelVideosRequest {
	r.licenceOneOf = &licenceOneOf
	return r
}
// language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language
func (r ApiGetVideoChannelVideosRequest) LanguageOneOf(languageOneOf OneOfstringarray) ApiGetVideoChannelVideosRequest {
	r.languageOneOf = &languageOneOf
	return r
}
// whether to include nsfw videos, if any
func (r ApiGetVideoChannelVideosRequest) Nsfw(nsfw string) ApiGetVideoChannelVideosRequest {
	r.nsfw = &nsfw
	return r
}
// Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges) 
func (r ApiGetVideoChannelVideosRequest) Filter(filter string) ApiGetVideoChannelVideosRequest {
	r.filter = &filter
	return r
}
// if you don&#39;t need the &#x60;total&#x60; in the response
func (r ApiGetVideoChannelVideosRequest) SkipCount(skipCount string) ApiGetVideoChannelVideosRequest {
	r.skipCount = &skipCount
	return r
}
// Offset used to paginate results
func (r ApiGetVideoChannelVideosRequest) Start(start int32) ApiGetVideoChannelVideosRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiGetVideoChannelVideosRequest) Count(count int32) ApiGetVideoChannelVideosRequest {
	r.count = &count
	return r
}
// Sort videos by criteria
func (r ApiGetVideoChannelVideosRequest) Sort(sort string) ApiGetVideoChannelVideosRequest {
	r.sort = &sort
	return r
}

func (r ApiGetVideoChannelVideosRequest) Execute() (VideoListResponse, *_nethttp.Response, error) {
	return r.ApiService.GetVideoChannelVideosExecute(r)
}

/*
GetVideoChannelVideos List videos of a video channel

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param channelHandle The video channel handle
 @return ApiGetVideoChannelVideosRequest
*/
func (a *VideoChannelsApiService) GetVideoChannelVideos(ctx _context.Context, channelHandle string) ApiGetVideoChannelVideosRequest {
	return ApiGetVideoChannelVideosRequest{
		ApiService: a,
		ctx: ctx,
		channelHandle: channelHandle,
	}
}

// Execute executes the request
//  @return VideoListResponse
func (a *VideoChannelsApiService) GetVideoChannelVideosExecute(r ApiGetVideoChannelVideosRequest) (VideoListResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoListResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.GetVideoChannelVideos")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels/{channelHandle}/videos"
	localVarPath = strings.Replace(localVarPath, "{"+"channelHandle"+"}", _neturl.PathEscape(parameterToString(r.channelHandle, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.categoryOneOf != nil {
		localVarQueryParams.Add("categoryOneOf", parameterToString(*r.categoryOneOf, ""))
	}
	if r.isLive != nil {
		localVarQueryParams.Add("isLive", parameterToString(*r.isLive, ""))
	}
	if r.tagsOneOf != nil {
		localVarQueryParams.Add("tagsOneOf", parameterToString(*r.tagsOneOf, ""))
	}
	if r.tagsAllOf != nil {
		localVarQueryParams.Add("tagsAllOf", parameterToString(*r.tagsAllOf, ""))
	}
	if r.licenceOneOf != nil {
		localVarQueryParams.Add("licenceOneOf", parameterToString(*r.licenceOneOf, ""))
	}
	if r.languageOneOf != nil {
		localVarQueryParams.Add("languageOneOf", parameterToString(*r.languageOneOf, ""))
	}
	if r.nsfw != nil {
		localVarQueryParams.Add("nsfw", parameterToString(*r.nsfw, ""))
	}
	if r.filter != nil {
		localVarQueryParams.Add("filter", parameterToString(*r.filter, ""))
	}
	if r.skipCount != nil {
		localVarQueryParams.Add("skipCount", parameterToString(*r.skipCount, ""))
	}
	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetVideoChannelsRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	start *int32
	count *int32
	sort *string
}

// Offset used to paginate results
func (r ApiGetVideoChannelsRequest) Start(start int32) ApiGetVideoChannelsRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiGetVideoChannelsRequest) Count(count int32) ApiGetVideoChannelsRequest {
	r.count = &count
	return r
}
// Sort column
func (r ApiGetVideoChannelsRequest) Sort(sort string) ApiGetVideoChannelsRequest {
	r.sort = &sort
	return r
}

func (r ApiGetVideoChannelsRequest) Execute() (VideoChannelList, *_nethttp.Response, error) {
	return r.ApiService.GetVideoChannelsExecute(r)
}

/*
GetVideoChannels List video channels

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGetVideoChannelsRequest
*/
func (a *VideoChannelsApiService) GetVideoChannels(ctx _context.Context) ApiGetVideoChannelsRequest {
	return ApiGetVideoChannelsRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoChannelList
func (a *VideoChannelsApiService) GetVideoChannelsExecute(r ApiGetVideoChannelsRequest) (VideoChannelList, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoChannelList
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.GetVideoChannels")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiPutVideoChannelRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	channelHandle string
	videoChannelUpdate *VideoChannelUpdate
}

func (r ApiPutVideoChannelRequest) VideoChannelUpdate(videoChannelUpdate VideoChannelUpdate) ApiPutVideoChannelRequest {
	r.videoChannelUpdate = &videoChannelUpdate
	return r
}

func (r ApiPutVideoChannelRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.PutVideoChannelExecute(r)
}

/*
PutVideoChannel Update a video channel

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param channelHandle The video channel handle
 @return ApiPutVideoChannelRequest
*/
func (a *VideoChannelsApiService) PutVideoChannel(ctx _context.Context, channelHandle string) ApiPutVideoChannelRequest {
	return ApiPutVideoChannelRequest{
		ApiService: a,
		ctx: ctx,
		channelHandle: channelHandle,
	}
}

// Execute executes the request
func (a *VideoChannelsApiService) PutVideoChannelExecute(r ApiPutVideoChannelRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPut
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.PutVideoChannel")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels/{channelHandle}"
	localVarPath = strings.Replace(localVarPath, "{"+"channelHandle"+"}", _neturl.PathEscape(parameterToString(r.channelHandle, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.videoChannelUpdate
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiVideoChannelsChannelHandleAvatarDeleteRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	channelHandle string
}


func (r ApiVideoChannelsChannelHandleAvatarDeleteRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.VideoChannelsChannelHandleAvatarDeleteExecute(r)
}

/*
VideoChannelsChannelHandleAvatarDelete Delete channel avatar

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param channelHandle The video channel handle
 @return ApiVideoChannelsChannelHandleAvatarDeleteRequest
*/
func (a *VideoChannelsApiService) VideoChannelsChannelHandleAvatarDelete(ctx _context.Context, channelHandle string) ApiVideoChannelsChannelHandleAvatarDeleteRequest {
	return ApiVideoChannelsChannelHandleAvatarDeleteRequest{
		ApiService: a,
		ctx: ctx,
		channelHandle: channelHandle,
	}
}

// Execute executes the request
func (a *VideoChannelsApiService) VideoChannelsChannelHandleAvatarDeleteExecute(r ApiVideoChannelsChannelHandleAvatarDeleteRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodDelete
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.VideoChannelsChannelHandleAvatarDelete")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels/{channelHandle}/avatar"
	localVarPath = strings.Replace(localVarPath, "{"+"channelHandle"+"}", _neturl.PathEscape(parameterToString(r.channelHandle, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiVideoChannelsChannelHandleAvatarPickPostRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	channelHandle string
	avatarfile **os.File
}

// The file to upload.
func (r ApiVideoChannelsChannelHandleAvatarPickPostRequest) Avatarfile(avatarfile *os.File) ApiVideoChannelsChannelHandleAvatarPickPostRequest {
	r.avatarfile = &avatarfile
	return r
}

func (r ApiVideoChannelsChannelHandleAvatarPickPostRequest) Execute() (InlineResponse2004, *_nethttp.Response, error) {
	return r.ApiService.VideoChannelsChannelHandleAvatarPickPostExecute(r)
}

/*
VideoChannelsChannelHandleAvatarPickPost Update channel avatar

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param channelHandle The video channel handle
 @return ApiVideoChannelsChannelHandleAvatarPickPostRequest
*/
func (a *VideoChannelsApiService) VideoChannelsChannelHandleAvatarPickPost(ctx _context.Context, channelHandle string) ApiVideoChannelsChannelHandleAvatarPickPostRequest {
	return ApiVideoChannelsChannelHandleAvatarPickPostRequest{
		ApiService: a,
		ctx: ctx,
		channelHandle: channelHandle,
	}
}

// Execute executes the request
//  @return InlineResponse2004
func (a *VideoChannelsApiService) VideoChannelsChannelHandleAvatarPickPostExecute(r ApiVideoChannelsChannelHandleAvatarPickPostRequest) (InlineResponse2004, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse2004
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.VideoChannelsChannelHandleAvatarPickPost")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels/{channelHandle}/avatar/pick"
	localVarPath = strings.Replace(localVarPath, "{"+"channelHandle"+"}", _neturl.PathEscape(parameterToString(r.channelHandle, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"multipart/form-data"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarFormFileName = "avatarfile"
	var localVarFile *os.File
	if r.avatarfile != nil {
		localVarFile = *r.avatarfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiVideoChannelsChannelHandleBannerDeleteRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	channelHandle string
}


func (r ApiVideoChannelsChannelHandleBannerDeleteRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.VideoChannelsChannelHandleBannerDeleteExecute(r)
}

/*
VideoChannelsChannelHandleBannerDelete Delete channel banner

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param channelHandle The video channel handle
 @return ApiVideoChannelsChannelHandleBannerDeleteRequest
*/
func (a *VideoChannelsApiService) VideoChannelsChannelHandleBannerDelete(ctx _context.Context, channelHandle string) ApiVideoChannelsChannelHandleBannerDeleteRequest {
	return ApiVideoChannelsChannelHandleBannerDeleteRequest{
		ApiService: a,
		ctx: ctx,
		channelHandle: channelHandle,
	}
}

// Execute executes the request
func (a *VideoChannelsApiService) VideoChannelsChannelHandleBannerDeleteExecute(r ApiVideoChannelsChannelHandleBannerDeleteRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodDelete
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.VideoChannelsChannelHandleBannerDelete")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels/{channelHandle}/banner"
	localVarPath = strings.Replace(localVarPath, "{"+"channelHandle"+"}", _neturl.PathEscape(parameterToString(r.channelHandle, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiVideoChannelsChannelHandleBannerPickPostRequest struct {
	ctx _context.Context
	ApiService *VideoChannelsApiService
	channelHandle string
	bannerfile **os.File
}

// The file to upload.
func (r ApiVideoChannelsChannelHandleBannerPickPostRequest) Bannerfile(bannerfile *os.File) ApiVideoChannelsChannelHandleBannerPickPostRequest {
	r.bannerfile = &bannerfile
	return r
}

func (r ApiVideoChannelsChannelHandleBannerPickPostRequest) Execute() (InlineResponse20010, *_nethttp.Response, error) {
	return r.ApiService.VideoChannelsChannelHandleBannerPickPostExecute(r)
}

/*
VideoChannelsChannelHandleBannerPickPost Update channel banner

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param channelHandle The video channel handle
 @return ApiVideoChannelsChannelHandleBannerPickPostRequest
*/
func (a *VideoChannelsApiService) VideoChannelsChannelHandleBannerPickPost(ctx _context.Context, channelHandle string) ApiVideoChannelsChannelHandleBannerPickPostRequest {
	return ApiVideoChannelsChannelHandleBannerPickPostRequest{
		ApiService: a,
		ctx: ctx,
		channelHandle: channelHandle,
	}
}

// Execute executes the request
//  @return InlineResponse20010
func (a *VideoChannelsApiService) VideoChannelsChannelHandleBannerPickPostExecute(r ApiVideoChannelsChannelHandleBannerPickPostRequest) (InlineResponse20010, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse20010
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoChannelsApiService.VideoChannelsChannelHandleBannerPickPost")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-channels/{channelHandle}/banner/pick"
	localVarPath = strings.Replace(localVarPath, "{"+"channelHandle"+"}", _neturl.PathEscape(parameterToString(r.channelHandle, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"multipart/form-data"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarFormFileName = "bannerfile"
	var localVarFile *os.File
	if r.bannerfile != nil {
		localVarFile = *r.bannerfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
