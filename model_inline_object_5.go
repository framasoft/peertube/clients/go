/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"encoding/json"
)

// InlineObject5 struct for InlineObject5
type InlineObject5 struct {
	NewVideoFromSubscription *NotificationSettingValue `json:"newVideoFromSubscription,omitempty"`
	NewCommentOnMyVideo *NotificationSettingValue `json:"newCommentOnMyVideo,omitempty"`
	AbuseAsModerator *NotificationSettingValue `json:"abuseAsModerator,omitempty"`
	VideoAutoBlacklistAsModerator *NotificationSettingValue `json:"videoAutoBlacklistAsModerator,omitempty"`
	BlacklistOnMyVideo *NotificationSettingValue `json:"blacklistOnMyVideo,omitempty"`
	MyVideoPublished *NotificationSettingValue `json:"myVideoPublished,omitempty"`
	MyVideoImportFinished *NotificationSettingValue `json:"myVideoImportFinished,omitempty"`
	NewFollow *NotificationSettingValue `json:"newFollow,omitempty"`
	NewUserRegistration *NotificationSettingValue `json:"newUserRegistration,omitempty"`
	CommentMention *NotificationSettingValue `json:"commentMention,omitempty"`
	NewInstanceFollower *NotificationSettingValue `json:"newInstanceFollower,omitempty"`
	AutoInstanceFollowing *NotificationSettingValue `json:"autoInstanceFollowing,omitempty"`
}

// NewInlineObject5 instantiates a new InlineObject5 object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewInlineObject5() *InlineObject5 {
	this := InlineObject5{}
	return &this
}

// NewInlineObject5WithDefaults instantiates a new InlineObject5 object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewInlineObject5WithDefaults() *InlineObject5 {
	this := InlineObject5{}
	return &this
}

// GetNewVideoFromSubscription returns the NewVideoFromSubscription field value if set, zero value otherwise.
func (o *InlineObject5) GetNewVideoFromSubscription() NotificationSettingValue {
	if o == nil || o.NewVideoFromSubscription == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.NewVideoFromSubscription
}

// GetNewVideoFromSubscriptionOk returns a tuple with the NewVideoFromSubscription field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetNewVideoFromSubscriptionOk() (*NotificationSettingValue, bool) {
	if o == nil || o.NewVideoFromSubscription == nil {
		return nil, false
	}
	return o.NewVideoFromSubscription, true
}

// HasNewVideoFromSubscription returns a boolean if a field has been set.
func (o *InlineObject5) HasNewVideoFromSubscription() bool {
	if o != nil && o.NewVideoFromSubscription != nil {
		return true
	}

	return false
}

// SetNewVideoFromSubscription gets a reference to the given NotificationSettingValue and assigns it to the NewVideoFromSubscription field.
func (o *InlineObject5) SetNewVideoFromSubscription(v NotificationSettingValue) {
	o.NewVideoFromSubscription = &v
}

// GetNewCommentOnMyVideo returns the NewCommentOnMyVideo field value if set, zero value otherwise.
func (o *InlineObject5) GetNewCommentOnMyVideo() NotificationSettingValue {
	if o == nil || o.NewCommentOnMyVideo == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.NewCommentOnMyVideo
}

// GetNewCommentOnMyVideoOk returns a tuple with the NewCommentOnMyVideo field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetNewCommentOnMyVideoOk() (*NotificationSettingValue, bool) {
	if o == nil || o.NewCommentOnMyVideo == nil {
		return nil, false
	}
	return o.NewCommentOnMyVideo, true
}

// HasNewCommentOnMyVideo returns a boolean if a field has been set.
func (o *InlineObject5) HasNewCommentOnMyVideo() bool {
	if o != nil && o.NewCommentOnMyVideo != nil {
		return true
	}

	return false
}

// SetNewCommentOnMyVideo gets a reference to the given NotificationSettingValue and assigns it to the NewCommentOnMyVideo field.
func (o *InlineObject5) SetNewCommentOnMyVideo(v NotificationSettingValue) {
	o.NewCommentOnMyVideo = &v
}

// GetAbuseAsModerator returns the AbuseAsModerator field value if set, zero value otherwise.
func (o *InlineObject5) GetAbuseAsModerator() NotificationSettingValue {
	if o == nil || o.AbuseAsModerator == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.AbuseAsModerator
}

// GetAbuseAsModeratorOk returns a tuple with the AbuseAsModerator field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetAbuseAsModeratorOk() (*NotificationSettingValue, bool) {
	if o == nil || o.AbuseAsModerator == nil {
		return nil, false
	}
	return o.AbuseAsModerator, true
}

// HasAbuseAsModerator returns a boolean if a field has been set.
func (o *InlineObject5) HasAbuseAsModerator() bool {
	if o != nil && o.AbuseAsModerator != nil {
		return true
	}

	return false
}

// SetAbuseAsModerator gets a reference to the given NotificationSettingValue and assigns it to the AbuseAsModerator field.
func (o *InlineObject5) SetAbuseAsModerator(v NotificationSettingValue) {
	o.AbuseAsModerator = &v
}

// GetVideoAutoBlacklistAsModerator returns the VideoAutoBlacklistAsModerator field value if set, zero value otherwise.
func (o *InlineObject5) GetVideoAutoBlacklistAsModerator() NotificationSettingValue {
	if o == nil || o.VideoAutoBlacklistAsModerator == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.VideoAutoBlacklistAsModerator
}

// GetVideoAutoBlacklistAsModeratorOk returns a tuple with the VideoAutoBlacklistAsModerator field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetVideoAutoBlacklistAsModeratorOk() (*NotificationSettingValue, bool) {
	if o == nil || o.VideoAutoBlacklistAsModerator == nil {
		return nil, false
	}
	return o.VideoAutoBlacklistAsModerator, true
}

// HasVideoAutoBlacklistAsModerator returns a boolean if a field has been set.
func (o *InlineObject5) HasVideoAutoBlacklistAsModerator() bool {
	if o != nil && o.VideoAutoBlacklistAsModerator != nil {
		return true
	}

	return false
}

// SetVideoAutoBlacklistAsModerator gets a reference to the given NotificationSettingValue and assigns it to the VideoAutoBlacklistAsModerator field.
func (o *InlineObject5) SetVideoAutoBlacklistAsModerator(v NotificationSettingValue) {
	o.VideoAutoBlacklistAsModerator = &v
}

// GetBlacklistOnMyVideo returns the BlacklistOnMyVideo field value if set, zero value otherwise.
func (o *InlineObject5) GetBlacklistOnMyVideo() NotificationSettingValue {
	if o == nil || o.BlacklistOnMyVideo == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.BlacklistOnMyVideo
}

// GetBlacklistOnMyVideoOk returns a tuple with the BlacklistOnMyVideo field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetBlacklistOnMyVideoOk() (*NotificationSettingValue, bool) {
	if o == nil || o.BlacklistOnMyVideo == nil {
		return nil, false
	}
	return o.BlacklistOnMyVideo, true
}

// HasBlacklistOnMyVideo returns a boolean if a field has been set.
func (o *InlineObject5) HasBlacklistOnMyVideo() bool {
	if o != nil && o.BlacklistOnMyVideo != nil {
		return true
	}

	return false
}

// SetBlacklistOnMyVideo gets a reference to the given NotificationSettingValue and assigns it to the BlacklistOnMyVideo field.
func (o *InlineObject5) SetBlacklistOnMyVideo(v NotificationSettingValue) {
	o.BlacklistOnMyVideo = &v
}

// GetMyVideoPublished returns the MyVideoPublished field value if set, zero value otherwise.
func (o *InlineObject5) GetMyVideoPublished() NotificationSettingValue {
	if o == nil || o.MyVideoPublished == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.MyVideoPublished
}

// GetMyVideoPublishedOk returns a tuple with the MyVideoPublished field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetMyVideoPublishedOk() (*NotificationSettingValue, bool) {
	if o == nil || o.MyVideoPublished == nil {
		return nil, false
	}
	return o.MyVideoPublished, true
}

// HasMyVideoPublished returns a boolean if a field has been set.
func (o *InlineObject5) HasMyVideoPublished() bool {
	if o != nil && o.MyVideoPublished != nil {
		return true
	}

	return false
}

// SetMyVideoPublished gets a reference to the given NotificationSettingValue and assigns it to the MyVideoPublished field.
func (o *InlineObject5) SetMyVideoPublished(v NotificationSettingValue) {
	o.MyVideoPublished = &v
}

// GetMyVideoImportFinished returns the MyVideoImportFinished field value if set, zero value otherwise.
func (o *InlineObject5) GetMyVideoImportFinished() NotificationSettingValue {
	if o == nil || o.MyVideoImportFinished == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.MyVideoImportFinished
}

// GetMyVideoImportFinishedOk returns a tuple with the MyVideoImportFinished field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetMyVideoImportFinishedOk() (*NotificationSettingValue, bool) {
	if o == nil || o.MyVideoImportFinished == nil {
		return nil, false
	}
	return o.MyVideoImportFinished, true
}

// HasMyVideoImportFinished returns a boolean if a field has been set.
func (o *InlineObject5) HasMyVideoImportFinished() bool {
	if o != nil && o.MyVideoImportFinished != nil {
		return true
	}

	return false
}

// SetMyVideoImportFinished gets a reference to the given NotificationSettingValue and assigns it to the MyVideoImportFinished field.
func (o *InlineObject5) SetMyVideoImportFinished(v NotificationSettingValue) {
	o.MyVideoImportFinished = &v
}

// GetNewFollow returns the NewFollow field value if set, zero value otherwise.
func (o *InlineObject5) GetNewFollow() NotificationSettingValue {
	if o == nil || o.NewFollow == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.NewFollow
}

// GetNewFollowOk returns a tuple with the NewFollow field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetNewFollowOk() (*NotificationSettingValue, bool) {
	if o == nil || o.NewFollow == nil {
		return nil, false
	}
	return o.NewFollow, true
}

// HasNewFollow returns a boolean if a field has been set.
func (o *InlineObject5) HasNewFollow() bool {
	if o != nil && o.NewFollow != nil {
		return true
	}

	return false
}

// SetNewFollow gets a reference to the given NotificationSettingValue and assigns it to the NewFollow field.
func (o *InlineObject5) SetNewFollow(v NotificationSettingValue) {
	o.NewFollow = &v
}

// GetNewUserRegistration returns the NewUserRegistration field value if set, zero value otherwise.
func (o *InlineObject5) GetNewUserRegistration() NotificationSettingValue {
	if o == nil || o.NewUserRegistration == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.NewUserRegistration
}

// GetNewUserRegistrationOk returns a tuple with the NewUserRegistration field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetNewUserRegistrationOk() (*NotificationSettingValue, bool) {
	if o == nil || o.NewUserRegistration == nil {
		return nil, false
	}
	return o.NewUserRegistration, true
}

// HasNewUserRegistration returns a boolean if a field has been set.
func (o *InlineObject5) HasNewUserRegistration() bool {
	if o != nil && o.NewUserRegistration != nil {
		return true
	}

	return false
}

// SetNewUserRegistration gets a reference to the given NotificationSettingValue and assigns it to the NewUserRegistration field.
func (o *InlineObject5) SetNewUserRegistration(v NotificationSettingValue) {
	o.NewUserRegistration = &v
}

// GetCommentMention returns the CommentMention field value if set, zero value otherwise.
func (o *InlineObject5) GetCommentMention() NotificationSettingValue {
	if o == nil || o.CommentMention == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.CommentMention
}

// GetCommentMentionOk returns a tuple with the CommentMention field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetCommentMentionOk() (*NotificationSettingValue, bool) {
	if o == nil || o.CommentMention == nil {
		return nil, false
	}
	return o.CommentMention, true
}

// HasCommentMention returns a boolean if a field has been set.
func (o *InlineObject5) HasCommentMention() bool {
	if o != nil && o.CommentMention != nil {
		return true
	}

	return false
}

// SetCommentMention gets a reference to the given NotificationSettingValue and assigns it to the CommentMention field.
func (o *InlineObject5) SetCommentMention(v NotificationSettingValue) {
	o.CommentMention = &v
}

// GetNewInstanceFollower returns the NewInstanceFollower field value if set, zero value otherwise.
func (o *InlineObject5) GetNewInstanceFollower() NotificationSettingValue {
	if o == nil || o.NewInstanceFollower == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.NewInstanceFollower
}

// GetNewInstanceFollowerOk returns a tuple with the NewInstanceFollower field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetNewInstanceFollowerOk() (*NotificationSettingValue, bool) {
	if o == nil || o.NewInstanceFollower == nil {
		return nil, false
	}
	return o.NewInstanceFollower, true
}

// HasNewInstanceFollower returns a boolean if a field has been set.
func (o *InlineObject5) HasNewInstanceFollower() bool {
	if o != nil && o.NewInstanceFollower != nil {
		return true
	}

	return false
}

// SetNewInstanceFollower gets a reference to the given NotificationSettingValue and assigns it to the NewInstanceFollower field.
func (o *InlineObject5) SetNewInstanceFollower(v NotificationSettingValue) {
	o.NewInstanceFollower = &v
}

// GetAutoInstanceFollowing returns the AutoInstanceFollowing field value if set, zero value otherwise.
func (o *InlineObject5) GetAutoInstanceFollowing() NotificationSettingValue {
	if o == nil || o.AutoInstanceFollowing == nil {
		var ret NotificationSettingValue
		return ret
	}
	return *o.AutoInstanceFollowing
}

// GetAutoInstanceFollowingOk returns a tuple with the AutoInstanceFollowing field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject5) GetAutoInstanceFollowingOk() (*NotificationSettingValue, bool) {
	if o == nil || o.AutoInstanceFollowing == nil {
		return nil, false
	}
	return o.AutoInstanceFollowing, true
}

// HasAutoInstanceFollowing returns a boolean if a field has been set.
func (o *InlineObject5) HasAutoInstanceFollowing() bool {
	if o != nil && o.AutoInstanceFollowing != nil {
		return true
	}

	return false
}

// SetAutoInstanceFollowing gets a reference to the given NotificationSettingValue and assigns it to the AutoInstanceFollowing field.
func (o *InlineObject5) SetAutoInstanceFollowing(v NotificationSettingValue) {
	o.AutoInstanceFollowing = &v
}

func (o InlineObject5) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.NewVideoFromSubscription != nil {
		toSerialize["newVideoFromSubscription"] = o.NewVideoFromSubscription
	}
	if o.NewCommentOnMyVideo != nil {
		toSerialize["newCommentOnMyVideo"] = o.NewCommentOnMyVideo
	}
	if o.AbuseAsModerator != nil {
		toSerialize["abuseAsModerator"] = o.AbuseAsModerator
	}
	if o.VideoAutoBlacklistAsModerator != nil {
		toSerialize["videoAutoBlacklistAsModerator"] = o.VideoAutoBlacklistAsModerator
	}
	if o.BlacklistOnMyVideo != nil {
		toSerialize["blacklistOnMyVideo"] = o.BlacklistOnMyVideo
	}
	if o.MyVideoPublished != nil {
		toSerialize["myVideoPublished"] = o.MyVideoPublished
	}
	if o.MyVideoImportFinished != nil {
		toSerialize["myVideoImportFinished"] = o.MyVideoImportFinished
	}
	if o.NewFollow != nil {
		toSerialize["newFollow"] = o.NewFollow
	}
	if o.NewUserRegistration != nil {
		toSerialize["newUserRegistration"] = o.NewUserRegistration
	}
	if o.CommentMention != nil {
		toSerialize["commentMention"] = o.CommentMention
	}
	if o.NewInstanceFollower != nil {
		toSerialize["newInstanceFollower"] = o.NewInstanceFollower
	}
	if o.AutoInstanceFollowing != nil {
		toSerialize["autoInstanceFollowing"] = o.AutoInstanceFollowing
	}
	return json.Marshal(toSerialize)
}

type NullableInlineObject5 struct {
	value *InlineObject5
	isSet bool
}

func (v NullableInlineObject5) Get() *InlineObject5 {
	return v.value
}

func (v *NullableInlineObject5) Set(val *InlineObject5) {
	v.value = val
	v.isSet = true
}

func (v NullableInlineObject5) IsSet() bool {
	return v.isSet
}

func (v *NullableInlineObject5) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableInlineObject5(val *InlineObject5) *NullableInlineObject5 {
	return &NullableInlineObject5{value: val, isSet: true}
}

func (v NullableInlineObject5) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableInlineObject5) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


