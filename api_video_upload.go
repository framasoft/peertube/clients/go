/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"time"
	"os"
)

// Linger please
var (
	_ _context.Context
)

// VideoUploadApiService VideoUploadApi service
type VideoUploadApiService service

type ApiImportVideoRequest struct {
	ctx _context.Context
	ApiService *VideoUploadApiService
	name *string
	channelId *int32
	targetUrl *TargetUrl
	magnetUri *MagnetUri
	torrentfile *Torrentfile
	privacy *VideoPrivacySet
	category *int32
	licence *int32
	language *string
	description *string
	waitTranscoding *bool
	support *string
	nsfw *bool
	tags *[]string
	commentsEnabled *bool
	downloadEnabled *bool
	originallyPublishedAt *time.Time
	scheduleUpdate *VideoScheduledUpdate
	thumbnailfile **os.File
	previewfile **os.File
}

// Video name
func (r ApiImportVideoRequest) Name(name string) ApiImportVideoRequest {
	r.name = &name
	return r
}
// Channel id that will contain this video
func (r ApiImportVideoRequest) ChannelId(channelId int32) ApiImportVideoRequest {
	r.channelId = &channelId
	return r
}
func (r ApiImportVideoRequest) TargetUrl(targetUrl TargetUrl) ApiImportVideoRequest {
	r.targetUrl = &targetUrl
	return r
}
func (r ApiImportVideoRequest) MagnetUri(magnetUri MagnetUri) ApiImportVideoRequest {
	r.magnetUri = &magnetUri
	return r
}
func (r ApiImportVideoRequest) Torrentfile(torrentfile Torrentfile) ApiImportVideoRequest {
	r.torrentfile = &torrentfile
	return r
}
func (r ApiImportVideoRequest) Privacy(privacy VideoPrivacySet) ApiImportVideoRequest {
	r.privacy = &privacy
	return r
}
// category id of the video (see [/videos/categories](#operation/getCategories))
func (r ApiImportVideoRequest) Category(category int32) ApiImportVideoRequest {
	r.category = &category
	return r
}
// licence id of the video (see [/videos/licences](#operation/getLicences))
func (r ApiImportVideoRequest) Licence(licence int32) ApiImportVideoRequest {
	r.licence = &licence
	return r
}
// language id of the video (see [/videos/languages](#operation/getLanguages))
func (r ApiImportVideoRequest) Language(language string) ApiImportVideoRequest {
	r.language = &language
	return r
}
// Video description
func (r ApiImportVideoRequest) Description(description string) ApiImportVideoRequest {
	r.description = &description
	return r
}
// Whether or not we wait transcoding before publish the video
func (r ApiImportVideoRequest) WaitTranscoding(waitTranscoding bool) ApiImportVideoRequest {
	r.waitTranscoding = &waitTranscoding
	return r
}
// A text tell the audience how to support the video creator
func (r ApiImportVideoRequest) Support(support string) ApiImportVideoRequest {
	r.support = &support
	return r
}
// Whether or not this video contains sensitive content
func (r ApiImportVideoRequest) Nsfw(nsfw bool) ApiImportVideoRequest {
	r.nsfw = &nsfw
	return r
}
// Video tags (maximum 5 tags each between 2 and 30 characters)
func (r ApiImportVideoRequest) Tags(tags []string) ApiImportVideoRequest {
	r.tags = &tags
	return r
}
// Enable or disable comments for this video
func (r ApiImportVideoRequest) CommentsEnabled(commentsEnabled bool) ApiImportVideoRequest {
	r.commentsEnabled = &commentsEnabled
	return r
}
// Enable or disable downloading for this video
func (r ApiImportVideoRequest) DownloadEnabled(downloadEnabled bool) ApiImportVideoRequest {
	r.downloadEnabled = &downloadEnabled
	return r
}
// Date when the content was originally published
func (r ApiImportVideoRequest) OriginallyPublishedAt(originallyPublishedAt time.Time) ApiImportVideoRequest {
	r.originallyPublishedAt = &originallyPublishedAt
	return r
}
func (r ApiImportVideoRequest) ScheduleUpdate(scheduleUpdate VideoScheduledUpdate) ApiImportVideoRequest {
	r.scheduleUpdate = &scheduleUpdate
	return r
}
// Video thumbnail file
func (r ApiImportVideoRequest) Thumbnailfile(thumbnailfile *os.File) ApiImportVideoRequest {
	r.thumbnailfile = &thumbnailfile
	return r
}
// Video preview file
func (r ApiImportVideoRequest) Previewfile(previewfile *os.File) ApiImportVideoRequest {
	r.previewfile = &previewfile
	return r
}

func (r ApiImportVideoRequest) Execute() (VideoUploadResponse, *_nethttp.Response, error) {
	return r.ApiService.ImportVideoExecute(r)
}

/*
ImportVideo Import a video

Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiImportVideoRequest
*/
func (a *VideoUploadApiService) ImportVideo(ctx _context.Context) ApiImportVideoRequest {
	return ApiImportVideoRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoUploadResponse
func (a *VideoUploadApiService) ImportVideoExecute(r ApiImportVideoRequest) (VideoUploadResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoUploadResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoUploadApiService.ImportVideo")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/videos/imports"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.name == nil {
		return localVarReturnValue, nil, reportError("name is required and must be specified")
	}
	if strlen(*r.name) < 3 {
		return localVarReturnValue, nil, reportError("name must have at least 3 elements")
	}
	if strlen(*r.name) > 120 {
		return localVarReturnValue, nil, reportError("name must have less than 120 elements")
	}
	if r.channelId == nil {
		return localVarReturnValue, nil, reportError("channelId is required and must be specified")
	}
	if *r.channelId < 1 {
		return localVarReturnValue, nil, reportError("channelId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"multipart/form-data"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	if r.targetUrl != nil {
		localVarFormParams.Add("targetUrl", parameterToString(*r.targetUrl, ""))
	}
	if r.magnetUri != nil {
		localVarFormParams.Add("magnetUri", parameterToString(*r.magnetUri, ""))
	}
	if r.torrentfile != nil {
		localVarFormParams.Add("torrentfile", parameterToString(*r.torrentfile, ""))
	}
	localVarFormParams.Add("name", parameterToString(*r.name, ""))
	localVarFormParams.Add("channelId", parameterToString(*r.channelId, ""))
	if r.privacy != nil {
		localVarFormParams.Add("privacy", parameterToString(*r.privacy, ""))
	}
	if r.category != nil {
		localVarFormParams.Add("category", parameterToString(*r.category, ""))
	}
	if r.licence != nil {
		localVarFormParams.Add("licence", parameterToString(*r.licence, ""))
	}
	if r.language != nil {
		localVarFormParams.Add("language", parameterToString(*r.language, ""))
	}
	if r.description != nil {
		localVarFormParams.Add("description", parameterToString(*r.description, ""))
	}
	if r.waitTranscoding != nil {
		localVarFormParams.Add("waitTranscoding", parameterToString(*r.waitTranscoding, ""))
	}
	if r.support != nil {
		localVarFormParams.Add("support", parameterToString(*r.support, ""))
	}
	if r.nsfw != nil {
		localVarFormParams.Add("nsfw", parameterToString(*r.nsfw, ""))
	}
	if r.tags != nil {
		localVarFormParams.Add("tags", parameterToString(*r.tags, "csv"))
	}
	if r.commentsEnabled != nil {
		localVarFormParams.Add("commentsEnabled", parameterToString(*r.commentsEnabled, ""))
	}
	if r.downloadEnabled != nil {
		localVarFormParams.Add("downloadEnabled", parameterToString(*r.downloadEnabled, ""))
	}
	if r.originallyPublishedAt != nil {
		localVarFormParams.Add("originallyPublishedAt", parameterToString(*r.originallyPublishedAt, ""))
	}
	if r.scheduleUpdate != nil {
		paramJson, err := parameterToJson(*r.scheduleUpdate)
		if err != nil {
			return localVarReturnValue, nil, err
		}
		localVarFormParams.Add("scheduleUpdate", paramJson)
	}
	localVarFormFileName = "thumbnailfile"
	var localVarFile *os.File
	if r.thumbnailfile != nil {
		localVarFile = *r.thumbnailfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	localVarFormFileName = "previewfile"
	var localVarFile *os.File
	if r.previewfile != nil {
		localVarFile = *r.previewfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiUploadLegacyRequest struct {
	ctx _context.Context
	ApiService *VideoUploadApiService
	name *string
	channelId *int32
	privacy *VideoPrivacySet
	category *int32
	licence *int32
	language *string
	description *string
	waitTranscoding *bool
	support *string
	nsfw *bool
	tags *[]string
	commentsEnabled *bool
	downloadEnabled *bool
	originallyPublishedAt *time.Time
	scheduleUpdate *VideoScheduledUpdate
	thumbnailfile **os.File
	previewfile **os.File
	videofile **os.File
}

// Video name
func (r ApiUploadLegacyRequest) Name(name string) ApiUploadLegacyRequest {
	r.name = &name
	return r
}
// Channel id that will contain this video
func (r ApiUploadLegacyRequest) ChannelId(channelId int32) ApiUploadLegacyRequest {
	r.channelId = &channelId
	return r
}
func (r ApiUploadLegacyRequest) Privacy(privacy VideoPrivacySet) ApiUploadLegacyRequest {
	r.privacy = &privacy
	return r
}
// category id of the video (see [/videos/categories](#operation/getCategories))
func (r ApiUploadLegacyRequest) Category(category int32) ApiUploadLegacyRequest {
	r.category = &category
	return r
}
// licence id of the video (see [/videos/licences](#operation/getLicences))
func (r ApiUploadLegacyRequest) Licence(licence int32) ApiUploadLegacyRequest {
	r.licence = &licence
	return r
}
// language id of the video (see [/videos/languages](#operation/getLanguages))
func (r ApiUploadLegacyRequest) Language(language string) ApiUploadLegacyRequest {
	r.language = &language
	return r
}
// Video description
func (r ApiUploadLegacyRequest) Description(description string) ApiUploadLegacyRequest {
	r.description = &description
	return r
}
// Whether or not we wait transcoding before publish the video
func (r ApiUploadLegacyRequest) WaitTranscoding(waitTranscoding bool) ApiUploadLegacyRequest {
	r.waitTranscoding = &waitTranscoding
	return r
}
// A text tell the audience how to support the video creator
func (r ApiUploadLegacyRequest) Support(support string) ApiUploadLegacyRequest {
	r.support = &support
	return r
}
// Whether or not this video contains sensitive content
func (r ApiUploadLegacyRequest) Nsfw(nsfw bool) ApiUploadLegacyRequest {
	r.nsfw = &nsfw
	return r
}
// Video tags (maximum 5 tags each between 2 and 30 characters)
func (r ApiUploadLegacyRequest) Tags(tags []string) ApiUploadLegacyRequest {
	r.tags = &tags
	return r
}
// Enable or disable comments for this video
func (r ApiUploadLegacyRequest) CommentsEnabled(commentsEnabled bool) ApiUploadLegacyRequest {
	r.commentsEnabled = &commentsEnabled
	return r
}
// Enable or disable downloading for this video
func (r ApiUploadLegacyRequest) DownloadEnabled(downloadEnabled bool) ApiUploadLegacyRequest {
	r.downloadEnabled = &downloadEnabled
	return r
}
// Date when the content was originally published
func (r ApiUploadLegacyRequest) OriginallyPublishedAt(originallyPublishedAt time.Time) ApiUploadLegacyRequest {
	r.originallyPublishedAt = &originallyPublishedAt
	return r
}
func (r ApiUploadLegacyRequest) ScheduleUpdate(scheduleUpdate VideoScheduledUpdate) ApiUploadLegacyRequest {
	r.scheduleUpdate = &scheduleUpdate
	return r
}
// Video thumbnail file
func (r ApiUploadLegacyRequest) Thumbnailfile(thumbnailfile *os.File) ApiUploadLegacyRequest {
	r.thumbnailfile = &thumbnailfile
	return r
}
// Video preview file
func (r ApiUploadLegacyRequest) Previewfile(previewfile *os.File) ApiUploadLegacyRequest {
	r.previewfile = &previewfile
	return r
}
// Video file
func (r ApiUploadLegacyRequest) Videofile(videofile *os.File) ApiUploadLegacyRequest {
	r.videofile = &videofile
	return r
}

func (r ApiUploadLegacyRequest) Execute() (VideoUploadResponse, *_nethttp.Response, error) {
	return r.ApiService.UploadLegacyExecute(r)
}

/*
UploadLegacy Upload a video

Uses a single request to upload a video.

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiUploadLegacyRequest
*/
func (a *VideoUploadApiService) UploadLegacy(ctx _context.Context) ApiUploadLegacyRequest {
	return ApiUploadLegacyRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoUploadResponse
func (a *VideoUploadApiService) UploadLegacyExecute(r ApiUploadLegacyRequest) (VideoUploadResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoUploadResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoUploadApiService.UploadLegacy")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/videos/upload"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"multipart/form-data"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	if r.name != nil {
		localVarFormParams.Add("name", parameterToString(*r.name, ""))
	}
	if r.channelId != nil {
		localVarFormParams.Add("channelId", parameterToString(*r.channelId, ""))
	}
	if r.privacy != nil {
		localVarFormParams.Add("privacy", parameterToString(*r.privacy, ""))
	}
	if r.category != nil {
		localVarFormParams.Add("category", parameterToString(*r.category, ""))
	}
	if r.licence != nil {
		localVarFormParams.Add("licence", parameterToString(*r.licence, ""))
	}
	if r.language != nil {
		localVarFormParams.Add("language", parameterToString(*r.language, ""))
	}
	if r.description != nil {
		localVarFormParams.Add("description", parameterToString(*r.description, ""))
	}
	if r.waitTranscoding != nil {
		localVarFormParams.Add("waitTranscoding", parameterToString(*r.waitTranscoding, ""))
	}
	if r.support != nil {
		localVarFormParams.Add("support", parameterToString(*r.support, ""))
	}
	if r.nsfw != nil {
		localVarFormParams.Add("nsfw", parameterToString(*r.nsfw, ""))
	}
	if r.tags != nil {
		localVarFormParams.Add("tags", parameterToString(*r.tags, "csv"))
	}
	if r.commentsEnabled != nil {
		localVarFormParams.Add("commentsEnabled", parameterToString(*r.commentsEnabled, ""))
	}
	if r.downloadEnabled != nil {
		localVarFormParams.Add("downloadEnabled", parameterToString(*r.downloadEnabled, ""))
	}
	if r.originallyPublishedAt != nil {
		localVarFormParams.Add("originallyPublishedAt", parameterToString(*r.originallyPublishedAt, ""))
	}
	if r.scheduleUpdate != nil {
		paramJson, err := parameterToJson(*r.scheduleUpdate)
		if err != nil {
			return localVarReturnValue, nil, err
		}
		localVarFormParams.Add("scheduleUpdate", paramJson)
	}
	localVarFormFileName = "thumbnailfile"
	var localVarFile *os.File
	if r.thumbnailfile != nil {
		localVarFile = *r.thumbnailfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	localVarFormFileName = "previewfile"
	var localVarFile *os.File
	if r.previewfile != nil {
		localVarFile = *r.previewfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	localVarFormFileName = "videofile"
	var localVarFile *os.File
	if r.videofile != nil {
		localVarFile = *r.videofile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiUploadResumableRequest struct {
	ctx _context.Context
	ApiService *VideoUploadApiService
	uploadId *string
	contentRange *string
	contentLength *float32
	body **os.File
}

// Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload. 
func (r ApiUploadResumableRequest) UploadId(uploadId string) ApiUploadResumableRequest {
	r.uploadId = &uploadId
	return r
}
// Specifies the bytes in the file that the request is uploading.  For example, a value of &#x60;bytes 0-262143/1000000&#x60; shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file. 
func (r ApiUploadResumableRequest) ContentRange(contentRange string) ApiUploadResumableRequest {
	r.contentRange = &contentRange
	return r
}
// Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn&#39;t mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube&#39;s web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health. 
func (r ApiUploadResumableRequest) ContentLength(contentLength float32) ApiUploadResumableRequest {
	r.contentLength = &contentLength
	return r
}
func (r ApiUploadResumableRequest) Body(body *os.File) ApiUploadResumableRequest {
	r.body = &body
	return r
}

func (r ApiUploadResumableRequest) Execute() (VideoUploadResponse, *_nethttp.Response, error) {
	return r.ApiService.UploadResumableExecute(r)
}

/*
UploadResumable Send chunk for the resumable upload of a video

Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to continue, pause or resume the upload of a video

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiUploadResumableRequest
*/
func (a *VideoUploadApiService) UploadResumable(ctx _context.Context) ApiUploadResumableRequest {
	return ApiUploadResumableRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoUploadResponse
func (a *VideoUploadApiService) UploadResumableExecute(r ApiUploadResumableRequest) (VideoUploadResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPut
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoUploadResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoUploadApiService.UploadResumable")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/videos/upload-resumable"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.uploadId == nil {
		return localVarReturnValue, nil, reportError("uploadId is required and must be specified")
	}
	if r.contentRange == nil {
		return localVarReturnValue, nil, reportError("contentRange is required and must be specified")
	}
	if r.contentLength == nil {
		return localVarReturnValue, nil, reportError("contentLength is required and must be specified")
	}

	localVarQueryParams.Add("upload_id", parameterToString(*r.uploadId, ""))
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/octet-stream"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarHeaderParams["Content-Range"] = parameterToString(*r.contentRange, "")
	localVarHeaderParams["Content-Length"] = parameterToString(*r.contentLength, "")
	// body params
	localVarPostBody = r.body
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiUploadResumableCancelRequest struct {
	ctx _context.Context
	ApiService *VideoUploadApiService
	uploadId *string
	contentLength *float32
}

// Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-) 
func (r ApiUploadResumableCancelRequest) UploadId(uploadId string) ApiUploadResumableCancelRequest {
	r.uploadId = &uploadId
	return r
}
func (r ApiUploadResumableCancelRequest) ContentLength(contentLength float32) ApiUploadResumableCancelRequest {
	r.contentLength = &contentLength
	return r
}

func (r ApiUploadResumableCancelRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.UploadResumableCancelExecute(r)
}

/*
UploadResumableCancel Cancel the resumable upload of a video, deleting any data uploaded so far

Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to cancel the upload of a video

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiUploadResumableCancelRequest
*/
func (a *VideoUploadApiService) UploadResumableCancel(ctx _context.Context) ApiUploadResumableCancelRequest {
	return ApiUploadResumableCancelRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
func (a *VideoUploadApiService) UploadResumableCancelExecute(r ApiUploadResumableCancelRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodDelete
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoUploadApiService.UploadResumableCancel")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/videos/upload-resumable"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.uploadId == nil {
		return nil, reportError("uploadId is required and must be specified")
	}
	if r.contentLength == nil {
		return nil, reportError("contentLength is required and must be specified")
	}

	localVarQueryParams.Add("upload_id", parameterToString(*r.uploadId, ""))
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarHeaderParams["Content-Length"] = parameterToString(*r.contentLength, "")
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiUploadResumableInitRequest struct {
	ctx _context.Context
	ApiService *VideoUploadApiService
	xUploadContentLength *float32
	xUploadContentType *string
	videoUploadRequestResumable *VideoUploadRequestResumable
}

// Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading.
func (r ApiUploadResumableInitRequest) XUploadContentLength(xUploadContentLength float32) ApiUploadResumableInitRequest {
	r.xUploadContentLength = &xUploadContentLength
	return r
}
// MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary.
func (r ApiUploadResumableInitRequest) XUploadContentType(xUploadContentType string) ApiUploadResumableInitRequest {
	r.xUploadContentType = &xUploadContentType
	return r
}
func (r ApiUploadResumableInitRequest) VideoUploadRequestResumable(videoUploadRequestResumable VideoUploadRequestResumable) ApiUploadResumableInitRequest {
	r.videoUploadRequestResumable = &videoUploadRequestResumable
	return r
}

func (r ApiUploadResumableInitRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.UploadResumableInitExecute(r)
}

/*
UploadResumableInit Initialize the resumable upload of a video

Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to initialize the upload of a video

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiUploadResumableInitRequest
*/
func (a *VideoUploadApiService) UploadResumableInit(ctx _context.Context) ApiUploadResumableInitRequest {
	return ApiUploadResumableInitRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
func (a *VideoUploadApiService) UploadResumableInitExecute(r ApiUploadResumableInitRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoUploadApiService.UploadResumableInit")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/videos/upload-resumable"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.xUploadContentLength == nil {
		return nil, reportError("xUploadContentLength is required and must be specified")
	}
	if r.xUploadContentType == nil {
		return nil, reportError("xUploadContentType is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarHeaderParams["X-Upload-Content-Length"] = parameterToString(*r.xUploadContentLength, "")
	localVarHeaderParams["X-Upload-Content-Type"] = parameterToString(*r.xUploadContentType, "")
	// body params
	localVarPostBody = r.videoUploadRequestResumable
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}
