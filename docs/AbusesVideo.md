# AbusesVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int32** | Video id to report | [optional] 
**StartAt** | Pointer to **int32** | Timestamp in the video that marks the beginning of the report | [optional] 
**EndAt** | Pointer to **int32** | Timestamp in the video that marks the ending of the report | [optional] 

## Methods

### NewAbusesVideo

`func NewAbusesVideo() *AbusesVideo`

NewAbusesVideo instantiates a new AbusesVideo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAbusesVideoWithDefaults

`func NewAbusesVideoWithDefaults() *AbusesVideo`

NewAbusesVideoWithDefaults instantiates a new AbusesVideo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *AbusesVideo) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *AbusesVideo) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *AbusesVideo) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *AbusesVideo) HasId() bool`

HasId returns a boolean if a field has been set.

### GetStartAt

`func (o *AbusesVideo) GetStartAt() int32`

GetStartAt returns the StartAt field if non-nil, zero value otherwise.

### GetStartAtOk

`func (o *AbusesVideo) GetStartAtOk() (*int32, bool)`

GetStartAtOk returns a tuple with the StartAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartAt

`func (o *AbusesVideo) SetStartAt(v int32)`

SetStartAt sets StartAt field to given value.

### HasStartAt

`func (o *AbusesVideo) HasStartAt() bool`

HasStartAt returns a boolean if a field has been set.

### GetEndAt

`func (o *AbusesVideo) GetEndAt() int32`

GetEndAt returns the EndAt field if non-nil, zero value otherwise.

### GetEndAtOk

`func (o *AbusesVideo) GetEndAtOk() (*int32, bool)`

GetEndAtOk returns a tuple with the EndAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndAt

`func (o *AbusesVideo) SetEndAt(v int32)`

SetEndAt sets EndAt field to given value.

### HasEndAt

`func (o *AbusesVideo) HasEndAt() bool`

HasEndAt returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


