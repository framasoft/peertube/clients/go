# InlineResponse20014VideoId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PlaylistElementId** | Pointer to **int32** |  | [optional] 
**PlaylistId** | Pointer to **int32** |  | [optional] 
**StartTimestamp** | Pointer to **int32** |  | [optional] 
**StopTimestamp** | Pointer to **int32** |  | [optional] 

## Methods

### NewInlineResponse20014VideoId

`func NewInlineResponse20014VideoId() *InlineResponse20014VideoId`

NewInlineResponse20014VideoId instantiates a new InlineResponse20014VideoId object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse20014VideoIdWithDefaults

`func NewInlineResponse20014VideoIdWithDefaults() *InlineResponse20014VideoId`

NewInlineResponse20014VideoIdWithDefaults instantiates a new InlineResponse20014VideoId object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPlaylistElementId

`func (o *InlineResponse20014VideoId) GetPlaylistElementId() int32`

GetPlaylistElementId returns the PlaylistElementId field if non-nil, zero value otherwise.

### GetPlaylistElementIdOk

`func (o *InlineResponse20014VideoId) GetPlaylistElementIdOk() (*int32, bool)`

GetPlaylistElementIdOk returns a tuple with the PlaylistElementId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlaylistElementId

`func (o *InlineResponse20014VideoId) SetPlaylistElementId(v int32)`

SetPlaylistElementId sets PlaylistElementId field to given value.

### HasPlaylistElementId

`func (o *InlineResponse20014VideoId) HasPlaylistElementId() bool`

HasPlaylistElementId returns a boolean if a field has been set.

### GetPlaylistId

`func (o *InlineResponse20014VideoId) GetPlaylistId() int32`

GetPlaylistId returns the PlaylistId field if non-nil, zero value otherwise.

### GetPlaylistIdOk

`func (o *InlineResponse20014VideoId) GetPlaylistIdOk() (*int32, bool)`

GetPlaylistIdOk returns a tuple with the PlaylistId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlaylistId

`func (o *InlineResponse20014VideoId) SetPlaylistId(v int32)`

SetPlaylistId sets PlaylistId field to given value.

### HasPlaylistId

`func (o *InlineResponse20014VideoId) HasPlaylistId() bool`

HasPlaylistId returns a boolean if a field has been set.

### GetStartTimestamp

`func (o *InlineResponse20014VideoId) GetStartTimestamp() int32`

GetStartTimestamp returns the StartTimestamp field if non-nil, zero value otherwise.

### GetStartTimestampOk

`func (o *InlineResponse20014VideoId) GetStartTimestampOk() (*int32, bool)`

GetStartTimestampOk returns a tuple with the StartTimestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartTimestamp

`func (o *InlineResponse20014VideoId) SetStartTimestamp(v int32)`

SetStartTimestamp sets StartTimestamp field to given value.

### HasStartTimestamp

`func (o *InlineResponse20014VideoId) HasStartTimestamp() bool`

HasStartTimestamp returns a boolean if a field has been set.

### GetStopTimestamp

`func (o *InlineResponse20014VideoId) GetStopTimestamp() int32`

GetStopTimestamp returns the StopTimestamp field if non-nil, zero value otherwise.

### GetStopTimestampOk

`func (o *InlineResponse20014VideoId) GetStopTimestampOk() (*int32, bool)`

GetStopTimestampOk returns a tuple with the StopTimestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStopTimestamp

`func (o *InlineResponse20014VideoId) SetStopTimestamp(v int32)`

SetStopTimestamp sets StopTimestamp field to given value.

### HasStopTimestamp

`func (o *InlineResponse20014VideoId) HasStopTimestamp() bool`

HasStopTimestamp returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


