# InlineResponse2006

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Abuse** | Pointer to [**InlineResponse2006Abuse**](InlineResponse2006Abuse.md) |  | [optional] 

## Methods

### NewInlineResponse2006

`func NewInlineResponse2006() *InlineResponse2006`

NewInlineResponse2006 instantiates a new InlineResponse2006 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse2006WithDefaults

`func NewInlineResponse2006WithDefaults() *InlineResponse2006`

NewInlineResponse2006WithDefaults instantiates a new InlineResponse2006 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAbuse

`func (o *InlineResponse2006) GetAbuse() InlineResponse2006Abuse`

GetAbuse returns the Abuse field if non-nil, zero value otherwise.

### GetAbuseOk

`func (o *InlineResponse2006) GetAbuseOk() (*InlineResponse2006Abuse, bool)`

GetAbuseOk returns a tuple with the Abuse field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbuse

`func (o *InlineResponse2006) SetAbuse(v InlineResponse2006Abuse)`

SetAbuse sets Abuse field to given value.

### HasAbuse

`func (o *InlineResponse2006) HasAbuse() bool`

HasAbuse returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


