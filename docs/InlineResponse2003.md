# InlineResponse2003

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VideoQuotaUsed** | Pointer to **float32** | The user video quota used so far in bytes | [optional] 
**VideoQuotaUsedDaily** | Pointer to **float32** | The user video quota used today in bytes | [optional] 

## Methods

### NewInlineResponse2003

`func NewInlineResponse2003() *InlineResponse2003`

NewInlineResponse2003 instantiates a new InlineResponse2003 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse2003WithDefaults

`func NewInlineResponse2003WithDefaults() *InlineResponse2003`

NewInlineResponse2003WithDefaults instantiates a new InlineResponse2003 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVideoQuotaUsed

`func (o *InlineResponse2003) GetVideoQuotaUsed() float32`

GetVideoQuotaUsed returns the VideoQuotaUsed field if non-nil, zero value otherwise.

### GetVideoQuotaUsedOk

`func (o *InlineResponse2003) GetVideoQuotaUsedOk() (*float32, bool)`

GetVideoQuotaUsedOk returns a tuple with the VideoQuotaUsed field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoQuotaUsed

`func (o *InlineResponse2003) SetVideoQuotaUsed(v float32)`

SetVideoQuotaUsed sets VideoQuotaUsed field to given value.

### HasVideoQuotaUsed

`func (o *InlineResponse2003) HasVideoQuotaUsed() bool`

HasVideoQuotaUsed returns a boolean if a field has been set.

### GetVideoQuotaUsedDaily

`func (o *InlineResponse2003) GetVideoQuotaUsedDaily() float32`

GetVideoQuotaUsedDaily returns the VideoQuotaUsedDaily field if non-nil, zero value otherwise.

### GetVideoQuotaUsedDailyOk

`func (o *InlineResponse2003) GetVideoQuotaUsedDailyOk() (*float32, bool)`

GetVideoQuotaUsedDailyOk returns a tuple with the VideoQuotaUsedDaily field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoQuotaUsedDaily

`func (o *InlineResponse2003) SetVideoQuotaUsedDaily(v float32)`

SetVideoQuotaUsedDaily sets VideoQuotaUsedDaily field to given value.

### HasVideoQuotaUsedDaily

`func (o *InlineResponse2003) HasVideoQuotaUsedDaily() bool`

HasVideoQuotaUsedDaily returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


