# InlineObject30

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Settings** | Pointer to **map[string]map[string]interface{}** |  | [optional] 

## Methods

### NewInlineObject30

`func NewInlineObject30() *InlineObject30`

NewInlineObject30 instantiates a new InlineObject30 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject30WithDefaults

`func NewInlineObject30WithDefaults() *InlineObject30`

NewInlineObject30WithDefaults instantiates a new InlineObject30 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSettings

`func (o *InlineObject30) GetSettings() map[string]map[string]interface{}`

GetSettings returns the Settings field if non-nil, zero value otherwise.

### GetSettingsOk

`func (o *InlineObject30) GetSettingsOk() (*map[string]map[string]interface{}, bool)`

GetSettingsOk returns a tuple with the Settings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSettings

`func (o *InlineObject30) SetSettings(v map[string]map[string]interface{})`

SetSettings sets Settings field to given value.

### HasSettings

`func (o *InlineObject30) HasSettings() bool`

HasSettings returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


