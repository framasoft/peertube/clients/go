# InlineObject9

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | **int32** | Channel id that will contain this live video | 
**SaveReplay** | **bool** |  | [optional] 
**Thumbnailfile** | [***os.File**](*os.File.md) | Live video/replay thumbnail file | [optional] 
**Previewfile** | [***os.File**](*os.File.md) | Live video/replay preview file | [optional] 
**Privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**Category** | **string** | Live video/replay category | [optional] 
**Licence** | **string** | Live video/replay licence | [optional] 
**Language** | **string** | Live video/replay language | [optional] 
**Description** | **string** | Live video/replay description | [optional] 
**Support** | **string** | A text tell the audience how to support the creator | [optional] 
**Nsfw** | **bool** | Whether or not this live video/replay contains sensitive content | [optional] 
**Name** | **string** | Live video/replay name | 
**Tags** | **[]string** | Live video/replay tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
**CommentsEnabled** | **bool** | Enable or disable comments for this live video/replay | [optional] 
**DownloadEnabled** | **bool** | Enable or disable downloading for the replay of this live | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


