# InlineObject27

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RedundancyAllowed** | **bool** | allow mirroring of the host&#39;s local videos | 

## Methods

### NewInlineObject27

`func NewInlineObject27(redundancyAllowed bool, ) *InlineObject27`

NewInlineObject27 instantiates a new InlineObject27 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject27WithDefaults

`func NewInlineObject27WithDefaults() *InlineObject27`

NewInlineObject27WithDefaults instantiates a new InlineObject27 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRedundancyAllowed

`func (o *InlineObject27) GetRedundancyAllowed() bool`

GetRedundancyAllowed returns the RedundancyAllowed field if non-nil, zero value otherwise.

### GetRedundancyAllowedOk

`func (o *InlineObject27) GetRedundancyAllowedOk() (*bool, bool)`

GetRedundancyAllowedOk returns a tuple with the RedundancyAllowed field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRedundancyAllowed

`func (o *InlineObject27) SetRedundancyAllowed(v bool)`

SetRedundancyAllowed sets RedundancyAllowed field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


