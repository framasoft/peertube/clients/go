# VideoStreamingPlaylistsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int32** |  | [optional] 
**Type** | Pointer to **int32** | Playlist type: - &#x60;1&#x60;: HLS  | [optional] 

## Methods

### NewVideoStreamingPlaylistsAllOf

`func NewVideoStreamingPlaylistsAllOf() *VideoStreamingPlaylistsAllOf`

NewVideoStreamingPlaylistsAllOf instantiates a new VideoStreamingPlaylistsAllOf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVideoStreamingPlaylistsAllOfWithDefaults

`func NewVideoStreamingPlaylistsAllOfWithDefaults() *VideoStreamingPlaylistsAllOf`

NewVideoStreamingPlaylistsAllOfWithDefaults instantiates a new VideoStreamingPlaylistsAllOf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VideoStreamingPlaylistsAllOf) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VideoStreamingPlaylistsAllOf) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VideoStreamingPlaylistsAllOf) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *VideoStreamingPlaylistsAllOf) HasId() bool`

HasId returns a boolean if a field has been set.

### GetType

`func (o *VideoStreamingPlaylistsAllOf) GetType() int32`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *VideoStreamingPlaylistsAllOf) GetTypeOk() (*int32, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *VideoStreamingPlaylistsAllOf) SetType(v int32)`

SetType sets Type field to given value.

### HasType

`func (o *VideoStreamingPlaylistsAllOf) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


