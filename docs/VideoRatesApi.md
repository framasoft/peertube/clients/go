# \VideoRatesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**UsersMeVideosVideoIdRatingGet**](VideoRatesApi.md#UsersMeVideosVideoIdRatingGet) | **Get** /users/me/videos/{videoId}/rating | Get rate of my user for a video
[**VideosIdRatePut**](VideoRatesApi.md#VideosIdRatePut) | **Put** /videos/{id}/rate | Like/dislike a video



## UsersMeVideosVideoIdRatingGet

> GetMeVideoRating UsersMeVideosVideoIdRatingGet(ctx, videoId).Execute()

Get rate of my user for a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    videoId := int32(56) // int32 | The video id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoRatesApi.UsersMeVideosVideoIdRatingGet(context.Background(), videoId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoRatesApi.UsersMeVideosVideoIdRatingGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeVideosVideoIdRatingGet`: GetMeVideoRating
    fmt.Fprintf(os.Stdout, "Response from `VideoRatesApi.UsersMeVideosVideoIdRatingGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**videoId** | **int32** | The video id | 

### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeVideosVideoIdRatingGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdRatePut

> VideosIdRatePut(ctx, id).InlineObject24(inlineObject24).Execute()

Like/dislike a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    inlineObject24 := *openapiclient.NewInlineObject24("Rating_example") // InlineObject24 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoRatesApi.VideosIdRatePut(context.Background(), id).InlineObject24(inlineObject24).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoRatesApi.VideosIdRatePut``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideosIdRatePutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **inlineObject24** | [**InlineObject24**](InlineObject24.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

