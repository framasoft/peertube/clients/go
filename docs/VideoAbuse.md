# VideoAbuse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] 
**Reason** | **string** |  | [optional] 
**PredefinedReasons** | **[]string** |  | [optional] 
**ReporterAccount** | [**Account**](Account.md) |  | [optional] 
**State** | [**AbuseStateConstant**](AbuseStateConstant.md) |  | [optional] 
**ModerationComment** | **string** |  | [optional] 
**Video** | [**VideoAbuseVideo**](VideoAbuse_video.md) |  | [optional] 
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


