# \VideoAbusesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VideosAbuseGet**](VideoAbusesApi.md#VideosAbuseGet) | **Get** /videos/abuse | List video abuses
[**VideosIdAbuseAbuseIdDelete**](VideoAbusesApi.md#VideosIdAbuseAbuseIdDelete) | **Delete** /videos/{id}/abuse/{abuseId} | Delete an abuse
[**VideosIdAbuseAbuseIdPut**](VideoAbusesApi.md#VideosIdAbuseAbuseIdPut) | **Put** /videos/{id}/abuse/{abuseId} | Update an abuse
[**VideosIdAbusePost**](VideoAbusesApi.md#VideosIdAbusePost) | **Post** /videos/{id}/abuse | Report an abuse



## VideosAbuseGet

> []VideoAbuse VideosAbuseGet(ctx, optional)

List video abuses

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***VideosAbuseGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosAbuseGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **optional.Int32**| only list the report with this id | 
 **predefinedReason** | **optional.String**| predefined reason the listed reports should contain | 
 **search** | **optional.String**| plain search that will match with video titles, reporter names and more | 
 **state** | **optional.Int32**| The video playlist privacy (Pending &#x3D; &#x60;1&#x60;, Rejected &#x3D; &#x60;2&#x60;, Accepted &#x3D; &#x60;3&#x60;) | 
 **searchReporter** | **optional.String**| only list reports of a specific reporter | 
 **searchReportee** | **optional.String**| only list reports of a specific reportee | 
 **searchVideo** | **optional.String**| only list reports of a specific video | 
 **searchVideoChannel** | **optional.String**| only list reports of a specific video channel | 
 **start** | **optional.Int32**| Offset used to paginate results | 
 **count** | **optional.Int32**| Number of items to return | [default to 15]
 **sort** | **optional.String**| Sort abuses by criteria | 

### Return type

[**[]VideoAbuse**](VideoAbuse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdAbuseAbuseIdDelete

> VideosIdAbuseAbuseIdDelete(ctx, id, abuseId)

Delete an abuse

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUuid**](.md)| The object id or uuid | 
**abuseId** | **int32**| Video abuse id | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdAbuseAbuseIdPut

> VideosIdAbuseAbuseIdPut(ctx, id, abuseId, optional)

Update an abuse

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUuid**](.md)| The object id or uuid | 
**abuseId** | **int32**| Video abuse id | 
 **optional** | ***VideosIdAbuseAbuseIdPutOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosIdAbuseAbuseIdPutOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **inlineObject10** | [**optional.Interface of InlineObject10**](InlineObject10.md)|  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdAbusePost

> VideosIdAbusePost(ctx, id, inlineObject9)

Report an abuse

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUuid**](.md)| The object id or uuid | 
**inlineObject9** | [**InlineObject9**](InlineObject9.md)|  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

