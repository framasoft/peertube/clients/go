# InlineObject20

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StartPosition** | **int32** | Start position of the element to reorder | 
**InsertAfterPosition** | **int32** | New position for the block to reorder, to add the block before the first element | 
**ReorderLength** | Pointer to **int32** | How many element from &#x60;startPosition&#x60; to reorder | [optional] 

## Methods

### NewInlineObject20

`func NewInlineObject20(startPosition int32, insertAfterPosition int32, ) *InlineObject20`

NewInlineObject20 instantiates a new InlineObject20 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject20WithDefaults

`func NewInlineObject20WithDefaults() *InlineObject20`

NewInlineObject20WithDefaults instantiates a new InlineObject20 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStartPosition

`func (o *InlineObject20) GetStartPosition() int32`

GetStartPosition returns the StartPosition field if non-nil, zero value otherwise.

### GetStartPositionOk

`func (o *InlineObject20) GetStartPositionOk() (*int32, bool)`

GetStartPositionOk returns a tuple with the StartPosition field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartPosition

`func (o *InlineObject20) SetStartPosition(v int32)`

SetStartPosition sets StartPosition field to given value.


### GetInsertAfterPosition

`func (o *InlineObject20) GetInsertAfterPosition() int32`

GetInsertAfterPosition returns the InsertAfterPosition field if non-nil, zero value otherwise.

### GetInsertAfterPositionOk

`func (o *InlineObject20) GetInsertAfterPositionOk() (*int32, bool)`

GetInsertAfterPositionOk returns a tuple with the InsertAfterPosition field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInsertAfterPosition

`func (o *InlineObject20) SetInsertAfterPosition(v int32)`

SetInsertAfterPosition sets InsertAfterPosition field to given value.


### GetReorderLength

`func (o *InlineObject20) GetReorderLength() int32`

GetReorderLength returns the ReorderLength field if non-nil, zero value otherwise.

### GetReorderLengthOk

`func (o *InlineObject20) GetReorderLengthOk() (*int32, bool)`

GetReorderLengthOk returns a tuple with the ReorderLength field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReorderLength

`func (o *InlineObject20) SetReorderLength(v int32)`

SetReorderLength sets ReorderLength field to given value.

### HasReorderLength

`func (o *InlineObject20) HasReorderLength() bool`

HasReorderLength returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


