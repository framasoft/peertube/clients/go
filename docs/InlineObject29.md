# InlineObject29

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NpmName** | **string** | name of the plugin/theme in its package.json | 

## Methods

### NewInlineObject29

`func NewInlineObject29(npmName string, ) *InlineObject29`

NewInlineObject29 instantiates a new InlineObject29 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject29WithDefaults

`func NewInlineObject29WithDefaults() *InlineObject29`

NewInlineObject29WithDefaults instantiates a new InlineObject29 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNpmName

`func (o *InlineObject29) GetNpmName() string`

GetNpmName returns the NpmName field if non-nil, zero value otherwise.

### GetNpmNameOk

`func (o *InlineObject29) GetNpmNameOk() (*string, bool)`

GetNpmNameOk returns a tuple with the NpmName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNpmName

`func (o *InlineObject29) SetNpmName(v string)`

SetNpmName sets NpmName field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


