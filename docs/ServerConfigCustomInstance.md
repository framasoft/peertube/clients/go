# ServerConfigCustomInstance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**ShortDescription** | Pointer to **string** |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**Terms** | Pointer to **string** |  | [optional] 
**DefaultClientRoute** | Pointer to **string** |  | [optional] 
**IsNSFW** | Pointer to **bool** |  | [optional] 
**DefaultNSFWPolicy** | Pointer to **string** |  | [optional] 
**Customizations** | Pointer to [**ServerConfigInstanceCustomizations**](ServerConfigInstanceCustomizations.md) |  | [optional] 

## Methods

### NewServerConfigCustomInstance

`func NewServerConfigCustomInstance() *ServerConfigCustomInstance`

NewServerConfigCustomInstance instantiates a new ServerConfigCustomInstance object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewServerConfigCustomInstanceWithDefaults

`func NewServerConfigCustomInstanceWithDefaults() *ServerConfigCustomInstance`

NewServerConfigCustomInstanceWithDefaults instantiates a new ServerConfigCustomInstance object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ServerConfigCustomInstance) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ServerConfigCustomInstance) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ServerConfigCustomInstance) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ServerConfigCustomInstance) HasName() bool`

HasName returns a boolean if a field has been set.

### GetShortDescription

`func (o *ServerConfigCustomInstance) GetShortDescription() string`

GetShortDescription returns the ShortDescription field if non-nil, zero value otherwise.

### GetShortDescriptionOk

`func (o *ServerConfigCustomInstance) GetShortDescriptionOk() (*string, bool)`

GetShortDescriptionOk returns a tuple with the ShortDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetShortDescription

`func (o *ServerConfigCustomInstance) SetShortDescription(v string)`

SetShortDescription sets ShortDescription field to given value.

### HasShortDescription

`func (o *ServerConfigCustomInstance) HasShortDescription() bool`

HasShortDescription returns a boolean if a field has been set.

### GetDescription

`func (o *ServerConfigCustomInstance) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ServerConfigCustomInstance) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ServerConfigCustomInstance) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ServerConfigCustomInstance) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetTerms

`func (o *ServerConfigCustomInstance) GetTerms() string`

GetTerms returns the Terms field if non-nil, zero value otherwise.

### GetTermsOk

`func (o *ServerConfigCustomInstance) GetTermsOk() (*string, bool)`

GetTermsOk returns a tuple with the Terms field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerms

`func (o *ServerConfigCustomInstance) SetTerms(v string)`

SetTerms sets Terms field to given value.

### HasTerms

`func (o *ServerConfigCustomInstance) HasTerms() bool`

HasTerms returns a boolean if a field has been set.

### GetDefaultClientRoute

`func (o *ServerConfigCustomInstance) GetDefaultClientRoute() string`

GetDefaultClientRoute returns the DefaultClientRoute field if non-nil, zero value otherwise.

### GetDefaultClientRouteOk

`func (o *ServerConfigCustomInstance) GetDefaultClientRouteOk() (*string, bool)`

GetDefaultClientRouteOk returns a tuple with the DefaultClientRoute field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultClientRoute

`func (o *ServerConfigCustomInstance) SetDefaultClientRoute(v string)`

SetDefaultClientRoute sets DefaultClientRoute field to given value.

### HasDefaultClientRoute

`func (o *ServerConfigCustomInstance) HasDefaultClientRoute() bool`

HasDefaultClientRoute returns a boolean if a field has been set.

### GetIsNSFW

`func (o *ServerConfigCustomInstance) GetIsNSFW() bool`

GetIsNSFW returns the IsNSFW field if non-nil, zero value otherwise.

### GetIsNSFWOk

`func (o *ServerConfigCustomInstance) GetIsNSFWOk() (*bool, bool)`

GetIsNSFWOk returns a tuple with the IsNSFW field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsNSFW

`func (o *ServerConfigCustomInstance) SetIsNSFW(v bool)`

SetIsNSFW sets IsNSFW field to given value.

### HasIsNSFW

`func (o *ServerConfigCustomInstance) HasIsNSFW() bool`

HasIsNSFW returns a boolean if a field has been set.

### GetDefaultNSFWPolicy

`func (o *ServerConfigCustomInstance) GetDefaultNSFWPolicy() string`

GetDefaultNSFWPolicy returns the DefaultNSFWPolicy field if non-nil, zero value otherwise.

### GetDefaultNSFWPolicyOk

`func (o *ServerConfigCustomInstance) GetDefaultNSFWPolicyOk() (*string, bool)`

GetDefaultNSFWPolicyOk returns a tuple with the DefaultNSFWPolicy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultNSFWPolicy

`func (o *ServerConfigCustomInstance) SetDefaultNSFWPolicy(v string)`

SetDefaultNSFWPolicy sets DefaultNSFWPolicy field to given value.

### HasDefaultNSFWPolicy

`func (o *ServerConfigCustomInstance) HasDefaultNSFWPolicy() bool`

HasDefaultNSFWPolicy returns a boolean if a field has been set.

### GetCustomizations

`func (o *ServerConfigCustomInstance) GetCustomizations() ServerConfigInstanceCustomizations`

GetCustomizations returns the Customizations field if non-nil, zero value otherwise.

### GetCustomizationsOk

`func (o *ServerConfigCustomInstance) GetCustomizationsOk() (*ServerConfigInstanceCustomizations, bool)`

GetCustomizationsOk returns a tuple with the Customizations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomizations

`func (o *ServerConfigCustomInstance) SetCustomizations(v ServerConfigInstanceCustomizations)`

SetCustomizations sets Customizations field to given value.

### HasCustomizations

`func (o *ServerConfigCustomInstance) HasCustomizations() bool`

HasCustomizations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


