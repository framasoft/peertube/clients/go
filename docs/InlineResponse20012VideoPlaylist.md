# InlineResponse20012VideoPlaylist

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int32** |  | [optional] 
**Uuid** | Pointer to [**Uuid**](Uuid.md) |  | [optional] 
**ShortUUID** | Pointer to **string** | translation of a uuid v4 with a bigger alphabet to have a shorter uuid | [optional] 

## Methods

### NewInlineResponse20012VideoPlaylist

`func NewInlineResponse20012VideoPlaylist() *InlineResponse20012VideoPlaylist`

NewInlineResponse20012VideoPlaylist instantiates a new InlineResponse20012VideoPlaylist object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse20012VideoPlaylistWithDefaults

`func NewInlineResponse20012VideoPlaylistWithDefaults() *InlineResponse20012VideoPlaylist`

NewInlineResponse20012VideoPlaylistWithDefaults instantiates a new InlineResponse20012VideoPlaylist object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *InlineResponse20012VideoPlaylist) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *InlineResponse20012VideoPlaylist) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *InlineResponse20012VideoPlaylist) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *InlineResponse20012VideoPlaylist) HasId() bool`

HasId returns a boolean if a field has been set.

### GetUuid

`func (o *InlineResponse20012VideoPlaylist) GetUuid() Uuid`

GetUuid returns the Uuid field if non-nil, zero value otherwise.

### GetUuidOk

`func (o *InlineResponse20012VideoPlaylist) GetUuidOk() (*Uuid, bool)`

GetUuidOk returns a tuple with the Uuid field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUuid

`func (o *InlineResponse20012VideoPlaylist) SetUuid(v Uuid)`

SetUuid sets Uuid field to given value.

### HasUuid

`func (o *InlineResponse20012VideoPlaylist) HasUuid() bool`

HasUuid returns a boolean if a field has been set.

### GetShortUUID

`func (o *InlineResponse20012VideoPlaylist) GetShortUUID() string`

GetShortUUID returns the ShortUUID field if non-nil, zero value otherwise.

### GetShortUUIDOk

`func (o *InlineResponse20012VideoPlaylist) GetShortUUIDOk() (*string, bool)`

GetShortUUIDOk returns a tuple with the ShortUUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetShortUUID

`func (o *InlineResponse20012VideoPlaylist) SetShortUUID(v string)`

SetShortUUID sets ShortUUID field to given value.

### HasShortUUID

`func (o *InlineResponse20012VideoPlaylist) HasShortUUID() bool`

HasShortUUID returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


