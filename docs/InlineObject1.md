# InlineObject1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hosts** | Pointer to **[]string** |  | [optional] 
**Handles** | Pointer to **[]string** |  | [optional] 

## Methods

### NewInlineObject1

`func NewInlineObject1() *InlineObject1`

NewInlineObject1 instantiates a new InlineObject1 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject1WithDefaults

`func NewInlineObject1WithDefaults() *InlineObject1`

NewInlineObject1WithDefaults instantiates a new InlineObject1 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHosts

`func (o *InlineObject1) GetHosts() []string`

GetHosts returns the Hosts field if non-nil, zero value otherwise.

### GetHostsOk

`func (o *InlineObject1) GetHostsOk() (*[]string, bool)`

GetHostsOk returns a tuple with the Hosts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHosts

`func (o *InlineObject1) SetHosts(v []string)`

SetHosts sets Hosts field to given value.

### HasHosts

`func (o *InlineObject1) HasHosts() bool`

HasHosts returns a boolean if a field has been set.

### GetHandles

`func (o *InlineObject1) GetHandles() []string`

GetHandles returns the Handles field if non-nil, zero value otherwise.

### GetHandlesOk

`func (o *InlineObject1) GetHandlesOk() (*[]string, bool)`

GetHandlesOk returns a tuple with the Handles field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHandles

`func (o *InlineObject1) SetHandles(v []string)`

SetHandles sets Handles field to given value.

### HasHandles

`func (o *InlineObject1) HasHandles() bool`

HasHandles returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


