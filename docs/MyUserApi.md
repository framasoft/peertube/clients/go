# \MyUserApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetMyAbuses**](MyUserApi.md#GetMyAbuses) | **Get** /users/me/abuses | List my abuses
[**GetUserInfo**](MyUserApi.md#GetUserInfo) | **Get** /users/me | Get my user information
[**PutUserInfo**](MyUserApi.md#PutUserInfo) | **Put** /users/me | Update my user information
[**UsersMeAvatarDelete**](MyUserApi.md#UsersMeAvatarDelete) | **Delete** /users/me/avatar | Delete my avatar
[**UsersMeAvatarPickPost**](MyUserApi.md#UsersMeAvatarPickPost) | **Post** /users/me/avatar/pick | Update my user avatar
[**UsersMeVideoQuotaUsedGet**](MyUserApi.md#UsersMeVideoQuotaUsedGet) | **Get** /users/me/video-quota-used | Get my user used quota
[**UsersMeVideosGet**](MyUserApi.md#UsersMeVideosGet) | **Get** /users/me/videos | Get videos of my user
[**UsersMeVideosImportsGet**](MyUserApi.md#UsersMeVideosImportsGet) | **Get** /users/me/videos/imports | Get video imports of my user
[**UsersMeVideosVideoIdRatingGet**](MyUserApi.md#UsersMeVideosVideoIdRatingGet) | **Get** /users/me/videos/{videoId}/rating | Get rate of my user for a video



## GetMyAbuses

> InlineResponse2005 GetMyAbuses(ctx).Id(id).State(state).Sort(sort).Start(start).Count(count).Execute()

List my abuses

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int32(56) // int32 | only list the report with this id (optional)
    state := openapiclient.AbuseStateSet(1) // AbuseStateSet |  (optional)
    sort := "sort_example" // string | Sort abuses by criteria (optional)
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyUserApi.GetMyAbuses(context.Background()).Id(id).State(state).Sort(sort).Start(start).Count(count).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyUserApi.GetMyAbuses``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMyAbuses`: InlineResponse2005
    fmt.Fprintf(os.Stdout, "Response from `MyUserApi.GetMyAbuses`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetMyAbusesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32** | only list the report with this id | 
 **state** | [**AbuseStateSet**](AbuseStateSet.md) |  | 
 **sort** | **string** | Sort abuses by criteria | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetUserInfo

> []User GetUserInfo(ctx).Execute()

Get my user information

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyUserApi.GetUserInfo(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyUserApi.GetUserInfo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetUserInfo`: []User
    fmt.Fprintf(os.Stdout, "Response from `MyUserApi.GetUserInfo`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetUserInfoRequest struct via the builder pattern


### Return type

[**[]User**](User.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutUserInfo

> PutUserInfo(ctx).UpdateMe(updateMe).Execute()

Update my user information

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    updateMe := *openapiclient.NewUpdateMe() // UpdateMe | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyUserApi.PutUserInfo(context.Background()).UpdateMe(updateMe).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyUserApi.PutUserInfo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiPutUserInfoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateMe** | [**UpdateMe**](UpdateMe.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeAvatarDelete

> UsersMeAvatarDelete(ctx).Execute()

Delete my avatar

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyUserApi.UsersMeAvatarDelete(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyUserApi.UsersMeAvatarDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeAvatarDeleteRequest struct via the builder pattern


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeAvatarPickPost

> InlineResponse2004 UsersMeAvatarPickPost(ctx).Avatarfile(avatarfile).Execute()

Update my user avatar

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    avatarfile := os.NewFile(1234, "some_file") // *os.File | The file to upload (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyUserApi.UsersMeAvatarPickPost(context.Background()).Avatarfile(avatarfile).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyUserApi.UsersMeAvatarPickPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeAvatarPickPost`: InlineResponse2004
    fmt.Fprintf(os.Stdout, "Response from `MyUserApi.UsersMeAvatarPickPost`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeAvatarPickPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **avatarfile** | ***os.File** | The file to upload | 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideoQuotaUsedGet

> InlineResponse2003 UsersMeVideoQuotaUsedGet(ctx).Execute()

Get my user used quota

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyUserApi.UsersMeVideoQuotaUsedGet(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyUserApi.UsersMeVideoQuotaUsedGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeVideoQuotaUsedGet`: InlineResponse2003
    fmt.Fprintf(os.Stdout, "Response from `MyUserApi.UsersMeVideoQuotaUsedGet`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeVideoQuotaUsedGetRequest struct via the builder pattern


### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideosGet

> VideoListResponse UsersMeVideosGet(ctx).Start(start).Count(count).Sort(sort).Execute()

Get videos of my user

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyUserApi.UsersMeVideosGet(context.Background()).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyUserApi.UsersMeVideosGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeVideosGet`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `MyUserApi.UsersMeVideosGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeVideosGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideosImportsGet

> VideoImportsList UsersMeVideosImportsGet(ctx).Start(start).Count(count).Sort(sort).Execute()

Get video imports of my user

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyUserApi.UsersMeVideosImportsGet(context.Background()).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyUserApi.UsersMeVideosImportsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeVideosImportsGet`: VideoImportsList
    fmt.Fprintf(os.Stdout, "Response from `MyUserApi.UsersMeVideosImportsGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeVideosImportsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**VideoImportsList**](VideoImportsList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideosVideoIdRatingGet

> GetMeVideoRating UsersMeVideosVideoIdRatingGet(ctx, videoId).Execute()

Get rate of my user for a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    videoId := int32(56) // int32 | The video id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyUserApi.UsersMeVideosVideoIdRatingGet(context.Background(), videoId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyUserApi.UsersMeVideosVideoIdRatingGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeVideosVideoIdRatingGet`: GetMeVideoRating
    fmt.Fprintf(os.Stdout, "Response from `MyUserApi.UsersMeVideosVideoIdRatingGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**videoId** | **int32** | The video id | 

### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeVideosVideoIdRatingGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

