# InlineResponse20013VideoPlaylistElement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int32** |  | [optional] 

## Methods

### NewInlineResponse20013VideoPlaylistElement

`func NewInlineResponse20013VideoPlaylistElement() *InlineResponse20013VideoPlaylistElement`

NewInlineResponse20013VideoPlaylistElement instantiates a new InlineResponse20013VideoPlaylistElement object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse20013VideoPlaylistElementWithDefaults

`func NewInlineResponse20013VideoPlaylistElementWithDefaults() *InlineResponse20013VideoPlaylistElement`

NewInlineResponse20013VideoPlaylistElementWithDefaults instantiates a new InlineResponse20013VideoPlaylistElement object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *InlineResponse20013VideoPlaylistElement) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *InlineResponse20013VideoPlaylistElement) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *InlineResponse20013VideoPlaylistElement) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *InlineResponse20013VideoPlaylistElement) HasId() bool`

HasId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


