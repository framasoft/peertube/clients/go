# \VideoCaptionApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VideosIdCaptionsCaptionLanguageDelete**](VideoCaptionApi.md#VideosIdCaptionsCaptionLanguageDelete) | **Delete** /videos/{id}/captions/{captionLanguage} | Delete a video caption
[**VideosIdCaptionsCaptionLanguagePut**](VideoCaptionApi.md#VideosIdCaptionsCaptionLanguagePut) | **Put** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
[**VideosIdCaptionsGet**](VideoCaptionApi.md#VideosIdCaptionsGet) | **Get** /videos/{id}/captions | List captions of a video



## VideosIdCaptionsCaptionLanguageDelete

> VideosIdCaptionsCaptionLanguageDelete(ctx, id, captionLanguage)

Delete a video caption

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The object id or uuid | 
**captionLanguage** | **string**| The caption language | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCaptionsCaptionLanguagePut

> VideosIdCaptionsCaptionLanguagePut(ctx, id, captionLanguage, optional)

Add or replace a video caption

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The object id or uuid | 
**captionLanguage** | **string**| The caption language | 
 **optional** | ***VideosIdCaptionsCaptionLanguagePutOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosIdCaptionsCaptionLanguagePutOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **captionfile** | **optional.Interface of *os.File****optional.*os.File**| The file to upload. | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCaptionsGet

> InlineResponse200 VideosIdCaptionsGet(ctx, id)

List captions of a video

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The object id or uuid | 

### Return type

[**InlineResponse200**](inline_response_200.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

