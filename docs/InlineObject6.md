# InlineObject6

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Thumbnailfile** | [***os.File**](*os.File.md) | Video thumbnail file | [optional] 
**Previewfile** | [***os.File**](*os.File.md) | Video preview file | [optional] 
**Category** | **int32** | Video category | [optional] 
**Licence** | **int32** | Video licence | [optional] 
**Language** | **string** | Video language | [optional] 
**Privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**Description** | **string** | Video description | [optional] 
**WaitTranscoding** | **string** | Whether or not we wait transcoding before publish the video | [optional] 
**Support** | **string** | A text tell the audience how to support the video creator | [optional] 
**Nsfw** | **bool** | Whether or not this video contains sensitive content | [optional] 
**Name** | **string** | Video name | [optional] 
**Tags** | **[]string** | Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
**CommentsEnabled** | **bool** | Enable or disable comments for this video | [optional] 
**OriginallyPublishedAt** | [**time.Time**](time.Time.md) | Date when the content was originally published | [optional] 
**ScheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


