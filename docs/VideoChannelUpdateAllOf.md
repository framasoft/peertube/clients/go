# VideoChannelUpdateAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BulkVideosSupportUpdate** | Pointer to **bool** | Update the support field for all videos of this channel | [optional] 

## Methods

### NewVideoChannelUpdateAllOf

`func NewVideoChannelUpdateAllOf() *VideoChannelUpdateAllOf`

NewVideoChannelUpdateAllOf instantiates a new VideoChannelUpdateAllOf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVideoChannelUpdateAllOfWithDefaults

`func NewVideoChannelUpdateAllOfWithDefaults() *VideoChannelUpdateAllOf`

NewVideoChannelUpdateAllOfWithDefaults instantiates a new VideoChannelUpdateAllOf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetBulkVideosSupportUpdate

`func (o *VideoChannelUpdateAllOf) GetBulkVideosSupportUpdate() bool`

GetBulkVideosSupportUpdate returns the BulkVideosSupportUpdate field if non-nil, zero value otherwise.

### GetBulkVideosSupportUpdateOk

`func (o *VideoChannelUpdateAllOf) GetBulkVideosSupportUpdateOk() (*bool, bool)`

GetBulkVideosSupportUpdateOk returns a tuple with the BulkVideosSupportUpdate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBulkVideosSupportUpdate

`func (o *VideoChannelUpdateAllOf) SetBulkVideosSupportUpdate(v bool)`

SetBulkVideosSupportUpdate sets BulkVideosSupportUpdate field to given value.

### HasBulkVideosSupportUpdate

`func (o *VideoChannelUpdateAllOf) HasBulkVideosSupportUpdate() bool`

HasBulkVideosSupportUpdate returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


