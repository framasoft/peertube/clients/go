# \VideoCommentsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VideosIdCommentThreadsGet**](VideoCommentsApi.md#VideosIdCommentThreadsGet) | **Get** /videos/{id}/comment-threads | List threads of a video
[**VideosIdCommentThreadsPost**](VideoCommentsApi.md#VideosIdCommentThreadsPost) | **Post** /videos/{id}/comment-threads | Create a thread
[**VideosIdCommentThreadsThreadIdGet**](VideoCommentsApi.md#VideosIdCommentThreadsThreadIdGet) | **Get** /videos/{id}/comment-threads/{threadId} | Get a thread
[**VideosIdCommentsCommentIdDelete**](VideoCommentsApi.md#VideosIdCommentsCommentIdDelete) | **Delete** /videos/{id}/comments/{commentId} | Delete a comment or a reply
[**VideosIdCommentsCommentIdPost**](VideoCommentsApi.md#VideosIdCommentsCommentIdPost) | **Post** /videos/{id}/comments/{commentId} | Reply to a thread of a video



## VideosIdCommentThreadsGet

> CommentThreadResponse VideosIdCommentThreadsGet(ctx, id).Start(start).Count(count).Sort(sort).Execute()

List threads of a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "sort_example" // string | Sort comments by criteria (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoCommentsApi.VideosIdCommentThreadsGet(context.Background(), id).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoCommentsApi.VideosIdCommentThreadsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VideosIdCommentThreadsGet`: CommentThreadResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoCommentsApi.VideosIdCommentThreadsGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideosIdCommentThreadsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort comments by criteria | 

### Return type

[**CommentThreadResponse**](CommentThreadResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCommentThreadsPost

> CommentThreadPostResponse VideosIdCommentThreadsPost(ctx, id).InlineObject22(inlineObject22).Execute()

Create a thread

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    inlineObject22 := *openapiclient.NewInlineObject22("TODO") // InlineObject22 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoCommentsApi.VideosIdCommentThreadsPost(context.Background(), id).InlineObject22(inlineObject22).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoCommentsApi.VideosIdCommentThreadsPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VideosIdCommentThreadsPost`: CommentThreadPostResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoCommentsApi.VideosIdCommentThreadsPost`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideosIdCommentThreadsPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **inlineObject22** | [**InlineObject22**](InlineObject22.md) |  | 

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCommentThreadsThreadIdGet

> VideoCommentThreadTree VideosIdCommentThreadsThreadIdGet(ctx, id, threadId).Execute()

Get a thread

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    threadId := int32(56) // int32 | The thread id (root comment id)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoCommentsApi.VideosIdCommentThreadsThreadIdGet(context.Background(), id, threadId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoCommentsApi.VideosIdCommentThreadsThreadIdGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VideosIdCommentThreadsThreadIdGet`: VideoCommentThreadTree
    fmt.Fprintf(os.Stdout, "Response from `VideoCommentsApi.VideosIdCommentThreadsThreadIdGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 
**threadId** | **int32** | The thread id (root comment id) | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideosIdCommentThreadsThreadIdGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**VideoCommentThreadTree**](VideoCommentThreadTree.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCommentsCommentIdDelete

> VideosIdCommentsCommentIdDelete(ctx, id, commentId).Execute()

Delete a comment or a reply

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    commentId := int32(56) // int32 | The comment id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoCommentsApi.VideosIdCommentsCommentIdDelete(context.Background(), id, commentId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoCommentsApi.VideosIdCommentsCommentIdDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 
**commentId** | **int32** | The comment id | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideosIdCommentsCommentIdDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCommentsCommentIdPost

> CommentThreadPostResponse VideosIdCommentsCommentIdPost(ctx, id, commentId).InlineObject23(inlineObject23).Execute()

Reply to a thread of a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    commentId := int32(56) // int32 | The comment id
    inlineObject23 := *openapiclient.NewInlineObject23("TODO") // InlineObject23 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoCommentsApi.VideosIdCommentsCommentIdPost(context.Background(), id, commentId).InlineObject23(inlineObject23).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoCommentsApi.VideosIdCommentsCommentIdPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VideosIdCommentsCommentIdPost`: CommentThreadPostResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoCommentsApi.VideosIdCommentsCommentIdPost`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 
**commentId** | **int32** | The comment id | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideosIdCommentsCommentIdPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **inlineObject23** | [**InlineObject23**](InlineObject23.md) |  | 

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

