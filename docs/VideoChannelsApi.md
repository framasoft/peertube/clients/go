# \VideoChannelsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AccountsNameVideoChannelsGet**](VideoChannelsApi.md#AccountsNameVideoChannelsGet) | **Get** /accounts/{name}/video-channels | List video channels of an account
[**AddVideoChannel**](VideoChannelsApi.md#AddVideoChannel) | **Post** /video-channels | Create a video channel
[**DelVideoChannel**](VideoChannelsApi.md#DelVideoChannel) | **Delete** /video-channels/{channelHandle} | Delete a video channel
[**GetVideoChannel**](VideoChannelsApi.md#GetVideoChannel) | **Get** /video-channels/{channelHandle} | Get a video channel
[**GetVideoChannelVideos**](VideoChannelsApi.md#GetVideoChannelVideos) | **Get** /video-channels/{channelHandle}/videos | List videos of a video channel
[**GetVideoChannels**](VideoChannelsApi.md#GetVideoChannels) | **Get** /video-channels | List video channels
[**PutVideoChannel**](VideoChannelsApi.md#PutVideoChannel) | **Put** /video-channels/{channelHandle} | Update a video channel
[**VideoChannelsChannelHandleAvatarDelete**](VideoChannelsApi.md#VideoChannelsChannelHandleAvatarDelete) | **Delete** /video-channels/{channelHandle}/avatar | Delete channel avatar
[**VideoChannelsChannelHandleAvatarPickPost**](VideoChannelsApi.md#VideoChannelsChannelHandleAvatarPickPost) | **Post** /video-channels/{channelHandle}/avatar/pick | Update channel avatar
[**VideoChannelsChannelHandleBannerDelete**](VideoChannelsApi.md#VideoChannelsChannelHandleBannerDelete) | **Delete** /video-channels/{channelHandle}/banner | Delete channel banner
[**VideoChannelsChannelHandleBannerPickPost**](VideoChannelsApi.md#VideoChannelsChannelHandleBannerPickPost) | **Post** /video-channels/{channelHandle}/banner/pick | Update channel banner



## AccountsNameVideoChannelsGet

> VideoChannelList AccountsNameVideoChannelsGet(ctx, name).WithStats(withStats).Start(start).Count(count).Sort(sort).Execute()

List video channels of an account

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    name := "chocobozzz | chocobozzz@example.org" // string | The username or handle of the account
    withStats := true // bool | include view statistics for the last 30 days (only if authentified as the account user) (optional)
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.AccountsNameVideoChannelsGet(context.Background(), name).WithStats(withStats).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.AccountsNameVideoChannelsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AccountsNameVideoChannelsGet`: VideoChannelList
    fmt.Fprintf(os.Stdout, "Response from `VideoChannelsApi.AccountsNameVideoChannelsGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**name** | **string** | The username or handle of the account | 

### Other Parameters

Other parameters are passed through a pointer to a apiAccountsNameVideoChannelsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **withStats** | **bool** | include view statistics for the last 30 days (only if authentified as the account user) | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**VideoChannelList**](VideoChannelList.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddVideoChannel

> InlineResponse204 AddVideoChannel(ctx).VideoChannelCreate(videoChannelCreate).Execute()

Create a video channel

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    videoChannelCreate := *openapiclient.NewVideoChannelCreate("Videos of Framasoft", string(123)) // VideoChannelCreate |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.AddVideoChannel(context.Background()).VideoChannelCreate(videoChannelCreate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.AddVideoChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddVideoChannel`: InlineResponse204
    fmt.Fprintf(os.Stdout, "Response from `VideoChannelsApi.AddVideoChannel`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAddVideoChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoChannelCreate** | [**VideoChannelCreate**](VideoChannelCreate.md) |  | 

### Return type

[**InlineResponse204**](InlineResponse204.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DelVideoChannel

> DelVideoChannel(ctx, channelHandle).Execute()

Delete a video channel

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelHandle := "my_username | my_username@example.com" // string | The video channel handle

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.DelVideoChannel(context.Background(), channelHandle).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.DelVideoChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string** | The video channel handle | 

### Other Parameters

Other parameters are passed through a pointer to a apiDelVideoChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideoChannel

> VideoChannel GetVideoChannel(ctx, channelHandle).Execute()

Get a video channel

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelHandle := "my_username | my_username@example.com" // string | The video channel handle

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.GetVideoChannel(context.Background(), channelHandle).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.GetVideoChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideoChannel`: VideoChannel
    fmt.Fprintf(os.Stdout, "Response from `VideoChannelsApi.GetVideoChannel`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string** | The video channel handle | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideoChannelVideos

> VideoListResponse GetVideoChannelVideos(ctx, channelHandle).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()

List videos of a video channel

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelHandle := "my_username | my_username@example.com" // string | The video channel handle
    categoryOneOf := TODO // OneOfintegerarray | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    isLive := true // bool | whether or not the video is a live (optional)
    tagsOneOf := TODO // OneOfstringarray | tag(s) of the video (optional)
    tagsAllOf := TODO // OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
    licenceOneOf := TODO // OneOfintegerarray | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    languageOneOf := TODO // OneOfstringarray | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language (optional)
    nsfw := "nsfw_example" // string | whether to include nsfw videos, if any (optional)
    filter := "filter_example" // string | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    skipCount := "skipCount_example" // string | if you don't need the `total` in the response (optional) (default to "false")
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "sort_example" // string | Sort videos by criteria (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.GetVideoChannelVideos(context.Background(), channelHandle).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.GetVideoChannelVideos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideoChannelVideos`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoChannelsApi.GetVideoChannelVideos`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string** | The video channel handle | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoChannelVideosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **categoryOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **isLive** | **bool** | whether or not the video is a live | 
 **tagsOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video | 
 **tagsAllOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video, where all should be present in the video | 
 **licenceOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **languageOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | 
 **nsfw** | **string** | whether to include nsfw videos, if any | 
 **filter** | **string** | Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | 
 **skipCount** | **string** | if you don&#39;t need the &#x60;total&#x60; in the response | [default to &quot;false&quot;]
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort videos by criteria | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideoChannels

> VideoChannelList GetVideoChannels(ctx).Start(start).Count(count).Sort(sort).Execute()

List video channels

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.GetVideoChannels(context.Background()).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.GetVideoChannels``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideoChannels`: VideoChannelList
    fmt.Fprintf(os.Stdout, "Response from `VideoChannelsApi.GetVideoChannels`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoChannelsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**VideoChannelList**](VideoChannelList.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutVideoChannel

> PutVideoChannel(ctx, channelHandle).VideoChannelUpdate(videoChannelUpdate).Execute()

Update a video channel

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelHandle := "my_username | my_username@example.com" // string | The video channel handle
    videoChannelUpdate := *openapiclient.NewVideoChannelUpdate() // VideoChannelUpdate |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.PutVideoChannel(context.Background(), channelHandle).VideoChannelUpdate(videoChannelUpdate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.PutVideoChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string** | The video channel handle | 

### Other Parameters

Other parameters are passed through a pointer to a apiPutVideoChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **videoChannelUpdate** | [**VideoChannelUpdate**](VideoChannelUpdate.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsChannelHandleAvatarDelete

> VideoChannelsChannelHandleAvatarDelete(ctx, channelHandle).Execute()

Delete channel avatar

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelHandle := "my_username | my_username@example.com" // string | The video channel handle

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.VideoChannelsChannelHandleAvatarDelete(context.Background(), channelHandle).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.VideoChannelsChannelHandleAvatarDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string** | The video channel handle | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideoChannelsChannelHandleAvatarDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsChannelHandleAvatarPickPost

> InlineResponse2004 VideoChannelsChannelHandleAvatarPickPost(ctx, channelHandle).Avatarfile(avatarfile).Execute()

Update channel avatar

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelHandle := "my_username | my_username@example.com" // string | The video channel handle
    avatarfile := os.NewFile(1234, "some_file") // *os.File | The file to upload. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.VideoChannelsChannelHandleAvatarPickPost(context.Background(), channelHandle).Avatarfile(avatarfile).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.VideoChannelsChannelHandleAvatarPickPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VideoChannelsChannelHandleAvatarPickPost`: InlineResponse2004
    fmt.Fprintf(os.Stdout, "Response from `VideoChannelsApi.VideoChannelsChannelHandleAvatarPickPost`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string** | The video channel handle | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideoChannelsChannelHandleAvatarPickPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **avatarfile** | ***os.File** | The file to upload. | 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsChannelHandleBannerDelete

> VideoChannelsChannelHandleBannerDelete(ctx, channelHandle).Execute()

Delete channel banner

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelHandle := "my_username | my_username@example.com" // string | The video channel handle

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.VideoChannelsChannelHandleBannerDelete(context.Background(), channelHandle).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.VideoChannelsChannelHandleBannerDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string** | The video channel handle | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideoChannelsChannelHandleBannerDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsChannelHandleBannerPickPost

> InlineResponse20010 VideoChannelsChannelHandleBannerPickPost(ctx, channelHandle).Bannerfile(bannerfile).Execute()

Update channel banner

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelHandle := "my_username | my_username@example.com" // string | The video channel handle
    bannerfile := os.NewFile(1234, "some_file") // *os.File | The file to upload. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoChannelsApi.VideoChannelsChannelHandleBannerPickPost(context.Background(), channelHandle).Bannerfile(bannerfile).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoChannelsApi.VideoChannelsChannelHandleBannerPickPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VideoChannelsChannelHandleBannerPickPost`: InlineResponse20010
    fmt.Fprintf(os.Stdout, "Response from `VideoChannelsApi.VideoChannelsChannelHandleBannerPickPost`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string** | The video channel handle | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideoChannelsChannelHandleBannerPickPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **bannerfile** | ***os.File** | The file to upload. | 

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

