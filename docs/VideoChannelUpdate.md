# VideoChannelUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | Pointer to **string** | editable name of the channel, displayed in its representations | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**Support** | Pointer to **string** | text shown by default on all videos of this channel, to tell the audience how to support it | [optional] 
**Id** | Pointer to **int32** |  | [optional] [readonly] 
**IsLocal** | Pointer to **bool** |  | [optional] [readonly] 
**UpdatedAt** | Pointer to **time.Time** |  | [optional] [readonly] 
**OwnerAccount** | Pointer to [**NullableVideoChannelOwnerAccount**](VideoChannelOwnerAccount.md) |  | [optional] 
**BulkVideosSupportUpdate** | Pointer to **bool** | Update the support field for all videos of this channel | [optional] 

## Methods

### NewVideoChannelUpdate

`func NewVideoChannelUpdate() *VideoChannelUpdate`

NewVideoChannelUpdate instantiates a new VideoChannelUpdate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVideoChannelUpdateWithDefaults

`func NewVideoChannelUpdateWithDefaults() *VideoChannelUpdate`

NewVideoChannelUpdateWithDefaults instantiates a new VideoChannelUpdate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDisplayName

`func (o *VideoChannelUpdate) GetDisplayName() string`

GetDisplayName returns the DisplayName field if non-nil, zero value otherwise.

### GetDisplayNameOk

`func (o *VideoChannelUpdate) GetDisplayNameOk() (*string, bool)`

GetDisplayNameOk returns a tuple with the DisplayName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayName

`func (o *VideoChannelUpdate) SetDisplayName(v string)`

SetDisplayName sets DisplayName field to given value.

### HasDisplayName

`func (o *VideoChannelUpdate) HasDisplayName() bool`

HasDisplayName returns a boolean if a field has been set.

### GetDescription

`func (o *VideoChannelUpdate) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *VideoChannelUpdate) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *VideoChannelUpdate) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *VideoChannelUpdate) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetSupport

`func (o *VideoChannelUpdate) GetSupport() string`

GetSupport returns the Support field if non-nil, zero value otherwise.

### GetSupportOk

`func (o *VideoChannelUpdate) GetSupportOk() (*string, bool)`

GetSupportOk returns a tuple with the Support field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSupport

`func (o *VideoChannelUpdate) SetSupport(v string)`

SetSupport sets Support field to given value.

### HasSupport

`func (o *VideoChannelUpdate) HasSupport() bool`

HasSupport returns a boolean if a field has been set.

### GetId

`func (o *VideoChannelUpdate) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VideoChannelUpdate) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VideoChannelUpdate) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *VideoChannelUpdate) HasId() bool`

HasId returns a boolean if a field has been set.

### GetIsLocal

`func (o *VideoChannelUpdate) GetIsLocal() bool`

GetIsLocal returns the IsLocal field if non-nil, zero value otherwise.

### GetIsLocalOk

`func (o *VideoChannelUpdate) GetIsLocalOk() (*bool, bool)`

GetIsLocalOk returns a tuple with the IsLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsLocal

`func (o *VideoChannelUpdate) SetIsLocal(v bool)`

SetIsLocal sets IsLocal field to given value.

### HasIsLocal

`func (o *VideoChannelUpdate) HasIsLocal() bool`

HasIsLocal returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *VideoChannelUpdate) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *VideoChannelUpdate) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *VideoChannelUpdate) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *VideoChannelUpdate) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.

### GetOwnerAccount

`func (o *VideoChannelUpdate) GetOwnerAccount() VideoChannelOwnerAccount`

GetOwnerAccount returns the OwnerAccount field if non-nil, zero value otherwise.

### GetOwnerAccountOk

`func (o *VideoChannelUpdate) GetOwnerAccountOk() (*VideoChannelOwnerAccount, bool)`

GetOwnerAccountOk returns a tuple with the OwnerAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwnerAccount

`func (o *VideoChannelUpdate) SetOwnerAccount(v VideoChannelOwnerAccount)`

SetOwnerAccount sets OwnerAccount field to given value.

### HasOwnerAccount

`func (o *VideoChannelUpdate) HasOwnerAccount() bool`

HasOwnerAccount returns a boolean if a field has been set.

### SetOwnerAccountNil

`func (o *VideoChannelUpdate) SetOwnerAccountNil(b bool)`

 SetOwnerAccountNil sets the value for OwnerAccount to be an explicit nil

### UnsetOwnerAccount
`func (o *VideoChannelUpdate) UnsetOwnerAccount()`

UnsetOwnerAccount ensures that no value is present for OwnerAccount, not even an explicit nil
### GetBulkVideosSupportUpdate

`func (o *VideoChannelUpdate) GetBulkVideosSupportUpdate() bool`

GetBulkVideosSupportUpdate returns the BulkVideosSupportUpdate field if non-nil, zero value otherwise.

### GetBulkVideosSupportUpdateOk

`func (o *VideoChannelUpdate) GetBulkVideosSupportUpdateOk() (*bool, bool)`

GetBulkVideosSupportUpdateOk returns a tuple with the BulkVideosSupportUpdate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBulkVideosSupportUpdate

`func (o *VideoChannelUpdate) SetBulkVideosSupportUpdate(v bool)`

SetBulkVideosSupportUpdate sets BulkVideosSupportUpdate field to given value.

### HasBulkVideosSupportUpdate

`func (o *VideoChannelUpdate) HasBulkVideosSupportUpdate() bool`

HasBulkVideosSupportUpdate returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


