# LiveVideoResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RtmpUrl** | Pointer to **string** |  | [optional] 
**StreamKey** | Pointer to **string** | RTMP stream key to use to stream into this live video | [optional] 
**SaveReplay** | Pointer to **bool** |  | [optional] 
**PermanentLive** | Pointer to **bool** | User can stream multiple times in a permanent live | [optional] 

## Methods

### NewLiveVideoResponse

`func NewLiveVideoResponse() *LiveVideoResponse`

NewLiveVideoResponse instantiates a new LiveVideoResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLiveVideoResponseWithDefaults

`func NewLiveVideoResponseWithDefaults() *LiveVideoResponse`

NewLiveVideoResponseWithDefaults instantiates a new LiveVideoResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRtmpUrl

`func (o *LiveVideoResponse) GetRtmpUrl() string`

GetRtmpUrl returns the RtmpUrl field if non-nil, zero value otherwise.

### GetRtmpUrlOk

`func (o *LiveVideoResponse) GetRtmpUrlOk() (*string, bool)`

GetRtmpUrlOk returns a tuple with the RtmpUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRtmpUrl

`func (o *LiveVideoResponse) SetRtmpUrl(v string)`

SetRtmpUrl sets RtmpUrl field to given value.

### HasRtmpUrl

`func (o *LiveVideoResponse) HasRtmpUrl() bool`

HasRtmpUrl returns a boolean if a field has been set.

### GetStreamKey

`func (o *LiveVideoResponse) GetStreamKey() string`

GetStreamKey returns the StreamKey field if non-nil, zero value otherwise.

### GetStreamKeyOk

`func (o *LiveVideoResponse) GetStreamKeyOk() (*string, bool)`

GetStreamKeyOk returns a tuple with the StreamKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreamKey

`func (o *LiveVideoResponse) SetStreamKey(v string)`

SetStreamKey sets StreamKey field to given value.

### HasStreamKey

`func (o *LiveVideoResponse) HasStreamKey() bool`

HasStreamKey returns a boolean if a field has been set.

### GetSaveReplay

`func (o *LiveVideoResponse) GetSaveReplay() bool`

GetSaveReplay returns the SaveReplay field if non-nil, zero value otherwise.

### GetSaveReplayOk

`func (o *LiveVideoResponse) GetSaveReplayOk() (*bool, bool)`

GetSaveReplayOk returns a tuple with the SaveReplay field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSaveReplay

`func (o *LiveVideoResponse) SetSaveReplay(v bool)`

SetSaveReplay sets SaveReplay field to given value.

### HasSaveReplay

`func (o *LiveVideoResponse) HasSaveReplay() bool`

HasSaveReplay returns a boolean if a field has been set.

### GetPermanentLive

`func (o *LiveVideoResponse) GetPermanentLive() bool`

GetPermanentLive returns the PermanentLive field if non-nil, zero value otherwise.

### GetPermanentLiveOk

`func (o *LiveVideoResponse) GetPermanentLiveOk() (*bool, bool)`

GetPermanentLiveOk returns a tuple with the PermanentLive field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermanentLive

`func (o *LiveVideoResponse) SetPermanentLive(v bool)`

SetPermanentLive sets PermanentLive field to given value.

### HasPermanentLive

`func (o *LiveVideoResponse) HasPermanentLive() bool`

HasPermanentLive returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


