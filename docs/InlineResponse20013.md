# InlineResponse20013

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VideoPlaylistElement** | Pointer to [**InlineResponse20013VideoPlaylistElement**](InlineResponse20013VideoPlaylistElement.md) |  | [optional] 

## Methods

### NewInlineResponse20013

`func NewInlineResponse20013() *InlineResponse20013`

NewInlineResponse20013 instantiates a new InlineResponse20013 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse20013WithDefaults

`func NewInlineResponse20013WithDefaults() *InlineResponse20013`

NewInlineResponse20013WithDefaults instantiates a new InlineResponse20013 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVideoPlaylistElement

`func (o *InlineResponse20013) GetVideoPlaylistElement() InlineResponse20013VideoPlaylistElement`

GetVideoPlaylistElement returns the VideoPlaylistElement field if non-nil, zero value otherwise.

### GetVideoPlaylistElementOk

`func (o *InlineResponse20013) GetVideoPlaylistElementOk() (*InlineResponse20013VideoPlaylistElement, bool)`

GetVideoPlaylistElementOk returns a tuple with the VideoPlaylistElement field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoPlaylistElement

`func (o *InlineResponse20013) SetVideoPlaylistElement(v InlineResponse20013VideoPlaylistElement)`

SetVideoPlaylistElement sets VideoPlaylistElement field to given value.

### HasVideoPlaylistElement

`func (o *InlineResponse20013) HasVideoPlaylistElement() bool`

HasVideoPlaylistElement returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


