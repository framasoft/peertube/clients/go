# InlineObject23

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | [**Text**](text.md) |  | 

## Methods

### NewInlineObject23

`func NewInlineObject23(text Text, ) *InlineObject23`

NewInlineObject23 instantiates a new InlineObject23 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject23WithDefaults

`func NewInlineObject23WithDefaults() *InlineObject23`

NewInlineObject23WithDefaults instantiates a new InlineObject23 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetText

`func (o *InlineObject23) GetText() Text`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *InlineObject23) GetTextOk() (*Text, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *InlineObject23) SetText(v Text)`

SetText sets Text field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


