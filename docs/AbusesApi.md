# \AbusesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AbusesAbuseIdDelete**](AbusesApi.md#AbusesAbuseIdDelete) | **Delete** /abuses/{abuseId} | Delete an abuse
[**AbusesAbuseIdMessagesAbuseMessageIdDelete**](AbusesApi.md#AbusesAbuseIdMessagesAbuseMessageIdDelete) | **Delete** /abuses/{abuseId}/messages/{abuseMessageId} | Delete an abuse message
[**AbusesAbuseIdMessagesGet**](AbusesApi.md#AbusesAbuseIdMessagesGet) | **Get** /abuses/{abuseId}/messages | List messages of an abuse
[**AbusesAbuseIdMessagesPost**](AbusesApi.md#AbusesAbuseIdMessagesPost) | **Post** /abuses/{abuseId}/messages | Add message to an abuse
[**AbusesAbuseIdPut**](AbusesApi.md#AbusesAbuseIdPut) | **Put** /abuses/{abuseId} | Update an abuse
[**AbusesPost**](AbusesApi.md#AbusesPost) | **Post** /abuses | Report an abuse
[**GetAbuses**](AbusesApi.md#GetAbuses) | **Get** /abuses | List abuses
[**GetMyAbuses**](AbusesApi.md#GetMyAbuses) | **Get** /users/me/abuses | List my abuses



## AbusesAbuseIdDelete

> AbusesAbuseIdDelete(ctx, abuseId).Execute()

Delete an abuse

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    abuseId := int32(56) // int32 | Abuse id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AbusesApi.AbusesAbuseIdDelete(context.Background(), abuseId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AbusesApi.AbusesAbuseIdDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**abuseId** | **int32** | Abuse id | 

### Other Parameters

Other parameters are passed through a pointer to a apiAbusesAbuseIdDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AbusesAbuseIdMessagesAbuseMessageIdDelete

> AbusesAbuseIdMessagesAbuseMessageIdDelete(ctx, abuseId, abuseMessageId).Execute()

Delete an abuse message

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    abuseId := int32(56) // int32 | Abuse id
    abuseMessageId := int32(56) // int32 | Abuse message id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AbusesApi.AbusesAbuseIdMessagesAbuseMessageIdDelete(context.Background(), abuseId, abuseMessageId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AbusesApi.AbusesAbuseIdMessagesAbuseMessageIdDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**abuseId** | **int32** | Abuse id | 
**abuseMessageId** | **int32** | Abuse message id | 

### Other Parameters

Other parameters are passed through a pointer to a apiAbusesAbuseIdMessagesAbuseMessageIdDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AbusesAbuseIdMessagesGet

> InlineResponse2007 AbusesAbuseIdMessagesGet(ctx, abuseId).Execute()

List messages of an abuse

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    abuseId := int32(56) // int32 | Abuse id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AbusesApi.AbusesAbuseIdMessagesGet(context.Background(), abuseId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AbusesApi.AbusesAbuseIdMessagesGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AbusesAbuseIdMessagesGet`: InlineResponse2007
    fmt.Fprintf(os.Stdout, "Response from `AbusesApi.AbusesAbuseIdMessagesGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**abuseId** | **int32** | Abuse id | 

### Other Parameters

Other parameters are passed through a pointer to a apiAbusesAbuseIdMessagesGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AbusesAbuseIdMessagesPost

> AbusesAbuseIdMessagesPost(ctx, abuseId).InlineObject13(inlineObject13).Execute()

Add message to an abuse

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    abuseId := int32(56) // int32 | Abuse id
    inlineObject13 := *openapiclient.NewInlineObject13("Message_example") // InlineObject13 | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AbusesApi.AbusesAbuseIdMessagesPost(context.Background(), abuseId).InlineObject13(inlineObject13).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AbusesApi.AbusesAbuseIdMessagesPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**abuseId** | **int32** | Abuse id | 

### Other Parameters

Other parameters are passed through a pointer to a apiAbusesAbuseIdMessagesPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **inlineObject13** | [**InlineObject13**](InlineObject13.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AbusesAbuseIdPut

> AbusesAbuseIdPut(ctx, abuseId).InlineObject12(inlineObject12).Execute()

Update an abuse

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    abuseId := int32(56) // int32 | Abuse id
    inlineObject12 := *openapiclient.NewInlineObject12() // InlineObject12 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AbusesApi.AbusesAbuseIdPut(context.Background(), abuseId).InlineObject12(inlineObject12).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AbusesApi.AbusesAbuseIdPut``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**abuseId** | **int32** | Abuse id | 

### Other Parameters

Other parameters are passed through a pointer to a apiAbusesAbuseIdPutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **inlineObject12** | [**InlineObject12**](InlineObject12.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AbusesPost

> InlineResponse2006 AbusesPost(ctx).InlineObject11(inlineObject11).Execute()

Report an abuse

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    inlineObject11 := *openapiclient.NewInlineObject11("Reason_example") // InlineObject11 | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AbusesApi.AbusesPost(context.Background()).InlineObject11(inlineObject11).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AbusesApi.AbusesPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AbusesPost`: InlineResponse2006
    fmt.Fprintf(os.Stdout, "Response from `AbusesApi.AbusesPost`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAbusesPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject11** | [**InlineObject11**](InlineObject11.md) |  | 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAbuses

> InlineResponse2005 GetAbuses(ctx).Id(id).PredefinedReason(predefinedReason).Search(search).State(state).SearchReporter(searchReporter).SearchReportee(searchReportee).SearchVideo(searchVideo).SearchVideoChannel(searchVideoChannel).VideoIs(videoIs).Filter(filter).Start(start).Count(count).Sort(sort).Execute()

List abuses

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int32(56) // int32 | only list the report with this id (optional)
    predefinedReason := []string{"PredefinedReason_example"} // []string | predefined reason the listed reports should contain (optional)
    search := "search_example" // string | plain search that will match with video titles, reporter names and more (optional)
    state := openapiclient.AbuseStateSet(1) // AbuseStateSet |  (optional)
    searchReporter := "searchReporter_example" // string | only list reports of a specific reporter (optional)
    searchReportee := "searchReportee_example" // string | only list reports of a specific reportee (optional)
    searchVideo := "searchVideo_example" // string | only list reports of a specific video (optional)
    searchVideoChannel := "searchVideoChannel_example" // string | only list reports of a specific video channel (optional)
    videoIs := "videoIs_example" // string | only list deleted or blocklisted videos (optional)
    filter := "filter_example" // string | only list account, comment or video reports (optional)
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "sort_example" // string | Sort abuses by criteria (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AbusesApi.GetAbuses(context.Background()).Id(id).PredefinedReason(predefinedReason).Search(search).State(state).SearchReporter(searchReporter).SearchReportee(searchReportee).SearchVideo(searchVideo).SearchVideoChannel(searchVideoChannel).VideoIs(videoIs).Filter(filter).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AbusesApi.GetAbuses``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAbuses`: InlineResponse2005
    fmt.Fprintf(os.Stdout, "Response from `AbusesApi.GetAbuses`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetAbusesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32** | only list the report with this id | 
 **predefinedReason** | **[]string** | predefined reason the listed reports should contain | 
 **search** | **string** | plain search that will match with video titles, reporter names and more | 
 **state** | [**AbuseStateSet**](AbuseStateSet.md) |  | 
 **searchReporter** | **string** | only list reports of a specific reporter | 
 **searchReportee** | **string** | only list reports of a specific reportee | 
 **searchVideo** | **string** | only list reports of a specific video | 
 **searchVideoChannel** | **string** | only list reports of a specific video channel | 
 **videoIs** | **string** | only list deleted or blocklisted videos | 
 **filter** | **string** | only list account, comment or video reports | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort abuses by criteria | 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMyAbuses

> InlineResponse2005 GetMyAbuses(ctx).Id(id).State(state).Sort(sort).Start(start).Count(count).Execute()

List my abuses

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int32(56) // int32 | only list the report with this id (optional)
    state := openapiclient.AbuseStateSet(1) // AbuseStateSet |  (optional)
    sort := "sort_example" // string | Sort abuses by criteria (optional)
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AbusesApi.GetMyAbuses(context.Background()).Id(id).State(state).Sort(sort).Start(start).Count(count).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AbusesApi.GetMyAbuses``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMyAbuses`: InlineResponse2005
    fmt.Fprintf(os.Stdout, "Response from `AbusesApi.GetMyAbuses`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetMyAbusesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32** | only list the report with this id | 
 **state** | [**AbuseStateSet**](AbuseStateSet.md) |  | 
 **sort** | **string** | Sort abuses by criteria | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

