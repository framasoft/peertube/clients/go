# InlineObject10

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Reason** | **string** | Reason why the user reports this video | 
**PredefinedReasons** | **[]string** | Reason categories that help triage reports | [optional] 
**Video** | [**AbusesVideo**](_abuses_video.md) |  | [optional] 
**Comment** | [**AbusesComment**](_abuses_comment.md) |  | [optional] 
**Account** | [**AbusesAccount**](_abuses_account.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


