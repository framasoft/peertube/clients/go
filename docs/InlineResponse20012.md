# InlineResponse20012

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VideoPlaylist** | Pointer to [**InlineResponse20012VideoPlaylist**](InlineResponse20012VideoPlaylist.md) |  | [optional] 

## Methods

### NewInlineResponse20012

`func NewInlineResponse20012() *InlineResponse20012`

NewInlineResponse20012 instantiates a new InlineResponse20012 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse20012WithDefaults

`func NewInlineResponse20012WithDefaults() *InlineResponse20012`

NewInlineResponse20012WithDefaults instantiates a new InlineResponse20012 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVideoPlaylist

`func (o *InlineResponse20012) GetVideoPlaylist() InlineResponse20012VideoPlaylist`

GetVideoPlaylist returns the VideoPlaylist field if non-nil, zero value otherwise.

### GetVideoPlaylistOk

`func (o *InlineResponse20012) GetVideoPlaylistOk() (*InlineResponse20012VideoPlaylist, bool)`

GetVideoPlaylistOk returns a tuple with the VideoPlaylist field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoPlaylist

`func (o *InlineResponse20012) SetVideoPlaylist(v InlineResponse20012VideoPlaylist)`

SetVideoPlaylist sets VideoPlaylist field to given value.

### HasVideoPlaylist

`func (o *InlineResponse20012) HasVideoPlaylist() bool`

HasVideoPlaylist returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


