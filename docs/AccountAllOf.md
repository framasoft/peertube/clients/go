# AccountAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UserId** | Pointer to **int32** | object id for the user tied to this account | [optional] 
**DisplayName** | Pointer to **string** | editable name of the account, displayed in its representations | [optional] 
**Description** | Pointer to **string** | text or bio displayed on the account&#39;s profile | [optional] 

## Methods

### NewAccountAllOf

`func NewAccountAllOf() *AccountAllOf`

NewAccountAllOf instantiates a new AccountAllOf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAccountAllOfWithDefaults

`func NewAccountAllOfWithDefaults() *AccountAllOf`

NewAccountAllOfWithDefaults instantiates a new AccountAllOf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUserId

`func (o *AccountAllOf) GetUserId() int32`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *AccountAllOf) GetUserIdOk() (*int32, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *AccountAllOf) SetUserId(v int32)`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *AccountAllOf) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### GetDisplayName

`func (o *AccountAllOf) GetDisplayName() string`

GetDisplayName returns the DisplayName field if non-nil, zero value otherwise.

### GetDisplayNameOk

`func (o *AccountAllOf) GetDisplayNameOk() (*string, bool)`

GetDisplayNameOk returns a tuple with the DisplayName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayName

`func (o *AccountAllOf) SetDisplayName(v string)`

SetDisplayName sets DisplayName field to given value.

### HasDisplayName

`func (o *AccountAllOf) HasDisplayName() bool`

HasDisplayName returns a boolean if a field has been set.

### GetDescription

`func (o *AccountAllOf) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *AccountAllOf) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *AccountAllOf) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *AccountAllOf) HasDescription() bool`

HasDescription returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


