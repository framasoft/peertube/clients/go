# InlineObject5

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NewVideoFromSubscription** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**NewCommentOnMyVideo** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**AbuseAsModerator** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**VideoAutoBlacklistAsModerator** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**BlacklistOnMyVideo** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**MyVideoPublished** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**MyVideoImportFinished** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**NewFollow** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**NewUserRegistration** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**CommentMention** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**NewInstanceFollower** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**AutoInstanceFollowing** | Pointer to [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 

## Methods

### NewInlineObject5

`func NewInlineObject5() *InlineObject5`

NewInlineObject5 instantiates a new InlineObject5 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject5WithDefaults

`func NewInlineObject5WithDefaults() *InlineObject5`

NewInlineObject5WithDefaults instantiates a new InlineObject5 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNewVideoFromSubscription

`func (o *InlineObject5) GetNewVideoFromSubscription() NotificationSettingValue`

GetNewVideoFromSubscription returns the NewVideoFromSubscription field if non-nil, zero value otherwise.

### GetNewVideoFromSubscriptionOk

`func (o *InlineObject5) GetNewVideoFromSubscriptionOk() (*NotificationSettingValue, bool)`

GetNewVideoFromSubscriptionOk returns a tuple with the NewVideoFromSubscription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewVideoFromSubscription

`func (o *InlineObject5) SetNewVideoFromSubscription(v NotificationSettingValue)`

SetNewVideoFromSubscription sets NewVideoFromSubscription field to given value.

### HasNewVideoFromSubscription

`func (o *InlineObject5) HasNewVideoFromSubscription() bool`

HasNewVideoFromSubscription returns a boolean if a field has been set.

### GetNewCommentOnMyVideo

`func (o *InlineObject5) GetNewCommentOnMyVideo() NotificationSettingValue`

GetNewCommentOnMyVideo returns the NewCommentOnMyVideo field if non-nil, zero value otherwise.

### GetNewCommentOnMyVideoOk

`func (o *InlineObject5) GetNewCommentOnMyVideoOk() (*NotificationSettingValue, bool)`

GetNewCommentOnMyVideoOk returns a tuple with the NewCommentOnMyVideo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewCommentOnMyVideo

`func (o *InlineObject5) SetNewCommentOnMyVideo(v NotificationSettingValue)`

SetNewCommentOnMyVideo sets NewCommentOnMyVideo field to given value.

### HasNewCommentOnMyVideo

`func (o *InlineObject5) HasNewCommentOnMyVideo() bool`

HasNewCommentOnMyVideo returns a boolean if a field has been set.

### GetAbuseAsModerator

`func (o *InlineObject5) GetAbuseAsModerator() NotificationSettingValue`

GetAbuseAsModerator returns the AbuseAsModerator field if non-nil, zero value otherwise.

### GetAbuseAsModeratorOk

`func (o *InlineObject5) GetAbuseAsModeratorOk() (*NotificationSettingValue, bool)`

GetAbuseAsModeratorOk returns a tuple with the AbuseAsModerator field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbuseAsModerator

`func (o *InlineObject5) SetAbuseAsModerator(v NotificationSettingValue)`

SetAbuseAsModerator sets AbuseAsModerator field to given value.

### HasAbuseAsModerator

`func (o *InlineObject5) HasAbuseAsModerator() bool`

HasAbuseAsModerator returns a boolean if a field has been set.

### GetVideoAutoBlacklistAsModerator

`func (o *InlineObject5) GetVideoAutoBlacklistAsModerator() NotificationSettingValue`

GetVideoAutoBlacklistAsModerator returns the VideoAutoBlacklistAsModerator field if non-nil, zero value otherwise.

### GetVideoAutoBlacklistAsModeratorOk

`func (o *InlineObject5) GetVideoAutoBlacklistAsModeratorOk() (*NotificationSettingValue, bool)`

GetVideoAutoBlacklistAsModeratorOk returns a tuple with the VideoAutoBlacklistAsModerator field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoAutoBlacklistAsModerator

`func (o *InlineObject5) SetVideoAutoBlacklistAsModerator(v NotificationSettingValue)`

SetVideoAutoBlacklistAsModerator sets VideoAutoBlacklistAsModerator field to given value.

### HasVideoAutoBlacklistAsModerator

`func (o *InlineObject5) HasVideoAutoBlacklistAsModerator() bool`

HasVideoAutoBlacklistAsModerator returns a boolean if a field has been set.

### GetBlacklistOnMyVideo

`func (o *InlineObject5) GetBlacklistOnMyVideo() NotificationSettingValue`

GetBlacklistOnMyVideo returns the BlacklistOnMyVideo field if non-nil, zero value otherwise.

### GetBlacklistOnMyVideoOk

`func (o *InlineObject5) GetBlacklistOnMyVideoOk() (*NotificationSettingValue, bool)`

GetBlacklistOnMyVideoOk returns a tuple with the BlacklistOnMyVideo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlacklistOnMyVideo

`func (o *InlineObject5) SetBlacklistOnMyVideo(v NotificationSettingValue)`

SetBlacklistOnMyVideo sets BlacklistOnMyVideo field to given value.

### HasBlacklistOnMyVideo

`func (o *InlineObject5) HasBlacklistOnMyVideo() bool`

HasBlacklistOnMyVideo returns a boolean if a field has been set.

### GetMyVideoPublished

`func (o *InlineObject5) GetMyVideoPublished() NotificationSettingValue`

GetMyVideoPublished returns the MyVideoPublished field if non-nil, zero value otherwise.

### GetMyVideoPublishedOk

`func (o *InlineObject5) GetMyVideoPublishedOk() (*NotificationSettingValue, bool)`

GetMyVideoPublishedOk returns a tuple with the MyVideoPublished field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMyVideoPublished

`func (o *InlineObject5) SetMyVideoPublished(v NotificationSettingValue)`

SetMyVideoPublished sets MyVideoPublished field to given value.

### HasMyVideoPublished

`func (o *InlineObject5) HasMyVideoPublished() bool`

HasMyVideoPublished returns a boolean if a field has been set.

### GetMyVideoImportFinished

`func (o *InlineObject5) GetMyVideoImportFinished() NotificationSettingValue`

GetMyVideoImportFinished returns the MyVideoImportFinished field if non-nil, zero value otherwise.

### GetMyVideoImportFinishedOk

`func (o *InlineObject5) GetMyVideoImportFinishedOk() (*NotificationSettingValue, bool)`

GetMyVideoImportFinishedOk returns a tuple with the MyVideoImportFinished field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMyVideoImportFinished

`func (o *InlineObject5) SetMyVideoImportFinished(v NotificationSettingValue)`

SetMyVideoImportFinished sets MyVideoImportFinished field to given value.

### HasMyVideoImportFinished

`func (o *InlineObject5) HasMyVideoImportFinished() bool`

HasMyVideoImportFinished returns a boolean if a field has been set.

### GetNewFollow

`func (o *InlineObject5) GetNewFollow() NotificationSettingValue`

GetNewFollow returns the NewFollow field if non-nil, zero value otherwise.

### GetNewFollowOk

`func (o *InlineObject5) GetNewFollowOk() (*NotificationSettingValue, bool)`

GetNewFollowOk returns a tuple with the NewFollow field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewFollow

`func (o *InlineObject5) SetNewFollow(v NotificationSettingValue)`

SetNewFollow sets NewFollow field to given value.

### HasNewFollow

`func (o *InlineObject5) HasNewFollow() bool`

HasNewFollow returns a boolean if a field has been set.

### GetNewUserRegistration

`func (o *InlineObject5) GetNewUserRegistration() NotificationSettingValue`

GetNewUserRegistration returns the NewUserRegistration field if non-nil, zero value otherwise.

### GetNewUserRegistrationOk

`func (o *InlineObject5) GetNewUserRegistrationOk() (*NotificationSettingValue, bool)`

GetNewUserRegistrationOk returns a tuple with the NewUserRegistration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewUserRegistration

`func (o *InlineObject5) SetNewUserRegistration(v NotificationSettingValue)`

SetNewUserRegistration sets NewUserRegistration field to given value.

### HasNewUserRegistration

`func (o *InlineObject5) HasNewUserRegistration() bool`

HasNewUserRegistration returns a boolean if a field has been set.

### GetCommentMention

`func (o *InlineObject5) GetCommentMention() NotificationSettingValue`

GetCommentMention returns the CommentMention field if non-nil, zero value otherwise.

### GetCommentMentionOk

`func (o *InlineObject5) GetCommentMentionOk() (*NotificationSettingValue, bool)`

GetCommentMentionOk returns a tuple with the CommentMention field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCommentMention

`func (o *InlineObject5) SetCommentMention(v NotificationSettingValue)`

SetCommentMention sets CommentMention field to given value.

### HasCommentMention

`func (o *InlineObject5) HasCommentMention() bool`

HasCommentMention returns a boolean if a field has been set.

### GetNewInstanceFollower

`func (o *InlineObject5) GetNewInstanceFollower() NotificationSettingValue`

GetNewInstanceFollower returns the NewInstanceFollower field if non-nil, zero value otherwise.

### GetNewInstanceFollowerOk

`func (o *InlineObject5) GetNewInstanceFollowerOk() (*NotificationSettingValue, bool)`

GetNewInstanceFollowerOk returns a tuple with the NewInstanceFollower field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewInstanceFollower

`func (o *InlineObject5) SetNewInstanceFollower(v NotificationSettingValue)`

SetNewInstanceFollower sets NewInstanceFollower field to given value.

### HasNewInstanceFollower

`func (o *InlineObject5) HasNewInstanceFollower() bool`

HasNewInstanceFollower returns a boolean if a field has been set.

### GetAutoInstanceFollowing

`func (o *InlineObject5) GetAutoInstanceFollowing() NotificationSettingValue`

GetAutoInstanceFollowing returns the AutoInstanceFollowing field if non-nil, zero value otherwise.

### GetAutoInstanceFollowingOk

`func (o *InlineObject5) GetAutoInstanceFollowingOk() (*NotificationSettingValue, bool)`

GetAutoInstanceFollowingOk returns a tuple with the AutoInstanceFollowing field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAutoInstanceFollowing

`func (o *InlineObject5) SetAutoInstanceFollowing(v NotificationSettingValue)`

SetAutoInstanceFollowing sets AutoInstanceFollowing field to given value.

### HasAutoInstanceFollowing

`func (o *InlineObject5) HasAutoInstanceFollowing() bool`

HasAutoInstanceFollowing returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


