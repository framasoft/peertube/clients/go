# InlineObject19

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VideoId** | [**OneOfuuidinteger**](oneOf&lt;uuid,integer&gt;.md) | Video to add in the playlist | 
**StartTimestamp** | Pointer to **int32** | Start the video at this specific timestamp | [optional] 
**StopTimestamp** | Pointer to **int32** | Stop the video at this specific timestamp | [optional] 

## Methods

### NewInlineObject19

`func NewInlineObject19(videoId OneOfuuidinteger, ) *InlineObject19`

NewInlineObject19 instantiates a new InlineObject19 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject19WithDefaults

`func NewInlineObject19WithDefaults() *InlineObject19`

NewInlineObject19WithDefaults instantiates a new InlineObject19 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVideoId

`func (o *InlineObject19) GetVideoId() OneOfuuidinteger`

GetVideoId returns the VideoId field if non-nil, zero value otherwise.

### GetVideoIdOk

`func (o *InlineObject19) GetVideoIdOk() (*OneOfuuidinteger, bool)`

GetVideoIdOk returns a tuple with the VideoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoId

`func (o *InlineObject19) SetVideoId(v OneOfuuidinteger)`

SetVideoId sets VideoId field to given value.


### GetStartTimestamp

`func (o *InlineObject19) GetStartTimestamp() int32`

GetStartTimestamp returns the StartTimestamp field if non-nil, zero value otherwise.

### GetStartTimestampOk

`func (o *InlineObject19) GetStartTimestampOk() (*int32, bool)`

GetStartTimestampOk returns a tuple with the StartTimestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartTimestamp

`func (o *InlineObject19) SetStartTimestamp(v int32)`

SetStartTimestamp sets StartTimestamp field to given value.

### HasStartTimestamp

`func (o *InlineObject19) HasStartTimestamp() bool`

HasStartTimestamp returns a boolean if a field has been set.

### GetStopTimestamp

`func (o *InlineObject19) GetStopTimestamp() int32`

GetStopTimestamp returns the StopTimestamp field if non-nil, zero value otherwise.

### GetStopTimestampOk

`func (o *InlineObject19) GetStopTimestampOk() (*int32, bool)`

GetStopTimestampOk returns a tuple with the StopTimestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStopTimestamp

`func (o *InlineObject19) SetStopTimestamp(v int32)`

SetStopTimestamp sets StopTimestamp field to given value.

### HasStopTimestamp

`func (o *InlineObject19) HasStopTimestamp() bool`

HasStopTimestamp returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


