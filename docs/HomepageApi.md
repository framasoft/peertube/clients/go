# \HomepageApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CustomPagesHomepageInstanceGet**](HomepageApi.md#CustomPagesHomepageInstanceGet) | **Get** /custom-pages/homepage/instance | Get instance custom homepage
[**CustomPagesHomepageInstancePut**](HomepageApi.md#CustomPagesHomepageInstancePut) | **Put** /custom-pages/homepage/instance | Set instance custom homepage



## CustomPagesHomepageInstanceGet

> CustomHomepage CustomPagesHomepageInstanceGet(ctx).Execute()

Get instance custom homepage

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.HomepageApi.CustomPagesHomepageInstanceGet(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `HomepageApi.CustomPagesHomepageInstanceGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CustomPagesHomepageInstanceGet`: CustomHomepage
    fmt.Fprintf(os.Stdout, "Response from `HomepageApi.CustomPagesHomepageInstanceGet`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiCustomPagesHomepageInstanceGetRequest struct via the builder pattern


### Return type

[**CustomHomepage**](CustomHomepage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CustomPagesHomepageInstancePut

> CustomPagesHomepageInstancePut(ctx).InlineObject(inlineObject).Execute()

Set instance custom homepage

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    inlineObject := *openapiclient.NewInlineObject() // InlineObject |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.HomepageApi.CustomPagesHomepageInstancePut(context.Background()).InlineObject(inlineObject).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `HomepageApi.CustomPagesHomepageInstancePut``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCustomPagesHomepageInstancePutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject** | [**InlineObject**](InlineObject.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

