# \MyHistoryApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**UsersMeHistoryVideosGet**](MyHistoryApi.md#UsersMeHistoryVideosGet) | **Get** /users/me/history/videos | List watched videos history
[**UsersMeHistoryVideosRemovePost**](MyHistoryApi.md#UsersMeHistoryVideosRemovePost) | **Post** /users/me/history/videos/remove | Clear video history



## UsersMeHistoryVideosGet

> VideoListResponse UsersMeHistoryVideosGet(ctx).Start(start).Count(count).Search(search).Execute()

List watched videos history

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    search := "search_example" // string | Plain text search, applied to various parts of the model depending on endpoint (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyHistoryApi.UsersMeHistoryVideosGet(context.Background()).Start(start).Count(count).Search(search).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyHistoryApi.UsersMeHistoryVideosGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeHistoryVideosGet`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `MyHistoryApi.UsersMeHistoryVideosGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeHistoryVideosGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **search** | **string** | Plain text search, applied to various parts of the model depending on endpoint | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeHistoryVideosRemovePost

> UsersMeHistoryVideosRemovePost(ctx).BeforeDate(beforeDate).Execute()

Clear video history

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "./openapi"
)

func main() {
    beforeDate := time.Now() // time.Time | history before this date will be deleted (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MyHistoryApi.UsersMeHistoryVideosRemovePost(context.Background()).BeforeDate(beforeDate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MyHistoryApi.UsersMeHistoryVideosRemovePost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeHistoryVideosRemovePostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **beforeDate** | **time.Time** | history before this date will be deleted | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

