# InlineObject26

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Host** | **string** | server domain to block | 

## Methods

### NewInlineObject26

`func NewInlineObject26(host string, ) *InlineObject26`

NewInlineObject26 instantiates a new InlineObject26 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject26WithDefaults

`func NewInlineObject26WithDefaults() *InlineObject26`

NewInlineObject26WithDefaults instantiates a new InlineObject26 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHost

`func (o *InlineObject26) GetHost() string`

GetHost returns the Host field if non-nil, zero value otherwise.

### GetHostOk

`func (o *InlineObject26) GetHostOk() (*string, bool)`

GetHostOk returns a tuple with the Host field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHost

`func (o *InlineObject26) SetHost(v string)`

SetHost sets Host field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


