# \VideoMirroringApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DelMirroredVideo**](VideoMirroringApi.md#DelMirroredVideo) | **Delete** /server/redundancy/videos/{redundancyId} | Delete a mirror done on a video
[**GetMirroredVideos**](VideoMirroringApi.md#GetMirroredVideos) | **Get** /server/redundancy/videos | List videos being mirrored
[**PutMirroredVideo**](VideoMirroringApi.md#PutMirroredVideo) | **Post** /server/redundancy/videos | Mirror a video



## DelMirroredVideo

> DelMirroredVideo(ctx, redundancyId).Execute()

Delete a mirror done on a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    redundancyId := "redundancyId_example" // string | id of an existing redundancy on a video

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoMirroringApi.DelMirroredVideo(context.Background(), redundancyId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoMirroringApi.DelMirroredVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**redundancyId** | **string** | id of an existing redundancy on a video | 

### Other Parameters

Other parameters are passed through a pointer to a apiDelMirroredVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMirroredVideos

> []VideoRedundancy GetMirroredVideos(ctx).Target(target).Start(start).Count(count).Sort(sort).Execute()

List videos being mirrored

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    target := "target_example" // string | direction of the mirror
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "sort_example" // string | Sort abuses by criteria (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoMirroringApi.GetMirroredVideos(context.Background()).Target(target).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoMirroringApi.GetMirroredVideos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMirroredVideos`: []VideoRedundancy
    fmt.Fprintf(os.Stdout, "Response from `VideoMirroringApi.GetMirroredVideos`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetMirroredVideosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **target** | **string** | direction of the mirror | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort abuses by criteria | 

### Return type

[**[]VideoRedundancy**](VideoRedundancy.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutMirroredVideo

> PutMirroredVideo(ctx).InlineObject28(inlineObject28).Execute()

Mirror a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    inlineObject28 := *openapiclient.NewInlineObject28(int32(42)) // InlineObject28 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoMirroringApi.PutMirroredVideo(context.Background()).InlineObject28(inlineObject28).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoMirroringApi.PutMirroredVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiPutMirroredVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject28** | [**InlineObject28**](InlineObject28.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

