# \VideoCaptionsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddVideoCaption**](VideoCaptionsApi.md#AddVideoCaption) | **Put** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
[**DelVideoCaption**](VideoCaptionsApi.md#DelVideoCaption) | **Delete** /videos/{id}/captions/{captionLanguage} | Delete a video caption
[**GetVideoCaptions**](VideoCaptionsApi.md#GetVideoCaptions) | **Get** /videos/{id}/captions | List captions of a video



## AddVideoCaption

> AddVideoCaption(ctx, id, captionLanguage).Captionfile(captionfile).Execute()

Add or replace a video caption

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    captionLanguage := "captionLanguage_example" // string | The caption language
    captionfile := os.NewFile(1234, "some_file") // *os.File | The file to upload. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoCaptionsApi.AddVideoCaption(context.Background(), id, captionLanguage).Captionfile(captionfile).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoCaptionsApi.AddVideoCaption``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 
**captionLanguage** | **string** | The caption language | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddVideoCaptionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **captionfile** | ***os.File** | The file to upload. | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DelVideoCaption

> DelVideoCaption(ctx, id, captionLanguage).Execute()

Delete a video caption

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    captionLanguage := "captionLanguage_example" // string | The caption language

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoCaptionsApi.DelVideoCaption(context.Background(), id, captionLanguage).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoCaptionsApi.DelVideoCaption``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 
**captionLanguage** | **string** | The caption language | 

### Other Parameters

Other parameters are passed through a pointer to a apiDelVideoCaptionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideoCaptions

> InlineResponse2009 GetVideoCaptions(ctx, id).Execute()

List captions of a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoCaptionsApi.GetVideoCaptions(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoCaptionsApi.GetVideoCaptions``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideoCaptions`: InlineResponse2009
    fmt.Fprintf(os.Stdout, "Response from `VideoCaptionsApi.GetVideoCaptions`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoCaptionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

