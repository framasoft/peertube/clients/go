# \VideoChannelApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AccountsNameVideoChannelsGet**](VideoChannelApi.md#AccountsNameVideoChannelsGet) | **Get** /accounts/{name}/video-channels | Get video channels of an account by its name
[**VideoChannelsChannelHandleDelete**](VideoChannelApi.md#VideoChannelsChannelHandleDelete) | **Delete** /video-channels/{channelHandle} | Delete a video channel by its id
[**VideoChannelsChannelHandleGet**](VideoChannelApi.md#VideoChannelsChannelHandleGet) | **Get** /video-channels/{channelHandle} | Get a video channel by its id
[**VideoChannelsChannelHandlePut**](VideoChannelApi.md#VideoChannelsChannelHandlePut) | **Put** /video-channels/{channelHandle} | Update a video channel by its id
[**VideoChannelsChannelHandleVideosGet**](VideoChannelApi.md#VideoChannelsChannelHandleVideosGet) | **Get** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
[**VideoChannelsGet**](VideoChannelApi.md#VideoChannelsGet) | **Get** /video-channels | Get list of video channels
[**VideoChannelsPost**](VideoChannelApi.md#VideoChannelsPost) | **Post** /video-channels | Creates a video channel for the current user



## AccountsNameVideoChannelsGet

> []VideoChannel AccountsNameVideoChannelsGet(ctx, name)

Get video channels of an account by its name

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**name** | **string**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) | 

### Return type

[**[]VideoChannel**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsChannelHandleDelete

> VideoChannelsChannelHandleDelete(ctx, channelHandle)

Delete a video channel by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsChannelHandleGet

> VideoChannel VideoChannelsChannelHandleGet(ctx, channelHandle)

Get a video channel by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsChannelHandlePut

> VideoChannelsChannelHandlePut(ctx, channelHandle, optional)

Update a video channel by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 
 **optional** | ***VideoChannelsChannelHandlePutOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideoChannelsChannelHandlePutOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **videoChannelUpdate** | [**optional.Interface of VideoChannelUpdate**](VideoChannelUpdate.md)|  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsChannelHandleVideosGet

> VideoListResponse VideoChannelsChannelHandleVideosGet(ctx, channelHandle)

Get videos of a video channel by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsGet

> []VideoChannel VideoChannelsGet(ctx, optional)

Get list of video channels

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***VideoChannelsGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideoChannelsGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort column (-createdAt for example) | 

### Return type

[**[]VideoChannel**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsPost

> VideoChannelsPost(ctx, optional)

Creates a video channel for the current user

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***VideoChannelsPostOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideoChannelsPostOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoChannelCreate** | [**optional.Interface of VideoChannelCreate**](VideoChannelCreate.md)|  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

