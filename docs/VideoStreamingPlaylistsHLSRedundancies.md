# VideoStreamingPlaylistsHLSRedundancies

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BaseUrl** | Pointer to **string** |  | [optional] 

## Methods

### NewVideoStreamingPlaylistsHLSRedundancies

`func NewVideoStreamingPlaylistsHLSRedundancies() *VideoStreamingPlaylistsHLSRedundancies`

NewVideoStreamingPlaylistsHLSRedundancies instantiates a new VideoStreamingPlaylistsHLSRedundancies object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVideoStreamingPlaylistsHLSRedundanciesWithDefaults

`func NewVideoStreamingPlaylistsHLSRedundanciesWithDefaults() *VideoStreamingPlaylistsHLSRedundancies`

NewVideoStreamingPlaylistsHLSRedundanciesWithDefaults instantiates a new VideoStreamingPlaylistsHLSRedundancies object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetBaseUrl

`func (o *VideoStreamingPlaylistsHLSRedundancies) GetBaseUrl() string`

GetBaseUrl returns the BaseUrl field if non-nil, zero value otherwise.

### GetBaseUrlOk

`func (o *VideoStreamingPlaylistsHLSRedundancies) GetBaseUrlOk() (*string, bool)`

GetBaseUrlOk returns a tuple with the BaseUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseUrl

`func (o *VideoStreamingPlaylistsHLSRedundancies) SetBaseUrl(v string)`

SetBaseUrl sets BaseUrl field to given value.

### HasBaseUrl

`func (o *VideoStreamingPlaylistsHLSRedundancies) HasBaseUrl() bool`

HasBaseUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


