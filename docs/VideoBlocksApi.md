# \VideoBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddVideoBlock**](VideoBlocksApi.md#AddVideoBlock) | **Post** /videos/{id}/blacklist | Block a video
[**DelVideoBlock**](VideoBlocksApi.md#DelVideoBlock) | **Delete** /videos/{id}/blacklist | Unblock a video by its id
[**GetVideoBlocks**](VideoBlocksApi.md#GetVideoBlocks) | **Get** /videos/blacklist | List video blocks



## AddVideoBlock

> AddVideoBlock(ctx, id).Execute()

Block a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoBlocksApi.AddVideoBlock(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoBlocksApi.AddVideoBlock``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddVideoBlockRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DelVideoBlock

> DelVideoBlock(ctx, id).Execute()

Unblock a video by its id

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoBlocksApi.DelVideoBlock(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoBlocksApi.DelVideoBlock``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiDelVideoBlockRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideoBlocks

> InlineResponse2008 GetVideoBlocks(ctx).Type_(type_).Search(search).Start(start).Count(count).Sort(sort).Execute()

List video blocks

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    type_ := int32(56) // int32 | list only blocks that match this type: - `1`: manual block - `2`: automatic block that needs review  (optional)
    search := "search_example" // string | plain search that will match with video titles, and more (optional)
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "sort_example" // string | Sort blocklists by criteria (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoBlocksApi.GetVideoBlocks(context.Background()).Type_(type_).Search(search).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoBlocksApi.GetVideoBlocks``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideoBlocks`: InlineResponse2008
    fmt.Fprintf(os.Stdout, "Response from `VideoBlocksApi.GetVideoBlocks`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoBlocksRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type_** | **int32** | list only blocks that match this type: - &#x60;1&#x60;: manual block - &#x60;2&#x60;: automatic block that needs review  | 
 **search** | **string** | plain search that will match with video titles, and more | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort blocklists by criteria | 

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

