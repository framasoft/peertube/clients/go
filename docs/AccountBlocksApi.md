# \AccountBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ServerBlocklistAccountsAccountNameDelete**](AccountBlocksApi.md#ServerBlocklistAccountsAccountNameDelete) | **Delete** /server/blocklist/accounts/{accountName} | Unblock an account by its handle
[**ServerBlocklistAccountsGet**](AccountBlocksApi.md#ServerBlocklistAccountsGet) | **Get** /server/blocklist/accounts | List account blocks
[**ServerBlocklistAccountsPost**](AccountBlocksApi.md#ServerBlocklistAccountsPost) | **Post** /server/blocklist/accounts | Block an account



## ServerBlocklistAccountsAccountNameDelete

> ServerBlocklistAccountsAccountNameDelete(ctx, accountName).Execute()

Unblock an account by its handle

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    accountName := "accountName_example" // string | account to unblock, in the form `username@domain`

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountBlocksApi.ServerBlocklistAccountsAccountNameDelete(context.Background(), accountName).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountBlocksApi.ServerBlocklistAccountsAccountNameDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountName** | **string** | account to unblock, in the form &#x60;username@domain&#x60; | 

### Other Parameters

Other parameters are passed through a pointer to a apiServerBlocklistAccountsAccountNameDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerBlocklistAccountsGet

> ServerBlocklistAccountsGet(ctx).Start(start).Count(count).Sort(sort).Execute()

List account blocks

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountBlocksApi.ServerBlocklistAccountsGet(context.Background()).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountBlocksApi.ServerBlocklistAccountsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiServerBlocklistAccountsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerBlocklistAccountsPost

> ServerBlocklistAccountsPost(ctx).InlineObject25(inlineObject25).Execute()

Block an account

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    inlineObject25 := *openapiclient.NewInlineObject25("chocobozzz@example.org") // InlineObject25 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountBlocksApi.ServerBlocklistAccountsPost(context.Background()).InlineObject25(inlineObject25).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountBlocksApi.ServerBlocklistAccountsPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiServerBlocklistAccountsPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject25** | [**InlineObject25**](InlineObject25.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

