# \VideoApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddLive**](VideoApi.md#AddLive) | **Post** /videos/live | Create a live
[**AddView**](VideoApi.md#AddView) | **Post** /videos/{id}/views | Add a view to a video
[**DelVideo**](VideoApi.md#DelVideo) | **Delete** /videos/{id} | Delete a video
[**GetAccountVideos**](VideoApi.md#GetAccountVideos) | **Get** /accounts/{name}/videos | List videos of an account
[**GetCategories**](VideoApi.md#GetCategories) | **Get** /videos/categories | List available video categories
[**GetLanguages**](VideoApi.md#GetLanguages) | **Get** /videos/languages | List available video languages
[**GetLicences**](VideoApi.md#GetLicences) | **Get** /videos/licences | List available video licences
[**GetLiveId**](VideoApi.md#GetLiveId) | **Get** /videos/live/{id} | Get information about a live
[**GetPrivacyPolicies**](VideoApi.md#GetPrivacyPolicies) | **Get** /videos/privacies | List available video privacy policies
[**GetVideo**](VideoApi.md#GetVideo) | **Get** /videos/{id} | Get a video
[**GetVideoChannelVideos**](VideoApi.md#GetVideoChannelVideos) | **Get** /video-channels/{channelHandle}/videos | List videos of a video channel
[**GetVideoDesc**](VideoApi.md#GetVideoDesc) | **Get** /videos/{id}/description | Get complete video description
[**GetVideos**](VideoApi.md#GetVideos) | **Get** /videos | List videos
[**ImportVideo**](VideoApi.md#ImportVideo) | **Post** /videos/imports | Import a video
[**PutVideo**](VideoApi.md#PutVideo) | **Put** /videos/{id} | Update a video
[**SetProgress**](VideoApi.md#SetProgress) | **Put** /videos/{id}/watching | Set watching progress of a video
[**UpdateLiveId**](VideoApi.md#UpdateLiveId) | **Put** /videos/live/{id} | Update information about a live
[**UploadLegacy**](VideoApi.md#UploadLegacy) | **Post** /videos/upload | Upload a video
[**UploadResumable**](VideoApi.md#UploadResumable) | **Put** /videos/upload-resumable | Send chunk for the resumable upload of a video
[**UploadResumableCancel**](VideoApi.md#UploadResumableCancel) | **Delete** /videos/upload-resumable | Cancel the resumable upload of a video, deleting any data uploaded so far
[**UploadResumableInit**](VideoApi.md#UploadResumableInit) | **Post** /videos/upload-resumable | Initialize the resumable upload of a video



## AddLive

> VideoUploadResponse AddLive(ctx).ChannelId(channelId).Name(name).SaveReplay(saveReplay).PermanentLive(permanentLive).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).Execute()

Create a live

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelId := int32(56) // int32 | Channel id that will contain this live video
    name := "name_example" // string | Live video/replay name
    saveReplay := true // bool |  (optional)
    permanentLive := true // bool | User can stream multiple times in a permanent live (optional)
    thumbnailfile := os.NewFile(1234, "some_file") // *os.File | Live video/replay thumbnail file (optional)
    previewfile := os.NewFile(1234, "some_file") // *os.File | Live video/replay preview file (optional)
    privacy := openapiclient.VideoPrivacySet(1) // VideoPrivacySet |  (optional)
    category := int32(56) // int32 | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    licence := int32(56) // int32 | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    language := "language_example" // string | language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    description := "description_example" // string | Live video/replay description (optional)
    support := "support_example" // string | A text tell the audience how to support the creator (optional)
    nsfw := true // bool | Whether or not this live video/replay contains sensitive content (optional)
    tags := []string{"Inner_example"} // []string | Live video/replay tags (maximum 5 tags each between 2 and 30 characters) (optional)
    commentsEnabled := true // bool | Enable or disable comments for this live video/replay (optional)
    downloadEnabled := true // bool | Enable or disable downloading for the replay of this live video (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.AddLive(context.Background()).ChannelId(channelId).Name(name).SaveReplay(saveReplay).PermanentLive(permanentLive).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.AddLive``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddLive`: VideoUploadResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.AddLive`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAddLiveRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **int32** | Channel id that will contain this live video | 
 **name** | **string** | Live video/replay name | 
 **saveReplay** | **bool** |  | 
 **permanentLive** | **bool** | User can stream multiple times in a permanent live | 
 **thumbnailfile** | ***os.File** | Live video/replay thumbnail file | 
 **previewfile** | ***os.File** | Live video/replay preview file | 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | 
 **category** | **int32** | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **licence** | **int32** | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **language** | **string** | language id of the video (see [/videos/languages](#operation/getLanguages)) | 
 **description** | **string** | Live video/replay description | 
 **support** | **string** | A text tell the audience how to support the creator | 
 **nsfw** | **bool** | Whether or not this live video/replay contains sensitive content | 
 **tags** | **[]string** | Live video/replay tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **bool** | Enable or disable comments for this live video/replay | 
 **downloadEnabled** | **bool** | Enable or disable downloading for the replay of this live video | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddView

> AddView(ctx, id).Execute()

Add a view to a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.AddView(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.AddView``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddViewRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DelVideo

> DelVideo(ctx, id).Execute()

Delete a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.DelVideo(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.DelVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiDelVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAccountVideos

> VideoListResponse GetAccountVideos(ctx, name).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()

List videos of an account

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    name := "chocobozzz | chocobozzz@example.org" // string | The username or handle of the account
    categoryOneOf := TODO // OneOfintegerarray | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    isLive := true // bool | whether or not the video is a live (optional)
    tagsOneOf := TODO // OneOfstringarray | tag(s) of the video (optional)
    tagsAllOf := TODO // OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
    licenceOneOf := TODO // OneOfintegerarray | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    languageOneOf := TODO // OneOfstringarray | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language (optional)
    nsfw := "nsfw_example" // string | whether to include nsfw videos, if any (optional)
    filter := "filter_example" // string | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    skipCount := "skipCount_example" // string | if you don't need the `total` in the response (optional) (default to "false")
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "sort_example" // string | Sort videos by criteria (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetAccountVideos(context.Background(), name).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetAccountVideos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAccountVideos`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetAccountVideos`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**name** | **string** | The username or handle of the account | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAccountVideosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **categoryOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **isLive** | **bool** | whether or not the video is a live | 
 **tagsOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video | 
 **tagsAllOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video, where all should be present in the video | 
 **licenceOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **languageOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | 
 **nsfw** | **string** | whether to include nsfw videos, if any | 
 **filter** | **string** | Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | 
 **skipCount** | **string** | if you don&#39;t need the &#x60;total&#x60; in the response | [default to &quot;false&quot;]
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort videos by criteria | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCategories

> []string GetCategories(ctx).Execute()

List available video categories

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetCategories(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetCategories``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCategories`: []string
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetCategories`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetCategoriesRequest struct via the builder pattern


### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetLanguages

> []string GetLanguages(ctx).Execute()

List available video languages

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetLanguages(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetLanguages``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetLanguages`: []string
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetLanguages`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetLanguagesRequest struct via the builder pattern


### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetLicences

> []string GetLicences(ctx).Execute()

List available video licences

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetLicences(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetLicences``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetLicences`: []string
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetLicences`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetLicencesRequest struct via the builder pattern


### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetLiveId

> LiveVideoResponse GetLiveId(ctx, id).Execute()

Get information about a live

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetLiveId(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetLiveId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetLiveId`: LiveVideoResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetLiveId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetLiveIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**LiveVideoResponse**](LiveVideoResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPrivacyPolicies

> []string GetPrivacyPolicies(ctx).Execute()

List available video privacy policies

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetPrivacyPolicies(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetPrivacyPolicies``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetPrivacyPolicies`: []string
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetPrivacyPolicies`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetPrivacyPoliciesRequest struct via the builder pattern


### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideo

> VideoDetails GetVideo(ctx, id).Execute()

Get a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetVideo(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideo`: VideoDetails
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetVideo`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**VideoDetails**](VideoDetails.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideoChannelVideos

> VideoListResponse GetVideoChannelVideos(ctx, channelHandle).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()

List videos of a video channel

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelHandle := "my_username | my_username@example.com" // string | The video channel handle
    categoryOneOf := TODO // OneOfintegerarray | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    isLive := true // bool | whether or not the video is a live (optional)
    tagsOneOf := TODO // OneOfstringarray | tag(s) of the video (optional)
    tagsAllOf := TODO // OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
    licenceOneOf := TODO // OneOfintegerarray | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    languageOneOf := TODO // OneOfstringarray | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language (optional)
    nsfw := "nsfw_example" // string | whether to include nsfw videos, if any (optional)
    filter := "filter_example" // string | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    skipCount := "skipCount_example" // string | if you don't need the `total` in the response (optional) (default to "false")
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "sort_example" // string | Sort videos by criteria (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetVideoChannelVideos(context.Background(), channelHandle).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetVideoChannelVideos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideoChannelVideos`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetVideoChannelVideos`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string** | The video channel handle | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoChannelVideosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **categoryOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **isLive** | **bool** | whether or not the video is a live | 
 **tagsOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video | 
 **tagsAllOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video, where all should be present in the video | 
 **licenceOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **languageOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | 
 **nsfw** | **string** | whether to include nsfw videos, if any | 
 **filter** | **string** | Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | 
 **skipCount** | **string** | if you don&#39;t need the &#x60;total&#x60; in the response | [default to &quot;false&quot;]
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort videos by criteria | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideoDesc

> string GetVideoDesc(ctx, id).Execute()

Get complete video description

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetVideoDesc(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetVideoDesc``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideoDesc`: string
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetVideoDesc`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoDescRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideos

> VideoListResponse GetVideos(ctx).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()

List videos

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    categoryOneOf := TODO // OneOfintegerarray | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    isLive := true // bool | whether or not the video is a live (optional)
    tagsOneOf := TODO // OneOfstringarray | tag(s) of the video (optional)
    tagsAllOf := TODO // OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
    licenceOneOf := TODO // OneOfintegerarray | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    languageOneOf := TODO // OneOfstringarray | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language (optional)
    nsfw := "nsfw_example" // string | whether to include nsfw videos, if any (optional)
    filter := "filter_example" // string | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    skipCount := "skipCount_example" // string | if you don't need the `total` in the response (optional) (default to "false")
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "sort_example" // string | Sort videos by criteria (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.GetVideos(context.Background()).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.GetVideos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideos`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.GetVideos`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetVideosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **isLive** | **bool** | whether or not the video is a live | 
 **tagsOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video | 
 **tagsAllOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video, where all should be present in the video | 
 **licenceOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **languageOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | 
 **nsfw** | **string** | whether to include nsfw videos, if any | 
 **filter** | **string** | Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | 
 **skipCount** | **string** | if you don&#39;t need the &#x60;total&#x60; in the response | [default to &quot;false&quot;]
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort videos by criteria | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ImportVideo

> VideoUploadResponse ImportVideo(ctx).Name(name).ChannelId(channelId).TargetUrl(targetUrl).MagnetUri(magnetUri).Torrentfile(torrentfile).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Execute()

Import a video



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "./openapi"
)

func main() {
    name := "name_example" // string | Video name
    channelId := int32(56) // int32 | Channel id that will contain this video
    targetUrl := TODO // TargetUrl |  (optional)
    magnetUri := TODO // MagnetUri |  (optional)
    torrentfile := TODO // Torrentfile |  (optional)
    privacy := openapiclient.VideoPrivacySet(1) // VideoPrivacySet |  (optional)
    category := int32(56) // int32 | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    licence := int32(56) // int32 | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    language := "language_example" // string | language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    description := "description_example" // string | Video description (optional)
    waitTranscoding := true // bool | Whether or not we wait transcoding before publish the video (optional)
    support := "support_example" // string | A text tell the audience how to support the video creator (optional)
    nsfw := true // bool | Whether or not this video contains sensitive content (optional)
    tags := []string{"Inner_example"} // []string | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    commentsEnabled := true // bool | Enable or disable comments for this video (optional)
    downloadEnabled := true // bool | Enable or disable downloading for this video (optional)
    originallyPublishedAt := time.Now() // time.Time | Date when the content was originally published (optional)
    scheduleUpdate := *openapiclient.NewVideoScheduledUpdate(time.Now()) // VideoScheduledUpdate |  (optional)
    thumbnailfile := os.NewFile(1234, "some_file") // *os.File | Video thumbnail file (optional)
    previewfile := os.NewFile(1234, "some_file") // *os.File | Video preview file (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.ImportVideo(context.Background()).Name(name).ChannelId(channelId).TargetUrl(targetUrl).MagnetUri(magnetUri).Torrentfile(torrentfile).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.ImportVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ImportVideo`: VideoUploadResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.ImportVideo`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiImportVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string** | Video name | 
 **channelId** | **int32** | Channel id that will contain this video | 
 **targetUrl** | [**TargetUrl**](TargetUrl.md) |  | 
 **magnetUri** | [**MagnetUri**](MagnetUri.md) |  | 
 **torrentfile** | [**Torrentfile**](Torrentfile.md) |  | 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | 
 **category** | **int32** | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **licence** | **int32** | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **language** | **string** | language id of the video (see [/videos/languages](#operation/getLanguages)) | 
 **description** | **string** | Video description | 
 **waitTranscoding** | **bool** | Whether or not we wait transcoding before publish the video | 
 **support** | **string** | A text tell the audience how to support the video creator | 
 **nsfw** | **bool** | Whether or not this video contains sensitive content | 
 **tags** | **[]string** | Video tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **bool** | Enable or disable comments for this video | 
 **downloadEnabled** | **bool** | Enable or disable downloading for this video | 
 **originallyPublishedAt** | **time.Time** | Date when the content was originally published | 
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | 
 **thumbnailfile** | ***os.File** | Video thumbnail file | 
 **previewfile** | ***os.File** | Video preview file | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutVideo

> PutVideo(ctx, id).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Category(category).Licence(licence).Language(language).Privacy(privacy).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Name(name).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Execute()

Update a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    thumbnailfile := os.NewFile(1234, "some_file") // *os.File | Video thumbnail file (optional)
    previewfile := os.NewFile(1234, "some_file") // *os.File | Video preview file (optional)
    category := int32(56) // int32 | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    licence := int32(56) // int32 | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    language := "language_example" // string | language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    privacy := openapiclient.VideoPrivacySet(1) // VideoPrivacySet |  (optional)
    description := "description_example" // string | Video description (optional)
    waitTranscoding := "waitTranscoding_example" // string | Whether or not we wait transcoding before publish the video (optional)
    support := "support_example" // string | A text tell the audience how to support the video creator (optional)
    nsfw := true // bool | Whether or not this video contains sensitive content (optional)
    name := "name_example" // string | Video name (optional)
    tags := []string{"Inner_example"} // []string | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    commentsEnabled := true // bool | Enable or disable comments for this video (optional)
    downloadEnabled := true // bool | Enable or disable downloading for this video (optional)
    originallyPublishedAt := time.Now() // time.Time | Date when the content was originally published (optional)
    scheduleUpdate := *openapiclient.NewVideoScheduledUpdate(time.Now()) // VideoScheduledUpdate |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.PutVideo(context.Background(), id).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Category(category).Licence(licence).Language(language).Privacy(privacy).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Name(name).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.PutVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiPutVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **thumbnailfile** | ***os.File** | Video thumbnail file | 
 **previewfile** | ***os.File** | Video preview file | 
 **category** | **int32** | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **licence** | **int32** | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **language** | **string** | language id of the video (see [/videos/languages](#operation/getLanguages)) | 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | 
 **description** | **string** | Video description | 
 **waitTranscoding** | **string** | Whether or not we wait transcoding before publish the video | 
 **support** | **string** | A text tell the audience how to support the video creator | 
 **nsfw** | **bool** | Whether or not this video contains sensitive content | 
 **name** | **string** | Video name | 
 **tags** | **[]string** | Video tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **bool** | Enable or disable comments for this video | 
 **downloadEnabled** | **bool** | Enable or disable downloading for this video | 
 **originallyPublishedAt** | **time.Time** | Date when the content was originally published | 
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetProgress

> SetProgress(ctx, id).UserWatchingVideo(userWatchingVideo).Execute()

Set watching progress of a video

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    userWatchingVideo := *openapiclient.NewUserWatchingVideo() // UserWatchingVideo | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.SetProgress(context.Background(), id).UserWatchingVideo(userWatchingVideo).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.SetProgress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiSetProgressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **userWatchingVideo** | [**UserWatchingVideo**](UserWatchingVideo.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateLiveId

> UpdateLiveId(ctx, id).LiveVideoUpdate(liveVideoUpdate).Execute()

Update information about a live

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    liveVideoUpdate := *openapiclient.NewLiveVideoUpdate() // LiveVideoUpdate |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.UpdateLiveId(context.Background(), id).LiveVideoUpdate(liveVideoUpdate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.UpdateLiveId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateLiveIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **liveVideoUpdate** | [**LiveVideoUpdate**](LiveVideoUpdate.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadLegacy

> VideoUploadResponse UploadLegacy(ctx).Name(name).ChannelId(channelId).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Videofile(videofile).Execute()

Upload a video



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "./openapi"
)

func main() {
    name := "name_example" // string | Video name (optional)
    channelId := int32(56) // int32 | Channel id that will contain this video (optional)
    privacy := openapiclient.VideoPrivacySet(1) // VideoPrivacySet |  (optional)
    category := int32(56) // int32 | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    licence := int32(56) // int32 | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    language := "language_example" // string | language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    description := "description_example" // string | Video description (optional)
    waitTranscoding := true // bool | Whether or not we wait transcoding before publish the video (optional)
    support := "support_example" // string | A text tell the audience how to support the video creator (optional)
    nsfw := true // bool | Whether or not this video contains sensitive content (optional)
    tags := []string{"Inner_example"} // []string | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    commentsEnabled := true // bool | Enable or disable comments for this video (optional)
    downloadEnabled := true // bool | Enable or disable downloading for this video (optional)
    originallyPublishedAt := time.Now() // time.Time | Date when the content was originally published (optional)
    scheduleUpdate := *openapiclient.NewVideoScheduledUpdate(time.Now()) // VideoScheduledUpdate |  (optional)
    thumbnailfile := os.NewFile(1234, "some_file") // *os.File | Video thumbnail file (optional)
    previewfile := os.NewFile(1234, "some_file") // *os.File | Video preview file (optional)
    videofile := os.NewFile(1234, "some_file") // *os.File | Video file (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.UploadLegacy(context.Background()).Name(name).ChannelId(channelId).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Videofile(videofile).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.UploadLegacy``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UploadLegacy`: VideoUploadResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.UploadLegacy`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUploadLegacyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string** | Video name | 
 **channelId** | **int32** | Channel id that will contain this video | 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | 
 **category** | **int32** | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **licence** | **int32** | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **language** | **string** | language id of the video (see [/videos/languages](#operation/getLanguages)) | 
 **description** | **string** | Video description | 
 **waitTranscoding** | **bool** | Whether or not we wait transcoding before publish the video | 
 **support** | **string** | A text tell the audience how to support the video creator | 
 **nsfw** | **bool** | Whether or not this video contains sensitive content | 
 **tags** | **[]string** | Video tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **bool** | Enable or disable comments for this video | 
 **downloadEnabled** | **bool** | Enable or disable downloading for this video | 
 **originallyPublishedAt** | **time.Time** | Date when the content was originally published | 
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | 
 **thumbnailfile** | ***os.File** | Video thumbnail file | 
 **previewfile** | ***os.File** | Video preview file | 
 **videofile** | ***os.File** | Video file | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadResumable

> VideoUploadResponse UploadResumable(ctx).UploadId(uploadId).ContentRange(contentRange).ContentLength(contentLength).Body(body).Execute()

Send chunk for the resumable upload of a video



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    uploadId := "uploadId_example" // string | Created session id to proceed with. If you didn't send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload. 
    contentRange := "bytes 0-262143/2469036" // string | Specifies the bytes in the file that the request is uploading.  For example, a value of `bytes 0-262143/1000000` shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file. 
    contentLength := float32(262144) // float32 | Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn't mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube's web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health. 
    body := os.NewFile(1234, "some_file") // *os.File |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.UploadResumable(context.Background()).UploadId(uploadId).ContentRange(contentRange).ContentLength(contentLength).Body(body).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.UploadResumable``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UploadResumable`: VideoUploadResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoApi.UploadResumable`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUploadResumableRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadId** | **string** | Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload.  | 
 **contentRange** | **string** | Specifies the bytes in the file that the request is uploading.  For example, a value of &#x60;bytes 0-262143/1000000&#x60; shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file.  | 
 **contentLength** | **float32** | Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn&#39;t mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube&#39;s web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health.  | 
 **body** | ***os.File** |  | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/octet-stream
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadResumableCancel

> UploadResumableCancel(ctx).UploadId(uploadId).ContentLength(contentLength).Execute()

Cancel the resumable upload of a video, deleting any data uploaded so far



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    uploadId := "uploadId_example" // string | Created session id to proceed with. If you didn't send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-) 
    contentLength := float32(0) // float32 | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.UploadResumableCancel(context.Background()).UploadId(uploadId).ContentLength(contentLength).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.UploadResumableCancel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUploadResumableCancelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadId** | **string** | Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-)  | 
 **contentLength** | **float32** |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadResumableInit

> UploadResumableInit(ctx).XUploadContentLength(xUploadContentLength).XUploadContentType(xUploadContentType).VideoUploadRequestResumable(videoUploadRequestResumable).Execute()

Initialize the resumable upload of a video



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    xUploadContentLength := float32(2469036) // float32 | Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading.
    xUploadContentType := "video/mp4" // string | MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary.
    videoUploadRequestResumable := *openapiclient.NewVideoUploadRequestResumable("What is PeerTube?", int32(3), "what_is_peertube.mp4") // VideoUploadRequestResumable |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoApi.UploadResumableInit(context.Background()).XUploadContentLength(xUploadContentLength).XUploadContentType(xUploadContentType).VideoUploadRequestResumable(videoUploadRequestResumable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoApi.UploadResumableInit``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUploadResumableInitRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xUploadContentLength** | **float32** | Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading. | 
 **xUploadContentType** | **string** | MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary. | 
 **videoUploadRequestResumable** | [**VideoUploadRequestResumable**](VideoUploadRequestResumable.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

