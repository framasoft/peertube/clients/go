# VideoUploadRequestResumableAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Filename** | **string** | Video filename including extension | 
**Thumbnailfile** | Pointer to ***os.File** | Video thumbnail file | [optional] 
**Previewfile** | Pointer to ***os.File** | Video preview file | [optional] 

## Methods

### NewVideoUploadRequestResumableAllOf

`func NewVideoUploadRequestResumableAllOf(filename string, ) *VideoUploadRequestResumableAllOf`

NewVideoUploadRequestResumableAllOf instantiates a new VideoUploadRequestResumableAllOf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVideoUploadRequestResumableAllOfWithDefaults

`func NewVideoUploadRequestResumableAllOfWithDefaults() *VideoUploadRequestResumableAllOf`

NewVideoUploadRequestResumableAllOfWithDefaults instantiates a new VideoUploadRequestResumableAllOf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFilename

`func (o *VideoUploadRequestResumableAllOf) GetFilename() string`

GetFilename returns the Filename field if non-nil, zero value otherwise.

### GetFilenameOk

`func (o *VideoUploadRequestResumableAllOf) GetFilenameOk() (*string, bool)`

GetFilenameOk returns a tuple with the Filename field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilename

`func (o *VideoUploadRequestResumableAllOf) SetFilename(v string)`

SetFilename sets Filename field to given value.


### GetThumbnailfile

`func (o *VideoUploadRequestResumableAllOf) GetThumbnailfile() *os.File`

GetThumbnailfile returns the Thumbnailfile field if non-nil, zero value otherwise.

### GetThumbnailfileOk

`func (o *VideoUploadRequestResumableAllOf) GetThumbnailfileOk() (**os.File, bool)`

GetThumbnailfileOk returns a tuple with the Thumbnailfile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetThumbnailfile

`func (o *VideoUploadRequestResumableAllOf) SetThumbnailfile(v *os.File)`

SetThumbnailfile sets Thumbnailfile field to given value.

### HasThumbnailfile

`func (o *VideoUploadRequestResumableAllOf) HasThumbnailfile() bool`

HasThumbnailfile returns a boolean if a field has been set.

### GetPreviewfile

`func (o *VideoUploadRequestResumableAllOf) GetPreviewfile() *os.File`

GetPreviewfile returns the Previewfile field if non-nil, zero value otherwise.

### GetPreviewfileOk

`func (o *VideoUploadRequestResumableAllOf) GetPreviewfileOk() (**os.File, bool)`

GetPreviewfileOk returns a tuple with the Previewfile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPreviewfile

`func (o *VideoUploadRequestResumableAllOf) SetPreviewfile(v *os.File)`

SetPreviewfile sets Previewfile field to given value.

### HasPreviewfile

`func (o *VideoUploadRequestResumableAllOf) HasPreviewfile() bool`

HasPreviewfile returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


