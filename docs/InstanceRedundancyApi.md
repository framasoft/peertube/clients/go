# \InstanceRedundancyApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ServerRedundancyHostPut**](InstanceRedundancyApi.md#ServerRedundancyHostPut) | **Put** /server/redundancy/{host} | Update a server redundancy policy



## ServerRedundancyHostPut

> ServerRedundancyHostPut(ctx, host).InlineObject27(inlineObject27).Execute()

Update a server redundancy policy

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    host := "host_example" // string | server domain to mirror
    inlineObject27 := *openapiclient.NewInlineObject27(false) // InlineObject27 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.InstanceRedundancyApi.ServerRedundancyHostPut(context.Background(), host).InlineObject27(inlineObject27).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InstanceRedundancyApi.ServerRedundancyHostPut``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**host** | **string** | server domain to mirror | 

### Other Parameters

Other parameters are passed through a pointer to a apiServerRedundancyHostPutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **inlineObject27** | [**InlineObject27**](InlineObject27.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

