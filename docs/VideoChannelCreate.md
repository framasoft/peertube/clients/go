# VideoChannelCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | editable name of the channel, displayed in its representations | 
**Description** | Pointer to **string** |  | [optional] 
**Support** | Pointer to **string** | text shown by default on all videos of this channel, to tell the audience how to support it | [optional] 
**Id** | Pointer to **int32** |  | [optional] [readonly] 
**IsLocal** | Pointer to **bool** |  | [optional] [readonly] 
**UpdatedAt** | Pointer to **time.Time** |  | [optional] [readonly] 
**OwnerAccount** | Pointer to [**NullableVideoChannelOwnerAccount**](VideoChannelOwnerAccount.md) |  | [optional] 
**Name** | **string** | username of the channel to create | 

## Methods

### NewVideoChannelCreate

`func NewVideoChannelCreate(displayName string, name string, ) *VideoChannelCreate`

NewVideoChannelCreate instantiates a new VideoChannelCreate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVideoChannelCreateWithDefaults

`func NewVideoChannelCreateWithDefaults() *VideoChannelCreate`

NewVideoChannelCreateWithDefaults instantiates a new VideoChannelCreate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDisplayName

`func (o *VideoChannelCreate) GetDisplayName() string`

GetDisplayName returns the DisplayName field if non-nil, zero value otherwise.

### GetDisplayNameOk

`func (o *VideoChannelCreate) GetDisplayNameOk() (*string, bool)`

GetDisplayNameOk returns a tuple with the DisplayName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayName

`func (o *VideoChannelCreate) SetDisplayName(v string)`

SetDisplayName sets DisplayName field to given value.


### GetDescription

`func (o *VideoChannelCreate) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *VideoChannelCreate) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *VideoChannelCreate) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *VideoChannelCreate) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetSupport

`func (o *VideoChannelCreate) GetSupport() string`

GetSupport returns the Support field if non-nil, zero value otherwise.

### GetSupportOk

`func (o *VideoChannelCreate) GetSupportOk() (*string, bool)`

GetSupportOk returns a tuple with the Support field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSupport

`func (o *VideoChannelCreate) SetSupport(v string)`

SetSupport sets Support field to given value.

### HasSupport

`func (o *VideoChannelCreate) HasSupport() bool`

HasSupport returns a boolean if a field has been set.

### GetId

`func (o *VideoChannelCreate) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VideoChannelCreate) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VideoChannelCreate) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *VideoChannelCreate) HasId() bool`

HasId returns a boolean if a field has been set.

### GetIsLocal

`func (o *VideoChannelCreate) GetIsLocal() bool`

GetIsLocal returns the IsLocal field if non-nil, zero value otherwise.

### GetIsLocalOk

`func (o *VideoChannelCreate) GetIsLocalOk() (*bool, bool)`

GetIsLocalOk returns a tuple with the IsLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsLocal

`func (o *VideoChannelCreate) SetIsLocal(v bool)`

SetIsLocal sets IsLocal field to given value.

### HasIsLocal

`func (o *VideoChannelCreate) HasIsLocal() bool`

HasIsLocal returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *VideoChannelCreate) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *VideoChannelCreate) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *VideoChannelCreate) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *VideoChannelCreate) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.

### GetOwnerAccount

`func (o *VideoChannelCreate) GetOwnerAccount() VideoChannelOwnerAccount`

GetOwnerAccount returns the OwnerAccount field if non-nil, zero value otherwise.

### GetOwnerAccountOk

`func (o *VideoChannelCreate) GetOwnerAccountOk() (*VideoChannelOwnerAccount, bool)`

GetOwnerAccountOk returns a tuple with the OwnerAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwnerAccount

`func (o *VideoChannelCreate) SetOwnerAccount(v VideoChannelOwnerAccount)`

SetOwnerAccount sets OwnerAccount field to given value.

### HasOwnerAccount

`func (o *VideoChannelCreate) HasOwnerAccount() bool`

HasOwnerAccount returns a boolean if a field has been set.

### SetOwnerAccountNil

`func (o *VideoChannelCreate) SetOwnerAccountNil(b bool)`

 SetOwnerAccountNil sets the value for OwnerAccount to be an explicit nil

### UnsetOwnerAccount
`func (o *VideoChannelCreate) UnsetOwnerAccount()`

UnsetOwnerAccount ensures that no value is present for OwnerAccount, not even an explicit nil
### GetName

`func (o *VideoChannelCreate) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *VideoChannelCreate) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *VideoChannelCreate) SetName(v string)`

SetName sets Name field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


