# VideoInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int32** |  | [optional] 
**Uuid** | Pointer to [**Uuid**](Uuid.md) |  | [optional] 
**Name** | Pointer to [**Name**](Name.md) |  | [optional] 

## Methods

### NewVideoInfo

`func NewVideoInfo() *VideoInfo`

NewVideoInfo instantiates a new VideoInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVideoInfoWithDefaults

`func NewVideoInfoWithDefaults() *VideoInfo`

NewVideoInfoWithDefaults instantiates a new VideoInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VideoInfo) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VideoInfo) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VideoInfo) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *VideoInfo) HasId() bool`

HasId returns a boolean if a field has been set.

### GetUuid

`func (o *VideoInfo) GetUuid() Uuid`

GetUuid returns the Uuid field if non-nil, zero value otherwise.

### GetUuidOk

`func (o *VideoInfo) GetUuidOk() (*Uuid, bool)`

GetUuidOk returns a tuple with the Uuid field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUuid

`func (o *VideoInfo) SetUuid(v Uuid)`

SetUuid sets Uuid field to given value.

### HasUuid

`func (o *VideoInfo) HasUuid() bool`

HasUuid returns a boolean if a field has been set.

### GetName

`func (o *VideoInfo) GetName() Name`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *VideoInfo) GetNameOk() (*Name, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *VideoInfo) SetName(v Name)`

SetName sets Name field to given value.

### HasName

`func (o *VideoInfo) HasName() bool`

HasName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


