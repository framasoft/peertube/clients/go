# InlineObject11

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Reason** | **string** | Reason why the user reports this video | 
**PredefinedReasons** | Pointer to **[]string** | Reason categories that help triage reports | [optional] 
**Video** | Pointer to [**AbusesVideo**](AbusesVideo.md) |  | [optional] 
**Comment** | Pointer to [**AbusesComment**](AbusesComment.md) |  | [optional] 
**Account** | Pointer to [**AbusesAccount**](AbusesAccount.md) |  | [optional] 

## Methods

### NewInlineObject11

`func NewInlineObject11(reason string, ) *InlineObject11`

NewInlineObject11 instantiates a new InlineObject11 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject11WithDefaults

`func NewInlineObject11WithDefaults() *InlineObject11`

NewInlineObject11WithDefaults instantiates a new InlineObject11 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetReason

`func (o *InlineObject11) GetReason() string`

GetReason returns the Reason field if non-nil, zero value otherwise.

### GetReasonOk

`func (o *InlineObject11) GetReasonOk() (*string, bool)`

GetReasonOk returns a tuple with the Reason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReason

`func (o *InlineObject11) SetReason(v string)`

SetReason sets Reason field to given value.


### GetPredefinedReasons

`func (o *InlineObject11) GetPredefinedReasons() []string`

GetPredefinedReasons returns the PredefinedReasons field if non-nil, zero value otherwise.

### GetPredefinedReasonsOk

`func (o *InlineObject11) GetPredefinedReasonsOk() (*[]string, bool)`

GetPredefinedReasonsOk returns a tuple with the PredefinedReasons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPredefinedReasons

`func (o *InlineObject11) SetPredefinedReasons(v []string)`

SetPredefinedReasons sets PredefinedReasons field to given value.

### HasPredefinedReasons

`func (o *InlineObject11) HasPredefinedReasons() bool`

HasPredefinedReasons returns a boolean if a field has been set.

### GetVideo

`func (o *InlineObject11) GetVideo() AbusesVideo`

GetVideo returns the Video field if non-nil, zero value otherwise.

### GetVideoOk

`func (o *InlineObject11) GetVideoOk() (*AbusesVideo, bool)`

GetVideoOk returns a tuple with the Video field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideo

`func (o *InlineObject11) SetVideo(v AbusesVideo)`

SetVideo sets Video field to given value.

### HasVideo

`func (o *InlineObject11) HasVideo() bool`

HasVideo returns a boolean if a field has been set.

### GetComment

`func (o *InlineObject11) GetComment() AbusesComment`

GetComment returns the Comment field if non-nil, zero value otherwise.

### GetCommentOk

`func (o *InlineObject11) GetCommentOk() (*AbusesComment, bool)`

GetCommentOk returns a tuple with the Comment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComment

`func (o *InlineObject11) SetComment(v AbusesComment)`

SetComment sets Comment field to given value.

### HasComment

`func (o *InlineObject11) HasComment() bool`

HasComment returns a boolean if a field has been set.

### GetAccount

`func (o *InlineObject11) GetAccount() AbusesAccount`

GetAccount returns the Account field if non-nil, zero value otherwise.

### GetAccountOk

`func (o *InlineObject11) GetAccountOk() (*AbusesAccount, bool)`

GetAccountOk returns a tuple with the Account field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAccount

`func (o *InlineObject11) SetAccount(v AbusesAccount)`

SetAccount sets Account field to given value.

### HasAccount

`func (o *InlineObject11) HasAccount() bool`

HasAccount returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


