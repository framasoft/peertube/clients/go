# VideoDetailsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DescriptionPath** | Pointer to **string** | path at which to get the full description of maximum &#x60;10000&#x60; characters | [optional] 
**Support** | Pointer to **string** | A text tell the audience how to support the video creator | [optional] 
**Channel** | Pointer to [**VideoChannel**](VideoChannel.md) |  | [optional] 
**Account** | Pointer to [**Account**](Account.md) |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**CommentsEnabled** | Pointer to **bool** |  | [optional] 
**DownloadEnabled** | Pointer to **bool** |  | [optional] 
**TrackerUrls** | Pointer to **[]string** |  | [optional] 
**Files** | Pointer to [**[]VideoFile**](VideoFile.md) | WebTorrent/raw video files. If WebTorrent is disabled on the server:  - field will be empty - video files will be found in &#x60;streamingPlaylists[].files&#x60; field  | [optional] 
**StreamingPlaylists** | Pointer to [**[]VideoStreamingPlaylists**](VideoStreamingPlaylists.md) | HLS playlists/manifest files. If HLS is disabled on the server:  - field will be empty - video files will be found in &#x60;files&#x60; field  | [optional] 

## Methods

### NewVideoDetailsAllOf

`func NewVideoDetailsAllOf() *VideoDetailsAllOf`

NewVideoDetailsAllOf instantiates a new VideoDetailsAllOf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVideoDetailsAllOfWithDefaults

`func NewVideoDetailsAllOfWithDefaults() *VideoDetailsAllOf`

NewVideoDetailsAllOfWithDefaults instantiates a new VideoDetailsAllOf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDescriptionPath

`func (o *VideoDetailsAllOf) GetDescriptionPath() string`

GetDescriptionPath returns the DescriptionPath field if non-nil, zero value otherwise.

### GetDescriptionPathOk

`func (o *VideoDetailsAllOf) GetDescriptionPathOk() (*string, bool)`

GetDescriptionPathOk returns a tuple with the DescriptionPath field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptionPath

`func (o *VideoDetailsAllOf) SetDescriptionPath(v string)`

SetDescriptionPath sets DescriptionPath field to given value.

### HasDescriptionPath

`func (o *VideoDetailsAllOf) HasDescriptionPath() bool`

HasDescriptionPath returns a boolean if a field has been set.

### GetSupport

`func (o *VideoDetailsAllOf) GetSupport() string`

GetSupport returns the Support field if non-nil, zero value otherwise.

### GetSupportOk

`func (o *VideoDetailsAllOf) GetSupportOk() (*string, bool)`

GetSupportOk returns a tuple with the Support field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSupport

`func (o *VideoDetailsAllOf) SetSupport(v string)`

SetSupport sets Support field to given value.

### HasSupport

`func (o *VideoDetailsAllOf) HasSupport() bool`

HasSupport returns a boolean if a field has been set.

### GetChannel

`func (o *VideoDetailsAllOf) GetChannel() VideoChannel`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *VideoDetailsAllOf) GetChannelOk() (*VideoChannel, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *VideoDetailsAllOf) SetChannel(v VideoChannel)`

SetChannel sets Channel field to given value.

### HasChannel

`func (o *VideoDetailsAllOf) HasChannel() bool`

HasChannel returns a boolean if a field has been set.

### GetAccount

`func (o *VideoDetailsAllOf) GetAccount() Account`

GetAccount returns the Account field if non-nil, zero value otherwise.

### GetAccountOk

`func (o *VideoDetailsAllOf) GetAccountOk() (*Account, bool)`

GetAccountOk returns a tuple with the Account field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAccount

`func (o *VideoDetailsAllOf) SetAccount(v Account)`

SetAccount sets Account field to given value.

### HasAccount

`func (o *VideoDetailsAllOf) HasAccount() bool`

HasAccount returns a boolean if a field has been set.

### GetTags

`func (o *VideoDetailsAllOf) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *VideoDetailsAllOf) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *VideoDetailsAllOf) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *VideoDetailsAllOf) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetCommentsEnabled

`func (o *VideoDetailsAllOf) GetCommentsEnabled() bool`

GetCommentsEnabled returns the CommentsEnabled field if non-nil, zero value otherwise.

### GetCommentsEnabledOk

`func (o *VideoDetailsAllOf) GetCommentsEnabledOk() (*bool, bool)`

GetCommentsEnabledOk returns a tuple with the CommentsEnabled field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCommentsEnabled

`func (o *VideoDetailsAllOf) SetCommentsEnabled(v bool)`

SetCommentsEnabled sets CommentsEnabled field to given value.

### HasCommentsEnabled

`func (o *VideoDetailsAllOf) HasCommentsEnabled() bool`

HasCommentsEnabled returns a boolean if a field has been set.

### GetDownloadEnabled

`func (o *VideoDetailsAllOf) GetDownloadEnabled() bool`

GetDownloadEnabled returns the DownloadEnabled field if non-nil, zero value otherwise.

### GetDownloadEnabledOk

`func (o *VideoDetailsAllOf) GetDownloadEnabledOk() (*bool, bool)`

GetDownloadEnabledOk returns a tuple with the DownloadEnabled field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDownloadEnabled

`func (o *VideoDetailsAllOf) SetDownloadEnabled(v bool)`

SetDownloadEnabled sets DownloadEnabled field to given value.

### HasDownloadEnabled

`func (o *VideoDetailsAllOf) HasDownloadEnabled() bool`

HasDownloadEnabled returns a boolean if a field has been set.

### GetTrackerUrls

`func (o *VideoDetailsAllOf) GetTrackerUrls() []string`

GetTrackerUrls returns the TrackerUrls field if non-nil, zero value otherwise.

### GetTrackerUrlsOk

`func (o *VideoDetailsAllOf) GetTrackerUrlsOk() (*[]string, bool)`

GetTrackerUrlsOk returns a tuple with the TrackerUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTrackerUrls

`func (o *VideoDetailsAllOf) SetTrackerUrls(v []string)`

SetTrackerUrls sets TrackerUrls field to given value.

### HasTrackerUrls

`func (o *VideoDetailsAllOf) HasTrackerUrls() bool`

HasTrackerUrls returns a boolean if a field has been set.

### GetFiles

`func (o *VideoDetailsAllOf) GetFiles() []VideoFile`

GetFiles returns the Files field if non-nil, zero value otherwise.

### GetFilesOk

`func (o *VideoDetailsAllOf) GetFilesOk() (*[]VideoFile, bool)`

GetFilesOk returns a tuple with the Files field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFiles

`func (o *VideoDetailsAllOf) SetFiles(v []VideoFile)`

SetFiles sets Files field to given value.

### HasFiles

`func (o *VideoDetailsAllOf) HasFiles() bool`

HasFiles returns a boolean if a field has been set.

### GetStreamingPlaylists

`func (o *VideoDetailsAllOf) GetStreamingPlaylists() []VideoStreamingPlaylists`

GetStreamingPlaylists returns the StreamingPlaylists field if non-nil, zero value otherwise.

### GetStreamingPlaylistsOk

`func (o *VideoDetailsAllOf) GetStreamingPlaylistsOk() (*[]VideoStreamingPlaylists, bool)`

GetStreamingPlaylistsOk returns a tuple with the StreamingPlaylists field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreamingPlaylists

`func (o *VideoDetailsAllOf) SetStreamingPlaylists(v []VideoStreamingPlaylists)`

SetStreamingPlaylists sets StreamingPlaylists field to given value.

### HasStreamingPlaylists

`func (o *VideoDetailsAllOf) HasStreamingPlaylists() bool`

HasStreamingPlaylists returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


