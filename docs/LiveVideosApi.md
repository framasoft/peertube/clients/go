# \LiveVideosApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddLive**](LiveVideosApi.md#AddLive) | **Post** /videos/live | Create a live
[**GetLiveId**](LiveVideosApi.md#GetLiveId) | **Get** /videos/live/{id} | Get information about a live
[**UpdateLiveId**](LiveVideosApi.md#UpdateLiveId) | **Put** /videos/live/{id} | Update information about a live



## AddLive

> VideoUploadResponse AddLive(ctx).ChannelId(channelId).Name(name).SaveReplay(saveReplay).PermanentLive(permanentLive).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).Execute()

Create a live

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    channelId := int32(56) // int32 | Channel id that will contain this live video
    name := "name_example" // string | Live video/replay name
    saveReplay := true // bool |  (optional)
    permanentLive := true // bool | User can stream multiple times in a permanent live (optional)
    thumbnailfile := os.NewFile(1234, "some_file") // *os.File | Live video/replay thumbnail file (optional)
    previewfile := os.NewFile(1234, "some_file") // *os.File | Live video/replay preview file (optional)
    privacy := openapiclient.VideoPrivacySet(1) // VideoPrivacySet |  (optional)
    category := int32(56) // int32 | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    licence := int32(56) // int32 | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    language := "language_example" // string | language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    description := "description_example" // string | Live video/replay description (optional)
    support := "support_example" // string | A text tell the audience how to support the creator (optional)
    nsfw := true // bool | Whether or not this live video/replay contains sensitive content (optional)
    tags := []string{"Inner_example"} // []string | Live video/replay tags (maximum 5 tags each between 2 and 30 characters) (optional)
    commentsEnabled := true // bool | Enable or disable comments for this live video/replay (optional)
    downloadEnabled := true // bool | Enable or disable downloading for the replay of this live video (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.LiveVideosApi.AddLive(context.Background()).ChannelId(channelId).Name(name).SaveReplay(saveReplay).PermanentLive(permanentLive).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LiveVideosApi.AddLive``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddLive`: VideoUploadResponse
    fmt.Fprintf(os.Stdout, "Response from `LiveVideosApi.AddLive`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAddLiveRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **int32** | Channel id that will contain this live video | 
 **name** | **string** | Live video/replay name | 
 **saveReplay** | **bool** |  | 
 **permanentLive** | **bool** | User can stream multiple times in a permanent live | 
 **thumbnailfile** | ***os.File** | Live video/replay thumbnail file | 
 **previewfile** | ***os.File** | Live video/replay preview file | 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | 
 **category** | **int32** | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **licence** | **int32** | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **language** | **string** | language id of the video (see [/videos/languages](#operation/getLanguages)) | 
 **description** | **string** | Live video/replay description | 
 **support** | **string** | A text tell the audience how to support the creator | 
 **nsfw** | **bool** | Whether or not this live video/replay contains sensitive content | 
 **tags** | **[]string** | Live video/replay tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **bool** | Enable or disable comments for this live video/replay | 
 **downloadEnabled** | **bool** | Enable or disable downloading for the replay of this live video | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetLiveId

> LiveVideoResponse GetLiveId(ctx, id).Execute()

Get information about a live

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.LiveVideosApi.GetLiveId(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LiveVideosApi.GetLiveId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetLiveId`: LiveVideoResponse
    fmt.Fprintf(os.Stdout, "Response from `LiveVideosApi.GetLiveId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetLiveIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**LiveVideoResponse**](LiveVideoResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateLiveId

> UpdateLiveId(ctx, id).LiveVideoUpdate(liveVideoUpdate).Execute()

Update information about a live

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := TODO // OneOfintegerUUIDstring | The object id, uuid or short uuid
    liveVideoUpdate := *openapiclient.NewLiveVideoUpdate() // LiveVideoUpdate |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.LiveVideosApi.UpdateLiveId(context.Background(), id).LiveVideoUpdate(liveVideoUpdate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LiveVideosApi.UpdateLiveId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | [**OneOfintegerUUIDstring**](.md) | The object id, uuid or short uuid | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateLiveIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **liveVideoUpdate** | [**LiveVideoUpdate**](LiveVideoUpdate.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

