# UserWithStatsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VideosCount** | Pointer to **int32** | Count of videos published | [optional] 
**AbusesCount** | Pointer to **int32** | Count of reports/abuses of which the user is a target | [optional] 
**AbusesAcceptedCount** | Pointer to **int32** | Count of reports/abuses created by the user and accepted/acted upon by the moderation team | [optional] 
**AbusesCreatedCount** | Pointer to **int32** | Count of reports/abuses created by the user | [optional] 
**VideoCommentsCount** | Pointer to **int32** | Count of comments published | [optional] 

## Methods

### NewUserWithStatsAllOf

`func NewUserWithStatsAllOf() *UserWithStatsAllOf`

NewUserWithStatsAllOf instantiates a new UserWithStatsAllOf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUserWithStatsAllOfWithDefaults

`func NewUserWithStatsAllOfWithDefaults() *UserWithStatsAllOf`

NewUserWithStatsAllOfWithDefaults instantiates a new UserWithStatsAllOf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVideosCount

`func (o *UserWithStatsAllOf) GetVideosCount() int32`

GetVideosCount returns the VideosCount field if non-nil, zero value otherwise.

### GetVideosCountOk

`func (o *UserWithStatsAllOf) GetVideosCountOk() (*int32, bool)`

GetVideosCountOk returns a tuple with the VideosCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideosCount

`func (o *UserWithStatsAllOf) SetVideosCount(v int32)`

SetVideosCount sets VideosCount field to given value.

### HasVideosCount

`func (o *UserWithStatsAllOf) HasVideosCount() bool`

HasVideosCount returns a boolean if a field has been set.

### GetAbusesCount

`func (o *UserWithStatsAllOf) GetAbusesCount() int32`

GetAbusesCount returns the AbusesCount field if non-nil, zero value otherwise.

### GetAbusesCountOk

`func (o *UserWithStatsAllOf) GetAbusesCountOk() (*int32, bool)`

GetAbusesCountOk returns a tuple with the AbusesCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbusesCount

`func (o *UserWithStatsAllOf) SetAbusesCount(v int32)`

SetAbusesCount sets AbusesCount field to given value.

### HasAbusesCount

`func (o *UserWithStatsAllOf) HasAbusesCount() bool`

HasAbusesCount returns a boolean if a field has been set.

### GetAbusesAcceptedCount

`func (o *UserWithStatsAllOf) GetAbusesAcceptedCount() int32`

GetAbusesAcceptedCount returns the AbusesAcceptedCount field if non-nil, zero value otherwise.

### GetAbusesAcceptedCountOk

`func (o *UserWithStatsAllOf) GetAbusesAcceptedCountOk() (*int32, bool)`

GetAbusesAcceptedCountOk returns a tuple with the AbusesAcceptedCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbusesAcceptedCount

`func (o *UserWithStatsAllOf) SetAbusesAcceptedCount(v int32)`

SetAbusesAcceptedCount sets AbusesAcceptedCount field to given value.

### HasAbusesAcceptedCount

`func (o *UserWithStatsAllOf) HasAbusesAcceptedCount() bool`

HasAbusesAcceptedCount returns a boolean if a field has been set.

### GetAbusesCreatedCount

`func (o *UserWithStatsAllOf) GetAbusesCreatedCount() int32`

GetAbusesCreatedCount returns the AbusesCreatedCount field if non-nil, zero value otherwise.

### GetAbusesCreatedCountOk

`func (o *UserWithStatsAllOf) GetAbusesCreatedCountOk() (*int32, bool)`

GetAbusesCreatedCountOk returns a tuple with the AbusesCreatedCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbusesCreatedCount

`func (o *UserWithStatsAllOf) SetAbusesCreatedCount(v int32)`

SetAbusesCreatedCount sets AbusesCreatedCount field to given value.

### HasAbusesCreatedCount

`func (o *UserWithStatsAllOf) HasAbusesCreatedCount() bool`

HasAbusesCreatedCount returns a boolean if a field has been set.

### GetVideoCommentsCount

`func (o *UserWithStatsAllOf) GetVideoCommentsCount() int32`

GetVideoCommentsCount returns the VideoCommentsCount field if non-nil, zero value otherwise.

### GetVideoCommentsCountOk

`func (o *UserWithStatsAllOf) GetVideoCommentsCountOk() (*int32, bool)`

GetVideoCommentsCountOk returns a tuple with the VideoCommentsCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoCommentsCount

`func (o *UserWithStatsAllOf) SetVideoCommentsCount(v int32)`

SetVideoCommentsCount sets VideoCommentsCount field to given value.

### HasVideoCommentsCount

`func (o *UserWithStatsAllOf) HasVideoCommentsCount() bool`

HasVideoCommentsCount returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


