# UserWatchingVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CurrentTime** | Pointer to **int32** | timestamp within the video, in seconds | [optional] 

## Methods

### NewUserWatchingVideo

`func NewUserWatchingVideo() *UserWatchingVideo`

NewUserWatchingVideo instantiates a new UserWatchingVideo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUserWatchingVideoWithDefaults

`func NewUserWatchingVideoWithDefaults() *UserWatchingVideo`

NewUserWatchingVideoWithDefaults instantiates a new UserWatchingVideo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCurrentTime

`func (o *UserWatchingVideo) GetCurrentTime() int32`

GetCurrentTime returns the CurrentTime field if non-nil, zero value otherwise.

### GetCurrentTimeOk

`func (o *UserWatchingVideo) GetCurrentTimeOk() (*int32, bool)`

GetCurrentTimeOk returns a tuple with the CurrentTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentTime

`func (o *UserWatchingVideo) SetCurrentTime(v int32)`

SetCurrentTime sets CurrentTime field to given value.

### HasCurrentTime

`func (o *UserWatchingVideo) HasCurrentTime() bool`

HasCurrentTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


