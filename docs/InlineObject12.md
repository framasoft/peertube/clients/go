# InlineObject12

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**State** | Pointer to [**AbuseStateSet**](AbuseStateSet.md) |  | [optional] 
**ModerationComment** | Pointer to **string** | Update the report comment visible only to the moderation team | [optional] 

## Methods

### NewInlineObject12

`func NewInlineObject12() *InlineObject12`

NewInlineObject12 instantiates a new InlineObject12 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject12WithDefaults

`func NewInlineObject12WithDefaults() *InlineObject12`

NewInlineObject12WithDefaults instantiates a new InlineObject12 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetState

`func (o *InlineObject12) GetState() AbuseStateSet`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *InlineObject12) GetStateOk() (*AbuseStateSet, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *InlineObject12) SetState(v AbuseStateSet)`

SetState sets State field to given value.

### HasState

`func (o *InlineObject12) HasState() bool`

HasState returns a boolean if a field has been set.

### GetModerationComment

`func (o *InlineObject12) GetModerationComment() string`

GetModerationComment returns the ModerationComment field if non-nil, zero value otherwise.

### GetModerationCommentOk

`func (o *InlineObject12) GetModerationCommentOk() (*string, bool)`

GetModerationCommentOk returns a tuple with the ModerationComment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModerationComment

`func (o *InlineObject12) SetModerationComment(v string)`

SetModerationComment sets ModerationComment field to given value.

### HasModerationComment

`func (o *InlineObject12) HasModerationComment() bool`

HasModerationComment returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


