# \VideoBlacklistApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VideosBlacklistGet**](VideoBlacklistApi.md#VideosBlacklistGet) | **Get** /videos/blacklist | List blacklisted videos
[**VideosIdBlacklistDelete**](VideoBlacklistApi.md#VideosIdBlacklistDelete) | **Delete** /videos/{id}/blacklist | Delete an entry of the blacklist of a video by its id
[**VideosIdBlacklistPost**](VideoBlacklistApi.md#VideosIdBlacklistPost) | **Post** /videos/{id}/blacklist | Blacklist a video



## VideosBlacklistGet

> []VideoBlacklist VideosBlacklistGet(ctx, optional)

List blacklisted videos

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***VideosBlacklistGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosBlacklistGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items (max: 100) | 
 **sort** | **optional.String**| Sort blacklists by criteria | 

### Return type

[**[]VideoBlacklist**](VideoBlacklist.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdBlacklistDelete

> VideosIdBlacklistDelete(ctx, id)

Delete an entry of the blacklist of a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The object id or uuid | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdBlacklistPost

> VideosIdBlacklistPost(ctx, id)

Blacklist a video

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The object id or uuid | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

