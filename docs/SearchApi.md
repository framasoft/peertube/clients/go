# \SearchApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SearchChannels**](SearchApi.md#SearchChannels) | **Get** /search/video-channels | Search channels
[**SearchPlaylists**](SearchApi.md#SearchPlaylists) | **Get** /search/video-playlists | Search playlists
[**SearchVideos**](SearchApi.md#SearchVideos) | **Get** /search/videos | Search videos



## SearchChannels

> VideoChannelList SearchChannels(ctx).Search(search).Start(start).Count(count).SearchTarget(searchTarget).Sort(sort).Execute()

Search channels

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    search := "search_example" // string | String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete channel information and interact with it. 
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    searchTarget := "searchTarget_example" // string | If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn't have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  (optional)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.SearchApi.SearchChannels(context.Background()).Search(search).Start(start).Count(count).SearchTarget(searchTarget).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SearchApi.SearchChannels``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SearchChannels`: VideoChannelList
    fmt.Fprintf(os.Stdout, "Response from `SearchApi.SearchChannels`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSearchChannelsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **string** | String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete channel information and interact with it.  | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **searchTarget** | **string** | If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  | 
 **sort** | **string** | Sort column | 

### Return type

[**VideoChannelList**](VideoChannelList.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SearchPlaylists

> InlineResponse20011 SearchPlaylists(ctx).Search(search).Start(start).Count(count).SearchTarget(searchTarget).Sort(sort).Execute()

Search playlists

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    search := "search_example" // string | String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete playlist information and interact with it. 
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    searchTarget := "searchTarget_example" // string | If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn't have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  (optional)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.SearchApi.SearchPlaylists(context.Background()).Search(search).Start(start).Count(count).SearchTarget(searchTarget).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SearchApi.SearchPlaylists``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SearchPlaylists`: InlineResponse20011
    fmt.Fprintf(os.Stdout, "Response from `SearchApi.SearchPlaylists`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSearchPlaylistsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **string** | String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete playlist information and interact with it.  | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **searchTarget** | **string** | If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  | 
 **sort** | **string** | Sort column | 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SearchVideos

> VideoListResponse SearchVideos(ctx).Search(search).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).SearchTarget(searchTarget).Sort(sort).StartDate(startDate).EndDate(endDate).OriginallyPublishedStartDate(originallyPublishedStartDate).OriginallyPublishedEndDate(originallyPublishedEndDate).DurationMin(durationMin).DurationMax(durationMax).Execute()

Search videos

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "./openapi"
)

func main() {
    search := "search_example" // string | String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete video information and interact with it. 
    categoryOneOf := TODO // OneOfintegerarray | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    isLive := true // bool | whether or not the video is a live (optional)
    tagsOneOf := TODO // OneOfstringarray | tag(s) of the video (optional)
    tagsAllOf := TODO // OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
    licenceOneOf := TODO // OneOfintegerarray | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    languageOneOf := TODO // OneOfstringarray | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language (optional)
    nsfw := "nsfw_example" // string | whether to include nsfw videos, if any (optional)
    filter := "filter_example" // string | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    skipCount := "skipCount_example" // string | if you don't need the `total` in the response (optional) (default to "false")
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    searchTarget := "searchTarget_example" // string | If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn't have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  (optional)
    sort := "sort_example" // string | Sort videos by criteria (optional)
    startDate := time.Now() // time.Time | Get videos that are published after this date (optional)
    endDate := time.Now() // time.Time | Get videos that are published before this date (optional)
    originallyPublishedStartDate := time.Now() // time.Time | Get videos that are originally published after this date (optional)
    originallyPublishedEndDate := time.Now() // time.Time | Get videos that are originally published before this date (optional)
    durationMin := int32(56) // int32 | Get videos that have this minimum duration (optional)
    durationMax := int32(56) // int32 | Get videos that have this maximum duration (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.SearchApi.SearchVideos(context.Background()).Search(search).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).SearchTarget(searchTarget).Sort(sort).StartDate(startDate).EndDate(endDate).OriginallyPublishedStartDate(originallyPublishedStartDate).OriginallyPublishedEndDate(originallyPublishedEndDate).DurationMin(durationMin).DurationMax(durationMax).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SearchApi.SearchVideos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SearchVideos`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `SearchApi.SearchVideos`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSearchVideosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **string** | String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete video information and interact with it.  | 
 **categoryOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **isLive** | **bool** | whether or not the video is a live | 
 **tagsOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video | 
 **tagsAllOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video, where all should be present in the video | 
 **licenceOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **languageOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | 
 **nsfw** | **string** | whether to include nsfw videos, if any | 
 **filter** | **string** | Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | 
 **skipCount** | **string** | if you don&#39;t need the &#x60;total&#x60; in the response | [default to &quot;false&quot;]
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **searchTarget** | **string** | If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  | 
 **sort** | **string** | Sort videos by criteria | 
 **startDate** | **time.Time** | Get videos that are published after this date | 
 **endDate** | **time.Time** | Get videos that are published before this date | 
 **originallyPublishedStartDate** | **time.Time** | Get videos that are originally published after this date | 
 **originallyPublishedEndDate** | **time.Time** | Get videos that are originally published before this date | 
 **durationMin** | **int32** | Get videos that have this minimum duration | 
 **durationMax** | **int32** | Get videos that have this maximum duration | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

