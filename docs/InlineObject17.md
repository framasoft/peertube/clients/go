# InlineObject17

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StartPosition** | **int32** | Start position of the element to reorder | 
**InsertAfterPosition** | **int32** | New position for the block to reorder, to add the block before the first element | 
**ReorderLength** | **int32** | How many element from &#x60;startPosition&#x60; to reorder | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


