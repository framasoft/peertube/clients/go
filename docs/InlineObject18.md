# InlineObject18

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StartTimestamp** | **int32** | Start the video at this specific timestamp (in seconds) | [optional] 
**StopTimestamp** | **int32** | Stop the video at this specific timestamp (in seconds) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


