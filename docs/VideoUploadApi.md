# \VideoUploadApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ImportVideo**](VideoUploadApi.md#ImportVideo) | **Post** /videos/imports | Import a video
[**UploadLegacy**](VideoUploadApi.md#UploadLegacy) | **Post** /videos/upload | Upload a video
[**UploadResumable**](VideoUploadApi.md#UploadResumable) | **Put** /videos/upload-resumable | Send chunk for the resumable upload of a video
[**UploadResumableCancel**](VideoUploadApi.md#UploadResumableCancel) | **Delete** /videos/upload-resumable | Cancel the resumable upload of a video, deleting any data uploaded so far
[**UploadResumableInit**](VideoUploadApi.md#UploadResumableInit) | **Post** /videos/upload-resumable | Initialize the resumable upload of a video



## ImportVideo

> VideoUploadResponse ImportVideo(ctx).Name(name).ChannelId(channelId).TargetUrl(targetUrl).MagnetUri(magnetUri).Torrentfile(torrentfile).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Execute()

Import a video



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "./openapi"
)

func main() {
    name := "name_example" // string | Video name
    channelId := int32(56) // int32 | Channel id that will contain this video
    targetUrl := TODO // TargetUrl |  (optional)
    magnetUri := TODO // MagnetUri |  (optional)
    torrentfile := TODO // Torrentfile |  (optional)
    privacy := openapiclient.VideoPrivacySet(1) // VideoPrivacySet |  (optional)
    category := int32(56) // int32 | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    licence := int32(56) // int32 | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    language := "language_example" // string | language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    description := "description_example" // string | Video description (optional)
    waitTranscoding := true // bool | Whether or not we wait transcoding before publish the video (optional)
    support := "support_example" // string | A text tell the audience how to support the video creator (optional)
    nsfw := true // bool | Whether or not this video contains sensitive content (optional)
    tags := []string{"Inner_example"} // []string | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    commentsEnabled := true // bool | Enable or disable comments for this video (optional)
    downloadEnabled := true // bool | Enable or disable downloading for this video (optional)
    originallyPublishedAt := time.Now() // time.Time | Date when the content was originally published (optional)
    scheduleUpdate := *openapiclient.NewVideoScheduledUpdate(time.Now()) // VideoScheduledUpdate |  (optional)
    thumbnailfile := os.NewFile(1234, "some_file") // *os.File | Video thumbnail file (optional)
    previewfile := os.NewFile(1234, "some_file") // *os.File | Video preview file (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoUploadApi.ImportVideo(context.Background()).Name(name).ChannelId(channelId).TargetUrl(targetUrl).MagnetUri(magnetUri).Torrentfile(torrentfile).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoUploadApi.ImportVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ImportVideo`: VideoUploadResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoUploadApi.ImportVideo`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiImportVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string** | Video name | 
 **channelId** | **int32** | Channel id that will contain this video | 
 **targetUrl** | [**TargetUrl**](TargetUrl.md) |  | 
 **magnetUri** | [**MagnetUri**](MagnetUri.md) |  | 
 **torrentfile** | [**Torrentfile**](Torrentfile.md) |  | 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | 
 **category** | **int32** | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **licence** | **int32** | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **language** | **string** | language id of the video (see [/videos/languages](#operation/getLanguages)) | 
 **description** | **string** | Video description | 
 **waitTranscoding** | **bool** | Whether or not we wait transcoding before publish the video | 
 **support** | **string** | A text tell the audience how to support the video creator | 
 **nsfw** | **bool** | Whether or not this video contains sensitive content | 
 **tags** | **[]string** | Video tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **bool** | Enable or disable comments for this video | 
 **downloadEnabled** | **bool** | Enable or disable downloading for this video | 
 **originallyPublishedAt** | **time.Time** | Date when the content was originally published | 
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | 
 **thumbnailfile** | ***os.File** | Video thumbnail file | 
 **previewfile** | ***os.File** | Video preview file | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadLegacy

> VideoUploadResponse UploadLegacy(ctx).Name(name).ChannelId(channelId).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Videofile(videofile).Execute()

Upload a video



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "./openapi"
)

func main() {
    name := "name_example" // string | Video name (optional)
    channelId := int32(56) // int32 | Channel id that will contain this video (optional)
    privacy := openapiclient.VideoPrivacySet(1) // VideoPrivacySet |  (optional)
    category := int32(56) // int32 | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    licence := int32(56) // int32 | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    language := "language_example" // string | language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    description := "description_example" // string | Video description (optional)
    waitTranscoding := true // bool | Whether or not we wait transcoding before publish the video (optional)
    support := "support_example" // string | A text tell the audience how to support the video creator (optional)
    nsfw := true // bool | Whether or not this video contains sensitive content (optional)
    tags := []string{"Inner_example"} // []string | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    commentsEnabled := true // bool | Enable or disable comments for this video (optional)
    downloadEnabled := true // bool | Enable or disable downloading for this video (optional)
    originallyPublishedAt := time.Now() // time.Time | Date when the content was originally published (optional)
    scheduleUpdate := *openapiclient.NewVideoScheduledUpdate(time.Now()) // VideoScheduledUpdate |  (optional)
    thumbnailfile := os.NewFile(1234, "some_file") // *os.File | Video thumbnail file (optional)
    previewfile := os.NewFile(1234, "some_file") // *os.File | Video preview file (optional)
    videofile := os.NewFile(1234, "some_file") // *os.File | Video file (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoUploadApi.UploadLegacy(context.Background()).Name(name).ChannelId(channelId).Privacy(privacy).Category(category).Licence(licence).Language(language).Description(description).WaitTranscoding(waitTranscoding).Support(support).Nsfw(nsfw).Tags(tags).CommentsEnabled(commentsEnabled).DownloadEnabled(downloadEnabled).OriginallyPublishedAt(originallyPublishedAt).ScheduleUpdate(scheduleUpdate).Thumbnailfile(thumbnailfile).Previewfile(previewfile).Videofile(videofile).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoUploadApi.UploadLegacy``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UploadLegacy`: VideoUploadResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoUploadApi.UploadLegacy`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUploadLegacyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string** | Video name | 
 **channelId** | **int32** | Channel id that will contain this video | 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | 
 **category** | **int32** | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **licence** | **int32** | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **language** | **string** | language id of the video (see [/videos/languages](#operation/getLanguages)) | 
 **description** | **string** | Video description | 
 **waitTranscoding** | **bool** | Whether or not we wait transcoding before publish the video | 
 **support** | **string** | A text tell the audience how to support the video creator | 
 **nsfw** | **bool** | Whether or not this video contains sensitive content | 
 **tags** | **[]string** | Video tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **bool** | Enable or disable comments for this video | 
 **downloadEnabled** | **bool** | Enable or disable downloading for this video | 
 **originallyPublishedAt** | **time.Time** | Date when the content was originally published | 
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | 
 **thumbnailfile** | ***os.File** | Video thumbnail file | 
 **previewfile** | ***os.File** | Video preview file | 
 **videofile** | ***os.File** | Video file | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadResumable

> VideoUploadResponse UploadResumable(ctx).UploadId(uploadId).ContentRange(contentRange).ContentLength(contentLength).Body(body).Execute()

Send chunk for the resumable upload of a video



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    uploadId := "uploadId_example" // string | Created session id to proceed with. If you didn't send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload. 
    contentRange := "bytes 0-262143/2469036" // string | Specifies the bytes in the file that the request is uploading.  For example, a value of `bytes 0-262143/1000000` shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file. 
    contentLength := float32(262144) // float32 | Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn't mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube's web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health. 
    body := os.NewFile(1234, "some_file") // *os.File |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoUploadApi.UploadResumable(context.Background()).UploadId(uploadId).ContentRange(contentRange).ContentLength(contentLength).Body(body).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoUploadApi.UploadResumable``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UploadResumable`: VideoUploadResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoUploadApi.UploadResumable`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUploadResumableRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadId** | **string** | Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload.  | 
 **contentRange** | **string** | Specifies the bytes in the file that the request is uploading.  For example, a value of &#x60;bytes 0-262143/1000000&#x60; shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file.  | 
 **contentLength** | **float32** | Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn&#39;t mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube&#39;s web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health.  | 
 **body** | ***os.File** |  | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/octet-stream
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadResumableCancel

> UploadResumableCancel(ctx).UploadId(uploadId).ContentLength(contentLength).Execute()

Cancel the resumable upload of a video, deleting any data uploaded so far



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    uploadId := "uploadId_example" // string | Created session id to proceed with. If you didn't send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-) 
    contentLength := float32(0) // float32 | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoUploadApi.UploadResumableCancel(context.Background()).UploadId(uploadId).ContentLength(contentLength).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoUploadApi.UploadResumableCancel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUploadResumableCancelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadId** | **string** | Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-)  | 
 **contentLength** | **float32** |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadResumableInit

> UploadResumableInit(ctx).XUploadContentLength(xUploadContentLength).XUploadContentType(xUploadContentType).VideoUploadRequestResumable(videoUploadRequestResumable).Execute()

Initialize the resumable upload of a video



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    xUploadContentLength := float32(2469036) // float32 | Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading.
    xUploadContentType := "video/mp4" // string | MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary.
    videoUploadRequestResumable := *openapiclient.NewVideoUploadRequestResumable("What is PeerTube?", int32(3), "what_is_peertube.mp4") // VideoUploadRequestResumable |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoUploadApi.UploadResumableInit(context.Background()).XUploadContentLength(xUploadContentLength).XUploadContentType(xUploadContentType).VideoUploadRequestResumable(videoUploadRequestResumable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoUploadApi.UploadResumableInit``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUploadResumableInitRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xUploadContentLength** | **float32** | Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading. | 
 **xUploadContentType** | **string** | MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary. | 
 **videoUploadRequestResumable** | [**VideoUploadRequestResumable**](VideoUploadRequestResumable.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

