# VideoChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | Pointer to **string** | editable name of the channel, displayed in its representations | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**Support** | Pointer to **string** | text shown by default on all videos of this channel, to tell the audience how to support it | [optional] 
**Id** | Pointer to **int32** |  | [optional] [readonly] 
**IsLocal** | Pointer to **bool** |  | [optional] [readonly] 
**UpdatedAt** | Pointer to **time.Time** |  | [optional] [readonly] 
**OwnerAccount** | Pointer to [**NullableVideoChannelOwnerAccount**](VideoChannelOwnerAccount.md) |  | [optional] 

## Methods

### NewVideoChannel

`func NewVideoChannel() *VideoChannel`

NewVideoChannel instantiates a new VideoChannel object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVideoChannelWithDefaults

`func NewVideoChannelWithDefaults() *VideoChannel`

NewVideoChannelWithDefaults instantiates a new VideoChannel object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDisplayName

`func (o *VideoChannel) GetDisplayName() string`

GetDisplayName returns the DisplayName field if non-nil, zero value otherwise.

### GetDisplayNameOk

`func (o *VideoChannel) GetDisplayNameOk() (*string, bool)`

GetDisplayNameOk returns a tuple with the DisplayName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayName

`func (o *VideoChannel) SetDisplayName(v string)`

SetDisplayName sets DisplayName field to given value.

### HasDisplayName

`func (o *VideoChannel) HasDisplayName() bool`

HasDisplayName returns a boolean if a field has been set.

### GetDescription

`func (o *VideoChannel) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *VideoChannel) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *VideoChannel) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *VideoChannel) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetSupport

`func (o *VideoChannel) GetSupport() string`

GetSupport returns the Support field if non-nil, zero value otherwise.

### GetSupportOk

`func (o *VideoChannel) GetSupportOk() (*string, bool)`

GetSupportOk returns a tuple with the Support field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSupport

`func (o *VideoChannel) SetSupport(v string)`

SetSupport sets Support field to given value.

### HasSupport

`func (o *VideoChannel) HasSupport() bool`

HasSupport returns a boolean if a field has been set.

### GetId

`func (o *VideoChannel) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VideoChannel) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VideoChannel) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *VideoChannel) HasId() bool`

HasId returns a boolean if a field has been set.

### GetIsLocal

`func (o *VideoChannel) GetIsLocal() bool`

GetIsLocal returns the IsLocal field if non-nil, zero value otherwise.

### GetIsLocalOk

`func (o *VideoChannel) GetIsLocalOk() (*bool, bool)`

GetIsLocalOk returns a tuple with the IsLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsLocal

`func (o *VideoChannel) SetIsLocal(v bool)`

SetIsLocal sets IsLocal field to given value.

### HasIsLocal

`func (o *VideoChannel) HasIsLocal() bool`

HasIsLocal returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *VideoChannel) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *VideoChannel) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *VideoChannel) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *VideoChannel) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.

### GetOwnerAccount

`func (o *VideoChannel) GetOwnerAccount() VideoChannelOwnerAccount`

GetOwnerAccount returns the OwnerAccount field if non-nil, zero value otherwise.

### GetOwnerAccountOk

`func (o *VideoChannel) GetOwnerAccountOk() (*VideoChannelOwnerAccount, bool)`

GetOwnerAccountOk returns a tuple with the OwnerAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwnerAccount

`func (o *VideoChannel) SetOwnerAccount(v VideoChannelOwnerAccount)`

SetOwnerAccount sets OwnerAccount field to given value.

### HasOwnerAccount

`func (o *VideoChannel) HasOwnerAccount() bool`

HasOwnerAccount returns a boolean if a field has been set.

### SetOwnerAccountNil

`func (o *VideoChannel) SetOwnerAccountNil(b bool)`

 SetOwnerAccountNil sets the value for OwnerAccount to be an explicit nil

### UnsetOwnerAccount
`func (o *VideoChannel) UnsetOwnerAccount()`

UnsetOwnerAccount ensures that no value is present for OwnerAccount, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


