# \VideosApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddVideoPlaylistVideo**](VideosApi.md#AddVideoPlaylistVideo) | **Post** /video-playlists/{playlistId}/videos | Add a video in a playlist
[**GetVideoPlaylistVideos**](VideosApi.md#GetVideoPlaylistVideos) | **Get** /video-playlists/{playlistId}/videos | List videos of a playlist
[**UsersMeSubscriptionsVideosGet**](VideosApi.md#UsersMeSubscriptionsVideosGet) | **Get** /users/me/subscriptions/videos | List videos of subscriptions of my user
[**UsersMeVideosGet**](VideosApi.md#UsersMeVideosGet) | **Get** /users/me/videos | Get videos of my user
[**UsersMeVideosImportsGet**](VideosApi.md#UsersMeVideosImportsGet) | **Get** /users/me/videos/imports | Get video imports of my user



## AddVideoPlaylistVideo

> InlineResponse20013 AddVideoPlaylistVideo(ctx, playlistId).InlineObject19(inlineObject19).Execute()

Add a video in a playlist

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id
    inlineObject19 := *openapiclient.NewInlineObject19("TODO") // InlineObject19 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideosApi.AddVideoPlaylistVideo(context.Background(), playlistId).InlineObject19(inlineObject19).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideosApi.AddVideoPlaylistVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddVideoPlaylistVideo`: InlineResponse20013
    fmt.Fprintf(os.Stdout, "Response from `VideosApi.AddVideoPlaylistVideo`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddVideoPlaylistVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **inlineObject19** | [**InlineObject19**](InlineObject19.md) |  | 

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideoPlaylistVideos

> VideoListResponse GetVideoPlaylistVideos(ctx, playlistId).Execute()

List videos of a playlist

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideosApi.GetVideoPlaylistVideos(context.Background(), playlistId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideosApi.GetVideoPlaylistVideos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideoPlaylistVideos`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `VideosApi.GetVideoPlaylistVideos`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoPlaylistVideosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeSubscriptionsVideosGet

> VideoListResponse UsersMeSubscriptionsVideosGet(ctx).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()

List videos of subscriptions of my user

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    categoryOneOf := TODO // OneOfintegerarray | category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    isLive := true // bool | whether or not the video is a live (optional)
    tagsOneOf := TODO // OneOfstringarray | tag(s) of the video (optional)
    tagsAllOf := TODO // OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
    licenceOneOf := TODO // OneOfintegerarray | licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    languageOneOf := TODO // OneOfstringarray | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language (optional)
    nsfw := "nsfw_example" // string | whether to include nsfw videos, if any (optional)
    filter := "filter_example" // string | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    skipCount := "skipCount_example" // string | if you don't need the `total` in the response (optional) (default to "false")
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "sort_example" // string | Sort videos by criteria (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideosApi.UsersMeSubscriptionsVideosGet(context.Background()).CategoryOneOf(categoryOneOf).IsLive(isLive).TagsOneOf(tagsOneOf).TagsAllOf(tagsAllOf).LicenceOneOf(licenceOneOf).LanguageOneOf(languageOneOf).Nsfw(nsfw).Filter(filter).SkipCount(skipCount).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideosApi.UsersMeSubscriptionsVideosGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeSubscriptionsVideosGet`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `VideosApi.UsersMeSubscriptionsVideosGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeSubscriptionsVideosGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | category id of the video (see [/videos/categories](#operation/getCategories)) | 
 **isLive** | **bool** | whether or not the video is a live | 
 **tagsOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video | 
 **tagsAllOf** | [**OneOfstringarray**](OneOfstringarray.md) | tag(s) of the video, where all should be present in the video | 
 **licenceOneOf** | [**OneOfintegerarray**](OneOfintegerarray.md) | licence id of the video (see [/videos/licences](#operation/getLicences)) | 
 **languageOneOf** | [**OneOfstringarray**](OneOfstringarray.md) | language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | 
 **nsfw** | **string** | whether to include nsfw videos, if any | 
 **filter** | **string** | Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | 
 **skipCount** | **string** | if you don&#39;t need the &#x60;total&#x60; in the response | [default to &quot;false&quot;]
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort videos by criteria | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideosGet

> VideoListResponse UsersMeVideosGet(ctx).Start(start).Count(count).Sort(sort).Execute()

Get videos of my user

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideosApi.UsersMeVideosGet(context.Background()).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideosApi.UsersMeVideosGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeVideosGet`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `VideosApi.UsersMeVideosGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeVideosGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideosImportsGet

> VideoImportsList UsersMeVideosImportsGet(ctx).Start(start).Count(count).Sort(sort).Execute()

Get video imports of my user

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideosApi.UsersMeVideosImportsGet(context.Background()).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideosApi.UsersMeVideosImportsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeVideosImportsGet`: VideoImportsList
    fmt.Fprintf(os.Stdout, "Response from `VideosApi.UsersMeVideosImportsGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeVideosImportsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**VideoImportsList**](VideoImportsList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

