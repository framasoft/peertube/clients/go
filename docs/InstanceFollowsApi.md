# \InstanceFollowsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ServerFollowersGet**](InstanceFollowsApi.md#ServerFollowersGet) | **Get** /server/followers | List instances following the server
[**ServerFollowersNameWithHostAcceptPost**](InstanceFollowsApi.md#ServerFollowersNameWithHostAcceptPost) | **Post** /server/followers/{nameWithHost}/accept | Accept a pending follower to your server
[**ServerFollowersNameWithHostDelete**](InstanceFollowsApi.md#ServerFollowersNameWithHostDelete) | **Delete** /server/followers/{nameWithHost} | Remove or reject a follower to your server
[**ServerFollowersNameWithHostRejectPost**](InstanceFollowsApi.md#ServerFollowersNameWithHostRejectPost) | **Post** /server/followers/{nameWithHost}/reject | Reject a pending follower to your server
[**ServerFollowingGet**](InstanceFollowsApi.md#ServerFollowingGet) | **Get** /server/following | List instances followed by the server
[**ServerFollowingHostOrHandleDelete**](InstanceFollowsApi.md#ServerFollowingHostOrHandleDelete) | **Delete** /server/following/{hostOrHandle} | Unfollow an actor (PeerTube instance, channel or account)
[**ServerFollowingPost**](InstanceFollowsApi.md#ServerFollowingPost) | **Post** /server/following | Follow a list of actors (PeerTube instance, channel or account)



## ServerFollowersGet

> InlineResponse2001 ServerFollowersGet(ctx).State(state).ActorType(actorType).Start(start).Count(count).Sort(sort).Execute()

List instances following the server

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string |  (optional)
    actorType := "actorType_example" // string |  (optional)
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.InstanceFollowsApi.ServerFollowersGet(context.Background()).State(state).ActorType(actorType).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InstanceFollowsApi.ServerFollowersGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ServerFollowersGet`: InlineResponse2001
    fmt.Fprintf(os.Stdout, "Response from `InstanceFollowsApi.ServerFollowersGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiServerFollowersGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **string** |  | 
 **actorType** | **string** |  | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerFollowersNameWithHostAcceptPost

> ServerFollowersNameWithHostAcceptPost(ctx, nameWithHost).Execute()

Accept a pending follower to your server

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    nameWithHost := "nameWithHost@example.com" // string | The remote actor handle to remove from your followers

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.InstanceFollowsApi.ServerFollowersNameWithHostAcceptPost(context.Background(), nameWithHost).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InstanceFollowsApi.ServerFollowersNameWithHostAcceptPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**nameWithHost** | [**string**](.md) | The remote actor handle to remove from your followers | 

### Other Parameters

Other parameters are passed through a pointer to a apiServerFollowersNameWithHostAcceptPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerFollowersNameWithHostDelete

> ServerFollowersNameWithHostDelete(ctx, nameWithHost).Execute()

Remove or reject a follower to your server

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    nameWithHost := "nameWithHost@example.com" // string | The remote actor handle to remove from your followers

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.InstanceFollowsApi.ServerFollowersNameWithHostDelete(context.Background(), nameWithHost).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InstanceFollowsApi.ServerFollowersNameWithHostDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**nameWithHost** | [**string**](.md) | The remote actor handle to remove from your followers | 

### Other Parameters

Other parameters are passed through a pointer to a apiServerFollowersNameWithHostDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerFollowersNameWithHostRejectPost

> ServerFollowersNameWithHostRejectPost(ctx, nameWithHost).Execute()

Reject a pending follower to your server

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    nameWithHost := "nameWithHost@example.com" // string | The remote actor handle to remove from your followers

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.InstanceFollowsApi.ServerFollowersNameWithHostRejectPost(context.Background(), nameWithHost).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InstanceFollowsApi.ServerFollowersNameWithHostRejectPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**nameWithHost** | [**string**](.md) | The remote actor handle to remove from your followers | 

### Other Parameters

Other parameters are passed through a pointer to a apiServerFollowersNameWithHostRejectPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerFollowingGet

> InlineResponse2001 ServerFollowingGet(ctx).State(state).ActorType(actorType).Start(start).Count(count).Sort(sort).Execute()

List instances followed by the server

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string |  (optional)
    actorType := "actorType_example" // string |  (optional)
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.InstanceFollowsApi.ServerFollowingGet(context.Background()).State(state).ActorType(actorType).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InstanceFollowsApi.ServerFollowingGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ServerFollowingGet`: InlineResponse2001
    fmt.Fprintf(os.Stdout, "Response from `InstanceFollowsApi.ServerFollowingGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiServerFollowingGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **string** |  | 
 **actorType** | **string** |  | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerFollowingHostOrHandleDelete

> ServerFollowingHostOrHandleDelete(ctx, hostOrHandle).Execute()

Unfollow an actor (PeerTube instance, channel or account)

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    hostOrHandle := "hostOrHandle_example" // string | The hostOrHandle to unfollow

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.InstanceFollowsApi.ServerFollowingHostOrHandleDelete(context.Background(), hostOrHandle).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InstanceFollowsApi.ServerFollowingHostOrHandleDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**hostOrHandle** | **string** | The hostOrHandle to unfollow | 

### Other Parameters

Other parameters are passed through a pointer to a apiServerFollowingHostOrHandleDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerFollowingPost

> ServerFollowingPost(ctx).InlineObject1(inlineObject1).Execute()

Follow a list of actors (PeerTube instance, channel or account)

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    inlineObject1 := *openapiclient.NewInlineObject1() // InlineObject1 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.InstanceFollowsApi.ServerFollowingPost(context.Background()).InlineObject1(inlineObject1).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InstanceFollowsApi.ServerFollowingPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiServerFollowingPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject1** | [**InlineObject1**](InlineObject1.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

