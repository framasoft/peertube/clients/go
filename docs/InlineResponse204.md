# InlineResponse204

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VideoChannel** | Pointer to [**InlineResponse204VideoChannel**](InlineResponse204VideoChannel.md) |  | [optional] 

## Methods

### NewInlineResponse204

`func NewInlineResponse204() *InlineResponse204`

NewInlineResponse204 instantiates a new InlineResponse204 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse204WithDefaults

`func NewInlineResponse204WithDefaults() *InlineResponse204`

NewInlineResponse204WithDefaults instantiates a new InlineResponse204 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVideoChannel

`func (o *InlineResponse204) GetVideoChannel() InlineResponse204VideoChannel`

GetVideoChannel returns the VideoChannel field if non-nil, zero value otherwise.

### GetVideoChannelOk

`func (o *InlineResponse204) GetVideoChannelOk() (*InlineResponse204VideoChannel, bool)`

GetVideoChannelOk returns a tuple with the VideoChannel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoChannel

`func (o *InlineResponse204) SetVideoChannel(v InlineResponse204VideoChannel)`

SetVideoChannel sets VideoChannel field to given value.

### HasVideoChannel

`func (o *InlineResponse204) HasVideoChannel() bool`

HasVideoChannel returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


