# InlineResponse204VideoChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int32** |  | [optional] 

## Methods

### NewInlineResponse204VideoChannel

`func NewInlineResponse204VideoChannel() *InlineResponse204VideoChannel`

NewInlineResponse204VideoChannel instantiates a new InlineResponse204VideoChannel object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse204VideoChannelWithDefaults

`func NewInlineResponse204VideoChannelWithDefaults() *InlineResponse204VideoChannel`

NewInlineResponse204VideoChannelWithDefaults instantiates a new InlineResponse204VideoChannel object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *InlineResponse204VideoChannel) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *InlineResponse204VideoChannel) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *InlineResponse204VideoChannel) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *InlineResponse204VideoChannel) HasId() bool`

HasId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


