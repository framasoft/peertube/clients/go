# InlineResponse20014

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VideoId** | Pointer to [**[]InlineResponse20014VideoId**](InlineResponse20014VideoId.md) |  | [optional] 

## Methods

### NewInlineResponse20014

`func NewInlineResponse20014() *InlineResponse20014`

NewInlineResponse20014 instantiates a new InlineResponse20014 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse20014WithDefaults

`func NewInlineResponse20014WithDefaults() *InlineResponse20014`

NewInlineResponse20014WithDefaults instantiates a new InlineResponse20014 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVideoId

`func (o *InlineResponse20014) GetVideoId() []InlineResponse20014VideoId`

GetVideoId returns the VideoId field if non-nil, zero value otherwise.

### GetVideoIdOk

`func (o *InlineResponse20014) GetVideoIdOk() (*[]InlineResponse20014VideoId, bool)`

GetVideoIdOk returns a tuple with the VideoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoId

`func (o *InlineResponse20014) SetVideoId(v []InlineResponse20014VideoId)`

SetVideoId sets VideoId field to given value.

### HasVideoId

`func (o *InlineResponse20014) HasVideoId() bool`

HasVideoId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


