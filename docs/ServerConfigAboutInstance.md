# ServerConfigAboutInstance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**ShortDescription** | Pointer to **string** |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**Terms** | Pointer to **string** |  | [optional] 

## Methods

### NewServerConfigAboutInstance

`func NewServerConfigAboutInstance() *ServerConfigAboutInstance`

NewServerConfigAboutInstance instantiates a new ServerConfigAboutInstance object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewServerConfigAboutInstanceWithDefaults

`func NewServerConfigAboutInstanceWithDefaults() *ServerConfigAboutInstance`

NewServerConfigAboutInstanceWithDefaults instantiates a new ServerConfigAboutInstance object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ServerConfigAboutInstance) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ServerConfigAboutInstance) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ServerConfigAboutInstance) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ServerConfigAboutInstance) HasName() bool`

HasName returns a boolean if a field has been set.

### GetShortDescription

`func (o *ServerConfigAboutInstance) GetShortDescription() string`

GetShortDescription returns the ShortDescription field if non-nil, zero value otherwise.

### GetShortDescriptionOk

`func (o *ServerConfigAboutInstance) GetShortDescriptionOk() (*string, bool)`

GetShortDescriptionOk returns a tuple with the ShortDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetShortDescription

`func (o *ServerConfigAboutInstance) SetShortDescription(v string)`

SetShortDescription sets ShortDescription field to given value.

### HasShortDescription

`func (o *ServerConfigAboutInstance) HasShortDescription() bool`

HasShortDescription returns a boolean if a field has been set.

### GetDescription

`func (o *ServerConfigAboutInstance) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ServerConfigAboutInstance) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ServerConfigAboutInstance) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ServerConfigAboutInstance) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetTerms

`func (o *ServerConfigAboutInstance) GetTerms() string`

GetTerms returns the Terms field if non-nil, zero value otherwise.

### GetTermsOk

`func (o *ServerConfigAboutInstance) GetTermsOk() (*string, bool)`

GetTermsOk returns a tuple with the Terms field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerms

`func (o *ServerConfigAboutInstance) SetTerms(v string)`

SetTerms sets Terms field to given value.

### HasTerms

`func (o *ServerConfigAboutInstance) HasTerms() bool`

HasTerms returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


