# InlineObject28

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VideoId** | **int32** |  | 

## Methods

### NewInlineObject28

`func NewInlineObject28(videoId int32, ) *InlineObject28`

NewInlineObject28 instantiates a new InlineObject28 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObject28WithDefaults

`func NewInlineObject28WithDefaults() *InlineObject28`

NewInlineObject28WithDefaults instantiates a new InlineObject28 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVideoId

`func (o *InlineObject28) GetVideoId() int32`

GetVideoId returns the VideoId field if non-nil, zero value otherwise.

### GetVideoIdOk

`func (o *InlineObject28) GetVideoIdOk() (*int32, bool)`

GetVideoIdOk returns a tuple with the VideoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideoId

`func (o *InlineObject28) SetVideoId(v int32)`

SetVideoId sets VideoId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


