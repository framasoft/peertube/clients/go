# InlineResponse2003VideoId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PlaylistElementId** | **int32** |  | [optional] 
**PlaylistId** | **int32** |  | [optional] 
**StartTimestamp** | **int32** |  | [optional] 
**StopTimestamp** | **int32** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


