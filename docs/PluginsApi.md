# \PluginsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddPlugin**](PluginsApi.md#AddPlugin) | **Post** /plugins/install | Install a plugin
[**GetAvailablePlugins**](PluginsApi.md#GetAvailablePlugins) | **Get** /plugins/available | List available plugins
[**GetPlugin**](PluginsApi.md#GetPlugin) | **Get** /plugins/{npmName} | Get a plugin
[**GetPlugins**](PluginsApi.md#GetPlugins) | **Get** /plugins | List plugins
[**PluginsNpmNamePublicSettingsGet**](PluginsApi.md#PluginsNpmNamePublicSettingsGet) | **Get** /plugins/{npmName}/public-settings | Get a plugin&#39;s public settings
[**PluginsNpmNameRegisteredSettingsGet**](PluginsApi.md#PluginsNpmNameRegisteredSettingsGet) | **Get** /plugins/{npmName}/registered-settings | Get a plugin&#39;s registered settings
[**PluginsNpmNameSettingsPut**](PluginsApi.md#PluginsNpmNameSettingsPut) | **Put** /plugins/{npmName}/settings | Set a plugin&#39;s settings
[**UninstallPlugin**](PluginsApi.md#UninstallPlugin) | **Post** /plugins/uninstall | Uninstall a plugin
[**UpdatePlugin**](PluginsApi.md#UpdatePlugin) | **Post** /plugins/update | Update a plugin



## AddPlugin

> AddPlugin(ctx).UNKNOWNBASETYPE(uNKNOWNBASETYPE).Execute()

Install a plugin

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    uNKNOWNBASETYPE := TODO // UNKNOWN_BASE_TYPE |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.PluginsApi.AddPlugin(context.Background()).UNKNOWNBASETYPE(uNKNOWNBASETYPE).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PluginsApi.AddPlugin``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAddPluginRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAvailablePlugins

> PluginResponse GetAvailablePlugins(ctx).Search(search).PluginType(pluginType).CurrentPeerTubeEngine(currentPeerTubeEngine).Start(start).Count(count).Sort(sort).Execute()

List available plugins

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    search := "search_example" // string |  (optional)
    pluginType := int32(56) // int32 |  (optional)
    currentPeerTubeEngine := "currentPeerTubeEngine_example" // string |  (optional)
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.PluginsApi.GetAvailablePlugins(context.Background()).Search(search).PluginType(pluginType).CurrentPeerTubeEngine(currentPeerTubeEngine).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PluginsApi.GetAvailablePlugins``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAvailablePlugins`: PluginResponse
    fmt.Fprintf(os.Stdout, "Response from `PluginsApi.GetAvailablePlugins`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetAvailablePluginsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **string** |  | 
 **pluginType** | **int32** |  | 
 **currentPeerTubeEngine** | **string** |  | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**PluginResponse**](PluginResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPlugin

> Plugin GetPlugin(ctx, npmName).Execute()

Get a plugin

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    npmName := "peertube-plugin-auth-ldap" // string | name of the plugin/theme on npmjs.com or in its package.json

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.PluginsApi.GetPlugin(context.Background(), npmName).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PluginsApi.GetPlugin``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetPlugin`: Plugin
    fmt.Fprintf(os.Stdout, "Response from `PluginsApi.GetPlugin`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**npmName** | **string** | name of the plugin/theme on npmjs.com or in its package.json | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetPluginRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Plugin**](Plugin.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPlugins

> PluginResponse GetPlugins(ctx).PluginType(pluginType).Uninstalled(uninstalled).Start(start).Count(count).Sort(sort).Execute()

List plugins

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    pluginType := int32(56) // int32 |  (optional)
    uninstalled := true // bool |  (optional)
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.PluginsApi.GetPlugins(context.Background()).PluginType(pluginType).Uninstalled(uninstalled).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PluginsApi.GetPlugins``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetPlugins`: PluginResponse
    fmt.Fprintf(os.Stdout, "Response from `PluginsApi.GetPlugins`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetPluginsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginType** | **int32** |  | 
 **uninstalled** | **bool** |  | 
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**PluginResponse**](PluginResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PluginsNpmNamePublicSettingsGet

> map[string]map[string]interface{} PluginsNpmNamePublicSettingsGet(ctx, npmName).Execute()

Get a plugin's public settings

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    npmName := "peertube-plugin-auth-ldap" // string | name of the plugin/theme on npmjs.com or in its package.json

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.PluginsApi.PluginsNpmNamePublicSettingsGet(context.Background(), npmName).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PluginsApi.PluginsNpmNamePublicSettingsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PluginsNpmNamePublicSettingsGet`: map[string]map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `PluginsApi.PluginsNpmNamePublicSettingsGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**npmName** | **string** | name of the plugin/theme on npmjs.com or in its package.json | 

### Other Parameters

Other parameters are passed through a pointer to a apiPluginsNpmNamePublicSettingsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

**map[string]map[string]interface{}**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PluginsNpmNameRegisteredSettingsGet

> map[string]map[string]interface{} PluginsNpmNameRegisteredSettingsGet(ctx, npmName).Execute()

Get a plugin's registered settings

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    npmName := "peertube-plugin-auth-ldap" // string | name of the plugin/theme on npmjs.com or in its package.json

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.PluginsApi.PluginsNpmNameRegisteredSettingsGet(context.Background(), npmName).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PluginsApi.PluginsNpmNameRegisteredSettingsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PluginsNpmNameRegisteredSettingsGet`: map[string]map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `PluginsApi.PluginsNpmNameRegisteredSettingsGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**npmName** | **string** | name of the plugin/theme on npmjs.com or in its package.json | 

### Other Parameters

Other parameters are passed through a pointer to a apiPluginsNpmNameRegisteredSettingsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

**map[string]map[string]interface{}**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PluginsNpmNameSettingsPut

> PluginsNpmNameSettingsPut(ctx, npmName).InlineObject30(inlineObject30).Execute()

Set a plugin's settings

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    npmName := "peertube-plugin-auth-ldap" // string | name of the plugin/theme on npmjs.com or in its package.json
    inlineObject30 := *openapiclient.NewInlineObject30() // InlineObject30 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.PluginsApi.PluginsNpmNameSettingsPut(context.Background(), npmName).InlineObject30(inlineObject30).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PluginsApi.PluginsNpmNameSettingsPut``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**npmName** | **string** | name of the plugin/theme on npmjs.com or in its package.json | 

### Other Parameters

Other parameters are passed through a pointer to a apiPluginsNpmNameSettingsPutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **inlineObject30** | [**InlineObject30**](InlineObject30.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UninstallPlugin

> UninstallPlugin(ctx).InlineObject29(inlineObject29).Execute()

Uninstall a plugin

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    inlineObject29 := *openapiclient.NewInlineObject29("peertube-plugin-auth-ldap") // InlineObject29 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.PluginsApi.UninstallPlugin(context.Background()).InlineObject29(inlineObject29).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PluginsApi.UninstallPlugin``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUninstallPluginRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject29** | [**InlineObject29**](InlineObject29.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdatePlugin

> UpdatePlugin(ctx).UNKNOWNBASETYPE(uNKNOWNBASETYPE).Execute()

Update a plugin

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    uNKNOWNBASETYPE := TODO // UNKNOWN_BASE_TYPE |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.PluginsApi.UpdatePlugin(context.Background()).UNKNOWNBASETYPE(uNKNOWNBASETYPE).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PluginsApi.UpdatePlugin``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUpdatePluginRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

