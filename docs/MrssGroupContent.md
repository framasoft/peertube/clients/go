# MrssGroupContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | **string** |  | [optional] 
**FileSize** | **int32** |  | [optional] 
**Type** | **string** |  | [optional] 
**Framerate** | **int32** |  | [optional] 
**Duration** | **int32** |  | [optional] 
**Height** | **int32** |  | [optional] 
**Lang** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


