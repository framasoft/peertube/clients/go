# \VideoPlaylistsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddPlaylist**](VideoPlaylistsApi.md#AddPlaylist) | **Post** /video-playlists | Create a video playlist
[**AddVideoPlaylistVideo**](VideoPlaylistsApi.md#AddVideoPlaylistVideo) | **Post** /video-playlists/{playlistId}/videos | Add a video in a playlist
[**DelVideoPlaylistVideo**](VideoPlaylistsApi.md#DelVideoPlaylistVideo) | **Delete** /video-playlists/{playlistId}/videos/{playlistElementId} | Delete an element from a playlist
[**GetPlaylistPrivacyPolicies**](VideoPlaylistsApi.md#GetPlaylistPrivacyPolicies) | **Get** /video-playlists/privacies | List available playlist privacy policies
[**GetPlaylists**](VideoPlaylistsApi.md#GetPlaylists) | **Get** /video-playlists | List video playlists
[**GetVideoPlaylistVideos**](VideoPlaylistsApi.md#GetVideoPlaylistVideos) | **Get** /video-playlists/{playlistId}/videos | List videos of a playlist
[**PutVideoPlaylistVideo**](VideoPlaylistsApi.md#PutVideoPlaylistVideo) | **Put** /video-playlists/{playlistId}/videos/{playlistElementId} | Update a playlist element
[**ReorderVideoPlaylist**](VideoPlaylistsApi.md#ReorderVideoPlaylist) | **Post** /video-playlists/{playlistId}/videos/reorder | Reorder a playlist
[**UsersMeVideoPlaylistsVideosExistGet**](VideoPlaylistsApi.md#UsersMeVideoPlaylistsVideosExistGet) | **Get** /users/me/video-playlists/videos-exist | Check video exists in my playlists
[**VideoPlaylistsPlaylistIdDelete**](VideoPlaylistsApi.md#VideoPlaylistsPlaylistIdDelete) | **Delete** /video-playlists/{playlistId} | Delete a video playlist
[**VideoPlaylistsPlaylistIdGet**](VideoPlaylistsApi.md#VideoPlaylistsPlaylistIdGet) | **Get** /video-playlists/{playlistId} | Get a video playlist
[**VideoPlaylistsPlaylistIdPut**](VideoPlaylistsApi.md#VideoPlaylistsPlaylistIdPut) | **Put** /video-playlists/{playlistId} | Update a video playlist



## AddPlaylist

> InlineResponse20012 AddPlaylist(ctx).DisplayName(displayName).Thumbnailfile(thumbnailfile).Privacy(privacy).Description(description).VideoChannelId(videoChannelId).Execute()

Create a video playlist



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    displayName := "displayName_example" // string | Video playlist display name
    thumbnailfile := os.NewFile(1234, "some_file") // *os.File | Video playlist thumbnail file (optional)
    privacy := openapiclient.VideoPlaylistPrivacySet(1) // VideoPlaylistPrivacySet |  (optional)
    description := "description_example" // string | Video playlist description (optional)
    videoChannelId := TODO // int32 | Video channel in which the playlist will be published (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.AddPlaylist(context.Background()).DisplayName(displayName).Thumbnailfile(thumbnailfile).Privacy(privacy).Description(description).VideoChannelId(videoChannelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.AddPlaylist``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddPlaylist`: InlineResponse20012
    fmt.Fprintf(os.Stdout, "Response from `VideoPlaylistsApi.AddPlaylist`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAddPlaylistRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **displayName** | **string** | Video playlist display name | 
 **thumbnailfile** | ***os.File** | Video playlist thumbnail file | 
 **privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md) |  | 
 **description** | **string** | Video playlist description | 
 **videoChannelId** | [**int32**](int32.md) | Video channel in which the playlist will be published | 

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddVideoPlaylistVideo

> InlineResponse20013 AddVideoPlaylistVideo(ctx, playlistId).InlineObject19(inlineObject19).Execute()

Add a video in a playlist

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id
    inlineObject19 := *openapiclient.NewInlineObject19("TODO") // InlineObject19 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.AddVideoPlaylistVideo(context.Background(), playlistId).InlineObject19(inlineObject19).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.AddVideoPlaylistVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddVideoPlaylistVideo`: InlineResponse20013
    fmt.Fprintf(os.Stdout, "Response from `VideoPlaylistsApi.AddVideoPlaylistVideo`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddVideoPlaylistVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **inlineObject19** | [**InlineObject19**](InlineObject19.md) |  | 

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DelVideoPlaylistVideo

> DelVideoPlaylistVideo(ctx, playlistId, playlistElementId).Execute()

Delete an element from a playlist

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id
    playlistElementId := int32(56) // int32 | Playlist element id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.DelVideoPlaylistVideo(context.Background(), playlistId, playlistElementId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.DelVideoPlaylistVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 
**playlistElementId** | **int32** | Playlist element id | 

### Other Parameters

Other parameters are passed through a pointer to a apiDelVideoPlaylistVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPlaylistPrivacyPolicies

> []string GetPlaylistPrivacyPolicies(ctx).Execute()

List available playlist privacy policies

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.GetPlaylistPrivacyPolicies(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.GetPlaylistPrivacyPolicies``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetPlaylistPrivacyPolicies`: []string
    fmt.Fprintf(os.Stdout, "Response from `VideoPlaylistsApi.GetPlaylistPrivacyPolicies`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetPlaylistPrivacyPoliciesRequest struct via the builder pattern


### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPlaylists

> InlineResponse20011 GetPlaylists(ctx).Start(start).Count(count).Sort(sort).Execute()

List video playlists

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.GetPlaylists(context.Background()).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.GetPlaylists``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetPlaylists`: InlineResponse20011
    fmt.Fprintf(os.Stdout, "Response from `VideoPlaylistsApi.GetPlaylists`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetPlaylistsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVideoPlaylistVideos

> VideoListResponse GetVideoPlaylistVideos(ctx, playlistId).Execute()

List videos of a playlist

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.GetVideoPlaylistVideos(context.Background(), playlistId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.GetVideoPlaylistVideos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVideoPlaylistVideos`: VideoListResponse
    fmt.Fprintf(os.Stdout, "Response from `VideoPlaylistsApi.GetVideoPlaylistVideos`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVideoPlaylistVideosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutVideoPlaylistVideo

> PutVideoPlaylistVideo(ctx, playlistId, playlistElementId).InlineObject21(inlineObject21).Execute()

Update a playlist element

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id
    playlistElementId := int32(56) // int32 | Playlist element id
    inlineObject21 := *openapiclient.NewInlineObject21() // InlineObject21 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.PutVideoPlaylistVideo(context.Background(), playlistId, playlistElementId).InlineObject21(inlineObject21).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.PutVideoPlaylistVideo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 
**playlistElementId** | **int32** | Playlist element id | 

### Other Parameters

Other parameters are passed through a pointer to a apiPutVideoPlaylistVideoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **inlineObject21** | [**InlineObject21**](InlineObject21.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ReorderVideoPlaylist

> ReorderVideoPlaylist(ctx, playlistId).InlineObject20(inlineObject20).Execute()

Reorder a playlist

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id
    inlineObject20 := *openapiclient.NewInlineObject20(int32(123), int32(123)) // InlineObject20 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.ReorderVideoPlaylist(context.Background(), playlistId).InlineObject20(inlineObject20).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.ReorderVideoPlaylist``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 

### Other Parameters

Other parameters are passed through a pointer to a apiReorderVideoPlaylistRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **inlineObject20** | [**InlineObject20**](InlineObject20.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideoPlaylistsVideosExistGet

> InlineResponse20014 UsersMeVideoPlaylistsVideosExistGet(ctx).VideoIds(videoIds).Execute()

Check video exists in my playlists

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    videoIds := []int32{int32(42)} // []int32 | The video ids to check

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.UsersMeVideoPlaylistsVideosExistGet(context.Background()).VideoIds(videoIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.UsersMeVideoPlaylistsVideosExistGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UsersMeVideoPlaylistsVideosExistGet`: InlineResponse20014
    fmt.Fprintf(os.Stdout, "Response from `VideoPlaylistsApi.UsersMeVideoPlaylistsVideosExistGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUsersMeVideoPlaylistsVideosExistGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoIds** | **[]int32** | The video ids to check | 

### Return type

[**InlineResponse20014**](InlineResponse20014.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoPlaylistsPlaylistIdDelete

> VideoPlaylistsPlaylistIdDelete(ctx, playlistId).Execute()

Delete a video playlist

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.VideoPlaylistsPlaylistIdDelete(context.Background(), playlistId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.VideoPlaylistsPlaylistIdDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideoPlaylistsPlaylistIdDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoPlaylistsPlaylistIdGet

> VideoPlaylist VideoPlaylistsPlaylistIdGet(ctx, playlistId).Execute()

Get a video playlist

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.VideoPlaylistsPlaylistIdGet(context.Background(), playlistId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.VideoPlaylistsPlaylistIdGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VideoPlaylistsPlaylistIdGet`: VideoPlaylist
    fmt.Fprintf(os.Stdout, "Response from `VideoPlaylistsApi.VideoPlaylistsPlaylistIdGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideoPlaylistsPlaylistIdGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**VideoPlaylist**](VideoPlaylist.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoPlaylistsPlaylistIdPut

> VideoPlaylistsPlaylistIdPut(ctx, playlistId).DisplayName(displayName).Thumbnailfile(thumbnailfile).Privacy(privacy).Description(description).VideoChannelId(videoChannelId).Execute()

Update a video playlist



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    playlistId := int32(56) // int32 | Playlist id
    displayName := "displayName_example" // string | Video playlist display name (optional)
    thumbnailfile := os.NewFile(1234, "some_file") // *os.File | Video playlist thumbnail file (optional)
    privacy := openapiclient.VideoPlaylistPrivacySet(1) // VideoPlaylistPrivacySet |  (optional)
    description := "description_example" // string | Video playlist description (optional)
    videoChannelId := TODO // int32 | Video channel in which the playlist will be published (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VideoPlaylistsApi.VideoPlaylistsPlaylistIdPut(context.Background(), playlistId).DisplayName(displayName).Thumbnailfile(thumbnailfile).Privacy(privacy).Description(description).VideoChannelId(videoChannelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VideoPlaylistsApi.VideoPlaylistsPlaylistIdPut``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**playlistId** | **int32** | Playlist id | 

### Other Parameters

Other parameters are passed through a pointer to a apiVideoPlaylistsPlaylistIdPutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **displayName** | **string** | Video playlist display name | 
 **thumbnailfile** | ***os.File** | Video playlist thumbnail file | 
 **privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md) |  | 
 **description** | **string** | Video playlist description | 
 **videoChannelId** | [**int32**](int32.md) | Video channel in which the playlist will be published | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

