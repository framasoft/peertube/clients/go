# InlineObject7

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Videofile** | [***os.File**](*os.File.md) | Video file | 
**ChannelId** | **int32** | Channel id that will contain this video | 
**Thumbnailfile** | [***os.File**](*os.File.md) | Video thumbnail file | [optional] 
**Previewfile** | [***os.File**](*os.File.md) | Video preview file | [optional] 
**Privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**Category** | **int32** | Video category | [optional] 
**Licence** | **string** | Video licence | [optional] 
**Language** | **int32** | Video language | [optional] 
**Description** | **string** | Video description | [optional] 
**WaitTranscoding** | **bool** | Whether or not we wait transcoding before publish the video | [optional] 
**Support** | **string** | A text tell the audience how to support the video creator | [optional] 
**Nsfw** | **bool** | Whether or not this video contains sensitive content | [optional] 
**Name** | **string** | Video name | 
**Tags** | **[]string** | Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
**CommentsEnabled** | **bool** | Enable or disable comments for this video | [optional] 
**DownloadEnabled** | **bool** | Enable or disable downloading for this video | [optional] 
**OriginallyPublishedAt** | [**time.Time**](time.Time.md) | Date when the content was originally published | [optional] 
**ScheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


