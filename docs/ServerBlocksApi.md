# \ServerBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ServerBlocklistServersGet**](ServerBlocksApi.md#ServerBlocklistServersGet) | **Get** /server/blocklist/servers | List server blocks
[**ServerBlocklistServersHostDelete**](ServerBlocksApi.md#ServerBlocklistServersHostDelete) | **Delete** /server/blocklist/servers/{host} | Unblock a server by its domain
[**ServerBlocklistServersPost**](ServerBlocksApi.md#ServerBlocklistServersPost) | **Post** /server/blocklist/servers | Block a server



## ServerBlocklistServersGet

> ServerBlocklistServersGet(ctx).Start(start).Count(count).Sort(sort).Execute()

List server blocks

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    start := int32(56) // int32 | Offset used to paginate results (optional)
    count := int32(56) // int32 | Number of items to return (optional) (default to 15)
    sort := "-createdAt" // string | Sort column (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ServerBlocksApi.ServerBlocklistServersGet(context.Background()).Start(start).Count(count).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ServerBlocksApi.ServerBlocklistServersGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiServerBlocklistServersGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int32** | Offset used to paginate results | 
 **count** | **int32** | Number of items to return | [default to 15]
 **sort** | **string** | Sort column | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerBlocklistServersHostDelete

> ServerBlocklistServersHostDelete(ctx, host).Execute()

Unblock a server by its domain

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    host := "host_example" // string | server domain to unblock

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ServerBlocksApi.ServerBlocklistServersHostDelete(context.Background(), host).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ServerBlocksApi.ServerBlocklistServersHostDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**host** | **string** | server domain to unblock | 

### Other Parameters

Other parameters are passed through a pointer to a apiServerBlocklistServersHostDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerBlocklistServersPost

> ServerBlocklistServersPost(ctx).InlineObject26(inlineObject26).Execute()

Block a server

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    inlineObject26 := *openapiclient.NewInlineObject26("Host_example") // InlineObject26 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ServerBlocksApi.ServerBlocklistServersPost(context.Background()).InlineObject26(inlineObject26).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ServerBlocksApi.ServerBlocklistServersPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiServerBlocklistServersPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject26** | [**InlineObject26**](InlineObject26.md) |  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

