# \AccountApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetSyndicatedSubscriptionVideos**](AccountApi.md#GetSyndicatedSubscriptionVideos) | **Get** /feeds/subscriptions.{format} | List videos of subscriptions tied to a token



## GetSyndicatedSubscriptionVideos

> []map[string]interface{} GetSyndicatedSubscriptionVideos(ctx, format).AccountId(accountId).Token(token).Sort(sort).Nsfw(nsfw).Filter(filter).Execute()

List videos of subscriptions tied to a token

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    format := "format_example" // string | format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
    accountId := "accountId_example" // string | limit listing to a specific account
    token := "token_example" // string | private token allowing access
    sort := "-createdAt" // string | Sort column (optional)
    nsfw := "nsfw_example" // string | whether to include nsfw videos, if any (optional)
    filter := "filter_example" // string | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountApi.GetSyndicatedSubscriptionVideos(context.Background(), format).AccountId(accountId).Token(token).Sort(sort).Nsfw(nsfw).Filter(filter).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountApi.GetSyndicatedSubscriptionVideos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetSyndicatedSubscriptionVideos`: []map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `AccountApi.GetSyndicatedSubscriptionVideos`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**format** | **string** | format expected (we focus on making &#x60;rss&#x60; the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss)) | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetSyndicatedSubscriptionVideosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **accountId** | **string** | limit listing to a specific account | 
 **token** | **string** | private token allowing access | 
 **sort** | **string** | Sort column | 
 **nsfw** | **string** | whether to include nsfw videos, if any | 
 **filter** | **string** | Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | 

### Return type

**[]map[string]interface{}**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/xml, application/rss+xml, text/xml, application/atom+xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

