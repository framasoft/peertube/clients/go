# InlineObject15

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Video playlist display name | [optional] 
**Thumbnailfile** | [***os.File**](*os.File.md) | Video playlist thumbnail file | [optional] 
**Privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md) |  | [optional] 
**Description** | **string** | Video playlist description | [optional] 
**VideoChannelId** | **int32** | Video channel in which the playlist will be published | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


