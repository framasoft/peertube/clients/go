# \VideoPlaylistApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VideoPlaylistsGet**](VideoPlaylistApi.md#VideoPlaylistsGet) | **Get** /video-playlists | Get list of video playlists



## VideoPlaylistsGet

> []VideoPlaylist VideoPlaylistsGet(ctx, optional)

Get list of video playlists

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***VideoPlaylistsGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideoPlaylistsGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort column (-createdAt for example) | 

### Return type

[**[]VideoPlaylist**](VideoPlaylist.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

