# Go API client for PeerTube

This Python package is automatically generated from [PeerTube's REST API](https://docs.joinpeertube.org/api-rest-reference.html),
using the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 3.4.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

For more information, please visit [https://joinpeertube.org](https://joinpeertube.org)

## Installation

Install the following dependencies:

```shell
go get github.com/stretchr/testify/assert
go get golang.org/x/oauth2
go get golang.org/x/net/context
go get github.com/antihax/optional
```

Put the package under your project folder and add the following in import:

```golang
import "./peertube"
```

## Documentation for API Endpoints

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AbusesApi* | [**AbusesAbuseIdDelete**](docs/AbusesApi.md#abusesabuseiddelete) | **Delete** /abuses/{abuseId} | Delete an abuse
*AbusesApi* | [**AbusesAbuseIdMessagesAbuseMessageIdDelete**](docs/AbusesApi.md#abusesabuseidmessagesabusemessageiddelete) | **Delete** /abuses/{abuseId}/messages/{abuseMessageId} | Delete an abuse message
*AbusesApi* | [**AbusesAbuseIdMessagesGet**](docs/AbusesApi.md#abusesabuseidmessagesget) | **Get** /abuses/{abuseId}/messages | List messages of an abuse
*AbusesApi* | [**AbusesAbuseIdMessagesPost**](docs/AbusesApi.md#abusesabuseidmessagespost) | **Post** /abuses/{abuseId}/messages | Add message to an abuse
*AbusesApi* | [**AbusesAbuseIdPut**](docs/AbusesApi.md#abusesabuseidput) | **Put** /abuses/{abuseId} | Update an abuse
*AbusesApi* | [**AbusesPost**](docs/AbusesApi.md#abusespost) | **Post** /abuses | Report an abuse
*AbusesApi* | [**GetAbuses**](docs/AbusesApi.md#getabuses) | **Get** /abuses | List abuses
*AbusesApi* | [**GetMyAbuses**](docs/AbusesApi.md#getmyabuses) | **Get** /users/me/abuses | List my abuses
*AccountApi* | [**GetSyndicatedSubscriptionVideos**](docs/AccountApi.md#getsyndicatedsubscriptionvideos) | **Get** /feeds/subscriptions.{format} | List videos of subscriptions tied to a token
*AccountBlocksApi* | [**ServerBlocklistAccountsAccountNameDelete**](docs/AccountBlocksApi.md#serverblocklistaccountsaccountnamedelete) | **Delete** /server/blocklist/accounts/{accountName} | Unblock an account by its handle
*AccountBlocksApi* | [**ServerBlocklistAccountsGet**](docs/AccountBlocksApi.md#serverblocklistaccountsget) | **Get** /server/blocklist/accounts | List account blocks
*AccountBlocksApi* | [**ServerBlocklistAccountsPost**](docs/AccountBlocksApi.md#serverblocklistaccountspost) | **Post** /server/blocklist/accounts | Block an account
*AccountsApi* | [**AccountsNameRatingsGet**](docs/AccountsApi.md#accountsnameratingsget) | **Get** /accounts/{name}/ratings | List ratings of an account
*AccountsApi* | [**AccountsNameVideoChannelsGet**](docs/AccountsApi.md#accountsnamevideochannelsget) | **Get** /accounts/{name}/video-channels | List video channels of an account
*AccountsApi* | [**GetAccount**](docs/AccountsApi.md#getaccount) | **Get** /accounts/{name} | Get an account
*AccountsApi* | [**GetAccountVideos**](docs/AccountsApi.md#getaccountvideos) | **Get** /accounts/{name}/videos | List videos of an account
*AccountsApi* | [**GetAccounts**](docs/AccountsApi.md#getaccounts) | **Get** /accounts | List accounts
*ConfigApi* | [**DelCustomConfig**](docs/ConfigApi.md#delcustomconfig) | **Delete** /config/custom | Delete instance runtime configuration
*ConfigApi* | [**GetAbout**](docs/ConfigApi.md#getabout) | **Get** /config/about | Get instance \&quot;About\&quot; information
*ConfigApi* | [**GetConfig**](docs/ConfigApi.md#getconfig) | **Get** /config | Get instance public configuration
*ConfigApi* | [**GetCustomConfig**](docs/ConfigApi.md#getcustomconfig) | **Get** /config/custom | Get instance runtime configuration
*ConfigApi* | [**PutCustomConfig**](docs/ConfigApi.md#putcustomconfig) | **Put** /config/custom | Set instance runtime configuration
*FeedsApi* | [**GetSyndicatedComments**](docs/FeedsApi.md#getsyndicatedcomments) | **Get** /feeds/video-comments.{format} | List comments on videos
*FeedsApi* | [**GetSyndicatedSubscriptionVideos**](docs/FeedsApi.md#getsyndicatedsubscriptionvideos) | **Get** /feeds/subscriptions.{format} | List videos of subscriptions tied to a token
*FeedsApi* | [**GetSyndicatedVideos**](docs/FeedsApi.md#getsyndicatedvideos) | **Get** /feeds/videos.{format} | List videos
*HomepageApi* | [**CustomPagesHomepageInstanceGet**](docs/HomepageApi.md#custompageshomepageinstanceget) | **Get** /custom-pages/homepage/instance | Get instance custom homepage
*HomepageApi* | [**CustomPagesHomepageInstancePut**](docs/HomepageApi.md#custompageshomepageinstanceput) | **Put** /custom-pages/homepage/instance | Set instance custom homepage
*InstanceFollowsApi* | [**ServerFollowersGet**](docs/InstanceFollowsApi.md#serverfollowersget) | **Get** /server/followers | List instances following the server
*InstanceFollowsApi* | [**ServerFollowersNameWithHostAcceptPost**](docs/InstanceFollowsApi.md#serverfollowersnamewithhostacceptpost) | **Post** /server/followers/{nameWithHost}/accept | Accept a pending follower to your server
*InstanceFollowsApi* | [**ServerFollowersNameWithHostDelete**](docs/InstanceFollowsApi.md#serverfollowersnamewithhostdelete) | **Delete** /server/followers/{nameWithHost} | Remove or reject a follower to your server
*InstanceFollowsApi* | [**ServerFollowersNameWithHostRejectPost**](docs/InstanceFollowsApi.md#serverfollowersnamewithhostrejectpost) | **Post** /server/followers/{nameWithHost}/reject | Reject a pending follower to your server
*InstanceFollowsApi* | [**ServerFollowingGet**](docs/InstanceFollowsApi.md#serverfollowingget) | **Get** /server/following | List instances followed by the server
*InstanceFollowsApi* | [**ServerFollowingHostOrHandleDelete**](docs/InstanceFollowsApi.md#serverfollowinghostorhandledelete) | **Delete** /server/following/{hostOrHandle} | Unfollow an actor (PeerTube instance, channel or account)
*InstanceFollowsApi* | [**ServerFollowingPost**](docs/InstanceFollowsApi.md#serverfollowingpost) | **Post** /server/following | Follow a list of actors (PeerTube instance, channel or account)
*InstanceRedundancyApi* | [**ServerRedundancyHostPut**](docs/InstanceRedundancyApi.md#serverredundancyhostput) | **Put** /server/redundancy/{host} | Update a server redundancy policy
*JobApi* | [**GetJobs**](docs/JobApi.md#getjobs) | **Get** /jobs/{state} | List instance jobs
*LiveVideosApi* | [**AddLive**](docs/LiveVideosApi.md#addlive) | **Post** /videos/live | Create a live
*LiveVideosApi* | [**GetLiveId**](docs/LiveVideosApi.md#getliveid) | **Get** /videos/live/{id} | Get information about a live
*LiveVideosApi* | [**UpdateLiveId**](docs/LiveVideosApi.md#updateliveid) | **Put** /videos/live/{id} | Update information about a live
*MyHistoryApi* | [**UsersMeHistoryVideosGet**](docs/MyHistoryApi.md#usersmehistoryvideosget) | **Get** /users/me/history/videos | List watched videos history
*MyHistoryApi* | [**UsersMeHistoryVideosRemovePost**](docs/MyHistoryApi.md#usersmehistoryvideosremovepost) | **Post** /users/me/history/videos/remove | Clear video history
*MyNotificationsApi* | [**UsersMeNotificationSettingsPut**](docs/MyNotificationsApi.md#usersmenotificationsettingsput) | **Put** /users/me/notification-settings | Update my notification settings
*MyNotificationsApi* | [**UsersMeNotificationsGet**](docs/MyNotificationsApi.md#usersmenotificationsget) | **Get** /users/me/notifications | List my notifications
*MyNotificationsApi* | [**UsersMeNotificationsReadAllPost**](docs/MyNotificationsApi.md#usersmenotificationsreadallpost) | **Post** /users/me/notifications/read-all | Mark all my notification as read
*MyNotificationsApi* | [**UsersMeNotificationsReadPost**](docs/MyNotificationsApi.md#usersmenotificationsreadpost) | **Post** /users/me/notifications/read | Mark notifications as read by their id
*MySubscriptionsApi* | [**UsersMeSubscriptionsExistGet**](docs/MySubscriptionsApi.md#usersmesubscriptionsexistget) | **Get** /users/me/subscriptions/exist | Get if subscriptions exist for my user
*MySubscriptionsApi* | [**UsersMeSubscriptionsGet**](docs/MySubscriptionsApi.md#usersmesubscriptionsget) | **Get** /users/me/subscriptions | Get my user subscriptions
*MySubscriptionsApi* | [**UsersMeSubscriptionsPost**](docs/MySubscriptionsApi.md#usersmesubscriptionspost) | **Post** /users/me/subscriptions | Add subscription to my user
*MySubscriptionsApi* | [**UsersMeSubscriptionsSubscriptionHandleDelete**](docs/MySubscriptionsApi.md#usersmesubscriptionssubscriptionhandledelete) | **Delete** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of my user
*MySubscriptionsApi* | [**UsersMeSubscriptionsSubscriptionHandleGet**](docs/MySubscriptionsApi.md#usersmesubscriptionssubscriptionhandleget) | **Get** /users/me/subscriptions/{subscriptionHandle} | Get subscription of my user
*MySubscriptionsApi* | [**UsersMeSubscriptionsVideosGet**](docs/MySubscriptionsApi.md#usersmesubscriptionsvideosget) | **Get** /users/me/subscriptions/videos | List videos of subscriptions of my user
*MyUserApi* | [**GetMyAbuses**](docs/MyUserApi.md#getmyabuses) | **Get** /users/me/abuses | List my abuses
*MyUserApi* | [**GetUserInfo**](docs/MyUserApi.md#getuserinfo) | **Get** /users/me | Get my user information
*MyUserApi* | [**PutUserInfo**](docs/MyUserApi.md#putuserinfo) | **Put** /users/me | Update my user information
*MyUserApi* | [**UsersMeAvatarDelete**](docs/MyUserApi.md#usersmeavatardelete) | **Delete** /users/me/avatar | Delete my avatar
*MyUserApi* | [**UsersMeAvatarPickPost**](docs/MyUserApi.md#usersmeavatarpickpost) | **Post** /users/me/avatar/pick | Update my user avatar
*MyUserApi* | [**UsersMeVideoQuotaUsedGet**](docs/MyUserApi.md#usersmevideoquotausedget) | **Get** /users/me/video-quota-used | Get my user used quota
*MyUserApi* | [**UsersMeVideosGet**](docs/MyUserApi.md#usersmevideosget) | **Get** /users/me/videos | Get videos of my user
*MyUserApi* | [**UsersMeVideosImportsGet**](docs/MyUserApi.md#usersmevideosimportsget) | **Get** /users/me/videos/imports | Get video imports of my user
*MyUserApi* | [**UsersMeVideosVideoIdRatingGet**](docs/MyUserApi.md#usersmevideosvideoidratingget) | **Get** /users/me/videos/{videoId}/rating | Get rate of my user for a video
*PluginsApi* | [**AddPlugin**](docs/PluginsApi.md#addplugin) | **Post** /plugins/install | Install a plugin
*PluginsApi* | [**GetAvailablePlugins**](docs/PluginsApi.md#getavailableplugins) | **Get** /plugins/available | List available plugins
*PluginsApi* | [**GetPlugin**](docs/PluginsApi.md#getplugin) | **Get** /plugins/{npmName} | Get a plugin
*PluginsApi* | [**GetPlugins**](docs/PluginsApi.md#getplugins) | **Get** /plugins | List plugins
*PluginsApi* | [**PluginsNpmNamePublicSettingsGet**](docs/PluginsApi.md#pluginsnpmnamepublicsettingsget) | **Get** /plugins/{npmName}/public-settings | Get a plugin&#39;s public settings
*PluginsApi* | [**PluginsNpmNameRegisteredSettingsGet**](docs/PluginsApi.md#pluginsnpmnameregisteredsettingsget) | **Get** /plugins/{npmName}/registered-settings | Get a plugin&#39;s registered settings
*PluginsApi* | [**PluginsNpmNameSettingsPut**](docs/PluginsApi.md#pluginsnpmnamesettingsput) | **Put** /plugins/{npmName}/settings | Set a plugin&#39;s settings
*PluginsApi* | [**UninstallPlugin**](docs/PluginsApi.md#uninstallplugin) | **Post** /plugins/uninstall | Uninstall a plugin
*PluginsApi* | [**UpdatePlugin**](docs/PluginsApi.md#updateplugin) | **Post** /plugins/update | Update a plugin
*RegisterApi* | [**RegisterUser**](docs/RegisterApi.md#registeruser) | **Post** /users/register | Register a user
*RegisterApi* | [**ResendEmailToVerifyUser**](docs/RegisterApi.md#resendemailtoverifyuser) | **Post** /users/ask-send-verify-email | Resend user verification link
*RegisterApi* | [**VerifyUser**](docs/RegisterApi.md#verifyuser) | **Post** /users/{id}/verify-email | Verify a user
*SearchApi* | [**SearchChannels**](docs/SearchApi.md#searchchannels) | **Get** /search/video-channels | Search channels
*SearchApi* | [**SearchPlaylists**](docs/SearchApi.md#searchplaylists) | **Get** /search/video-playlists | Search playlists
*SearchApi* | [**SearchVideos**](docs/SearchApi.md#searchvideos) | **Get** /search/videos | Search videos
*ServerBlocksApi* | [**ServerBlocklistServersGet**](docs/ServerBlocksApi.md#serverblocklistserversget) | **Get** /server/blocklist/servers | List server blocks
*ServerBlocksApi* | [**ServerBlocklistServersHostDelete**](docs/ServerBlocksApi.md#serverblocklistservershostdelete) | **Delete** /server/blocklist/servers/{host} | Unblock a server by its domain
*ServerBlocksApi* | [**ServerBlocklistServersPost**](docs/ServerBlocksApi.md#serverblocklistserverspost) | **Post** /server/blocklist/servers | Block a server
*SessionApi* | [**GetOAuthClient**](docs/SessionApi.md#getoauthclient) | **Get** /oauth-clients/local | Login prerequisite
*SessionApi* | [**GetOAuthToken**](docs/SessionApi.md#getoauthtoken) | **Post** /users/token | Login
*SessionApi* | [**RevokeOAuthToken**](docs/SessionApi.md#revokeoauthtoken) | **Post** /users/revoke-token | Logout
*UsersApi* | [**AddUser**](docs/UsersApi.md#adduser) | **Post** /users | Create a user
*UsersApi* | [**DelUser**](docs/UsersApi.md#deluser) | **Delete** /users/{id} | Delete a user
*UsersApi* | [**GetUser**](docs/UsersApi.md#getuser) | **Get** /users/{id} | Get a user
*UsersApi* | [**GetUsers**](docs/UsersApi.md#getusers) | **Get** /users | List users
*UsersApi* | [**PutUser**](docs/UsersApi.md#putuser) | **Put** /users/{id} | Update a user
*UsersApi* | [**RegisterUser**](docs/UsersApi.md#registeruser) | **Post** /users/register | Register a user
*UsersApi* | [**ResendEmailToVerifyUser**](docs/UsersApi.md#resendemailtoverifyuser) | **Post** /users/ask-send-verify-email | Resend user verification link
*UsersApi* | [**VerifyUser**](docs/UsersApi.md#verifyuser) | **Post** /users/{id}/verify-email | Verify a user
*VideoApi* | [**AddLive**](docs/VideoApi.md#addlive) | **Post** /videos/live | Create a live
*VideoApi* | [**AddView**](docs/VideoApi.md#addview) | **Post** /videos/{id}/views | Add a view to a video
*VideoApi* | [**DelVideo**](docs/VideoApi.md#delvideo) | **Delete** /videos/{id} | Delete a video
*VideoApi* | [**GetAccountVideos**](docs/VideoApi.md#getaccountvideos) | **Get** /accounts/{name}/videos | List videos of an account
*VideoApi* | [**GetCategories**](docs/VideoApi.md#getcategories) | **Get** /videos/categories | List available video categories
*VideoApi* | [**GetLanguages**](docs/VideoApi.md#getlanguages) | **Get** /videos/languages | List available video languages
*VideoApi* | [**GetLicences**](docs/VideoApi.md#getlicences) | **Get** /videos/licences | List available video licences
*VideoApi* | [**GetLiveId**](docs/VideoApi.md#getliveid) | **Get** /videos/live/{id} | Get information about a live
*VideoApi* | [**GetPrivacyPolicies**](docs/VideoApi.md#getprivacypolicies) | **Get** /videos/privacies | List available video privacy policies
*VideoApi* | [**GetVideo**](docs/VideoApi.md#getvideo) | **Get** /videos/{id} | Get a video
*VideoApi* | [**GetVideoChannelVideos**](docs/VideoApi.md#getvideochannelvideos) | **Get** /video-channels/{channelHandle}/videos | List videos of a video channel
*VideoApi* | [**GetVideoDesc**](docs/VideoApi.md#getvideodesc) | **Get** /videos/{id}/description | Get complete video description
*VideoApi* | [**GetVideos**](docs/VideoApi.md#getvideos) | **Get** /videos | List videos
*VideoApi* | [**ImportVideo**](docs/VideoApi.md#importvideo) | **Post** /videos/imports | Import a video
*VideoApi* | [**PutVideo**](docs/VideoApi.md#putvideo) | **Put** /videos/{id} | Update a video
*VideoApi* | [**SetProgress**](docs/VideoApi.md#setprogress) | **Put** /videos/{id}/watching | Set watching progress of a video
*VideoApi* | [**UpdateLiveId**](docs/VideoApi.md#updateliveid) | **Put** /videos/live/{id} | Update information about a live
*VideoApi* | [**UploadLegacy**](docs/VideoApi.md#uploadlegacy) | **Post** /videos/upload | Upload a video
*VideoApi* | [**UploadResumable**](docs/VideoApi.md#uploadresumable) | **Put** /videos/upload-resumable | Send chunk for the resumable upload of a video
*VideoApi* | [**UploadResumableCancel**](docs/VideoApi.md#uploadresumablecancel) | **Delete** /videos/upload-resumable | Cancel the resumable upload of a video, deleting any data uploaded so far
*VideoApi* | [**UploadResumableInit**](docs/VideoApi.md#uploadresumableinit) | **Post** /videos/upload-resumable | Initialize the resumable upload of a video
*VideoBlocksApi* | [**AddVideoBlock**](docs/VideoBlocksApi.md#addvideoblock) | **Post** /videos/{id}/blacklist | Block a video
*VideoBlocksApi* | [**DelVideoBlock**](docs/VideoBlocksApi.md#delvideoblock) | **Delete** /videos/{id}/blacklist | Unblock a video by its id
*VideoBlocksApi* | [**GetVideoBlocks**](docs/VideoBlocksApi.md#getvideoblocks) | **Get** /videos/blacklist | List video blocks
*VideoCaptionsApi* | [**AddVideoCaption**](docs/VideoCaptionsApi.md#addvideocaption) | **Put** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
*VideoCaptionsApi* | [**DelVideoCaption**](docs/VideoCaptionsApi.md#delvideocaption) | **Delete** /videos/{id}/captions/{captionLanguage} | Delete a video caption
*VideoCaptionsApi* | [**GetVideoCaptions**](docs/VideoCaptionsApi.md#getvideocaptions) | **Get** /videos/{id}/captions | List captions of a video
*VideoChannelsApi* | [**AccountsNameVideoChannelsGet**](docs/VideoChannelsApi.md#accountsnamevideochannelsget) | **Get** /accounts/{name}/video-channels | List video channels of an account
*VideoChannelsApi* | [**AddVideoChannel**](docs/VideoChannelsApi.md#addvideochannel) | **Post** /video-channels | Create a video channel
*VideoChannelsApi* | [**DelVideoChannel**](docs/VideoChannelsApi.md#delvideochannel) | **Delete** /video-channels/{channelHandle} | Delete a video channel
*VideoChannelsApi* | [**GetVideoChannel**](docs/VideoChannelsApi.md#getvideochannel) | **Get** /video-channels/{channelHandle} | Get a video channel
*VideoChannelsApi* | [**GetVideoChannelVideos**](docs/VideoChannelsApi.md#getvideochannelvideos) | **Get** /video-channels/{channelHandle}/videos | List videos of a video channel
*VideoChannelsApi* | [**GetVideoChannels**](docs/VideoChannelsApi.md#getvideochannels) | **Get** /video-channels | List video channels
*VideoChannelsApi* | [**PutVideoChannel**](docs/VideoChannelsApi.md#putvideochannel) | **Put** /video-channels/{channelHandle} | Update a video channel
*VideoChannelsApi* | [**VideoChannelsChannelHandleAvatarDelete**](docs/VideoChannelsApi.md#videochannelschannelhandleavatardelete) | **Delete** /video-channels/{channelHandle}/avatar | Delete channel avatar
*VideoChannelsApi* | [**VideoChannelsChannelHandleAvatarPickPost**](docs/VideoChannelsApi.md#videochannelschannelhandleavatarpickpost) | **Post** /video-channels/{channelHandle}/avatar/pick | Update channel avatar
*VideoChannelsApi* | [**VideoChannelsChannelHandleBannerDelete**](docs/VideoChannelsApi.md#videochannelschannelhandlebannerdelete) | **Delete** /video-channels/{channelHandle}/banner | Delete channel banner
*VideoChannelsApi* | [**VideoChannelsChannelHandleBannerPickPost**](docs/VideoChannelsApi.md#videochannelschannelhandlebannerpickpost) | **Post** /video-channels/{channelHandle}/banner/pick | Update channel banner
*VideoCommentsApi* | [**VideosIdCommentThreadsGet**](docs/VideoCommentsApi.md#videosidcommentthreadsget) | **Get** /videos/{id}/comment-threads | List threads of a video
*VideoCommentsApi* | [**VideosIdCommentThreadsPost**](docs/VideoCommentsApi.md#videosidcommentthreadspost) | **Post** /videos/{id}/comment-threads | Create a thread
*VideoCommentsApi* | [**VideosIdCommentThreadsThreadIdGet**](docs/VideoCommentsApi.md#videosidcommentthreadsthreadidget) | **Get** /videos/{id}/comment-threads/{threadId} | Get a thread
*VideoCommentsApi* | [**VideosIdCommentsCommentIdDelete**](docs/VideoCommentsApi.md#videosidcommentscommentiddelete) | **Delete** /videos/{id}/comments/{commentId} | Delete a comment or a reply
*VideoCommentsApi* | [**VideosIdCommentsCommentIdPost**](docs/VideoCommentsApi.md#videosidcommentscommentidpost) | **Post** /videos/{id}/comments/{commentId} | Reply to a thread of a video
*VideoMirroringApi* | [**DelMirroredVideo**](docs/VideoMirroringApi.md#delmirroredvideo) | **Delete** /server/redundancy/videos/{redundancyId} | Delete a mirror done on a video
*VideoMirroringApi* | [**GetMirroredVideos**](docs/VideoMirroringApi.md#getmirroredvideos) | **Get** /server/redundancy/videos | List videos being mirrored
*VideoMirroringApi* | [**PutMirroredVideo**](docs/VideoMirroringApi.md#putmirroredvideo) | **Post** /server/redundancy/videos | Mirror a video
*VideoOwnershipChangeApi* | [**VideosIdGiveOwnershipPost**](docs/VideoOwnershipChangeApi.md#videosidgiveownershippost) | **Post** /videos/{id}/give-ownership | Request ownership change
*VideoOwnershipChangeApi* | [**VideosOwnershipGet**](docs/VideoOwnershipChangeApi.md#videosownershipget) | **Get** /videos/ownership | List video ownership changes
*VideoOwnershipChangeApi* | [**VideosOwnershipIdAcceptPost**](docs/VideoOwnershipChangeApi.md#videosownershipidacceptpost) | **Post** /videos/ownership/{id}/accept | Accept ownership change request
*VideoOwnershipChangeApi* | [**VideosOwnershipIdRefusePost**](docs/VideoOwnershipChangeApi.md#videosownershipidrefusepost) | **Post** /videos/ownership/{id}/refuse | Refuse ownership change request
*VideoPlaylistsApi* | [**AddPlaylist**](docs/VideoPlaylistsApi.md#addplaylist) | **Post** /video-playlists | Create a video playlist
*VideoPlaylistsApi* | [**AddVideoPlaylistVideo**](docs/VideoPlaylistsApi.md#addvideoplaylistvideo) | **Post** /video-playlists/{playlistId}/videos | Add a video in a playlist
*VideoPlaylistsApi* | [**DelVideoPlaylistVideo**](docs/VideoPlaylistsApi.md#delvideoplaylistvideo) | **Delete** /video-playlists/{playlistId}/videos/{playlistElementId} | Delete an element from a playlist
*VideoPlaylistsApi* | [**GetPlaylistPrivacyPolicies**](docs/VideoPlaylistsApi.md#getplaylistprivacypolicies) | **Get** /video-playlists/privacies | List available playlist privacy policies
*VideoPlaylistsApi* | [**GetPlaylists**](docs/VideoPlaylistsApi.md#getplaylists) | **Get** /video-playlists | List video playlists
*VideoPlaylistsApi* | [**GetVideoPlaylistVideos**](docs/VideoPlaylistsApi.md#getvideoplaylistvideos) | **Get** /video-playlists/{playlistId}/videos | List videos of a playlist
*VideoPlaylistsApi* | [**PutVideoPlaylistVideo**](docs/VideoPlaylistsApi.md#putvideoplaylistvideo) | **Put** /video-playlists/{playlistId}/videos/{playlistElementId} | Update a playlist element
*VideoPlaylistsApi* | [**ReorderVideoPlaylist**](docs/VideoPlaylistsApi.md#reordervideoplaylist) | **Post** /video-playlists/{playlistId}/videos/reorder | Reorder a playlist
*VideoPlaylistsApi* | [**UsersMeVideoPlaylistsVideosExistGet**](docs/VideoPlaylistsApi.md#usersmevideoplaylistsvideosexistget) | **Get** /users/me/video-playlists/videos-exist | Check video exists in my playlists
*VideoPlaylistsApi* | [**VideoPlaylistsPlaylistIdDelete**](docs/VideoPlaylistsApi.md#videoplaylistsplaylistiddelete) | **Delete** /video-playlists/{playlistId} | Delete a video playlist
*VideoPlaylistsApi* | [**VideoPlaylistsPlaylistIdGet**](docs/VideoPlaylistsApi.md#videoplaylistsplaylistidget) | **Get** /video-playlists/{playlistId} | Get a video playlist
*VideoPlaylistsApi* | [**VideoPlaylistsPlaylistIdPut**](docs/VideoPlaylistsApi.md#videoplaylistsplaylistidput) | **Put** /video-playlists/{playlistId} | Update a video playlist
*VideoRatesApi* | [**UsersMeVideosVideoIdRatingGet**](docs/VideoRatesApi.md#usersmevideosvideoidratingget) | **Get** /users/me/videos/{videoId}/rating | Get rate of my user for a video
*VideoRatesApi* | [**VideosIdRatePut**](docs/VideoRatesApi.md#videosidrateput) | **Put** /videos/{id}/rate | Like/dislike a video
*VideoUploadApi* | [**ImportVideo**](docs/VideoUploadApi.md#importvideo) | **Post** /videos/imports | Import a video
*VideoUploadApi* | [**UploadLegacy**](docs/VideoUploadApi.md#uploadlegacy) | **Post** /videos/upload | Upload a video
*VideoUploadApi* | [**UploadResumable**](docs/VideoUploadApi.md#uploadresumable) | **Put** /videos/upload-resumable | Send chunk for the resumable upload of a video
*VideoUploadApi* | [**UploadResumableCancel**](docs/VideoUploadApi.md#uploadresumablecancel) | **Delete** /videos/upload-resumable | Cancel the resumable upload of a video, deleting any data uploaded so far
*VideoUploadApi* | [**UploadResumableInit**](docs/VideoUploadApi.md#uploadresumableinit) | **Post** /videos/upload-resumable | Initialize the resumable upload of a video
*VideosApi* | [**AddVideoPlaylistVideo**](docs/VideosApi.md#addvideoplaylistvideo) | **Post** /video-playlists/{playlistId}/videos | Add a video in a playlist
*VideosApi* | [**GetVideoPlaylistVideos**](docs/VideosApi.md#getvideoplaylistvideos) | **Get** /video-playlists/{playlistId}/videos | List videos of a playlist
*VideosApi* | [**UsersMeSubscriptionsVideosGet**](docs/VideosApi.md#usersmesubscriptionsvideosget) | **Get** /users/me/subscriptions/videos | List videos of subscriptions of my user
*VideosApi* | [**UsersMeVideosGet**](docs/VideosApi.md#usersmevideosget) | **Get** /users/me/videos | Get videos of my user
*VideosApi* | [**UsersMeVideosImportsGet**](docs/VideosApi.md#usersmevideosimportsget) | **Get** /users/me/videos/imports | Get video imports of my user


## Documentation For Models

 - [Abuse](docs/Abuse.md)
 - [AbuseMessage](docs/AbuseMessage.md)
 - [AbuseStateConstant](docs/AbuseStateConstant.md)
 - [AbuseStateSet](docs/AbuseStateSet.md)
 - [AbusesAccount](docs/AbusesAccount.md)
 - [AbusesComment](docs/AbusesComment.md)
 - [AbusesVideo](docs/AbusesVideo.md)
 - [Account](docs/Account.md)
 - [AccountAllOf](docs/AccountAllOf.md)
 - [AccountSummary](docs/AccountSummary.md)
 - [Actor](docs/Actor.md)
 - [ActorImage](docs/ActorImage.md)
 - [ActorInfo](docs/ActorInfo.md)
 - [ActorInfoAvatar](docs/ActorInfoAvatar.md)
 - [AddUser](docs/AddUser.md)
 - [AddUserResponse](docs/AddUserResponse.md)
 - [AddUserResponseUser](docs/AddUserResponseUser.md)
 - [CommentThreadPostResponse](docs/CommentThreadPostResponse.md)
 - [CommentThreadResponse](docs/CommentThreadResponse.md)
 - [CustomHomepage](docs/CustomHomepage.md)
 - [FileRedundancyInformation](docs/FileRedundancyInformation.md)
 - [Follow](docs/Follow.md)
 - [GetMeVideoRating](docs/GetMeVideoRating.md)
 - [InlineObject](docs/InlineObject.md)
 - [InlineObject1](docs/InlineObject1.md)
 - [InlineObject11](docs/InlineObject11.md)
 - [InlineObject12](docs/InlineObject12.md)
 - [InlineObject13](docs/InlineObject13.md)
 - [InlineObject19](docs/InlineObject19.md)
 - [InlineObject2](docs/InlineObject2.md)
 - [InlineObject20](docs/InlineObject20.md)
 - [InlineObject21](docs/InlineObject21.md)
 - [InlineObject22](docs/InlineObject22.md)
 - [InlineObject23](docs/InlineObject23.md)
 - [InlineObject24](docs/InlineObject24.md)
 - [InlineObject25](docs/InlineObject25.md)
 - [InlineObject26](docs/InlineObject26.md)
 - [InlineObject27](docs/InlineObject27.md)
 - [InlineObject28](docs/InlineObject28.md)
 - [InlineObject29](docs/InlineObject29.md)
 - [InlineObject3](docs/InlineObject3.md)
 - [InlineObject30](docs/InlineObject30.md)
 - [InlineObject4](docs/InlineObject4.md)
 - [InlineObject5](docs/InlineObject5.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [InlineResponse2001](docs/InlineResponse2001.md)
 - [InlineResponse20010](docs/InlineResponse20010.md)
 - [InlineResponse20011](docs/InlineResponse20011.md)
 - [InlineResponse20012](docs/InlineResponse20012.md)
 - [InlineResponse20012VideoPlaylist](docs/InlineResponse20012VideoPlaylist.md)
 - [InlineResponse20013](docs/InlineResponse20013.md)
 - [InlineResponse20013VideoPlaylistElement](docs/InlineResponse20013VideoPlaylistElement.md)
 - [InlineResponse20014](docs/InlineResponse20014.md)
 - [InlineResponse20014VideoId](docs/InlineResponse20014VideoId.md)
 - [InlineResponse2002](docs/InlineResponse2002.md)
 - [InlineResponse2003](docs/InlineResponse2003.md)
 - [InlineResponse2004](docs/InlineResponse2004.md)
 - [InlineResponse2005](docs/InlineResponse2005.md)
 - [InlineResponse2006](docs/InlineResponse2006.md)
 - [InlineResponse2006Abuse](docs/InlineResponse2006Abuse.md)
 - [InlineResponse2007](docs/InlineResponse2007.md)
 - [InlineResponse2008](docs/InlineResponse2008.md)
 - [InlineResponse2009](docs/InlineResponse2009.md)
 - [InlineResponse204](docs/InlineResponse204.md)
 - [InlineResponse204VideoChannel](docs/InlineResponse204VideoChannel.md)
 - [Job](docs/Job.md)
 - [LiveVideoResponse](docs/LiveVideoResponse.md)
 - [LiveVideoUpdate](docs/LiveVideoUpdate.md)
 - [MRSSGroupContent](docs/MRSSGroupContent.md)
 - [MRSSPeerLink](docs/MRSSPeerLink.md)
 - [NSFWPolicy](docs/NSFWPolicy.md)
 - [Notification](docs/Notification.md)
 - [NotificationActorFollow](docs/NotificationActorFollow.md)
 - [NotificationActorFollowFollowing](docs/NotificationActorFollowFollowing.md)
 - [NotificationComment](docs/NotificationComment.md)
 - [NotificationListResponse](docs/NotificationListResponse.md)
 - [NotificationSettingValue](docs/NotificationSettingValue.md)
 - [NotificationVideoAbuse](docs/NotificationVideoAbuse.md)
 - [NotificationVideoImport](docs/NotificationVideoImport.md)
 - [OAuthClient](docs/OAuthClient.md)
 - [PlaylistElement](docs/PlaylistElement.md)
 - [Plugin](docs/Plugin.md)
 - [PluginResponse](docs/PluginResponse.md)
 - [RegisterUser](docs/RegisterUser.md)
 - [RegisterUserChannel](docs/RegisterUserChannel.md)
 - [ServerConfig](docs/ServerConfig.md)
 - [ServerConfigAbout](docs/ServerConfigAbout.md)
 - [ServerConfigAboutInstance](docs/ServerConfigAboutInstance.md)
 - [ServerConfigAutoBlacklist](docs/ServerConfigAutoBlacklist.md)
 - [ServerConfigAutoBlacklistVideos](docs/ServerConfigAutoBlacklistVideos.md)
 - [ServerConfigAvatar](docs/ServerConfigAvatar.md)
 - [ServerConfigAvatarFile](docs/ServerConfigAvatarFile.md)
 - [ServerConfigAvatarFileSize](docs/ServerConfigAvatarFileSize.md)
 - [ServerConfigCustom](docs/ServerConfigCustom.md)
 - [ServerConfigCustomAdmin](docs/ServerConfigCustomAdmin.md)
 - [ServerConfigCustomCache](docs/ServerConfigCustomCache.md)
 - [ServerConfigCustomCachePreviews](docs/ServerConfigCustomCachePreviews.md)
 - [ServerConfigCustomFollowers](docs/ServerConfigCustomFollowers.md)
 - [ServerConfigCustomFollowersInstance](docs/ServerConfigCustomFollowersInstance.md)
 - [ServerConfigCustomInstance](docs/ServerConfigCustomInstance.md)
 - [ServerConfigCustomServices](docs/ServerConfigCustomServices.md)
 - [ServerConfigCustomServicesTwitter](docs/ServerConfigCustomServicesTwitter.md)
 - [ServerConfigCustomSignup](docs/ServerConfigCustomSignup.md)
 - [ServerConfigCustomTheme](docs/ServerConfigCustomTheme.md)
 - [ServerConfigCustomTranscoding](docs/ServerConfigCustomTranscoding.md)
 - [ServerConfigCustomTranscodingHls](docs/ServerConfigCustomTranscodingHls.md)
 - [ServerConfigCustomTranscodingResolutions](docs/ServerConfigCustomTranscodingResolutions.md)
 - [ServerConfigCustomTranscodingWebtorrent](docs/ServerConfigCustomTranscodingWebtorrent.md)
 - [ServerConfigCustomUser](docs/ServerConfigCustomUser.md)
 - [ServerConfigEmail](docs/ServerConfigEmail.md)
 - [ServerConfigFollowings](docs/ServerConfigFollowings.md)
 - [ServerConfigFollowingsInstance](docs/ServerConfigFollowingsInstance.md)
 - [ServerConfigFollowingsInstanceAutoFollowIndex](docs/ServerConfigFollowingsInstanceAutoFollowIndex.md)
 - [ServerConfigImport](docs/ServerConfigImport.md)
 - [ServerConfigImportVideos](docs/ServerConfigImportVideos.md)
 - [ServerConfigInstance](docs/ServerConfigInstance.md)
 - [ServerConfigInstanceCustomizations](docs/ServerConfigInstanceCustomizations.md)
 - [ServerConfigPlugin](docs/ServerConfigPlugin.md)
 - [ServerConfigSearch](docs/ServerConfigSearch.md)
 - [ServerConfigSearchRemoteUri](docs/ServerConfigSearchRemoteUri.md)
 - [ServerConfigSignup](docs/ServerConfigSignup.md)
 - [ServerConfigTranscoding](docs/ServerConfigTranscoding.md)
 - [ServerConfigTrending](docs/ServerConfigTrending.md)
 - [ServerConfigTrendingVideos](docs/ServerConfigTrendingVideos.md)
 - [ServerConfigUser](docs/ServerConfigUser.md)
 - [ServerConfigVideo](docs/ServerConfigVideo.md)
 - [ServerConfigVideoCaption](docs/ServerConfigVideoCaption.md)
 - [ServerConfigVideoCaptionFile](docs/ServerConfigVideoCaptionFile.md)
 - [ServerConfigVideoFile](docs/ServerConfigVideoFile.md)
 - [ServerConfigVideoImage](docs/ServerConfigVideoImage.md)
 - [UpdateMe](docs/UpdateMe.md)
 - [UpdateUser](docs/UpdateUser.md)
 - [User](docs/User.md)
 - [UserAdminFlags](docs/UserAdminFlags.md)
 - [UserRole](docs/UserRole.md)
 - [UserWatchingVideo](docs/UserWatchingVideo.md)
 - [UserWithStats](docs/UserWithStats.md)
 - [UserWithStatsAllOf](docs/UserWithStatsAllOf.md)
 - [Video](docs/Video.md)
 - [VideoBlacklist](docs/VideoBlacklist.md)
 - [VideoCaption](docs/VideoCaption.md)
 - [VideoChannel](docs/VideoChannel.md)
 - [VideoChannelCreate](docs/VideoChannelCreate.md)
 - [VideoChannelCreateAllOf](docs/VideoChannelCreateAllOf.md)
 - [VideoChannelList](docs/VideoChannelList.md)
 - [VideoChannelOwnerAccount](docs/VideoChannelOwnerAccount.md)
 - [VideoChannelSummary](docs/VideoChannelSummary.md)
 - [VideoChannelUpdate](docs/VideoChannelUpdate.md)
 - [VideoChannelUpdateAllOf](docs/VideoChannelUpdateAllOf.md)
 - [VideoComment](docs/VideoComment.md)
 - [VideoCommentThreadTree](docs/VideoCommentThreadTree.md)
 - [VideoConstantNumberCategory](docs/VideoConstantNumberCategory.md)
 - [VideoConstantNumberLicence](docs/VideoConstantNumberLicence.md)
 - [VideoConstantStringLanguage](docs/VideoConstantStringLanguage.md)
 - [VideoDetails](docs/VideoDetails.md)
 - [VideoDetailsAllOf](docs/VideoDetailsAllOf.md)
 - [VideoFile](docs/VideoFile.md)
 - [VideoImport](docs/VideoImport.md)
 - [VideoImportStateConstant](docs/VideoImportStateConstant.md)
 - [VideoImportsList](docs/VideoImportsList.md)
 - [VideoInfo](docs/VideoInfo.md)
 - [VideoListResponse](docs/VideoListResponse.md)
 - [VideoPlaylist](docs/VideoPlaylist.md)
 - [VideoPlaylistPrivacyConstant](docs/VideoPlaylistPrivacyConstant.md)
 - [VideoPlaylistPrivacySet](docs/VideoPlaylistPrivacySet.md)
 - [VideoPlaylistTypeConstant](docs/VideoPlaylistTypeConstant.md)
 - [VideoPlaylistTypeSet](docs/VideoPlaylistTypeSet.md)
 - [VideoPrivacyConstant](docs/VideoPrivacyConstant.md)
 - [VideoPrivacySet](docs/VideoPrivacySet.md)
 - [VideoRating](docs/VideoRating.md)
 - [VideoRedundancy](docs/VideoRedundancy.md)
 - [VideoRedundancyRedundancies](docs/VideoRedundancyRedundancies.md)
 - [VideoResolutionConstant](docs/VideoResolutionConstant.md)
 - [VideoScheduledUpdate](docs/VideoScheduledUpdate.md)
 - [VideoStateConstant](docs/VideoStateConstant.md)
 - [VideoStreamingPlaylists](docs/VideoStreamingPlaylists.md)
 - [VideoStreamingPlaylistsAllOf](docs/VideoStreamingPlaylistsAllOf.md)
 - [VideoStreamingPlaylistsHLS](docs/VideoStreamingPlaylistsHLS.md)
 - [VideoStreamingPlaylistsHLSRedundancies](docs/VideoStreamingPlaylistsHLSRedundancies.md)
 - [VideoUploadRequestCommon](docs/VideoUploadRequestCommon.md)
 - [VideoUploadRequestResumable](docs/VideoUploadRequestResumable.md)
 - [VideoUploadRequestResumableAllOf](docs/VideoUploadRequestResumableAllOf.md)
 - [VideoUploadResponse](docs/VideoUploadResponse.md)
 - [VideoUploadResponseVideo](docs/VideoUploadResponseVideo.md)
 - [VideoUserHistory](docs/VideoUserHistory.md)


## Documentation For Authorization



## OAuth2


- **Type**: OAuth
- **Flow**: password
- **Authorization URL**: 
- **Scopes**: 
 - **admin**: Admin scope
 - **moderator**: Moderator scope
 - **user**: User scope

Example

```golang
auth := context.WithValue(context.Background(), sw.ContextAccessToken, "ACCESSTOKENSTRING")
r, err := client.Service.Operation(auth, args)
```

Or via OAuth2 module to automatically refresh tokens and perform user authentication.

```golang
import "golang.org/x/oauth2"

/* Perform OAuth2 round trip request and obtain a token */

tokenSource := oauth2cfg.TokenSource(createContext(httpClient), &token)
auth := context.WithValue(oauth2.NoContext, sw.ContextOAuth2, tokenSource)
r, err := client.Service.Operation(auth, args)
```


## License

Copyright (C) 2015-2020 PeerTube Contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see http://www.gnu.org/licenses.
