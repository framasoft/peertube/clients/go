/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"encoding/json"
)

// VideoDetailsAllOf struct for VideoDetailsAllOf
type VideoDetailsAllOf struct {
	// path at which to get the full description of maximum `10000` characters
	DescriptionPath *string `json:"descriptionPath,omitempty"`
	// A text tell the audience how to support the video creator
	Support *string `json:"support,omitempty"`
	Channel *VideoChannel `json:"channel,omitempty"`
	Account *Account `json:"account,omitempty"`
	Tags *[]string `json:"tags,omitempty"`
	CommentsEnabled *bool `json:"commentsEnabled,omitempty"`
	DownloadEnabled *bool `json:"downloadEnabled,omitempty"`
	TrackerUrls *[]string `json:"trackerUrls,omitempty"`
	// WebTorrent/raw video files. If WebTorrent is disabled on the server:  - field will be empty - video files will be found in `streamingPlaylists[].files` field 
	Files *[]VideoFile `json:"files,omitempty"`
	// HLS playlists/manifest files. If HLS is disabled on the server:  - field will be empty - video files will be found in `files` field 
	StreamingPlaylists *[]VideoStreamingPlaylists `json:"streamingPlaylists,omitempty"`
}

// NewVideoDetailsAllOf instantiates a new VideoDetailsAllOf object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewVideoDetailsAllOf() *VideoDetailsAllOf {
	this := VideoDetailsAllOf{}
	return &this
}

// NewVideoDetailsAllOfWithDefaults instantiates a new VideoDetailsAllOf object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewVideoDetailsAllOfWithDefaults() *VideoDetailsAllOf {
	this := VideoDetailsAllOf{}
	return &this
}

// GetDescriptionPath returns the DescriptionPath field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetDescriptionPath() string {
	if o == nil || o.DescriptionPath == nil {
		var ret string
		return ret
	}
	return *o.DescriptionPath
}

// GetDescriptionPathOk returns a tuple with the DescriptionPath field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetDescriptionPathOk() (*string, bool) {
	if o == nil || o.DescriptionPath == nil {
		return nil, false
	}
	return o.DescriptionPath, true
}

// HasDescriptionPath returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasDescriptionPath() bool {
	if o != nil && o.DescriptionPath != nil {
		return true
	}

	return false
}

// SetDescriptionPath gets a reference to the given string and assigns it to the DescriptionPath field.
func (o *VideoDetailsAllOf) SetDescriptionPath(v string) {
	o.DescriptionPath = &v
}

// GetSupport returns the Support field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetSupport() string {
	if o == nil || o.Support == nil {
		var ret string
		return ret
	}
	return *o.Support
}

// GetSupportOk returns a tuple with the Support field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetSupportOk() (*string, bool) {
	if o == nil || o.Support == nil {
		return nil, false
	}
	return o.Support, true
}

// HasSupport returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasSupport() bool {
	if o != nil && o.Support != nil {
		return true
	}

	return false
}

// SetSupport gets a reference to the given string and assigns it to the Support field.
func (o *VideoDetailsAllOf) SetSupport(v string) {
	o.Support = &v
}

// GetChannel returns the Channel field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetChannel() VideoChannel {
	if o == nil || o.Channel == nil {
		var ret VideoChannel
		return ret
	}
	return *o.Channel
}

// GetChannelOk returns a tuple with the Channel field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetChannelOk() (*VideoChannel, bool) {
	if o == nil || o.Channel == nil {
		return nil, false
	}
	return o.Channel, true
}

// HasChannel returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasChannel() bool {
	if o != nil && o.Channel != nil {
		return true
	}

	return false
}

// SetChannel gets a reference to the given VideoChannel and assigns it to the Channel field.
func (o *VideoDetailsAllOf) SetChannel(v VideoChannel) {
	o.Channel = &v
}

// GetAccount returns the Account field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetAccount() Account {
	if o == nil || o.Account == nil {
		var ret Account
		return ret
	}
	return *o.Account
}

// GetAccountOk returns a tuple with the Account field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetAccountOk() (*Account, bool) {
	if o == nil || o.Account == nil {
		return nil, false
	}
	return o.Account, true
}

// HasAccount returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasAccount() bool {
	if o != nil && o.Account != nil {
		return true
	}

	return false
}

// SetAccount gets a reference to the given Account and assigns it to the Account field.
func (o *VideoDetailsAllOf) SetAccount(v Account) {
	o.Account = &v
}

// GetTags returns the Tags field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetTags() []string {
	if o == nil || o.Tags == nil {
		var ret []string
		return ret
	}
	return *o.Tags
}

// GetTagsOk returns a tuple with the Tags field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetTagsOk() (*[]string, bool) {
	if o == nil || o.Tags == nil {
		return nil, false
	}
	return o.Tags, true
}

// HasTags returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasTags() bool {
	if o != nil && o.Tags != nil {
		return true
	}

	return false
}

// SetTags gets a reference to the given []string and assigns it to the Tags field.
func (o *VideoDetailsAllOf) SetTags(v []string) {
	o.Tags = &v
}

// GetCommentsEnabled returns the CommentsEnabled field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetCommentsEnabled() bool {
	if o == nil || o.CommentsEnabled == nil {
		var ret bool
		return ret
	}
	return *o.CommentsEnabled
}

// GetCommentsEnabledOk returns a tuple with the CommentsEnabled field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetCommentsEnabledOk() (*bool, bool) {
	if o == nil || o.CommentsEnabled == nil {
		return nil, false
	}
	return o.CommentsEnabled, true
}

// HasCommentsEnabled returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasCommentsEnabled() bool {
	if o != nil && o.CommentsEnabled != nil {
		return true
	}

	return false
}

// SetCommentsEnabled gets a reference to the given bool and assigns it to the CommentsEnabled field.
func (o *VideoDetailsAllOf) SetCommentsEnabled(v bool) {
	o.CommentsEnabled = &v
}

// GetDownloadEnabled returns the DownloadEnabled field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetDownloadEnabled() bool {
	if o == nil || o.DownloadEnabled == nil {
		var ret bool
		return ret
	}
	return *o.DownloadEnabled
}

// GetDownloadEnabledOk returns a tuple with the DownloadEnabled field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetDownloadEnabledOk() (*bool, bool) {
	if o == nil || o.DownloadEnabled == nil {
		return nil, false
	}
	return o.DownloadEnabled, true
}

// HasDownloadEnabled returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasDownloadEnabled() bool {
	if o != nil && o.DownloadEnabled != nil {
		return true
	}

	return false
}

// SetDownloadEnabled gets a reference to the given bool and assigns it to the DownloadEnabled field.
func (o *VideoDetailsAllOf) SetDownloadEnabled(v bool) {
	o.DownloadEnabled = &v
}

// GetTrackerUrls returns the TrackerUrls field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetTrackerUrls() []string {
	if o == nil || o.TrackerUrls == nil {
		var ret []string
		return ret
	}
	return *o.TrackerUrls
}

// GetTrackerUrlsOk returns a tuple with the TrackerUrls field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetTrackerUrlsOk() (*[]string, bool) {
	if o == nil || o.TrackerUrls == nil {
		return nil, false
	}
	return o.TrackerUrls, true
}

// HasTrackerUrls returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasTrackerUrls() bool {
	if o != nil && o.TrackerUrls != nil {
		return true
	}

	return false
}

// SetTrackerUrls gets a reference to the given []string and assigns it to the TrackerUrls field.
func (o *VideoDetailsAllOf) SetTrackerUrls(v []string) {
	o.TrackerUrls = &v
}

// GetFiles returns the Files field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetFiles() []VideoFile {
	if o == nil || o.Files == nil {
		var ret []VideoFile
		return ret
	}
	return *o.Files
}

// GetFilesOk returns a tuple with the Files field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetFilesOk() (*[]VideoFile, bool) {
	if o == nil || o.Files == nil {
		return nil, false
	}
	return o.Files, true
}

// HasFiles returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasFiles() bool {
	if o != nil && o.Files != nil {
		return true
	}

	return false
}

// SetFiles gets a reference to the given []VideoFile and assigns it to the Files field.
func (o *VideoDetailsAllOf) SetFiles(v []VideoFile) {
	o.Files = &v
}

// GetStreamingPlaylists returns the StreamingPlaylists field value if set, zero value otherwise.
func (o *VideoDetailsAllOf) GetStreamingPlaylists() []VideoStreamingPlaylists {
	if o == nil || o.StreamingPlaylists == nil {
		var ret []VideoStreamingPlaylists
		return ret
	}
	return *o.StreamingPlaylists
}

// GetStreamingPlaylistsOk returns a tuple with the StreamingPlaylists field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoDetailsAllOf) GetStreamingPlaylistsOk() (*[]VideoStreamingPlaylists, bool) {
	if o == nil || o.StreamingPlaylists == nil {
		return nil, false
	}
	return o.StreamingPlaylists, true
}

// HasStreamingPlaylists returns a boolean if a field has been set.
func (o *VideoDetailsAllOf) HasStreamingPlaylists() bool {
	if o != nil && o.StreamingPlaylists != nil {
		return true
	}

	return false
}

// SetStreamingPlaylists gets a reference to the given []VideoStreamingPlaylists and assigns it to the StreamingPlaylists field.
func (o *VideoDetailsAllOf) SetStreamingPlaylists(v []VideoStreamingPlaylists) {
	o.StreamingPlaylists = &v
}

func (o VideoDetailsAllOf) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.DescriptionPath != nil {
		toSerialize["descriptionPath"] = o.DescriptionPath
	}
	if o.Support != nil {
		toSerialize["support"] = o.Support
	}
	if o.Channel != nil {
		toSerialize["channel"] = o.Channel
	}
	if o.Account != nil {
		toSerialize["account"] = o.Account
	}
	if o.Tags != nil {
		toSerialize["tags"] = o.Tags
	}
	if o.CommentsEnabled != nil {
		toSerialize["commentsEnabled"] = o.CommentsEnabled
	}
	if o.DownloadEnabled != nil {
		toSerialize["downloadEnabled"] = o.DownloadEnabled
	}
	if o.TrackerUrls != nil {
		toSerialize["trackerUrls"] = o.TrackerUrls
	}
	if o.Files != nil {
		toSerialize["files"] = o.Files
	}
	if o.StreamingPlaylists != nil {
		toSerialize["streamingPlaylists"] = o.StreamingPlaylists
	}
	return json.Marshal(toSerialize)
}

type NullableVideoDetailsAllOf struct {
	value *VideoDetailsAllOf
	isSet bool
}

func (v NullableVideoDetailsAllOf) Get() *VideoDetailsAllOf {
	return v.value
}

func (v *NullableVideoDetailsAllOf) Set(val *VideoDetailsAllOf) {
	v.value = val
	v.isSet = true
}

func (v NullableVideoDetailsAllOf) IsSet() bool {
	return v.isSet
}

func (v *NullableVideoDetailsAllOf) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableVideoDetailsAllOf(val *VideoDetailsAllOf) *NullableVideoDetailsAllOf {
	return &NullableVideoDetailsAllOf{value: val, isSet: true}
}

func (v NullableVideoDetailsAllOf) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableVideoDetailsAllOf) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


