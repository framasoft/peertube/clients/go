/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"strings"
	"os"
	"reflect"
)

// Linger please
var (
	_ _context.Context
)

// VideoPlaylistsApiService VideoPlaylistsApi service
type VideoPlaylistsApiService service

type ApiAddPlaylistRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	displayName *string
	thumbnailfile **os.File
	privacy *VideoPlaylistPrivacySet
	description *string
	videoChannelId *int32
}

// Video playlist display name
func (r ApiAddPlaylistRequest) DisplayName(displayName string) ApiAddPlaylistRequest {
	r.displayName = &displayName
	return r
}
// Video playlist thumbnail file
func (r ApiAddPlaylistRequest) Thumbnailfile(thumbnailfile *os.File) ApiAddPlaylistRequest {
	r.thumbnailfile = &thumbnailfile
	return r
}
func (r ApiAddPlaylistRequest) Privacy(privacy VideoPlaylistPrivacySet) ApiAddPlaylistRequest {
	r.privacy = &privacy
	return r
}
// Video playlist description
func (r ApiAddPlaylistRequest) Description(description string) ApiAddPlaylistRequest {
	r.description = &description
	return r
}
// Video channel in which the playlist will be published
func (r ApiAddPlaylistRequest) VideoChannelId(videoChannelId int32) ApiAddPlaylistRequest {
	r.videoChannelId = &videoChannelId
	return r
}

func (r ApiAddPlaylistRequest) Execute() (InlineResponse20012, *_nethttp.Response, error) {
	return r.ApiService.AddPlaylistExecute(r)
}

/*
AddPlaylist Create a video playlist

If the video playlist is set as public, `videoChannelId` is mandatory.

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAddPlaylistRequest
*/
func (a *VideoPlaylistsApiService) AddPlaylist(ctx _context.Context) ApiAddPlaylistRequest {
	return ApiAddPlaylistRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return InlineResponse20012
func (a *VideoPlaylistsApiService) AddPlaylistExecute(r ApiAddPlaylistRequest) (InlineResponse20012, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse20012
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.AddPlaylist")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.displayName == nil {
		return localVarReturnValue, nil, reportError("displayName is required and must be specified")
	}
	if strlen(*r.displayName) < 1 {
		return localVarReturnValue, nil, reportError("displayName must have at least 1 elements")
	}
	if strlen(*r.displayName) > 120 {
		return localVarReturnValue, nil, reportError("displayName must have less than 120 elements")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"multipart/form-data"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarFormParams.Add("displayName", parameterToString(*r.displayName, ""))
	localVarFormFileName = "thumbnailfile"
	var localVarFile *os.File
	if r.thumbnailfile != nil {
		localVarFile = *r.thumbnailfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	if r.privacy != nil {
		localVarFormParams.Add("privacy", parameterToString(*r.privacy, ""))
	}
	if r.description != nil {
		localVarFormParams.Add("description", parameterToString(*r.description, ""))
	}
	if r.videoChannelId != nil {
		paramJson, err := parameterToJson(*r.videoChannelId)
		if err != nil {
			return localVarReturnValue, nil, err
		}
		localVarFormParams.Add("videoChannelId", paramJson)
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAddVideoPlaylistVideoRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	playlistId int32
	inlineObject19 *InlineObject19
}

func (r ApiAddVideoPlaylistVideoRequest) InlineObject19(inlineObject19 InlineObject19) ApiAddVideoPlaylistVideoRequest {
	r.inlineObject19 = &inlineObject19
	return r
}

func (r ApiAddVideoPlaylistVideoRequest) Execute() (InlineResponse20013, *_nethttp.Response, error) {
	return r.ApiService.AddVideoPlaylistVideoExecute(r)
}

/*
AddVideoPlaylistVideo Add a video in a playlist

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @return ApiAddVideoPlaylistVideoRequest
*/
func (a *VideoPlaylistsApiService) AddVideoPlaylistVideo(ctx _context.Context, playlistId int32) ApiAddVideoPlaylistVideoRequest {
	return ApiAddVideoPlaylistVideoRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
	}
}

// Execute executes the request
//  @return InlineResponse20013
func (a *VideoPlaylistsApiService) AddVideoPlaylistVideoExecute(r ApiAddVideoPlaylistVideoRequest) (InlineResponse20013, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse20013
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.AddVideoPlaylistVideo")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}/videos"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return localVarReturnValue, nil, reportError("playlistId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.inlineObject19
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiDelVideoPlaylistVideoRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	playlistId int32
	playlistElementId int32
}


func (r ApiDelVideoPlaylistVideoRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.DelVideoPlaylistVideoExecute(r)
}

/*
DelVideoPlaylistVideo Delete an element from a playlist

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @param playlistElementId Playlist element id
 @return ApiDelVideoPlaylistVideoRequest
*/
func (a *VideoPlaylistsApiService) DelVideoPlaylistVideo(ctx _context.Context, playlistId int32, playlistElementId int32) ApiDelVideoPlaylistVideoRequest {
	return ApiDelVideoPlaylistVideoRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
		playlistElementId: playlistElementId,
	}
}

// Execute executes the request
func (a *VideoPlaylistsApiService) DelVideoPlaylistVideoExecute(r ApiDelVideoPlaylistVideoRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodDelete
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.DelVideoPlaylistVideo")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}/videos/{playlistElementId}"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"playlistElementId"+"}", _neturl.PathEscape(parameterToString(r.playlistElementId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return nil, reportError("playlistId must be greater than 1")
	}
	if r.playlistElementId < 1 {
		return nil, reportError("playlistElementId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiGetPlaylistPrivacyPoliciesRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
}


func (r ApiGetPlaylistPrivacyPoliciesRequest) Execute() ([]string, *_nethttp.Response, error) {
	return r.ApiService.GetPlaylistPrivacyPoliciesExecute(r)
}

/*
GetPlaylistPrivacyPolicies List available playlist privacy policies

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGetPlaylistPrivacyPoliciesRequest
*/
func (a *VideoPlaylistsApiService) GetPlaylistPrivacyPolicies(ctx _context.Context) ApiGetPlaylistPrivacyPoliciesRequest {
	return ApiGetPlaylistPrivacyPoliciesRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return []string
func (a *VideoPlaylistsApiService) GetPlaylistPrivacyPoliciesExecute(r ApiGetPlaylistPrivacyPoliciesRequest) ([]string, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  []string
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.GetPlaylistPrivacyPolicies")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/privacies"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetPlaylistsRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	start *int32
	count *int32
	sort *string
}

// Offset used to paginate results
func (r ApiGetPlaylistsRequest) Start(start int32) ApiGetPlaylistsRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiGetPlaylistsRequest) Count(count int32) ApiGetPlaylistsRequest {
	r.count = &count
	return r
}
// Sort column
func (r ApiGetPlaylistsRequest) Sort(sort string) ApiGetPlaylistsRequest {
	r.sort = &sort
	return r
}

func (r ApiGetPlaylistsRequest) Execute() (InlineResponse20011, *_nethttp.Response, error) {
	return r.ApiService.GetPlaylistsExecute(r)
}

/*
GetPlaylists List video playlists

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGetPlaylistsRequest
*/
func (a *VideoPlaylistsApiService) GetPlaylists(ctx _context.Context) ApiGetPlaylistsRequest {
	return ApiGetPlaylistsRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return InlineResponse20011
func (a *VideoPlaylistsApiService) GetPlaylistsExecute(r ApiGetPlaylistsRequest) (InlineResponse20011, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse20011
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.GetPlaylists")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetVideoPlaylistVideosRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	playlistId int32
}


func (r ApiGetVideoPlaylistVideosRequest) Execute() (VideoListResponse, *_nethttp.Response, error) {
	return r.ApiService.GetVideoPlaylistVideosExecute(r)
}

/*
GetVideoPlaylistVideos List videos of a playlist

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @return ApiGetVideoPlaylistVideosRequest
*/
func (a *VideoPlaylistsApiService) GetVideoPlaylistVideos(ctx _context.Context, playlistId int32) ApiGetVideoPlaylistVideosRequest {
	return ApiGetVideoPlaylistVideosRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
	}
}

// Execute executes the request
//  @return VideoListResponse
func (a *VideoPlaylistsApiService) GetVideoPlaylistVideosExecute(r ApiGetVideoPlaylistVideosRequest) (VideoListResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoListResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.GetVideoPlaylistVideos")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}/videos"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return localVarReturnValue, nil, reportError("playlistId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiPutVideoPlaylistVideoRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	playlistId int32
	playlistElementId int32
	inlineObject21 *InlineObject21
}

func (r ApiPutVideoPlaylistVideoRequest) InlineObject21(inlineObject21 InlineObject21) ApiPutVideoPlaylistVideoRequest {
	r.inlineObject21 = &inlineObject21
	return r
}

func (r ApiPutVideoPlaylistVideoRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.PutVideoPlaylistVideoExecute(r)
}

/*
PutVideoPlaylistVideo Update a playlist element

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @param playlistElementId Playlist element id
 @return ApiPutVideoPlaylistVideoRequest
*/
func (a *VideoPlaylistsApiService) PutVideoPlaylistVideo(ctx _context.Context, playlistId int32, playlistElementId int32) ApiPutVideoPlaylistVideoRequest {
	return ApiPutVideoPlaylistVideoRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
		playlistElementId: playlistElementId,
	}
}

// Execute executes the request
func (a *VideoPlaylistsApiService) PutVideoPlaylistVideoExecute(r ApiPutVideoPlaylistVideoRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPut
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.PutVideoPlaylistVideo")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}/videos/{playlistElementId}"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"playlistElementId"+"}", _neturl.PathEscape(parameterToString(r.playlistElementId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return nil, reportError("playlistId must be greater than 1")
	}
	if r.playlistElementId < 1 {
		return nil, reportError("playlistElementId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.inlineObject21
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiReorderVideoPlaylistRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	playlistId int32
	inlineObject20 *InlineObject20
}

func (r ApiReorderVideoPlaylistRequest) InlineObject20(inlineObject20 InlineObject20) ApiReorderVideoPlaylistRequest {
	r.inlineObject20 = &inlineObject20
	return r
}

func (r ApiReorderVideoPlaylistRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.ReorderVideoPlaylistExecute(r)
}

/*
ReorderVideoPlaylist Reorder a playlist

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @return ApiReorderVideoPlaylistRequest
*/
func (a *VideoPlaylistsApiService) ReorderVideoPlaylist(ctx _context.Context, playlistId int32) ApiReorderVideoPlaylistRequest {
	return ApiReorderVideoPlaylistRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
	}
}

// Execute executes the request
func (a *VideoPlaylistsApiService) ReorderVideoPlaylistExecute(r ApiReorderVideoPlaylistRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.ReorderVideoPlaylist")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}/videos/reorder"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return nil, reportError("playlistId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.inlineObject20
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiUsersMeVideoPlaylistsVideosExistGetRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	videoIds *[]int32
}

// The video ids to check
func (r ApiUsersMeVideoPlaylistsVideosExistGetRequest) VideoIds(videoIds []int32) ApiUsersMeVideoPlaylistsVideosExistGetRequest {
	r.videoIds = &videoIds
	return r
}

func (r ApiUsersMeVideoPlaylistsVideosExistGetRequest) Execute() (InlineResponse20014, *_nethttp.Response, error) {
	return r.ApiService.UsersMeVideoPlaylistsVideosExistGetExecute(r)
}

/*
UsersMeVideoPlaylistsVideosExistGet Check video exists in my playlists

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiUsersMeVideoPlaylistsVideosExistGetRequest
*/
func (a *VideoPlaylistsApiService) UsersMeVideoPlaylistsVideosExistGet(ctx _context.Context) ApiUsersMeVideoPlaylistsVideosExistGetRequest {
	return ApiUsersMeVideoPlaylistsVideosExistGetRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return InlineResponse20014
func (a *VideoPlaylistsApiService) UsersMeVideoPlaylistsVideosExistGetExecute(r ApiUsersMeVideoPlaylistsVideosExistGetRequest) (InlineResponse20014, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse20014
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.UsersMeVideoPlaylistsVideosExistGet")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/users/me/video-playlists/videos-exist"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.videoIds == nil {
		return localVarReturnValue, nil, reportError("videoIds is required and must be specified")
	}

	{
		t := *r.videoIds
		if reflect.TypeOf(t).Kind() == reflect.Slice {
			s := reflect.ValueOf(t)
			for i := 0; i < s.Len(); i++ {
				localVarQueryParams.Add("videoIds", parameterToString(s.Index(i), "multi"))
			}
		} else {
			localVarQueryParams.Add("videoIds", parameterToString(t, "multi"))
		}
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiVideoPlaylistsPlaylistIdDeleteRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	playlistId int32
}


func (r ApiVideoPlaylistsPlaylistIdDeleteRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.VideoPlaylistsPlaylistIdDeleteExecute(r)
}

/*
VideoPlaylistsPlaylistIdDelete Delete a video playlist

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @return ApiVideoPlaylistsPlaylistIdDeleteRequest
*/
func (a *VideoPlaylistsApiService) VideoPlaylistsPlaylistIdDelete(ctx _context.Context, playlistId int32) ApiVideoPlaylistsPlaylistIdDeleteRequest {
	return ApiVideoPlaylistsPlaylistIdDeleteRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
	}
}

// Execute executes the request
func (a *VideoPlaylistsApiService) VideoPlaylistsPlaylistIdDeleteExecute(r ApiVideoPlaylistsPlaylistIdDeleteRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodDelete
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.VideoPlaylistsPlaylistIdDelete")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return nil, reportError("playlistId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiVideoPlaylistsPlaylistIdGetRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	playlistId int32
}


func (r ApiVideoPlaylistsPlaylistIdGetRequest) Execute() (VideoPlaylist, *_nethttp.Response, error) {
	return r.ApiService.VideoPlaylistsPlaylistIdGetExecute(r)
}

/*
VideoPlaylistsPlaylistIdGet Get a video playlist

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @return ApiVideoPlaylistsPlaylistIdGetRequest
*/
func (a *VideoPlaylistsApiService) VideoPlaylistsPlaylistIdGet(ctx _context.Context, playlistId int32) ApiVideoPlaylistsPlaylistIdGetRequest {
	return ApiVideoPlaylistsPlaylistIdGetRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
	}
}

// Execute executes the request
//  @return VideoPlaylist
func (a *VideoPlaylistsApiService) VideoPlaylistsPlaylistIdGetExecute(r ApiVideoPlaylistsPlaylistIdGetRequest) (VideoPlaylist, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoPlaylist
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.VideoPlaylistsPlaylistIdGet")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return localVarReturnValue, nil, reportError("playlistId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiVideoPlaylistsPlaylistIdPutRequest struct {
	ctx _context.Context
	ApiService *VideoPlaylistsApiService
	playlistId int32
	displayName *string
	thumbnailfile **os.File
	privacy *VideoPlaylistPrivacySet
	description *string
	videoChannelId *int32
}

// Video playlist display name
func (r ApiVideoPlaylistsPlaylistIdPutRequest) DisplayName(displayName string) ApiVideoPlaylistsPlaylistIdPutRequest {
	r.displayName = &displayName
	return r
}
// Video playlist thumbnail file
func (r ApiVideoPlaylistsPlaylistIdPutRequest) Thumbnailfile(thumbnailfile *os.File) ApiVideoPlaylistsPlaylistIdPutRequest {
	r.thumbnailfile = &thumbnailfile
	return r
}
func (r ApiVideoPlaylistsPlaylistIdPutRequest) Privacy(privacy VideoPlaylistPrivacySet) ApiVideoPlaylistsPlaylistIdPutRequest {
	r.privacy = &privacy
	return r
}
// Video playlist description
func (r ApiVideoPlaylistsPlaylistIdPutRequest) Description(description string) ApiVideoPlaylistsPlaylistIdPutRequest {
	r.description = &description
	return r
}
// Video channel in which the playlist will be published
func (r ApiVideoPlaylistsPlaylistIdPutRequest) VideoChannelId(videoChannelId int32) ApiVideoPlaylistsPlaylistIdPutRequest {
	r.videoChannelId = &videoChannelId
	return r
}

func (r ApiVideoPlaylistsPlaylistIdPutRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.VideoPlaylistsPlaylistIdPutExecute(r)
}

/*
VideoPlaylistsPlaylistIdPut Update a video playlist

If the video playlist is set as public, the playlist must have a assigned channel.

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @return ApiVideoPlaylistsPlaylistIdPutRequest
*/
func (a *VideoPlaylistsApiService) VideoPlaylistsPlaylistIdPut(ctx _context.Context, playlistId int32) ApiVideoPlaylistsPlaylistIdPutRequest {
	return ApiVideoPlaylistsPlaylistIdPutRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
	}
}

// Execute executes the request
func (a *VideoPlaylistsApiService) VideoPlaylistsPlaylistIdPutExecute(r ApiVideoPlaylistsPlaylistIdPutRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPut
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideoPlaylistsApiService.VideoPlaylistsPlaylistIdPut")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return nil, reportError("playlistId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"multipart/form-data"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	if r.displayName != nil {
		localVarFormParams.Add("displayName", parameterToString(*r.displayName, ""))
	}
	localVarFormFileName = "thumbnailfile"
	var localVarFile *os.File
	if r.thumbnailfile != nil {
		localVarFile = *r.thumbnailfile
	}
	if localVarFile != nil {
		fbs, _ := _ioutil.ReadAll(localVarFile)
		localVarFileBytes = fbs
		localVarFileName = localVarFile.Name()
		localVarFile.Close()
	}
	if r.privacy != nil {
		localVarFormParams.Add("privacy", parameterToString(*r.privacy, ""))
	}
	if r.description != nil {
		localVarFormParams.Add("description", parameterToString(*r.description, ""))
	}
	if r.videoChannelId != nil {
		paramJson, err := parameterToJson(*r.videoChannelId)
		if err != nil {
			return nil, err
		}
		localVarFormParams.Add("videoChannelId", paramJson)
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}
