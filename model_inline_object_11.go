/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"encoding/json"
)

// InlineObject11 struct for InlineObject11
type InlineObject11 struct {
	// Reason why the user reports this video
	Reason string `json:"reason"`
	// Reason categories that help triage reports
	PredefinedReasons *[]string `json:"predefinedReasons,omitempty"`
	Video *AbusesVideo `json:"video,omitempty"`
	Comment *AbusesComment `json:"comment,omitempty"`
	Account *AbusesAccount `json:"account,omitempty"`
}

// NewInlineObject11 instantiates a new InlineObject11 object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewInlineObject11(reason string) *InlineObject11 {
	this := InlineObject11{}
	this.Reason = reason
	return &this
}

// NewInlineObject11WithDefaults instantiates a new InlineObject11 object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewInlineObject11WithDefaults() *InlineObject11 {
	this := InlineObject11{}
	return &this
}

// GetReason returns the Reason field value
func (o *InlineObject11) GetReason() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Reason
}

// GetReasonOk returns a tuple with the Reason field value
// and a boolean to check if the value has been set.
func (o *InlineObject11) GetReasonOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Reason, true
}

// SetReason sets field value
func (o *InlineObject11) SetReason(v string) {
	o.Reason = v
}

// GetPredefinedReasons returns the PredefinedReasons field value if set, zero value otherwise.
func (o *InlineObject11) GetPredefinedReasons() []string {
	if o == nil || o.PredefinedReasons == nil {
		var ret []string
		return ret
	}
	return *o.PredefinedReasons
}

// GetPredefinedReasonsOk returns a tuple with the PredefinedReasons field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject11) GetPredefinedReasonsOk() (*[]string, bool) {
	if o == nil || o.PredefinedReasons == nil {
		return nil, false
	}
	return o.PredefinedReasons, true
}

// HasPredefinedReasons returns a boolean if a field has been set.
func (o *InlineObject11) HasPredefinedReasons() bool {
	if o != nil && o.PredefinedReasons != nil {
		return true
	}

	return false
}

// SetPredefinedReasons gets a reference to the given []string and assigns it to the PredefinedReasons field.
func (o *InlineObject11) SetPredefinedReasons(v []string) {
	o.PredefinedReasons = &v
}

// GetVideo returns the Video field value if set, zero value otherwise.
func (o *InlineObject11) GetVideo() AbusesVideo {
	if o == nil || o.Video == nil {
		var ret AbusesVideo
		return ret
	}
	return *o.Video
}

// GetVideoOk returns a tuple with the Video field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject11) GetVideoOk() (*AbusesVideo, bool) {
	if o == nil || o.Video == nil {
		return nil, false
	}
	return o.Video, true
}

// HasVideo returns a boolean if a field has been set.
func (o *InlineObject11) HasVideo() bool {
	if o != nil && o.Video != nil {
		return true
	}

	return false
}

// SetVideo gets a reference to the given AbusesVideo and assigns it to the Video field.
func (o *InlineObject11) SetVideo(v AbusesVideo) {
	o.Video = &v
}

// GetComment returns the Comment field value if set, zero value otherwise.
func (o *InlineObject11) GetComment() AbusesComment {
	if o == nil || o.Comment == nil {
		var ret AbusesComment
		return ret
	}
	return *o.Comment
}

// GetCommentOk returns a tuple with the Comment field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject11) GetCommentOk() (*AbusesComment, bool) {
	if o == nil || o.Comment == nil {
		return nil, false
	}
	return o.Comment, true
}

// HasComment returns a boolean if a field has been set.
func (o *InlineObject11) HasComment() bool {
	if o != nil && o.Comment != nil {
		return true
	}

	return false
}

// SetComment gets a reference to the given AbusesComment and assigns it to the Comment field.
func (o *InlineObject11) SetComment(v AbusesComment) {
	o.Comment = &v
}

// GetAccount returns the Account field value if set, zero value otherwise.
func (o *InlineObject11) GetAccount() AbusesAccount {
	if o == nil || o.Account == nil {
		var ret AbusesAccount
		return ret
	}
	return *o.Account
}

// GetAccountOk returns a tuple with the Account field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineObject11) GetAccountOk() (*AbusesAccount, bool) {
	if o == nil || o.Account == nil {
		return nil, false
	}
	return o.Account, true
}

// HasAccount returns a boolean if a field has been set.
func (o *InlineObject11) HasAccount() bool {
	if o != nil && o.Account != nil {
		return true
	}

	return false
}

// SetAccount gets a reference to the given AbusesAccount and assigns it to the Account field.
func (o *InlineObject11) SetAccount(v AbusesAccount) {
	o.Account = &v
}

func (o InlineObject11) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["reason"] = o.Reason
	}
	if o.PredefinedReasons != nil {
		toSerialize["predefinedReasons"] = o.PredefinedReasons
	}
	if o.Video != nil {
		toSerialize["video"] = o.Video
	}
	if o.Comment != nil {
		toSerialize["comment"] = o.Comment
	}
	if o.Account != nil {
		toSerialize["account"] = o.Account
	}
	return json.Marshal(toSerialize)
}

type NullableInlineObject11 struct {
	value *InlineObject11
	isSet bool
}

func (v NullableInlineObject11) Get() *InlineObject11 {
	return v.value
}

func (v *NullableInlineObject11) Set(val *InlineObject11) {
	v.value = val
	v.isSet = true
}

func (v NullableInlineObject11) IsSet() bool {
	return v.isSet
}

func (v *NullableInlineObject11) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableInlineObject11(val *InlineObject11) *NullableInlineObject11 {
	return &NullableInlineObject11{value: val, isSet: true}
}

func (v NullableInlineObject11) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableInlineObject11) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


