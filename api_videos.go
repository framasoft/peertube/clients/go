/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"strings"
)

// Linger please
var (
	_ _context.Context
)

// VideosApiService VideosApi service
type VideosApiService service

type ApiAddVideoPlaylistVideoRequest struct {
	ctx _context.Context
	ApiService *VideosApiService
	playlistId int32
	inlineObject19 *InlineObject19
}

func (r ApiAddVideoPlaylistVideoRequest) InlineObject19(inlineObject19 InlineObject19) ApiAddVideoPlaylistVideoRequest {
	r.inlineObject19 = &inlineObject19
	return r
}

func (r ApiAddVideoPlaylistVideoRequest) Execute() (InlineResponse20013, *_nethttp.Response, error) {
	return r.ApiService.AddVideoPlaylistVideoExecute(r)
}

/*
AddVideoPlaylistVideo Add a video in a playlist

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @return ApiAddVideoPlaylistVideoRequest
*/
func (a *VideosApiService) AddVideoPlaylistVideo(ctx _context.Context, playlistId int32) ApiAddVideoPlaylistVideoRequest {
	return ApiAddVideoPlaylistVideoRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
	}
}

// Execute executes the request
//  @return InlineResponse20013
func (a *VideosApiService) AddVideoPlaylistVideoExecute(r ApiAddVideoPlaylistVideoRequest) (InlineResponse20013, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  InlineResponse20013
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideosApiService.AddVideoPlaylistVideo")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}/videos"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return localVarReturnValue, nil, reportError("playlistId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.inlineObject19
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetVideoPlaylistVideosRequest struct {
	ctx _context.Context
	ApiService *VideosApiService
	playlistId int32
}


func (r ApiGetVideoPlaylistVideosRequest) Execute() (VideoListResponse, *_nethttp.Response, error) {
	return r.ApiService.GetVideoPlaylistVideosExecute(r)
}

/*
GetVideoPlaylistVideos List videos of a playlist

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param playlistId Playlist id
 @return ApiGetVideoPlaylistVideosRequest
*/
func (a *VideosApiService) GetVideoPlaylistVideos(ctx _context.Context, playlistId int32) ApiGetVideoPlaylistVideosRequest {
	return ApiGetVideoPlaylistVideosRequest{
		ApiService: a,
		ctx: ctx,
		playlistId: playlistId,
	}
}

// Execute executes the request
//  @return VideoListResponse
func (a *VideosApiService) GetVideoPlaylistVideosExecute(r ApiGetVideoPlaylistVideosRequest) (VideoListResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoListResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideosApiService.GetVideoPlaylistVideos")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/video-playlists/{playlistId}/videos"
	localVarPath = strings.Replace(localVarPath, "{"+"playlistId"+"}", _neturl.PathEscape(parameterToString(r.playlistId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.playlistId < 1 {
		return localVarReturnValue, nil, reportError("playlistId must be greater than 1")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiUsersMeSubscriptionsVideosGetRequest struct {
	ctx _context.Context
	ApiService *VideosApiService
	categoryOneOf *OneOfintegerarray
	isLive *bool
	tagsOneOf *OneOfstringarray
	tagsAllOf *OneOfstringarray
	licenceOneOf *OneOfintegerarray
	languageOneOf *OneOfstringarray
	nsfw *string
	filter *string
	skipCount *string
	start *int32
	count *int32
	sort *string
}

// category id of the video (see [/videos/categories](#operation/getCategories))
func (r ApiUsersMeSubscriptionsVideosGetRequest) CategoryOneOf(categoryOneOf OneOfintegerarray) ApiUsersMeSubscriptionsVideosGetRequest {
	r.categoryOneOf = &categoryOneOf
	return r
}
// whether or not the video is a live
func (r ApiUsersMeSubscriptionsVideosGetRequest) IsLive(isLive bool) ApiUsersMeSubscriptionsVideosGetRequest {
	r.isLive = &isLive
	return r
}
// tag(s) of the video
func (r ApiUsersMeSubscriptionsVideosGetRequest) TagsOneOf(tagsOneOf OneOfstringarray) ApiUsersMeSubscriptionsVideosGetRequest {
	r.tagsOneOf = &tagsOneOf
	return r
}
// tag(s) of the video, where all should be present in the video
func (r ApiUsersMeSubscriptionsVideosGetRequest) TagsAllOf(tagsAllOf OneOfstringarray) ApiUsersMeSubscriptionsVideosGetRequest {
	r.tagsAllOf = &tagsAllOf
	return r
}
// licence id of the video (see [/videos/licences](#operation/getLicences))
func (r ApiUsersMeSubscriptionsVideosGetRequest) LicenceOneOf(licenceOneOf OneOfintegerarray) ApiUsersMeSubscriptionsVideosGetRequest {
	r.licenceOneOf = &licenceOneOf
	return r
}
// language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language
func (r ApiUsersMeSubscriptionsVideosGetRequest) LanguageOneOf(languageOneOf OneOfstringarray) ApiUsersMeSubscriptionsVideosGetRequest {
	r.languageOneOf = &languageOneOf
	return r
}
// whether to include nsfw videos, if any
func (r ApiUsersMeSubscriptionsVideosGetRequest) Nsfw(nsfw string) ApiUsersMeSubscriptionsVideosGetRequest {
	r.nsfw = &nsfw
	return r
}
// Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges) 
func (r ApiUsersMeSubscriptionsVideosGetRequest) Filter(filter string) ApiUsersMeSubscriptionsVideosGetRequest {
	r.filter = &filter
	return r
}
// if you don&#39;t need the &#x60;total&#x60; in the response
func (r ApiUsersMeSubscriptionsVideosGetRequest) SkipCount(skipCount string) ApiUsersMeSubscriptionsVideosGetRequest {
	r.skipCount = &skipCount
	return r
}
// Offset used to paginate results
func (r ApiUsersMeSubscriptionsVideosGetRequest) Start(start int32) ApiUsersMeSubscriptionsVideosGetRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiUsersMeSubscriptionsVideosGetRequest) Count(count int32) ApiUsersMeSubscriptionsVideosGetRequest {
	r.count = &count
	return r
}
// Sort videos by criteria
func (r ApiUsersMeSubscriptionsVideosGetRequest) Sort(sort string) ApiUsersMeSubscriptionsVideosGetRequest {
	r.sort = &sort
	return r
}

func (r ApiUsersMeSubscriptionsVideosGetRequest) Execute() (VideoListResponse, *_nethttp.Response, error) {
	return r.ApiService.UsersMeSubscriptionsVideosGetExecute(r)
}

/*
UsersMeSubscriptionsVideosGet List videos of subscriptions of my user

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiUsersMeSubscriptionsVideosGetRequest
*/
func (a *VideosApiService) UsersMeSubscriptionsVideosGet(ctx _context.Context) ApiUsersMeSubscriptionsVideosGetRequest {
	return ApiUsersMeSubscriptionsVideosGetRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoListResponse
func (a *VideosApiService) UsersMeSubscriptionsVideosGetExecute(r ApiUsersMeSubscriptionsVideosGetRequest) (VideoListResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoListResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideosApiService.UsersMeSubscriptionsVideosGet")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/users/me/subscriptions/videos"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.categoryOneOf != nil {
		localVarQueryParams.Add("categoryOneOf", parameterToString(*r.categoryOneOf, ""))
	}
	if r.isLive != nil {
		localVarQueryParams.Add("isLive", parameterToString(*r.isLive, ""))
	}
	if r.tagsOneOf != nil {
		localVarQueryParams.Add("tagsOneOf", parameterToString(*r.tagsOneOf, ""))
	}
	if r.tagsAllOf != nil {
		localVarQueryParams.Add("tagsAllOf", parameterToString(*r.tagsAllOf, ""))
	}
	if r.licenceOneOf != nil {
		localVarQueryParams.Add("licenceOneOf", parameterToString(*r.licenceOneOf, ""))
	}
	if r.languageOneOf != nil {
		localVarQueryParams.Add("languageOneOf", parameterToString(*r.languageOneOf, ""))
	}
	if r.nsfw != nil {
		localVarQueryParams.Add("nsfw", parameterToString(*r.nsfw, ""))
	}
	if r.filter != nil {
		localVarQueryParams.Add("filter", parameterToString(*r.filter, ""))
	}
	if r.skipCount != nil {
		localVarQueryParams.Add("skipCount", parameterToString(*r.skipCount, ""))
	}
	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiUsersMeVideosGetRequest struct {
	ctx _context.Context
	ApiService *VideosApiService
	start *int32
	count *int32
	sort *string
}

// Offset used to paginate results
func (r ApiUsersMeVideosGetRequest) Start(start int32) ApiUsersMeVideosGetRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiUsersMeVideosGetRequest) Count(count int32) ApiUsersMeVideosGetRequest {
	r.count = &count
	return r
}
// Sort column
func (r ApiUsersMeVideosGetRequest) Sort(sort string) ApiUsersMeVideosGetRequest {
	r.sort = &sort
	return r
}

func (r ApiUsersMeVideosGetRequest) Execute() (VideoListResponse, *_nethttp.Response, error) {
	return r.ApiService.UsersMeVideosGetExecute(r)
}

/*
UsersMeVideosGet Get videos of my user

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiUsersMeVideosGetRequest
*/
func (a *VideosApiService) UsersMeVideosGet(ctx _context.Context) ApiUsersMeVideosGetRequest {
	return ApiUsersMeVideosGetRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoListResponse
func (a *VideosApiService) UsersMeVideosGetExecute(r ApiUsersMeVideosGetRequest) (VideoListResponse, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoListResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideosApiService.UsersMeVideosGet")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/users/me/videos"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiUsersMeVideosImportsGetRequest struct {
	ctx _context.Context
	ApiService *VideosApiService
	start *int32
	count *int32
	sort *string
}

// Offset used to paginate results
func (r ApiUsersMeVideosImportsGetRequest) Start(start int32) ApiUsersMeVideosImportsGetRequest {
	r.start = &start
	return r
}
// Number of items to return
func (r ApiUsersMeVideosImportsGetRequest) Count(count int32) ApiUsersMeVideosImportsGetRequest {
	r.count = &count
	return r
}
// Sort column
func (r ApiUsersMeVideosImportsGetRequest) Sort(sort string) ApiUsersMeVideosImportsGetRequest {
	r.sort = &sort
	return r
}

func (r ApiUsersMeVideosImportsGetRequest) Execute() (VideoImportsList, *_nethttp.Response, error) {
	return r.ApiService.UsersMeVideosImportsGetExecute(r)
}

/*
UsersMeVideosImportsGet Get video imports of my user

 @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiUsersMeVideosImportsGetRequest
*/
func (a *VideosApiService) UsersMeVideosImportsGet(ctx _context.Context) ApiUsersMeVideosImportsGetRequest {
	return ApiUsersMeVideosImportsGetRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return VideoImportsList
func (a *VideosApiService) UsersMeVideosImportsGetExecute(r ApiUsersMeVideosImportsGetRequest) (VideoImportsList, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  VideoImportsList
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "VideosApiService.UsersMeVideosImportsGet")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/users/me/videos/imports"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.start != nil {
		localVarQueryParams.Add("start", parameterToString(*r.start, ""))
	}
	if r.count != nil {
		localVarQueryParams.Add("count", parameterToString(*r.count, ""))
	}
	if r.sort != nil {
		localVarQueryParams.Add("sort", parameterToString(*r.sort, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
