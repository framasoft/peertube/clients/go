/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"encoding/json"
	"time"
)

// VideoChannelCreate struct for VideoChannelCreate
type VideoChannelCreate struct {
	// editable name of the channel, displayed in its representations
	DisplayName string `json:"displayName"`
	Description *string `json:"description,omitempty"`
	// text shown by default on all videos of this channel, to tell the audience how to support it
	Support *string `json:"support,omitempty"`
	Id *int32 `json:"id,omitempty"`
	IsLocal *bool `json:"isLocal,omitempty"`
	UpdatedAt *time.Time `json:"updatedAt,omitempty"`
	OwnerAccount NullableVideoChannelOwnerAccount `json:"ownerAccount,omitempty"`
	// username of the channel to create
	Name string `json:"name"`
}

// NewVideoChannelCreate instantiates a new VideoChannelCreate object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewVideoChannelCreate(displayName string, name string) *VideoChannelCreate {
	this := VideoChannelCreate{}
	this.DisplayName = displayName
	this.Name = name
	return &this
}

// NewVideoChannelCreateWithDefaults instantiates a new VideoChannelCreate object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewVideoChannelCreateWithDefaults() *VideoChannelCreate {
	this := VideoChannelCreate{}
	return &this
}

// GetDisplayName returns the DisplayName field value
func (o *VideoChannelCreate) GetDisplayName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.DisplayName
}

// GetDisplayNameOk returns a tuple with the DisplayName field value
// and a boolean to check if the value has been set.
func (o *VideoChannelCreate) GetDisplayNameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.DisplayName, true
}

// SetDisplayName sets field value
func (o *VideoChannelCreate) SetDisplayName(v string) {
	o.DisplayName = v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *VideoChannelCreate) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoChannelCreate) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *VideoChannelCreate) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *VideoChannelCreate) SetDescription(v string) {
	o.Description = &v
}

// GetSupport returns the Support field value if set, zero value otherwise.
func (o *VideoChannelCreate) GetSupport() string {
	if o == nil || o.Support == nil {
		var ret string
		return ret
	}
	return *o.Support
}

// GetSupportOk returns a tuple with the Support field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoChannelCreate) GetSupportOk() (*string, bool) {
	if o == nil || o.Support == nil {
		return nil, false
	}
	return o.Support, true
}

// HasSupport returns a boolean if a field has been set.
func (o *VideoChannelCreate) HasSupport() bool {
	if o != nil && o.Support != nil {
		return true
	}

	return false
}

// SetSupport gets a reference to the given string and assigns it to the Support field.
func (o *VideoChannelCreate) SetSupport(v string) {
	o.Support = &v
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *VideoChannelCreate) GetId() int32 {
	if o == nil || o.Id == nil {
		var ret int32
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoChannelCreate) GetIdOk() (*int32, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *VideoChannelCreate) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given int32 and assigns it to the Id field.
func (o *VideoChannelCreate) SetId(v int32) {
	o.Id = &v
}

// GetIsLocal returns the IsLocal field value if set, zero value otherwise.
func (o *VideoChannelCreate) GetIsLocal() bool {
	if o == nil || o.IsLocal == nil {
		var ret bool
		return ret
	}
	return *o.IsLocal
}

// GetIsLocalOk returns a tuple with the IsLocal field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoChannelCreate) GetIsLocalOk() (*bool, bool) {
	if o == nil || o.IsLocal == nil {
		return nil, false
	}
	return o.IsLocal, true
}

// HasIsLocal returns a boolean if a field has been set.
func (o *VideoChannelCreate) HasIsLocal() bool {
	if o != nil && o.IsLocal != nil {
		return true
	}

	return false
}

// SetIsLocal gets a reference to the given bool and assigns it to the IsLocal field.
func (o *VideoChannelCreate) SetIsLocal(v bool) {
	o.IsLocal = &v
}

// GetUpdatedAt returns the UpdatedAt field value if set, zero value otherwise.
func (o *VideoChannelCreate) GetUpdatedAt() time.Time {
	if o == nil || o.UpdatedAt == nil {
		var ret time.Time
		return ret
	}
	return *o.UpdatedAt
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VideoChannelCreate) GetUpdatedAtOk() (*time.Time, bool) {
	if o == nil || o.UpdatedAt == nil {
		return nil, false
	}
	return o.UpdatedAt, true
}

// HasUpdatedAt returns a boolean if a field has been set.
func (o *VideoChannelCreate) HasUpdatedAt() bool {
	if o != nil && o.UpdatedAt != nil {
		return true
	}

	return false
}

// SetUpdatedAt gets a reference to the given time.Time and assigns it to the UpdatedAt field.
func (o *VideoChannelCreate) SetUpdatedAt(v time.Time) {
	o.UpdatedAt = &v
}

// GetOwnerAccount returns the OwnerAccount field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *VideoChannelCreate) GetOwnerAccount() VideoChannelOwnerAccount {
	if o == nil || o.OwnerAccount.Get() == nil {
		var ret VideoChannelOwnerAccount
		return ret
	}
	return *o.OwnerAccount.Get()
}

// GetOwnerAccountOk returns a tuple with the OwnerAccount field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *VideoChannelCreate) GetOwnerAccountOk() (*VideoChannelOwnerAccount, bool) {
	if o == nil  {
		return nil, false
	}
	return o.OwnerAccount.Get(), o.OwnerAccount.IsSet()
}

// HasOwnerAccount returns a boolean if a field has been set.
func (o *VideoChannelCreate) HasOwnerAccount() bool {
	if o != nil && o.OwnerAccount.IsSet() {
		return true
	}

	return false
}

// SetOwnerAccount gets a reference to the given NullableVideoChannelOwnerAccount and assigns it to the OwnerAccount field.
func (o *VideoChannelCreate) SetOwnerAccount(v VideoChannelOwnerAccount) {
	o.OwnerAccount.Set(&v)
}
// SetOwnerAccountNil sets the value for OwnerAccount to be an explicit nil
func (o *VideoChannelCreate) SetOwnerAccountNil() {
	o.OwnerAccount.Set(nil)
}

// UnsetOwnerAccount ensures that no value is present for OwnerAccount, not even an explicit nil
func (o *VideoChannelCreate) UnsetOwnerAccount() {
	o.OwnerAccount.Unset()
}

// GetName returns the Name field value
func (o *VideoChannelCreate) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *VideoChannelCreate) GetNameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *VideoChannelCreate) SetName(v string) {
	o.Name = v
}

func (o VideoChannelCreate) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["displayName"] = o.DisplayName
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.Support != nil {
		toSerialize["support"] = o.Support
	}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.IsLocal != nil {
		toSerialize["isLocal"] = o.IsLocal
	}
	if o.UpdatedAt != nil {
		toSerialize["updatedAt"] = o.UpdatedAt
	}
	if o.OwnerAccount.IsSet() {
		toSerialize["ownerAccount"] = o.OwnerAccount.Get()
	}
	if true {
		toSerialize["name"] = o.Name
	}
	return json.Marshal(toSerialize)
}

type NullableVideoChannelCreate struct {
	value *VideoChannelCreate
	isSet bool
}

func (v NullableVideoChannelCreate) Get() *VideoChannelCreate {
	return v.value
}

func (v *NullableVideoChannelCreate) Set(val *VideoChannelCreate) {
	v.value = val
	v.isSet = true
}

func (v NullableVideoChannelCreate) IsSet() bool {
	return v.isSet
}

func (v *NullableVideoChannelCreate) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableVideoChannelCreate(val *VideoChannelCreate) *NullableVideoChannelCreate {
	return &NullableVideoChannelCreate{value: val, isSet: true}
}

func (v NullableVideoChannelCreate) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableVideoChannelCreate) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


