/*
PeerTube

The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 

API version: 3.4.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package peertube

import (
	"encoding/json"
)

// InlineResponse20012VideoPlaylist struct for InlineResponse20012VideoPlaylist
type InlineResponse20012VideoPlaylist struct {
	Id *int32 `json:"id,omitempty"`
	Uuid *Uuid `json:"uuid,omitempty"`
	// translation of a uuid v4 with a bigger alphabet to have a shorter uuid
	ShortUUID *string `json:"shortUUID,omitempty"`
}

// NewInlineResponse20012VideoPlaylist instantiates a new InlineResponse20012VideoPlaylist object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewInlineResponse20012VideoPlaylist() *InlineResponse20012VideoPlaylist {
	this := InlineResponse20012VideoPlaylist{}
	return &this
}

// NewInlineResponse20012VideoPlaylistWithDefaults instantiates a new InlineResponse20012VideoPlaylist object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewInlineResponse20012VideoPlaylistWithDefaults() *InlineResponse20012VideoPlaylist {
	this := InlineResponse20012VideoPlaylist{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *InlineResponse20012VideoPlaylist) GetId() int32 {
	if o == nil || o.Id == nil {
		var ret int32
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineResponse20012VideoPlaylist) GetIdOk() (*int32, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *InlineResponse20012VideoPlaylist) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given int32 and assigns it to the Id field.
func (o *InlineResponse20012VideoPlaylist) SetId(v int32) {
	o.Id = &v
}

// GetUuid returns the Uuid field value if set, zero value otherwise.
func (o *InlineResponse20012VideoPlaylist) GetUuid() Uuid {
	if o == nil || o.Uuid == nil {
		var ret Uuid
		return ret
	}
	return *o.Uuid
}

// GetUuidOk returns a tuple with the Uuid field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineResponse20012VideoPlaylist) GetUuidOk() (*Uuid, bool) {
	if o == nil || o.Uuid == nil {
		return nil, false
	}
	return o.Uuid, true
}

// HasUuid returns a boolean if a field has been set.
func (o *InlineResponse20012VideoPlaylist) HasUuid() bool {
	if o != nil && o.Uuid != nil {
		return true
	}

	return false
}

// SetUuid gets a reference to the given Uuid and assigns it to the Uuid field.
func (o *InlineResponse20012VideoPlaylist) SetUuid(v Uuid) {
	o.Uuid = &v
}

// GetShortUUID returns the ShortUUID field value if set, zero value otherwise.
func (o *InlineResponse20012VideoPlaylist) GetShortUUID() string {
	if o == nil || o.ShortUUID == nil {
		var ret string
		return ret
	}
	return *o.ShortUUID
}

// GetShortUUIDOk returns a tuple with the ShortUUID field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InlineResponse20012VideoPlaylist) GetShortUUIDOk() (*string, bool) {
	if o == nil || o.ShortUUID == nil {
		return nil, false
	}
	return o.ShortUUID, true
}

// HasShortUUID returns a boolean if a field has been set.
func (o *InlineResponse20012VideoPlaylist) HasShortUUID() bool {
	if o != nil && o.ShortUUID != nil {
		return true
	}

	return false
}

// SetShortUUID gets a reference to the given string and assigns it to the ShortUUID field.
func (o *InlineResponse20012VideoPlaylist) SetShortUUID(v string) {
	o.ShortUUID = &v
}

func (o InlineResponse20012VideoPlaylist) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.Uuid != nil {
		toSerialize["uuid"] = o.Uuid
	}
	if o.ShortUUID != nil {
		toSerialize["shortUUID"] = o.ShortUUID
	}
	return json.Marshal(toSerialize)
}

type NullableInlineResponse20012VideoPlaylist struct {
	value *InlineResponse20012VideoPlaylist
	isSet bool
}

func (v NullableInlineResponse20012VideoPlaylist) Get() *InlineResponse20012VideoPlaylist {
	return v.value
}

func (v *NullableInlineResponse20012VideoPlaylist) Set(val *InlineResponse20012VideoPlaylist) {
	v.value = val
	v.isSet = true
}

func (v NullableInlineResponse20012VideoPlaylist) IsSet() bool {
	return v.isSet
}

func (v *NullableInlineResponse20012VideoPlaylist) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableInlineResponse20012VideoPlaylist(val *InlineResponse20012VideoPlaylist) *NullableInlineResponse20012VideoPlaylist {
	return &NullableInlineResponse20012VideoPlaylist{value: val, isSet: true}
}

func (v NullableInlineResponse20012VideoPlaylist) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableInlineResponse20012VideoPlaylist) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


